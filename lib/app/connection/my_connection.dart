import 'package:dio/dio.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';

class MyConnection {
  Dio getDioConnection(String token, String urlConnection, String currency) {
    var headers1 = {
      'content-type': 'application/json',
      'accept': 'application/json',
    };

    var headers2 = {
      'content-type': 'application/json',
      'accept': 'application/json',
      'currency': '$currency',
    };

    Dio dio = Dio();
    dio.options.headers = currency.isNotEmpty ? headers2 : headers1;
    dio.options.baseUrl = urlConnection;
    return dio;
  }

  List<LedgerModel> getLedgerNetworkList(){
    LedgerModel mainnetLedgerModel = LedgerModel(ledgerId: 0, name: "Main Network", isEditable: false,
        genesisId: "mainnet-v1.0", genesisHash: "", symbol: "", algodUrl: "https://wallet.wagmiswap.io/api/algod",
        indexerUrl: "https://wallet.wagmiswap.io/api/idx", headers: "", baseUrl: "https://wallet.wagmiswap.io/api/");

    LedgerModel testnetLedgerModel = LedgerModel(ledgerId: 1, name: "Test Network", isEditable: false,
        genesisId: "testnet-v1.0", genesisHash: "", symbol: "", algodUrl: "https://wallet.wagmiswap.io/testnet/api/algod",
        indexerUrl: "https://wallet.wagmiswap.io/testnet/api/idx", headers: "", baseUrl: "https://wallet.wagmiswap.io/testnet/api/");

    List<LedgerModel> ledgerNetworkList = [];
    ledgerNetworkList.add(mainnetLedgerModel);
    ledgerNetworkList.add(testnetLedgerModel);

    return ledgerNetworkList;
  }
}
