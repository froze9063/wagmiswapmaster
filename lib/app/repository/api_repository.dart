import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:dio/dio.dart' as dio_response;
import 'package:fl_chart/fl_chart.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:sembast/sembast.dart';
import 'package:waagmiswap/app/connection/my_connection.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/asset_pricing.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/model/transaction_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/home/views/home_view.dart';
import 'package:waagmiswap/app/util/my_util.dart';

class APIRepository {

  DatabaseDAO databaseDAO = DatabaseDAO();
  final MyConnection _myConnection = MyConnection();

  late GetxController controller;
  APIRepository(this.controller);

  bool isPendingLoading = true;

  Future<dio_response.Response> getPagesResponse(String address,String url,String currency){
    return _myConnection.getDioConnection("",url,currency).get("${MyConstant.ACCOUNTS}/$address");
  }

  Future<dio_response.Response> getAssetDetailResponse(String url,String currency, int assetId){
    return _myConnection.getDioConnection("",url,currency).get("/idx${MyConstant.ASSETS}/$assetId?include-all=true");
  }

  Future<dio_response.Response> getAssetPricingResponse(String unitName,String currency){
    Map body = {};
    body["url"] = "http://localhost:8002/api/v2/tickers/latest";
    body["method"] = "get";
    body["bearer"] = MyConstant.TOKEN_BEARER;

    Map paramsMap = {};
    paramsMap["symbol"] = "$unitName/$currency";

    body["params"] = paramsMap;

    return _myConnection.getDioConnection("",MyConstant.TOKEN_URL,"").post(MyConstant.FORWARD,
        data: body);
  }

  void getSearchAssets(UserModel userModel, int ledgerPosition,String url, String idxUrl,
      List<Map> assetList, List<Map>? searchedAssetList, int currentUserAlgo, Map loadingMap,
      String currency){
    _myConnection.getDioConnection("",url,currency).get("${MyConstant.ACCOUNTS}/${userModel.address}"
    ).then((response) async {

      Map dataMap = response.data;
      userModel.amount = dataMap["amount"] ?? 0;
      userModel.amountWithoutPendingRewards = dataMap["amount-without-pending-rewards"] ?? 0;
      userModel.pendingRewards = dataMap["pending-rewards"] ?? 0;
      userModel.rewardBase = dataMap["reward-base"] ?? 0;
      userModel.rewards = dataMap["rewards"] ?? 0;
      userModel.round = dataMap["round"] ?? 0;
      userModel.status = dataMap["status"] ?? "";
      userModel.price = dataMap["price"] ?? "";

      AssetsModel algoAssetsModel = AssetsModel(ledgerPosition: ledgerPosition,
          amount: userModel.amount, assetId: 0, userAddress: userModel.address,
          creator: "", isFrozen: false, optedInTtRound: 0, price: userModel.price,
          currency: currency);

      AssetsDetailModel algoAssetDetailModel = AssetsDetailModel(ledgerPosition: ledgerPosition,
          createdAtRound: 0, index: 0, clawback: "", creator: "",
          decimals: 0, freeze: "", manager: "", name: "Algorand", nameB64: "",
          reserve: "", total: 0, unitName: "ALGO", unitNameB64: "", url: url,
          urlB64: "", currentRound: 0, circulatingSupply: 0);

      var finder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
        Filter.equals("asset-id", algoAssetsModel.assetId), Filter.equals("user-address",
            userModel.address)]), limit: 1);

      var assets = await databaseDAO.getAllAssetBySorted(finder);

      if(assets.isEmpty){
        await databaseDAO.insertAsset(algoAssetsModel);
      }
      else{
        await databaseDAO.updateAsset(algoAssetsModel,finder);
      }

      var finderDetail = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
        Filter.equals("index", 0)]), limit: 1);

      var detail = await databaseDAO.getAllAssetDetailSorted(finderDetail);

      if(detail.isEmpty){
        await databaseDAO.insertAssetDetail(algoAssetDetailModel);
      }
      else{
        await databaseDAO.updateAssetDetail(detail[0], finderDetail);
      }

      var priceFinder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
        Filter.equals("asset-id", 0), Filter.equals("currency", currency)]), limit: 1);

      var pricess = await databaseDAO.getAssetPricingWithFinder(priceFinder);

      if(pricess.isEmpty){
        AssetsPricing assetPricingModel = AssetsPricing(ledgerPosition: ledgerPosition,
            assetId: 0, currency: currency, price: algoAssetsModel.price, percentage: "",
            percentagePrice: "0");

        await databaseDAO.insertAssetPricing(assetPricingModel);
      }
      else{
        pricess[0].price = algoAssetsModel.price;
        await databaseDAO.updateAssetPricing(pricess[0], priceFinder);
      }

      Map assetMap = {};
      assetMap["asset_model"] = algoAssetsModel;
      assetMap["asset_detail_model"] = algoAssetDetailModel;
      assetMap["latest_price"] = algoAssetsModel.price.isEmpty ? 0.0 : double.parse(algoAssetsModel.price);
      assetMap["is_pending"] = false;
      assetMap["status_name"] = "";

      assetList.add(assetMap);
      searchedAssetList?.add(assetMap);

      if(dataMap["assets"] != null){
        List<dynamic> responseAssetList = dataMap["assets"];

        for(int i = 0 ; i < responseAssetList.length; i++){
          Map<String,dynamic> responseMap = responseAssetList[i];
          responseMap["ledger-position"] = ledgerPosition;
          responseMap["user-address"] = userModel.address;

          AssetsModel assetsModel = AssetsModel.fromMap(responseMap);
          assetsModel.currency = currency;

          Map assetMap = {};
          assetMap["asset_model"] = assetsModel;
          assetMap["is_pending"] = false;
          assetMap["status_name"] = "";
          assetMap["latest_price"] = assetsModel.price.isEmpty ? 0.0 : double.parse(assetsModel.price);

          var priceFinder2 = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
            Filter.equals("asset-id", assetsModel.assetId), Filter.equals("currency", currency)]),
              limit: 1);

          var pricess2 = await databaseDAO.getAssetPricingWithFinder(priceFinder2);

          if(pricess2.isEmpty){
            AssetsPricing assetPricingModel2 = AssetsPricing(ledgerPosition: ledgerPosition,
                assetId: assetsModel.assetId, currency: currency, price: assetsModel.price,
                percentage: "", percentagePrice: "0");

            await databaseDAO.insertAssetPricing(assetPricingModel2);
          }
          else{
            pricess2[0].price = assetsModel.price;
            await databaseDAO.updateAssetPricing(pricess2[0], priceFinder2);
          }

          var finderAsset = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
            Filter.equals("asset-id", assetsModel.assetId), Filter.equals("user-address", userModel.address)]),
              limit: 1);

          var assetValue = await databaseDAO.getAllAssetBySorted(finderAsset);

          if(assetValue.isEmpty){
            await databaseDAO.insertAsset(assetsModel);
          }
          else{
            await databaseDAO.updateAsset(assetsModel,
                finderAsset);
          }

          assetList.add(assetMap);
          searchedAssetList?.add(assetMap);
        }
      }

      for(int i = 0; i < assetList.length; i++){
        AssetsModel assetsModel = assetList[i]["asset_model"];

        var finder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
          Filter.equals("index", assetsModel.assetId)]), limit: 1);

        databaseDAO.getAllAssetDetailSorted(finder).then((assetValue) {
          if(assetValue.isEmpty){
            _myConnection.getDioConnection("",url,currency).get("${MyConstant.ASSETS}/${assetsModel.assetId}"
                "?include-all=true"
            ).then((response) {
              Map<String,dynamic> dataMap = response.data;

              var assetsDetailModel;

              if(assetsModel.assetId != 0){
                assetsDetailModel = AssetsDetailModel.fromMap(dataMap,ledgerPosition);
              }
              else{
                assetsDetailModel = searchedAssetList?[0]["asset_detail_model"];
              }

              databaseDAO.insertAssetDetail(assetsDetailModel);

              assetList[i]["asset_detail_model"] = assetsDetailModel;

              searchedAssetList?[i]["asset_detail_model"] = assetsDetailModel;

              loadingMap["loading_count"] = loadingMap["loading_count"] + 1;

              if(loadingMap["loading_count"] == assetList.length ){

                loadingMap["loading"] = false;
                loadingMap["loading_count"] = 0;
                loadingMap["percentage_count"] = 0;

                controller.update(["assets","slider"]);
              }
            }).catchError((error){
              loadingMap["loading_count"] = loadingMap["loading_count"] + 1;

              if(loadingMap["loading_count"] == assetList.length ){

                loadingMap["loading"] = false;
                loadingMap["loading_count"] = 0;
                loadingMap["percentage_count"] = 0;

                controller.update(["assets","slider"]);
              }
              if(error is DioError){
                try{
                  var response = error.response;
                  if(response != null){
                    Map dataMap = response.data;
                    String message = dataMap["message"] ?? "";
                    print(message);
                  }
                  else{
                    print(error);
                  }
                }
                catch(error){
                  print(error);
                }
              }
              else{
                print(error);
              }
            });
          }
          else{
            loadingMap["loading_count"] = loadingMap["loading_count"] + 1;
            assetList[i]["asset_detail_model"] = assetValue[0];
            searchedAssetList?[i]["asset_detail_model"] = assetValue[0];

            if(loadingMap["loading_count"] == assetList.length ){

              loadingMap["loading"] = false;
              loadingMap["loading_count"] = 0;
              loadingMap["percentage_count"] = 0;

              controller.update(["assets","slider"]);
            }
          }
        });
      }
    }).catchError((error){
      loadingMap["loading"] = false;
      controller.update(["assets","slider"]);
      if(error is DioError){
        try{
          var response = error.response;
          if(response != null){
            Map dataMap = response.data;
            String message = dataMap["message"] ?? "";
            print(message);
          }
          else{
            print(error);
          }
        }
        catch(error){
          print(error);
        }
      }
      else{
        print(error);
      }
    });
  }

  Future<void> getUsersAmount(List<UserModel> userModelList,String url,String currency, Map loadingMap) async {
    loadingMap["loading"] = true;
    controller.update(["accounts"]);

    for(int i = 0; i < userModelList.length; i++){
      var response = await _myConnection.getDioConnection("",url,currency).get("${MyConstant.ACCOUNTS}"
          "/${userModelList[i].address}");
      var dataMap = response.data;
      userModelList[i].amount = dataMap["amount"] ?? 0;
      userModelList[i].amountWithoutPendingRewards = dataMap["amount-without-pending-rewards"] ?? 0;
      userModelList[i].pendingRewards = dataMap["pending-rewards"] ?? 0;
      userModelList[i].rewardBase = dataMap["reward-base"] ?? 0;
      userModelList[i].rewards = dataMap["rewards"] ?? 0;
      userModelList[i].round = dataMap["round"] ?? 0;
      userModelList[i].status = dataMap["status"] ?? "";
      userModelList[i].price = dataMap["price"] ?? "";
    }

    loadingMap["loading"] = false;
    controller.update(["accounts"]);
  }

  void getAssets(UserModel userModel, int ledgerPosition,String url, String idxUrl,
      List<Map> assetList, List<Map>? searchedAssetList, int currentUserAlgo, Map loadingMap,
      String currency, List<List<Map>>? walletList, int userPosition) {

    _myConnection.getDioConnection("",url,currency).get("${MyConstant.ACCOUNTS}/${userModel.address}"
    ).then((response) async {
      Map dataMap = response.data;
      userModel.amount = dataMap["amount"] ?? 0;
      userModel.amountWithoutPendingRewards = dataMap["amount-without-pending-rewards"] ?? 0;
      userModel.pendingRewards = dataMap["pending-rewards"] ?? 0;
      userModel.rewardBase = dataMap["reward-base"] ?? 0;
      userModel.rewards = dataMap["rewards"] ?? 0;
      userModel.round = dataMap["round"] ?? 0;
      userModel.status = dataMap["status"] ?? "";
      userModel.price = dataMap["price"] ?? "";

      AssetsModel algoAssetsModel = AssetsModel(ledgerPosition: ledgerPosition,
          amount: userModel.amount, assetId: 0, userAddress: userModel.address,
          creator: "", isFrozen: false, optedInTtRound: 0, price: userModel.price,
          currency: currency);

      AssetsDetailModel algoAssetDetailModel = AssetsDetailModel(ledgerPosition: ledgerPosition,
          createdAtRound: 0, index: 0, clawback: "", creator: "",
          decimals: 0, freeze: "", manager: "", name: "Algorand", nameB64: "",
          reserve: "", total: 0, unitName: "ALGO", unitNameB64: "", url: url,
          urlB64: "", currentRound: 0, circulatingSupply: 0);

      var finder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
        Filter.equals("asset-id", algoAssetsModel.assetId), Filter.equals("user-address",
            userModel.address)]), limit: 1);

      var assets = await databaseDAO.getAllAssetBySorted(finder);

      if(assets.isEmpty){
        await databaseDAO.insertAsset(algoAssetsModel);
      }
      else{
        await databaseDAO.updateAsset(algoAssetsModel,finder);
      }

      var finderDetail = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
        Filter.equals("index", 0)]), limit: 1);

      var detail = await databaseDAO.getAllAssetDetailSorted(finderDetail);

      if(detail.isEmpty){
        await databaseDAO.insertAssetDetail(algoAssetDetailModel);
      }
      else{
        await databaseDAO.updateAssetDetail(detail[0], finderDetail);
      }

      var priceFinder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
        Filter.equals("asset-id", 0), Filter.equals("currency", currency)]), limit: 1);

      var pricess = await databaseDAO.getAssetPricingWithFinder(priceFinder);

      if(pricess.isEmpty){
        AssetsPricing assetPricingModel = AssetsPricing(ledgerPosition: ledgerPosition,
            assetId: 0, currency: currency, price: algoAssetsModel.price, percentage: "",
            percentagePrice: "0");

        await databaseDAO.insertAssetPricing(assetPricingModel);
      }
      else{
        pricess[0].price = algoAssetsModel.price;
        await databaseDAO.updateAssetPricing(pricess[0], priceFinder);
      }

      Map assetMap = {};
      assetMap["asset_model"] = algoAssetsModel;
      assetMap["asset_detail_model"] = algoAssetDetailModel;
      assetMap["latest_price"] = algoAssetsModel.price.isEmpty ? 0.0 : double.parse(algoAssetsModel.price);
      assetMap["is_pending"] = false;
      assetMap["status_name"] = "";

      if(walletList != null){
        walletList[ledgerPosition][userPosition]["original_list"]?.add(assetMap);
        walletList[ledgerPosition][userPosition]["searched_list"]?.add(assetMap);
      }
      else{
        assetList.add(assetMap);
        searchedAssetList?.add(assetMap);
      }

      if(dataMap["assets"] != null){
        List<dynamic> responseAssetList = dataMap["assets"];

        for(int i = 0 ; i < responseAssetList.length; i++){
          Map<String,dynamic> responseMap = responseAssetList[i];
          responseMap["ledger-position"] = ledgerPosition;
          responseMap["user-address"] = userModel.address;

          AssetsModel assetsModel = AssetsModel.fromMap(responseMap);
          assetsModel.currency = currency;

          Map assetMap = {};
          assetMap["asset_model"] = assetsModel;
          assetMap["is_pending"] = false;
          assetMap["status_name"] = "";
          assetMap["latest_price"] = assetsModel.price.isEmpty ? 0.0 : double.parse(assetsModel.price);

          var priceFinder2 = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
            Filter.equals("asset-id", assetsModel.assetId), Filter.equals("currency", currency)]),
              limit: 1);

          var pricess2 = await databaseDAO.getAssetPricingWithFinder(priceFinder2);

          if(pricess2.isEmpty){
            AssetsPricing assetPricingModel2 = AssetsPricing(ledgerPosition: ledgerPosition,
                assetId: assetsModel.assetId, currency: currency, price: assetsModel.price,
                percentage: "", percentagePrice: "0");

            await databaseDAO.insertAssetPricing(assetPricingModel2);
          }
          else{
            pricess2[0].price = assetsModel.price;
            await databaseDAO.updateAssetPricing(pricess2[0], priceFinder2);
          }

          var finderAsset = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
            Filter.equals("asset-id", assetsModel.assetId), Filter.equals("user-address", userModel.address)]),
              limit: 1);

          var assetValue = await databaseDAO.getAllAssetBySorted(finderAsset);

          if(assetValue.isEmpty){
            await databaseDAO.insertAsset(assetsModel);
          }
          else{
            await databaseDAO.updateAsset(assetsModel,
                finderAsset);
          }

          if(walletList != null){
            walletList[ledgerPosition][userPosition]["original_list"]?.add(assetMap);
            walletList[ledgerPosition][userPosition]["searched_list"]?.add(assetMap);
          }
          else{
            assetList.add(assetMap);
            searchedAssetList?.add(assetMap);
          }
        }
      }

      if(walletList != null){
        for(int i = 0; i < walletList[ledgerPosition][userPosition]["original_list"].length; i++){
          AssetsModel assetsModel = walletList[ledgerPosition][userPosition]["original_list"][i]["asset_model"];
          getAssetsDetail(ledgerPosition, idxUrl, assetsModel.assetId, assetList,
              searchedAssetList, i, assetsModel.assetId, currency, loadingMap, walletList,
              userPosition);
        }
      }
      else{
        for(int i = 0; i < assetList.length; i++){
          AssetsModel assetsModel = assetList[i]["asset_model"];
          getAssetsDetail(ledgerPosition, idxUrl, assetsModel.assetId, assetList,
              searchedAssetList, i, assetsModel.assetId, currency, loadingMap, walletList,
              userPosition);
        }
      }
    }).catchError((error){
      if(walletList != null){
        walletList[ledgerPosition][userPosition]["loading"] = false;
      }
      else{
        loadingMap["loading"] = false;
      }
      controller.update(["assets","slider"]);
      if(error is DioError){
        try{
          var response = error.response;
          if(response != null){
            Map dataMap = response.data;
            String message = dataMap["message"] ?? "";
            print(message);
          }
          else{
            print(error);
          }
        }
        catch(error){
          print(error);
        }
      }
      else{
        print(error);
      }
    });
  }

  getAssetsDetail(int ledgerPosition, String url, int assetIndex, List<Map> assetList, List<Map>? searchedAssetList,
      int position, int assetId, String currency, Map loadingMap, List<List<Map>>? walletList, int userPosition) {

    var finder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
      Filter.equals("index", assetIndex)]), limit: 1);

    databaseDAO.getAllAssetDetailSorted(finder).then((assetValue) {
      if(assetValue.isEmpty){
        _myConnection.getDioConnection("",url,currency).get("${MyConstant.ASSETS}/$assetIndex?include-all=true"
        ).then((response) {
          Map<String,dynamic> dataMap = response.data;

          var assetsDetailModel;

          if(assetId != 0){
            assetsDetailModel = AssetsDetailModel.fromMap(dataMap,ledgerPosition);
          }
          else{
            if(walletList != null){
              assetsDetailModel = walletList[ledgerPosition][userPosition]["searched_list"][0][
              "asset_detail_model"];
            }
            else{
              assetsDetailModel = searchedAssetList?[0]["asset_detail_model"];
            }
          }

          databaseDAO.insertAssetDetail(assetsDetailModel);

          if(walletList != null){
            walletList[ledgerPosition][userPosition]["original_list"][position][
            "asset_detail_model"] = assetsDetailModel;
            walletList[ledgerPosition][userPosition]["searched_list"][position][
            "asset_detail_model"] = assetsDetailModel;
            walletList[ledgerPosition][userPosition]["loading_count"] = walletList[ledgerPosition][userPosition][
            "loading_count"] + 1;
          }
          else{
            assetList[position]["asset_detail_model"] = assetsDetailModel;
            searchedAssetList?[position]["asset_detail_model"] = assetsDetailModel;
            loadingMap["loading_count"] = loadingMap["loading_count"] + 1;
          }

          getAssetsPricing(assetsDetailModel.unitName, ledgerPosition, url, assetIndex, assetList,
              searchedAssetList, position, assetId, currency, loadingMap, walletList, userPosition);

        }).catchError((error){
          if(walletList != null){
            walletList[ledgerPosition][userPosition]["loading_count"] = walletList[ledgerPosition][userPosition][
            "loading_count"] + 1;
          }
          else{
            loadingMap["loading_count"] = loadingMap["loading_count"] + 1;
          }

          getAssetsPricing("", ledgerPosition, url, assetIndex, assetList,
              searchedAssetList, position, assetId, currency, loadingMap, walletList,
              userPosition);

          if(error is DioError){
            try{
              var response = error.response;
              if(response != null){
                Map dataMap = response.data;
                String message = dataMap["message"] ?? "";
                print(message);
              }
              else{
                print(error);
              }
            }
            catch(error){
              print(error);
            }
          }
          else{
            print(error);
          }
        });
      }
      else{
        if(walletList != null){
          walletList[ledgerPosition][userPosition]["loading_count"] = walletList[ledgerPosition][userPosition][
          "loading_count"] + 1;
          walletList[ledgerPosition][userPosition]["original_list"][position][
          "asset_detail_model"] = assetValue[0];
          walletList[ledgerPosition][userPosition]["searched_list"][position][
          "asset_detail_model"] = assetValue[0];
        }
        else{
          loadingMap["loading_count"] = loadingMap["loading_count"] + 1;
          assetList[position]["asset_detail_model"] = assetValue[0];
          searchedAssetList?[position]["asset_detail_model"] = assetValue[0];
        }

        var pricingFinder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
          Filter.equals("asset-id", assetIndex), Filter.equals("currency", currency)]), limit: 1);

        databaseDAO.getAssetPricingWithFinder(pricingFinder).then((assetPricing) {
          if(assetPricing[0].percentagePrice == "0" || assetPricing[0].percentagePrice == ""){
            getAssetsPricing(assetValue[0].unitName, ledgerPosition, url, assetIndex, assetList,
                searchedAssetList, position, assetId, currency, loadingMap, walletList, userPosition);
          }
          else{
            if(walletList != null){
              walletList[ledgerPosition][userPosition]["percentage_count"] = walletList[ledgerPosition][userPosition][
              "percentage_count"] + 1;
              (walletList[ledgerPosition][userPosition]["original_list"][position][
              "asset_model"] as AssetsModel).price = assetPricing[0].price;
              (walletList[ledgerPosition][userPosition]["searched_list"][position][
              "asset_model"] as AssetsModel).price = assetPricing[0].price;


              walletList[ledgerPosition][userPosition]["original_list"][position]["latest_price"] = double.parse(assetPricing[0].price.isEmpty ? "0.0" : assetPricing[0].price);
              walletList[ledgerPosition][userPosition]["searched_list"][position]["latest_price"] = double.parse(assetPricing[0].price.isEmpty ? "0.0" : assetPricing[0].price);

              walletList[ledgerPosition][userPosition]["original_list"][position]["percentage_price"] = double.parse(assetPricing[0].percentagePrice.isEmpty ? "0.0" : assetPricing[0].percentagePrice);
              walletList[ledgerPosition][userPosition]["searched_list"][position]["percentage_price"] = double.parse(assetPricing[0].percentagePrice.isEmpty ? "0.0" : assetPricing[0].percentagePrice);

              walletList[ledgerPosition][userPosition]["original_list"][position]["percentage"] = assetPricing[0].percentage;
              walletList[ledgerPosition][userPosition]["searched_list"][position]["percentage"] = assetPricing[0].percentage;
            }
            else{
              loadingMap["percentage_count"] = loadingMap["percentage_count"] + 1;
              (assetList[position]["asset_model"] as AssetsModel).price = assetPricing[0].price;
              (searchedAssetList?[position]["asset_model"] as AssetsModel).price = assetPricing[0].price;

              assetList[position]["latest_price"] = double.parse(assetPricing[0].price.isEmpty ? "0.0" : assetPricing[0].price);
              searchedAssetList?[position]["latest_price"] = double.parse(assetPricing[0].price.isEmpty ? "0.0" : assetPricing[0].price);

              assetList[position]["percentage_price"] = double.parse(assetPricing[0].percentagePrice.isEmpty ? "0.0" : assetPricing[0].percentagePrice);
              searchedAssetList?[position]["percentage_price"] = double.parse(assetPricing[0].percentagePrice.isEmpty ? "0.0" : assetPricing[0].percentagePrice);

              assetList[position]["percentage"] = assetPricing[0].percentage;
              searchedAssetList?[position]["percentage"] = assetPricing[0].percentage;
            }

            if(walletList != null){
              if(walletList[ledgerPosition][userPosition]["loading_count"] == walletList[ledgerPosition][userPosition]["original_list"].length &&
                  walletList[ledgerPosition][userPosition]["percentage_count"] == walletList[ledgerPosition][userPosition]["original_list"].length){

                walletList[ledgerPosition][userPosition]["loading"] = false;
                walletList[ledgerPosition][userPosition]["loading_count"] = 0;
                walletList[ledgerPosition][userPosition]["percentage_count"] = 0;

                controller.update(["assets","slider"]);
              }
            }
            else{
              if(loadingMap["loading_count"] == assetList.length && loadingMap["percentage_count"
                  ""] == assetList.length){

                loadingMap["loading"] = false;
                loadingMap["loading_count"] = 0;
                loadingMap["percentage_count"] = 0;

                controller.update(["assets","slider"]);
              }
            }
          }
        }
        );
      }
    });
  }

  getAssetsPricing(String unitName, int ledgerPosition, String url, int assetIndex, List<Map> assetList, List<Map>? searchedAssetList,
      int position, int assetId, String currency, Map loadingMap, List<List<Map>>? walletList, int userPosition) {

    var finder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
      Filter.equals("asset-id", assetIndex), Filter.equals("currency", currency)]), limit: 1);

    String assetPercentage = "";
    String assetPercentage2 = "";

    if(walletList != null ? walletList[ledgerPosition][userPosition]["original_list"][position]["percentage"] == null
        : assetList[position]["percentage"] == null){

      Map body2 = {};
      body2["url"] = "http://localhost:8002/api/v2/tickers/day";
      body2["method"] = "get";
      body2["bearer"] = MyConstant.TOKEN_BEARER;

      Map paramsMap2 = {};
      paramsMap2["symbol"] = "$unitName/$currency";

      body2["params"] = paramsMap2;

      _myConnection.getDioConnection("",MyConstant.TOKEN_URL,"").post(MyConstant.FORWARD,
          data: body2).then((detailResponse) {

        if(walletList != null){
          if(walletList[ledgerPosition][userPosition]["original_list"][position]["percentage"] == null){
            walletList[ledgerPosition][userPosition]["percentage_count"] = walletList[ledgerPosition][userPosition]["percentage_count"] + 1;
          }
        }
        else{
          if(assetList[position]["percentage"] == null){
            loadingMap["percentage_count"] = loadingMap["percentage_count"] + 1;
          }
        }

        if(detailResponse.data is Map){
          Map responseMap = detailResponse.data;

          List<dynamic> assetsPriceList = responseMap["$unitName/$currency"];
          List<Map> priceFiltered = [];

          for(int i = 0 ; i < assetsPriceList.length ; i++){
            Map assetPriceMap = assetsPriceList[i];
            String price = assetPriceMap["price"] ?? "";
            String created_at = assetPriceMap["created_at"] ?? "";

            Map priceMapPosition = Map();
            priceMapPosition["price"] = double.parse(price);
            priceMapPosition["created_at"] = created_at;
            priceMapPosition["prev_index"] = i;

            priceFiltered.add(priceMapPosition);
          }

          for(int i = 0 ; i < priceFiltered.length ; i++){
            for(int j = 0; j < priceFiltered.length; j++){
              if(j < priceFiltered.length - 1){
                double position1 = priceFiltered[j]["price"];
                double position2 = priceFiltered[j + 1]["price"];

                int prevIndex1 = priceFiltered[j]["prev_index"];
                int prevIndex2 = priceFiltered[j + 1]["prev_index"];

                if(position1 > position2){
                  priceFiltered[j]["price"] = position2;
                  priceFiltered[j + 1]["price"] =  position1;

                  priceFiltered[j]["prev_index"] = prevIndex2;
                  priceFiltered[j + 1]["prev_index"] =  prevIndex1;
                }
              }
            }
          }

          if(priceFiltered.length > 1){
            Map lowestMap = priceFiltered[0];
            Map highestMap = priceFiltered[priceFiltered.length - 1];

            double lowest = double.parse(assetsPriceList[lowestMap["prev_index"] > assetsPriceList.length - 1
                ? lowestMap["prev_index"] - 1 : lowestMap["prev_index"]]["price"] ?? "0");

            double highest = double.parse(assetsPriceList[highestMap["prev_index"] > assetsPriceList.length - 1
                ? highestMap["prev_index"] - 1 : highestMap["prev_index"]]["price"] ?? "0");

            double PercentagePrice = MyUtils.getPercentagePrice(lowest, highest);

            String percentage = PercentagePrice.toString().startsWith("-") ? "${
                PercentagePrice.toStringAsFixed(2)}%" : "+${PercentagePrice.toStringAsFixed(2)}%";

            if(walletList != null){
              walletList[ledgerPosition][userPosition]["original_list"][position]["percentage_price"] = PercentagePrice;
              walletList[ledgerPosition][userPosition]["searched_list"][position]["percentage_price"] = PercentagePrice;

              walletList[ledgerPosition][userPosition]["original_list"][position]["percentage"] = percentage;
              walletList[ledgerPosition][userPosition]["searched_list"][position]["percentage"] = percentage;
            }
            else{
              assetList[position]["percentage"] = percentage;
              searchedAssetList?[position]["percentage"] = percentage;

              assetList[position]["percentage_price"] = PercentagePrice;
              searchedAssetList?[position]["percentage_price"] = PercentagePrice;
            }

            assetPercentage = percentage;
            assetPercentage2 = PercentagePrice.toString();
          }
        }

        if(walletList != null){
          AssetsPricing assetPricingModel = AssetsPricing(ledgerPosition: ledgerPosition,
              assetId: assetId, currency: currency, price: (walletList[ledgerPosition][userPosition]["original_list"
                  ""][position]["asset_model"] as AssetsModel).price, percentage: assetPercentage,
              percentagePrice: assetPercentage2);

          databaseDAO.getAssetPricingWithFinder(finder).then((assetPricing) {
            if(assetPricing.isEmpty){
              databaseDAO.insertAssetPricing(assetPricingModel);
            }
            else{
              databaseDAO.updateAssetPricing(assetPricingModel, finder);
            }
          }
          );
        }
        else{
          AssetsPricing assetPricingModel = AssetsPricing(ledgerPosition: ledgerPosition,
              assetId: assetId, currency: currency, price: (assetList[position]["asset_model"] as
              AssetsModel).price, percentage: assetPercentage, percentagePrice: assetPercentage2);

          databaseDAO.getAssetPricingWithFinder(finder).then((assetPricing) {
            if(assetPricing.isEmpty){
              databaseDAO.insertAssetPricing(assetPricingModel);
            }
            else{
              databaseDAO.updateAssetPricing(assetPricingModel, finder);
            }
          }
          );
        }

        if(walletList != null){
          if(walletList[ledgerPosition][userPosition]["loading_count"] == walletList[ledgerPosition][userPosition]["original_list"].length &&
              walletList[ledgerPosition][userPosition]["percentage_count"] == walletList[ledgerPosition][userPosition]["original_list"].length){

            walletList[ledgerPosition][userPosition]["loading"] = false;
            walletList[ledgerPosition][userPosition]["loading_count"] = 0;
            walletList[ledgerPosition][userPosition]["percentage_count"] = 0;

            controller.update(["assets","slider"]);
          }
        }
        else{
          if(loadingMap["loading_count"] == assetList.length && loadingMap["percentage_count"
              ""] == assetList.length){

            loadingMap["loading"] = false;
            loadingMap["loading_count"] = 0;
            loadingMap["percentage_count"] = 0;

            controller.update(["assets","slider"]);
          }
        }
      }).catchError((error){
        loadingMap["loading"] = false;
        walletList?[ledgerPosition][userPosition]["loading"] = false;
        controller.update(["assets","slider"]);
      });
    }
  }

  void getAllAssetsDetail(String url, String assetUrl, String address, List<Map> detailAssetsList,
      List<Map> searchedAssetsList, Map loadingMap, String currency, String assetId,
      List<Map> holdAssetList, UserModel userModel, int ledgerPosition, List<AssetsModel> disableList){

    List<AssetsModel> assetModelList = [];

    _myConnection.getDioConnection("",assetUrl,currency).get("${MyConstant.ACCOUNTS}/$address").then((response) {
      Map dataMap = response.data;

      if(dataMap["assets"] != null){
        List<dynamic> responseAssetList = dataMap["assets"];

        for(int i = 0 ; i < responseAssetList.length; i++){
          Map<String,dynamic> responseMap = responseAssetList[i];
          AssetsModel assetsModel = AssetsModel.fromMap(responseMap);
          assetModelList.add(assetsModel);
        }
      }

      if(assetId.isNotEmpty){
        _myConnection.getDioConnection("",url,currency).get("/idx${MyConstant.ASSETS}/$assetId?"
            "include-all=true").then((response) async {
          Map<String, dynamic> dataMap = response.data;
          AssetsDetailModel assetsDetailModel = AssetsDetailModel.fromMap(dataMap,1);

          bool disable = false;
          String disableAddress = userModel.address;

          for(int j = 0; j < disableList.length; j++){
            if(disableList[j].assetId == assetsDetailModel.index){
              disable = true;
              disableAddress = disableList[j].userAddress;
              break;
            }
          }

          Map assetMap = {};
          assetMap["asset_model"] = AssetsModel(ledgerPosition: 0, amount: 0,
              assetId: assetsDetailModel.index, userAddress: "", creator: assetsDetailModel.creator,
              isFrozen: false, optedInTtRound: 0, price: "0", currency: currency);
          assetMap["asset_detail"] = assetsDetailModel;
          assetMap["asset_amount"] = 0;
          assetMap["exist"] = false;
          assetMap["disable"] = disable;
          assetMap["disable_address"] = disableAddress;

          for(int j = 0; j < assetModelList.length; j++){
            AssetsModel assetsModel = assetModelList[j];
            if(assetsModel.assetId == assetsDetailModel.index){
              assetMap["exist"] = true;
              assetMap["asset_amount"] = assetsModel.amount;
            }
          }

          bool isSameFound = false;
          int sameIndex = 0;

          for(int k = 0 ; k < detailAssetsList.length; k++){
            if((detailAssetsList[k]["asset_model"] as AssetsModel).assetId == assetsDetailModel.index && !detailAssetsList[k]["exist"]){
              isSameFound = true;
              sameIndex = k;
            }
          }

          if(isSameFound){
            detailAssetsList[sameIndex] = assetMap;
            searchedAssetsList[sameIndex] = assetMap;
          }
          else{
            bool isSecondSameFound = false;

            for(int k = 0 ; k < detailAssetsList.length; k++){
              if((detailAssetsList[k]["asset_model"] as AssetsModel).assetId == assetsDetailModel.index){
                isSecondSameFound = true;
              }
            }

            if(!isSecondSameFound){
              detailAssetsList.add(assetMap);
              searchedAssetsList.add(assetMap);
            }
          }

          loadingMap["loading"] = false;
          controller.update(["asset_list"]);
        }).catchError((error){
          loadingMap["loading"] = false;
          controller.update(["asset_list"]);
        });
      }
      else{
        _myConnection.getDioConnection("",url,currency).get(MyConstant.ASSETS_ONLY).then((detailResponse) async {
          List<dynamic> assetsList = detailResponse.data;

          for(int i = 0; i < assetsList.length; i++){
            AssetsDetailModel assetsDetailModel = AssetsDetailModel.fromDatabaseMap(assetsList[i]);

            var finder = Finder(filter: Filter.and([Filter.equals("index", assetsDetailModel.index)
            ]), limit: 1);

            var assetList = await databaseDAO.getAllCuratedFinder(finder);

            if(assetList.isEmpty){
              databaseDAO.insertCuratedAsset(assetsDetailModel);
            }

            bool disable = false;
            String disableAddress = userModel.address;

            for(int j = 0; j < disableList.length; j++){
              if(disableList[j].assetId == assetsDetailModel.index){
                disable = true;
                disableAddress = disableList[j].userAddress;
                break;
              }
            }

            Map assetMap = Map();
            assetMap["asset_model"] = AssetsModel(ledgerPosition: 0, amount: 0,
                assetId: assetsDetailModel.index, userAddress: "", creator: assetsDetailModel.creator,
                isFrozen: false, optedInTtRound: 0, price: "0", currency: currency);
            assetMap["asset_detail"] = assetsDetailModel;
            assetMap["asset_amount"] = 0;
            assetMap["exist"] = false;
            assetMap["disable"] = disable;
            assetMap["disable_address"] = disableAddress;

            for(int j = 0; j < assetModelList.length; j++){
              AssetsModel assetsModel = assetModelList[j];
              if(assetsModel.assetId == assetsDetailModel.index){
                assetMap["exist"] = true;
                assetMap["asset_amount"] = assetsModel.amount;
              }
            }

            bool isSameFound = false;
            int sameIndex = 0;

            for(int k = 0 ; k < detailAssetsList.length; k++){
              if((detailAssetsList[k]["asset_model"] as AssetsModel).assetId == assetsDetailModel.index && !detailAssetsList[k]["exist"]){
                isSameFound = true;
                sameIndex = k;
              }
            }

            if(isSameFound){
              detailAssetsList[sameIndex] = assetMap;
              searchedAssetsList[sameIndex] = assetMap;
            }
            else{
              bool isSecondSameFound = false;

              for(int k = 0 ; k < detailAssetsList.length; k++){
                if((detailAssetsList[k]["asset_model"] as AssetsModel).assetId == assetsDetailModel.index){
                  isSecondSameFound = true;
                }
              }

              if(!isSecondSameFound){
                detailAssetsList.add(assetMap);
                searchedAssetsList.add(assetMap);
              }
            }
          }

          loadingMap["loading"] = false;
          controller.update(["asset_list"]);
        }).catchError((error){
          loadingMap["loading"] = false;
          controller.update(["asset_list"]);
        });
      }
    }).catchError((error){
      loadingMap["loading"] = false;
      controller.update(["asset_list"]);
    });
  }

  void getAssetsPendingAdd(String algodUrl, String txId){
    _myConnection.getDioConnection("",algodUrl,"").get("${MyConstant.PENDING_TRANSACTION}"
        "/$txId").then((response) async {
      Map dataMap = response.data;
      int confirmedRound = dataMap["confirmed-round"] ?? 0;

      if(confirmedRound == 0){
        Timer(Duration(seconds: 2), () async {
          getAssetsPendingAdd(algodUrl, txId);
        });
      }
      else{
        Get.back();
        Get.back();
      }
    });
  }

  Future<void> getAssetPending(String txId, UserModel? userModel, int ledgerPosition,String algodUrl, String idxUrl,
      List<Map>? assetList, List<Map>? searchedAssetList, int currentUserAlgo, String type, String message,
      String currency, Map loadingMap, int selectedPosition, int pendingType, int assetId, String baseUrl,
      List<List<Map>>? walletList, int userPosition) async {

    _myConnection.getDioConnection("",algodUrl,currency).get("${MyConstant.PENDING_TRANSACTION}/$txId").then((response) async {
      Map dataMap = response.data;
      int confirmedRound = dataMap["confirmed-round"] ?? 0;

      if(confirmedRound == 0){
        Timer(Duration(seconds: 2), () async {
          getAssetPending(txId, userModel, ledgerPosition, algodUrl, idxUrl, assetList, searchedAssetList,
              currentUserAlgo, type, message, currency, loadingMap, selectedPosition, pendingType, assetId,
              baseUrl, walletList, userPosition);
        });
      }
      else{
        if(type == "my_asset"){

          if(pendingType == 1){
            try {
              var response = await getPagesResponse(userModel?.address ?? "", algodUrl,
                  currency);
              Map dataMap = response.data;

              if (dataMap["assets"] != null) {
                List<dynamic> responseAssetList = dataMap["assets"];

                for (int i = 0; i < responseAssetList.length; i++) {
                  Map<String, dynamic> responseMap = responseAssetList[i];
                  responseMap["ledger-position"] = ledgerPosition;
                  responseMap["user-address"] = userModel?.address;

                  AssetsModel assetsModel = AssetsModel.fromMap(responseMap);
                  assetsModel.currency = currency;

                  if (assetsModel.assetId == assetId) {
                    var finderAsset = Finder(filter: Filter.and([
                      Filter.equals("ledger-position", ledgerPosition),
                      Filter.equals("asset-id", assetsModel.assetId),
                      Filter.equals("user-address", userModel?.address)
                    ]),
                        limit: 1);

                    var assetValue = await databaseDAO.getAllAssetBySorted(
                        finderAsset);

                    if (assetValue.isEmpty) {
                      await databaseDAO.insertAsset(assetsModel);
                    }
                    else {
                      await databaseDAO.updateAsset(assetsModel, finderAsset);
                    }

                    var detailFinder = Finder(filter: Filter.and([Filter.equals(
                        "ledger-position", ledgerPosition),
                      Filter.equals("index", assetId)]), limit: 1);

                    databaseDAO.getAllAssetDetailSorted(detailFinder).then((
                        assetValue) async {
                      if (assetValue.isEmpty) {
                        var detailResponse = await getAssetDetailResponse(
                            baseUrl, currency, assetId);

                        Map<String, dynamic> dataMap = detailResponse.data;

                        var assetsDetailModel = AssetsDetailModel.fromMap(
                            dataMap, ledgerPosition);

                        databaseDAO.insertAssetDetail(assetsDetailModel);

                        var finder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
                          Filter.equals("asset-id", assetsModel.assetId), Filter.equals("currency", currency)]), limit: 1);

                        String assetPercentage = "";
                        String assetPercentage2 = "";

                        Map body2 = {};
                        body2["url"] = "http://localhost:8002/api/v2/tickers/day";
                        body2["method"] = "get";
                        body2["bearer"] = MyConstant.TOKEN_BEARER;

                        Map paramsMap2 = {};
                        paramsMap2["symbol"] = "${assetsDetailModel.unitName}/$currency";

                        body2["params"] = paramsMap2;

                        _myConnection.getDioConnection(
                            "", MyConstant.TOKEN_URL, "").post(
                            MyConstant.FORWARD,
                            data: body2).then((detailResponse) {

                          if (detailResponse.data is Map) {
                            Map responseMap = detailResponse.data;

                            List<
                                dynamic> assetsPriceList = responseMap["${assetsDetailModel.unitName}"
                                "/$currency"];
                            List<Map> priceFiltered = [];

                            for (int i = 0; i <
                                assetsPriceList.length; i++) {
                              Map assetPriceMap = assetsPriceList[i];
                              String price = assetPriceMap["price"] ?? "";
                              String created_at = assetPriceMap["created_at"] ??
                                  "";

                              Map priceMapPosition = Map();
                              priceMapPosition["price"] =
                                  double.parse(price);
                              priceMapPosition["created_at"] = created_at;
                              priceMapPosition["prev_index"] = i;

                              priceFiltered.add(priceMapPosition);
                            }

                            for (int i = 0; i < priceFiltered.length; i++) {
                              for (int j = 0; j <
                                  priceFiltered.length; j++) {
                                if (j < priceFiltered.length - 1) {
                                  double position1 = priceFiltered[j]["price"];
                                  double position2 = priceFiltered[j +
                                      1]["price"];

                                  int prevIndex1 = priceFiltered[j]["prev_index"];
                                  int prevIndex2 = priceFiltered[j +
                                      1]["prev_index"];

                                  if (position1 > position2) {
                                    priceFiltered[j]["price"] = position2;
                                    priceFiltered[j + 1]["price"] =
                                        position1;

                                    priceFiltered[j]["prev_index"] =
                                        prevIndex2;
                                    priceFiltered[j + 1]["prev_index"] =
                                        prevIndex1;
                                  }
                                }
                              }
                            }

                            if (priceFiltered.length > 1) {
                              Map lowestMap = priceFiltered[0];
                              Map highestMap = priceFiltered[priceFiltered
                                  .length - 1];

                              double lowest = double.parse(
                                  assetsPriceList[lowestMap["prev_index"] >
                                      assetsPriceList.length - 1
                                      ? lowestMap["prev_index"] - 1
                                      : lowestMap["prev_index"]]["price"] ??
                                      "0");

                              double highest = double.parse(
                                  assetsPriceList[highestMap["prev_index"] >
                                      assetsPriceList.length - 1
                                      ? highestMap["prev_index"] - 1
                                      : highestMap["prev_index"]]["price"] ??
                                      "0");

                              double PercentagePrice = MyUtils
                                  .getPercentagePrice(lowest, highest);

                              String percentage = PercentagePrice.toString()
                                  .startsWith("-")
                                  ? "${
                                  PercentagePrice.toStringAsFixed(2)}%"
                                  : "+${PercentagePrice.toStringAsFixed(
                                  2)}%";

                              assetPercentage = percentage;
                              assetPercentage2 = PercentagePrice.toString();
                            }
                          }

                          AssetsPricing assetPricingModel = AssetsPricing(
                              ledgerPosition: ledgerPosition,
                              assetId: assetId,
                              currency: currency,
                              price: assetsModel.price,
                              percentage: assetPercentage,
                              percentagePrice: assetPercentage2);

                          databaseDAO.getAssetPricingWithFinder(finder)
                              .then((assetPricing) async {
                            if (assetPricing.isEmpty) {
                              databaseDAO.insertAssetPricing(assetPricingModel);
                            }
                            else {
                              databaseDAO.updateAssetPricing(assetPricingModel, finder);
                            }

                            Map assetMap = {};
                            assetMap["asset_model"] = assetsModel;
                            assetMap["is_pending"] = false;
                            assetMap["asset_detail_model"] = assetsDetailModel;
                            assetMap["status_name"] = "";
                            assetMap["percentage"] = assetPercentage;

                            int searchedPosition = 0;

                            for(int j = 0; j < walletList![ledgerPosition][userPosition]["searched_list"].length; j++){
                              if((walletList[ledgerPosition][userPosition]["searched_list"][j]["asset_model"] as
                              AssetsModel).assetId == assetId){
                                searchedPosition = j;
                                break;
                              }
                            }

                            walletList[ledgerPosition][userPosition]["original_list"].add(assetMap);

                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["asset_model"] =
                                assetsModel;
                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["is_pending"] =
                            false;
                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["asset_detail_model"] =
                                assetsDetailModel;
                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["status_name"] = "";
                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["percentage"] = assetPercentage;

                            for(int i = 0; i < walletList[ledgerPosition][userPosition]["disable_list"].length; i++){
                              if(walletList[ledgerPosition][userPosition]["disable_list"][i].assetId == assetId && walletList[ledgerPosition][userPosition][
                              "disable_list"][i].userAddress == userModel?.address){walletList[ledgerPosition][userPosition]["disable_list"].removeAt(i);
                              break;
                              }
                            }

                            controller.update(["assets", "slider"]);
                          }
                          );
                        });
                      }
                      else {
                        var pricingFinder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
                          Filter.equals("asset-id", assetsModel.assetId), Filter.equals("currency", currency)]), limit: 1);

                        var pricess = await databaseDAO.getAssetPricingWithFinder(pricingFinder);

                        if(pricess.isNotEmpty){
                          if(pricess[0].percentagePrice == "0" || pricess[0].percentagePrice == ""){
                            var finder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
                              Filter.equals("asset-id", assetsModel.assetId), Filter.equals("currency", currency)]), limit: 1);

                            String assetPercentage = "";
                            String assetPercentage2 = "";

                            Map body2 = {};
                            body2["url"] = "http://localhost:8002/api/v2/tickers/day";
                            body2["method"] = "get";
                            body2["bearer"] = MyConstant.TOKEN_BEARER;

                            Map paramsMap2 = {};
                            paramsMap2["symbol"] = "${assetValue[0].unitName}/$currency";

                            body2["params"] = paramsMap2;

                            _myConnection.getDioConnection("", MyConstant.TOKEN_URL, ""
                            ).post(MyConstant.FORWARD,
                                data: body2).then((detailResponse) {

                              if (detailResponse.data is Map) {
                                Map responseMap = detailResponse.data;

                                List<
                                    dynamic> assetsPriceList = responseMap["${assetValue[0].unitName}"
                                    "/$currency"];
                                List<Map> priceFiltered = [];

                                for (int i = 0; i <
                                    assetsPriceList.length; i++) {
                                  Map assetPriceMap = assetsPriceList[i];
                                  String price = assetPriceMap["price"] ?? "";
                                  String created_at = assetPriceMap["created_at"] ??
                                      "";

                                  Map priceMapPosition = Map();
                                  priceMapPosition["price"] =
                                      double.parse(price);
                                  priceMapPosition["created_at"] = created_at;
                                  priceMapPosition["prev_index"] = i;

                                  priceFiltered.add(priceMapPosition);
                                }

                                for (int i = 0; i < priceFiltered.length; i++) {
                                  for (int j = 0; j <
                                      priceFiltered.length; j++) {
                                    if (j < priceFiltered.length - 1) {
                                      double position1 = priceFiltered[j]["price"];
                                      double position2 = priceFiltered[j +
                                          1]["price"];

                                      int prevIndex1 = priceFiltered[j]["prev_index"];
                                      int prevIndex2 = priceFiltered[j +
                                          1]["prev_index"];

                                      if (position1 > position2) {
                                        priceFiltered[j]["price"] = position2;
                                        priceFiltered[j + 1]["price"] =
                                            position1;

                                        priceFiltered[j]["prev_index"] =
                                            prevIndex2;
                                        priceFiltered[j + 1]["prev_index"] =
                                            prevIndex1;
                                      }
                                    }
                                  }
                                }

                                if (priceFiltered.length > 1) {
                                  Map lowestMap = priceFiltered[0];
                                  Map highestMap = priceFiltered[priceFiltered
                                      .length - 1];

                                  double lowest = double.parse(
                                      assetsPriceList[lowestMap["prev_index"] >
                                          assetsPriceList.length - 1
                                          ? lowestMap["prev_index"] - 1
                                          : lowestMap["prev_index"]]["price"] ??
                                          "0");

                                  double highest = double.parse(
                                      assetsPriceList[highestMap["prev_index"] >
                                          assetsPriceList.length - 1
                                          ? highestMap["prev_index"] - 1
                                          : highestMap["prev_index"]]["price"] ??
                                          "0");

                                  double PercentagePrice = MyUtils
                                      .getPercentagePrice(lowest, highest);

                                  String percentage = PercentagePrice.toString()
                                      .startsWith("-")
                                      ? "${
                                      PercentagePrice.toStringAsFixed(2)}%"
                                      : "+${PercentagePrice.toStringAsFixed(
                                      2)}%";

                                  assetPercentage = percentage;
                                  assetPercentage2 = PercentagePrice.toString();
                                }
                              }

                              AssetsPricing assetPricingModel = AssetsPricing(
                                  ledgerPosition: ledgerPosition,
                                  assetId: assetId,
                                  currency: currency,
                                  price: assetsModel.price,
                                  percentage: assetPercentage,
                                  percentagePrice: assetPercentage2);

                              databaseDAO.getAssetPricingWithFinder(finder)
                                  .then((assetPricing) async {
                                if (assetPricing.isEmpty) {
                                  databaseDAO.insertAssetPricing(assetPricingModel);
                                }
                                else {
                                  databaseDAO.updateAssetPricing(assetPricingModel, finder);
                                }

                                Map assetMap = {};
                                assetMap["asset_model"] = assetsModel;
                                assetMap["is_pending"] = false;
                                assetMap["asset_detail_model"] = assetValue[0];
                                assetMap["status_name"] = "";
                                assetMap["percentage"] = assetPercentage;

                                int searchedPosition = 0;

                                for(int j = 0; j < walletList![ledgerPosition][userPosition]["searched_list"].length; j++){
                                  if((walletList[ledgerPosition][userPosition]["searched_list"][j]["asset_model"] as AssetsModel).assetId == assetId){
                                    searchedPosition = j;
                                    break;
                                  }
                                }

                                walletList[ledgerPosition][userPosition]["original_list"].add(assetMap);

                                walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["asset_model"] =
                                    assetsModel;
                                walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["is_pending"] =
                                false;
                                walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["asset_detail_model"] =
                                assetValue[0];
                                walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["status_name"] = "";
                                walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["percentage"] = assetPercentage;

                                for(int i = 0; i < walletList[ledgerPosition][userPosition]["disable_list"].length; i++){
                                  if(walletList[ledgerPosition][userPosition]["disable_list"][i].assetId == assetId && walletList[ledgerPosition][userPosition]["disable_list"][i].userAddress == userModel?.address){
                                    walletList[ledgerPosition][userPosition]["disable_list"].removeAt(i);
                                    break;
                                  }
                                }

                                controller.update(["assets", "slider"]);
                              }
                              );
                            });
                          }
                          else{
                            Map assetMap = {};
                            assetMap["asset_model"] = assetsModel;
                            assetMap["is_pending"] = false;
                            assetMap["asset_detail_model"] = assetValue[0];
                            assetMap["status_name"] = "";
                            assetMap["percentage"] = pricess[0].percentage;

                            int searchedPosition = 0;

                            for(int j = 0; j < walletList![ledgerPosition][userPosition]["searched_list"]!.length; j++){
                              if((walletList[ledgerPosition][userPosition]["searched_list"][j]["asset_model"] as AssetsModel).assetId == assetId){
                                searchedPosition = j;
                                break;
                              }
                            }

                            walletList[ledgerPosition][userPosition]["original_list"].add(assetMap);

                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["asset_model"] =
                                assetsModel;
                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["is_pending"] =
                            false;
                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["asset_detail_model"] =
                            assetValue[0];
                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["status_name"] = "";
                            walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["percentage"] = pricess[0].percentage;

                            for(int i = 0; i < walletList[ledgerPosition][userPosition]["disable_list"].length; i++){
                              if(walletList[ledgerPosition][userPosition]["disable_list"][i].assetId == assetId && walletList[ledgerPosition][userPosition]["disable_list"][i].userAddress == userModel?.address){
                                walletList[ledgerPosition][userPosition]["disable_list"].removeAt(i);
                                break;
                              }
                            }

                            controller.update(["assets", "slider"]);
                          }
                        }
                        else{
                          var finder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
                            Filter.equals("asset-id", assetsModel.assetId), Filter.equals("currency", currency)]), limit: 1);

                          String assetPercentage = "";
                          String assetPercentage2 = "";

                          Map body2 = {};
                          body2["url"] =
                          "http://localhost:8002/api/v2/tickers/day";
                          body2["method"] = "get";
                          body2["bearer"] = MyConstant.TOKEN_BEARER;

                          Map paramsMap2 = {};
                          paramsMap2["symbol"] = "${assetValue[0].unitName}/$currency";

                          body2["params"] = paramsMap2;

                          _myConnection.getDioConnection(
                              "", MyConstant.TOKEN_URL, "").post(
                              MyConstant.FORWARD,
                              data: body2).then((detailResponse) {

                            if (detailResponse.data is Map) {
                              Map responseMap = detailResponse.data;

                              List<
                                  dynamic> assetsPriceList = responseMap["${assetValue[0].unitName}"
                                  "/$currency"];
                              List<Map> priceFiltered = [];

                              for (int i = 0; i <
                                  assetsPriceList.length; i++) {
                                Map assetPriceMap = assetsPriceList[i];
                                String price = assetPriceMap["price"] ?? "";
                                String created_at = assetPriceMap["created_at"] ??
                                    "";

                                Map priceMapPosition = Map();
                                priceMapPosition["price"] =
                                    double.parse(price);
                                priceMapPosition["created_at"] = created_at;
                                priceMapPosition["prev_index"] = i;

                                priceFiltered.add(priceMapPosition);
                              }

                              for (int i = 0; i < priceFiltered.length; i++) {
                                for (int j = 0; j <
                                    priceFiltered.length; j++) {
                                  if (j < priceFiltered.length - 1) {
                                    double position1 = priceFiltered[j]["price"];
                                    double position2 = priceFiltered[j +
                                        1]["price"];

                                    int prevIndex1 = priceFiltered[j]["prev_index"];
                                    int prevIndex2 = priceFiltered[j +
                                        1]["prev_index"];

                                    if (position1 > position2) {
                                      priceFiltered[j]["price"] = position2;
                                      priceFiltered[j + 1]["price"] =
                                          position1;

                                      priceFiltered[j]["prev_index"] =
                                          prevIndex2;
                                      priceFiltered[j + 1]["prev_index"] =
                                          prevIndex1;
                                    }
                                  }
                                }
                              }

                              if (priceFiltered.length > 1) {
                                Map lowestMap = priceFiltered[0];
                                Map highestMap = priceFiltered[priceFiltered
                                    .length - 1];

                                double lowest = double.parse(
                                    assetsPriceList[lowestMap["prev_index"] >
                                        assetsPriceList.length - 1
                                        ? lowestMap["prev_index"] - 1
                                        : lowestMap["prev_index"]]["price"] ??
                                        "0");

                                double highest = double.parse(
                                    assetsPriceList[highestMap["prev_index"] >
                                        assetsPriceList.length - 1
                                        ? highestMap["prev_index"] - 1
                                        : highestMap["prev_index"]]["price"] ??
                                        "0");

                                double PercentagePrice = MyUtils
                                    .getPercentagePrice(lowest, highest);

                                String percentage = PercentagePrice.toString()
                                    .startsWith("-")
                                    ? "${
                                    PercentagePrice.toStringAsFixed(2)}%"
                                    : "+${PercentagePrice.toStringAsFixed(
                                    2)}%";

                                assetPercentage = percentage;
                                assetPercentage2 = PercentagePrice.toString();
                              }
                            }

                            AssetsPricing assetPricingModel = AssetsPricing(
                                ledgerPosition: ledgerPosition,
                                assetId: assetId,
                                currency: currency,
                                price: assetsModel.price,
                                percentage: assetPercentage,
                                percentagePrice: assetPercentage2);

                            databaseDAO.getAssetPricingWithFinder(finder)
                                .then((assetPricing) async {
                              if (assetPricing.isEmpty) {
                                databaseDAO.insertAssetPricing(assetPricingModel);
                              }
                              else {
                                databaseDAO.updateAssetPricing(assetPricingModel, finder);
                              }

                              Map assetMap = {};
                              assetMap["asset_model"] = assetsModel;
                              assetMap["is_pending"] = false;
                              assetMap["asset_detail_model"] = assetValue[0];
                              assetMap["status_name"] = "";
                              assetMap["percentage"] = assetPercentage;

                              int searchedPosition = 0;

                              for(int j = 0; j < walletList![ledgerPosition][userPosition]["searched_list"].length; j++){
                                if((walletList[ledgerPosition][userPosition]["searched_list"][j]["asset_model"] as AssetsModel).assetId ==
                                    assetId){
                                  searchedPosition = j;
                                  break;
                                }
                              }

                              walletList[ledgerPosition][userPosition]["original_list"].add(assetMap);

                              walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["asset_model"] =
                                  assetsModel;
                              walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["is_pending"] =
                              false;
                              walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["asset_detail_model"] =
                              assetValue[0];
                              walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["status_name"] = "";
                              walletList[ledgerPosition][userPosition]["searched_list"][searchedPosition]["percentage"] = assetPercentage;

                              for(int i = 0; i < walletList[ledgerPosition][userPosition]["disable_list"].length; i++){
                                if(walletList[ledgerPosition][userPosition]["disable_list"][i].assetId == assetId && walletList[ledgerPosition][userPosition]["disable_list"][i].userAddress == userModel?.address){
                                  walletList[ledgerPosition][userPosition]["disable_list"].removeAt(i);
                                  break;
                                }
                              }

                              controller.update(["assets", "slider"]);
                            }
                            );
                          });
                        }
                      }
                    });
                    break;
                  }
                }
              }
            }catch(e){
              print(e);
            }
          }
          else{
            int assetPos = 0;
            int assetSearchedPos = 0;

            for(int i = 0; i < walletList![ledgerPosition][userPosition]["original_list"]!.length ; i++){
              if((walletList[ledgerPosition][userPosition]["original_list"][i]["asset_model"] as AssetsModel).assetId == assetId){
                assetPos = i;
                break;
              }
            }

            for(int i = 0; i < walletList[ledgerPosition][userPosition]["searched_list"].length ; i++){
              if((walletList[ledgerPosition][userPosition]["searched_list"][i]["asset_model"] as AssetsModel).assetId == assetId){
                assetSearchedPos = i;
                break;
              }
            }

            for(int i = 0; i < walletList[ledgerPosition][userPosition]["disable_list"]!.length; i++){
              if(walletList[ledgerPosition][userPosition]["disable_list"][i].assetId == assetId && walletList[ledgerPosition][userPosition][
              "disable_list"][i].userAddress == userModel?.address){walletList[ledgerPosition][userPosition]["disable_list"].removeAt(i);
              break;
              }
            }

            walletList[ledgerPosition][userPosition]["original_list"].removeAt(assetPos);
            walletList[ledgerPosition][userPosition]["searched_list"].removeAt(assetSearchedPos);

            controller.update(["assets","slider"]);

            var finder = Finder(filter: Filter.and([Filter.equals("ledger-position", ledgerPosition),
              Filter.equals("asset-id", assetId), Filter.equals("user-address",
                  userModel?.address)]), limit: 1);

            var assets = await databaseDAO.getAllAssetBySorted(finder);

            if(assets.isNotEmpty){
              await databaseDAO.deleteAssetById(assets[0]);
            }
          }
        }
        else if(type == "send"){
          isPendingLoading = false;
          Get.offAll(()=> HomeView(),arguments: {"transaction" : true,
            "message" : message});
        }
      }

    }).catchError((error){
      if(error is DioError){
        isPendingLoading = false;
        try{
          var response = error.response;
          if(response != null){
            Map dataMap = response.data;
            String message = dataMap["message"] ?? "";
            print(message);
          }
          else{
            print(error);
          }
        }
        catch(error){
          print(error);
        }
      }
      else{
        print(error);
      }
    });
  }

  void getAllAsset(String url, List<AssetsDetailModel> assetList){
    assetList.clear();

    _myConnection.getDioConnection("",url,"").get(MyConstant.ASSETS_ONLY).then((detailResponse) {
      List<dynamic> assetsList = detailResponse.data;

      for(int i = 0; i < assetsList.length; i++){
        AssetsDetailModel assetsDetailModel = AssetsDetailModel.fromDatabaseMap(assetsList[i]);
        assetList.add(assetsDetailModel);
      }

      assetList.insert(0, AssetsDetailModel(ledgerPosition: 0, createdAtRound: 0,
          index: 0, clawback: "", creator: "", decimals: 0, freeze: "",
          manager: "", name: "Algorand", nameB64: "", reserve: "", total: 0,
          unitName: "ALGO", unitNameB64: "", url: "", urlB64: "", currentRound: 0,
          circulatingSupply: 0
      ));

      controller.update(["asset_list"]);
    }).catchError((error){
      if(error is DioError){
        try{
          var response = error.response;
          if(response != null){
            Map dataMap = response.data;
            String message = dataMap["message"] ?? "";
            print(message);
          }
          else{
            print(error);
          }
        }
        catch(error){
          print(error);
        }
      }
      else{
        print(error);
      }
    });
  }

  Future<void> getAllTransaction(String algodUrl, String indexerUrl, String userAddress, String currency,
      String startDate, String endDate, int? assetId, List<Map> walletTransactionList, int userPosition) async {

    if(walletTransactionList[userPosition]["next_token"].isEmpty){
      walletTransactionList[userPosition]["trans_original_list"].clear();
      walletTransactionList[userPosition]["trans_searched_list"].clear();
      walletTransactionList[userPosition]["loading"] = true;
      controller.update(["transactions"]);
    }
    else{
      walletTransactionList[userPosition]["loading_more"] = true;
      controller.update(["transactions","theme"]);
    }

    String url = "${MyConstant.ACCOUNTS}/$userAddress/transactions?limit=20";

    if(startDate.isNotEmpty){
      url = "$url&start_date=$startDate";
    }

    if(endDate.isNotEmpty){
      url = "$url&end_date=$endDate";
    }

    if(walletTransactionList[userPosition]["next_token"].isNotEmpty){
      url = "$url&next=${walletTransactionList[userPosition]["next_token"]}";
    }

    if(assetId != null){
      url = "$url&asset_id=$assetId";
    }

    _myConnection.getDioConnection("",indexerUrl,currency).get(url).then((detailResponse) {
      Map dataMap = detailResponse.data;
      controller.update(["transactions"]);

      List<dynamic> transactionList = dataMap["transactions"];
      walletTransactionList[userPosition]["next_token"] = dataMap["next-token"] ?? "";

      if(transactionList.isNotEmpty){
        for(int i = 0; i < transactionList.length; i++){
          try{
            Map transactionMap = transactionList[i];
            Map appTransactionMap = transactionMap["application-transaction"];

            String txType = transactionMap["tx-type"] ?? "";
            String id = transactionMap["id"] ?? "";
            String note = transactionMap["note"] ?? "";
            int confirmedRound = transactionMap["confirmed-round"] ?? 0;

            String address = "";
            int assetId = 0;
            String assetName = "";
            int amount = 0;


            String price = "";

            String createdDate = DateFormat("MMM dd, yyyy").format(DateTime.fromMillisecondsSinceEpoch(
                transactionMap["round-time"] * 1000));
            String createdTime = DateFormat("HH:mm a").format(DateTime.fromMillisecondsSinceEpoch(
                transactionMap["round-time"] * 1000));

            String strCloseTo = "";
            String strReceiver = "";

            int fee = transactionMap["fee"] ?? 0;
            int appId = appTransactionMap["application-id"] ?? 0;

            if(txType == "pay"){
              String closeTo = transactionMap["payment-transaction"]["close-to"] ?? "";
              address = closeTo.isNotEmpty ? closeTo : transactionMap["payment-transaction"]["receiver"] ?? "";
              assetId = 0;
              assetName = "ALGO";
              amount = transactionMap["payment-transaction"]["amount"] ?? 0;
              price = transactionMap["payment-transaction"]["price"] ?? "0.0";
              strCloseTo = closeTo;
              strReceiver = transactionMap["payment-transaction"]["receiver"] ?? "";
            }
            else{
              String closeTo = transactionMap["asset-transfer-transaction"]["close-to"] ?? "";
              address = closeTo.isNotEmpty ? closeTo : ((transactionMap["asset-transfer-transaction"]["receiver"] ?? "") as
              String).isEmpty ? userAddress : transactionMap["asset-transfer-transaction"]["receiver"];
              assetId = transactionMap["asset-transfer-transaction"]["asset-id"] ?? 0;
              amount = transactionMap["asset-transfer-transaction"]["amount"] ?? 0;
              price = transactionMap["asset-transfer-transaction"]["price"] ?? "0.0";
              strCloseTo = closeTo;
              strReceiver = transactionMap["asset-transfer-transaction"]["receiver"] ?? "";
            }

            String type = "";

            if(txType == "appl"){
              type = "App Call";
            }
            else{
              if(strCloseTo.isNotEmpty){
                type = "Remove Asset";
              }
              else{
                if((transactionMap["sender"] ?? "") == userAddress && amount == 0){
                  if(strReceiver == userAddress){
                    type = "Add Asset";
                  }
                  else{
                    type = (transactionMap["sender"] ?? "") == userAddress ? "Send" : "Receive";
                  }
                }
                else{
                  type = (transactionMap["sender"] ?? "") == userAddress ? "Send" : "Receive";
                }
              }
            }

            TransactionModel transactionModel = TransactionModel(transaction_id: id, type: type,
                address: address, assetId: assetId, assetName: assetName, amount: amount,
                price: price, createdAt: createdDate,note: utf8.decode(base64.decode(note)),
                createdTime: createdTime, notificationMessage: "", read: false, confirmedRound: confirmedRound,
                decimal: 0, from: transactionMap["sender"] ?? "", ledgerId: 0, applicationId: appId, fee: fee

            );

            if(walletTransactionList[userPosition]["trans_original_list"].isNotEmpty){
              bool isSameDateFound = false;
              int sameDateIndex = 0;

              for(int j = 0; j < walletTransactionList[userPosition]["trans_original_list"].length; j++){
                Map transactionListMap = walletTransactionList[userPosition]["trans_original_list"][j];
                String strDate = transactionListMap["date"];

                if(strDate == createdDate){
                  isSameDateFound = true;
                  sameDateIndex = j;
                }
              }

              if(isSameDateFound){
                (walletTransactionList[userPosition]["trans_original_list"][sameDateIndex]["transaction_list"] as List<TransactionModel>).add(transactionModel);
                (walletTransactionList[userPosition]["trans_original_list"][sameDateIndex]["searched_transaction_list"] as List<TransactionModel>).add(transactionModel);
              }
              else{
                List<TransactionModel> resultTransactionList = [];
                resultTransactionList.add(transactionModel);

                Map transMap = Map();
                transMap["transaction_list"]= resultTransactionList;
                transMap["searched_transaction_list"]= resultTransactionList;
                transMap["date"] = createdDate;

                walletTransactionList[userPosition]["trans_original_list"].add(transMap);
              }
            }
            else{
              List<TransactionModel> resultTransactionList = [];
              resultTransactionList.add(transactionModel);

              Map transMap = Map();
              transMap["transaction_list"]= resultTransactionList;
              transMap["searched_transaction_list"]= resultTransactionList;
              transMap["date"] = createdDate;

              walletTransactionList[userPosition]["trans_original_list"].add(transMap);
            }
          }
          catch(e){
            print("$e ${transactionList[i]["id"]}");
          }
        }

        walletTransactionList[userPosition]["trans_searched_list"].clear();

        for(int g = 0 ; g < walletTransactionList[userPosition]["trans_original_list"].length; g++){
          walletTransactionList[userPosition]["trans_searched_list"].add(walletTransactionList[userPosition]["trans_original_list"][g]);
        }

        for(int i = 0 ; i < walletTransactionList[userPosition]["trans_original_list"].length; i++){
          if(walletTransactionList[userPosition]["trans_original_list"][i]["date"].isNotEmpty){
            List<TransactionModel> transModelList = walletTransactionList[userPosition]["trans_original_list"][i]["transaction_list"] = (walletTransactionList[userPosition]["trans_original_list"][i]["transaction_list"] as List<TransactionModel>).toSet().toList();
            walletTransactionList[userPosition]["trans_original_list"][i]["searched_transaction_list"] = (walletTransactionList[userPosition]["trans_original_list"][i]["transaction_list"] as List<TransactionModel>).toSet().toList();

            walletTransactionList[userPosition]["trans_searched_list"][i]["transaction_list"] = (walletTransactionList[userPosition]["trans_searched_list"][i]["transaction_list"] as List<TransactionModel>).toSet().toList();
            walletTransactionList[userPosition]["trans_searched_list"][i]["searched_transaction_list"] = (walletTransactionList[userPosition]["trans_searched_list"][i]["transaction_list"] as List<TransactionModel>).toSet().toList();

            for(int k = 0 ; k < transModelList.length; k++){
              var assetFinder = Finder(filter: Filter.and([Filter.equals("asset-id",
                  transModelList[k].assetId),Filter.equals("currency", currency)]),
                  limit: 1);

              var assetDetailFinder = Finder(filter: Filter.and([Filter.equals("index",
                  transModelList[k].assetId)]), limit: 1);

              databaseDAO.getAllAssetBySorted(assetFinder).then((assetValue) {
                if(assetValue.isEmpty){
                  getSingleAssetDetail(algodUrl,indexerUrl,transModelList[k],0,i,k,userAddress,currency
                      ,walletTransactionList,userPosition);
                }
                else{
                  if(transModelList[k].assetId != 0){
                    databaseDAO.getAllAssetDetailSorted(assetDetailFinder).then((assetDetailValue) {
                      if(assetDetailValue.isEmpty){
                        getSingleAssetDetail(algodUrl,indexerUrl,transModelList[k],0,i,k,userAddress,currency
                            ,walletTransactionList,userPosition);
                      }
                      else{
                        (walletTransactionList[userPosition]["trans_original_list"][i]["transaction_list"][k] as TransactionModel).price = assetValue[0].price;
                        (walletTransactionList[userPosition]["trans_original_list"][i]["transaction_list"][k] as TransactionModel).assetName = assetDetailValue[0].unitName;
                        (walletTransactionList[userPosition]["trans_original_list"][i]["transaction_list"][k] as TransactionModel).decimal = assetDetailValue[0].decimals;

                        walletTransactionList[userPosition]["loading"] = false;
                        controller.update(["transactions"]);
                      }
                    });
                  }
                  else{
                    (walletTransactionList[userPosition]["trans_original_list"][i]["transaction_list"][k] as TransactionModel).price = assetValue[0].price;
                    (walletTransactionList[userPosition]["trans_original_list"][i]["transaction_list"][k] as TransactionModel).assetName = "ALGO";

                    walletTransactionList[userPosition]["loading"] = false;
                    controller.update(["transactions"]);
                  }
                }
              });
            }
          }
        }
      }
      else{
        walletTransactionList[userPosition]["next_token"] = "";
      }

      walletTransactionList[userPosition]["loading"] = false;
      walletTransactionList[userPosition]["loading_more"] = false;

      controller.update(["transactions","theme"]);
    }).catchError((error){
      if(error is DioError){
        walletTransactionList[userPosition]["loading"] = false;
        controller.update(["transactions","theme"]);
        try{
          var response = error.response;
          if(response != null){
            Map dataMap = response.data;
            String message = dataMap["message"] ?? "";
            print(message);
          }
          else{
            print(error);
          }
        }
        catch(error){
          print(error);
        }
      }
      else{
        print(error);
      }
    });
  }

  void getTokenPrice(List<FlSpot> tokenSpotList, String assetUnitName,
      String priceType, Map priceMap, String currency){

    tokenSpotList.clear();

    priceMap["is_loading"] = true;
    controller.update(["token_info"]);

    Map body = Map();
    body["url"] = "http://localhost:8002/api/v2/tickers/$priceType";
    body["method"] = "get";
    body["bearer"] = MyConstant.TOKEN_BEARER;

    Map paramsMap = Map();
    paramsMap["symbol"] = "$assetUnitName/$currency";

    body["params"] = paramsMap;

    _myConnection.getDioConnection("",MyConstant.TOKEN_URL,"").post(MyConstant.FORWARD,
        data: body).then((detailResponse) {
      if(detailResponse.data is Map){
        Map responseMap = detailResponse.data;

        List<dynamic> assetsPriceList = responseMap["$assetUnitName/$currency"];
        List<Map> priceFiltered = [];

        for(int i = 0 ; i < assetsPriceList.length ; i++){
          Map assetPriceMap = assetsPriceList[i];
          String price = assetPriceMap["price"] ?? "";
          String created_at = assetPriceMap["created_at"] ?? "";

          Map priceMapPosition = Map();
          priceMapPosition["price"] = double.parse(price);
          priceMapPosition["created_at"] = created_at;
          priceMapPosition["prev_index"] = i;

          priceFiltered.add(priceMapPosition);

          FlSpot flSpot = FlSpot(i.toDouble(),double.parse(price));
          tokenSpotList.add(flSpot);
        }

        for(int i = 0 ; i < priceFiltered.length ; i++){
          for(int j = 0; j < priceFiltered.length; j++){
            if(j < priceFiltered.length - 1){
              double position1 = priceFiltered[j]["price"];
              double position2 = priceFiltered[j + 1]["price"];

              int prevIndex1 = priceFiltered[j]["prev_index"];
              int prevIndex2 = priceFiltered[j + 1]["prev_index"];

              if(position1 > position2){
                priceFiltered[j]["price"] = position2;
                priceFiltered[j + 1]["price"] =  position1;

                priceFiltered[j]["prev_index"] = prevIndex2;
                priceFiltered[j + 1]["prev_index"] =  prevIndex1;
              }
            }
          }
        }

        if(priceFiltered.length > 1){
          Map lowestMap = priceFiltered[0];
          Map highestMap = priceFiltered[priceFiltered.length - 1];

          priceMap["lowest"] = assetsPriceList[lowestMap["prev_index"] > assetsPriceList.length - 1
              ? lowestMap["prev_index"] - 1 : lowestMap["prev_index"]]["price"];

          priceMap["highest"] = assetsPriceList[highestMap["prev_index"] > assetsPriceList.length - 1
              ? highestMap["prev_index"] - 1 : highestMap["prev_index"]]["price"];
        }
      }

      priceMap["is_loading"] = false;
      controller.update(["token_info"]);

    }).catchError((error){
      if(error is DioError){
        try{
          var response = error.response;
          if(response != null){
            Map dataMap = response.data;
            String message = dataMap["message"] ?? "";
            print(message);
          }
          else{
            print(error);
          }
        }
        catch(error){
          print(error);
        }
      }
      else{
        print(error);
      }

      priceMap["is_loading"] = false;
      controller.update(["token_info"]);
    });
  }

  void getLatestPrice(String assetUnitName, String priceType, Map priceMap, String currency){

    Map body = Map();
    body["url"] = "http://localhost:8002/api/v2/tickers/$priceType";
    body["method"] = "get";
    body["bearer"] = MyConstant.TOKEN_BEARER;

    Map paramsMap = Map();
    paramsMap["symbol"] = "$assetUnitName/$currency";

    body["params"] = paramsMap;

    _myConnection.getDioConnection("",MyConstant.TOKEN_URL,"").post(MyConstant.FORWARD,
        data: body).then((detailResponse) {
      Map responseMap = detailResponse.data;
      priceMap["latest_price"] = responseMap["$assetUnitName/$currency"];

      controller.update(["token_info"]);
    }).catchError((error){
      if(error is DioError){
        try{
          var response = error.response;
          if(response != null){
            Map dataMap = response.data;
            String message = dataMap["message"] ?? "";
            print(message);
          }
          else{
            print(error);
          }
        }
        catch(error){
          print(error);
        }
      }
      else{
        print(error);
      }
    });
  }

  void getSingleAssetDetail(String algodUrl, String indexerUrl, TransactionModel transactionModel, int ledgerPosition,
      int mapIndex, int transacIndex, String userAddress, String currency, List<Map>? walletTransactionList, int userPosition){
    _myConnection.getDioConnection("",algodUrl,currency).get("${MyConstant.ACCOUNTS}/$userAddress").then((response) {
      Map dataMap = response.data;

      if(transactionModel.assetId == 0){
        if(walletTransactionList != null && walletTransactionList[userPosition]["trans_searched_list"] != null){
          (walletTransactionList[userPosition]["trans_original_list"][mapIndex]["transaction_list"] as List<TransactionModel>)[transacIndex].assetName = "ALGO";
          (walletTransactionList[userPosition]["trans_original_list"][mapIndex]["transaction_list"] as List<TransactionModel>)[transacIndex].price = dataMap["price"] ?? "0";
        }
        else{
          transactionModel.assetName = "ALGO";
          transactionModel.price = dataMap["price"] ?? "0";
        }
      }
      else{
        _myConnection.getDioConnection("",indexerUrl,"").get("${MyConstant.ASSETS}/${transactionModel.assetId}").then((response) {

          Map<String,dynamic> dataMap = response.data;
          AssetsDetailModel assetsDetailModel = AssetsDetailModel.fromMap(dataMap, ledgerPosition);
          transactionModel.assetName = assetsDetailModel.unitName;
          transactionModel.decimal = assetsDetailModel.decimals;

          if(walletTransactionList!= null && walletTransactionList[userPosition]["trans_searched_list"] != null){
            (walletTransactionList[userPosition]["trans_original_list"][mapIndex]["transaction_list"] as List<TransactionModel>)[transacIndex].assetName = assetsDetailModel.unitName;
            (walletTransactionList[userPosition]["trans_original_list"][mapIndex]["transaction_list"] as List<TransactionModel>)[transacIndex].decimal = assetsDetailModel.decimals;

            (walletTransactionList[userPosition]["trans_searched_list"][mapIndex]["transaction_list"] as List<TransactionModel>)[transacIndex].assetName = assetsDetailModel.unitName;
            (walletTransactionList[userPosition]["trans_searched_list"][mapIndex]["transaction_list"] as List<TransactionModel>)[transacIndex].decimal = assetsDetailModel.decimals;
          }

          controller.update(["theme"]);
        });
      }
    });
  }
}
