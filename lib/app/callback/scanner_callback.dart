abstract class ScannerCallback {
  void onScanned(String data);
}