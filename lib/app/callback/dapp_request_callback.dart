abstract class DAppsRequestCallback{
  void onDAppsLoading();
  void onDAppsRequested();
  void onDAppsFailed(String message);
}