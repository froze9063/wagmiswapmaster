import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/assets_model.dart';

abstract class MyAssetCallback{
  void onMyAsset(String txId, AssetsModel assetsModel, AssetsDetailModel assetsDetailModel,
      int type, List<AssetsModel> disableList);
}