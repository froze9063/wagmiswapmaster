import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';

abstract class SelectAssetCallback {
  void onAssetSelected(AssetsModel? assetsModel, AssetsDetailModel? assetsDetailModel,
      UserModel? holdAsset);
}