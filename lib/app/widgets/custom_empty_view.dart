import 'package:flutter/material.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/modules/home/controllers/home_controller.dart';

class CustomEmptyView{
  static Widget emptyView(String text, HomeController themeValue){
    return Text(
      text,
      style: TextStyle(
        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
        fontSize: 14,
        fontFamily: "BarlowMedium",
      ),
    );
  }
}