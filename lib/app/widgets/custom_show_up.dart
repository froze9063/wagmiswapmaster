import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:show_up_animation/show_up_animation.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/modules/home/controllers/home_controller.dart';

class CustomShowUp {
  static Widget showUpNotification(String message, HomeController themeValue){
    return ShowUpAnimation(
      animationDuration: Duration(seconds: 2),
      curve: Curves.bounceIn,
      direction: Direction.vertical,
      child: Column(
        children: [
          Container(width: double.maxFinite, height: 1, color: ThemeConstant.blueBorder),
          Container(
            color: themeValue.themeType == 0 ? ThemeConstant.selectHomeBackgroundDark : ThemeConstant.white,
            padding: EdgeInsets.only(left: 24, right: 24, top: 10, bottom: 10),
            child: Row(
              children: [
                Expanded(child: Text(
                  message,
                  style: TextStyle(
                    height: 1.5,
                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                    fontSize: 16,
                    fontFamily: "BarlowRegular",
                  ),
                ), flex: 1)
              ],
            ),
          ),
          Container(width: double.maxFinite, height: 1, color: ThemeConstant.blueBorder)
        ],
      ),
    );
  }
}