import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';

import '../controllers/secret_phase_controller.dart';

class SecretPhaseView extends GetView<SecretPhaseController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(29, 28, 28, 1.0),
                  Color.fromRGBO(52, 52, 52, 1.0)
                ]
            )
        ),
        child: Column(
          children: [
            SizedBox(height: 28),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(width: 3),
                GestureDetector(
                  child: Container(
                    color: Colors.transparent,
                    width: 55,
                    height: 55,
                    child: Center(
                      child: Image.asset("assets/icons/ic_white_back.png", width: 24,
                          height: 24, fit: BoxFit.contain),
                    ),
                  ),
                  onTap: (){
                    Get.back();
                  },
                ),
                Expanded(child: SizedBox(),flex: 1),
                Image.asset("assets/images/img_hori_messe.png", width: 182,
                    height: 18, fit: BoxFit.contain),
                Expanded(child: SizedBox(),flex: 1),
                Container(
                  width: 55,
                  height: 55,
                ),
                SizedBox(width: 3)
              ],
            ),

            SizedBox(height: 30),

            Text(
              StringContent.secret_phrase,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontFamily: "BarlowSemiBold",
                  letterSpacing: 2
              ),
            ),

            SizedBox(height: 16),

            Row(
              children: [
                SizedBox(width: 24),
                Expanded(child: Text(
                  StringContent.secret_phrase_desc,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                      letterSpacing: 2,
                      height: 1.5
                  ),
                ), flex: 1),
                SizedBox(width: 24),
              ],
            ),

            SizedBox(height: 30),

            Row(
              children: [
                SizedBox(width: 24),
                Text(
                  StringContent.seed_phrase_copy,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                      letterSpacing: 2
                  ),
                )
              ],
            ),

            SizedBox(height: 8),

            Container(
                margin: EdgeInsets.only(left: 24, right: 24),
                padding: EdgeInsets.all(8),
                width: double.maxFinite,
                height: 100,
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    border: Border.all(
                        width: 1,
                        color: Color.fromRGBO(124, 124, 124, 1.0)
                    )
                ),
                child: Text(
                  StringContent.secret_example,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                      letterSpacing: 2
                  ),
                ),
            ),

            Expanded(child: SizedBox(),flex: 1),

            GestureDetector(
              child: CustomButton(45, double.maxFinite, BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(
                      width: 1,
                      color: Colors.white
                  )
              ), StringContent.next, TextStyle(
                  color: Colors.white,
                  fontFamily: "BarlowRegular",
                  fontSize: 12,
                  letterSpacing: 2
              )),
              onTap: (){

              },
            ),

            SizedBox(height: 55)
          ],
        ),
      ),
    );
  }
}

