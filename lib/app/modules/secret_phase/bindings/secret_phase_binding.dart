import 'package:get/get.dart';

import '../controllers/secret_phase_controller.dart';

class SecretPhaseBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SecretPhaseController>(
      () => SecretPhaseController(),
    );
  }
}
