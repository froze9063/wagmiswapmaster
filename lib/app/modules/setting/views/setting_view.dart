import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/modules/about_us/views/about_us_view.dart';
import 'package:waagmiswap/app/modules/account/views/account_view.dart';
import 'package:waagmiswap/app/modules/currency_setting/views/currency_setting_view.dart';
import 'package:waagmiswap/app/modules/faq/views/faq_view.dart';
import 'package:waagmiswap/app/modules/language_setting/views/language_setting_view.dart';
import 'package:waagmiswap/app/modules/notification_setting/views/notification_setting_view.dart';
import 'package:waagmiswap/app/modules/security/views/security_view.dart';
import 'package:waagmiswap/app/modules/theme_setting/views/theme_setting_view.dart';
import 'package:waagmiswap/app/modules/wallet_connect_setting/views/wallet_connect_setting_view.dart';

import '../controllers/setting_controller.dart';

class SettingView extends GetView<SettingController> {

  SettingController _settingController = Get.put(SettingController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<SettingController>(
        id: "theme",
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Column(
            children: [
              SizedBox(height: 30),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 3),
                  GestureDetector(
                    child: Container(
                      color: Colors.transparent,
                      width: 55,
                      height: 55,
                      child: Center(
                        child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                        "assets/icons/ic_black_back.png", width: 24,
                            height: 24, fit: BoxFit.contain),
                      ),
                    ),
                    onTap: (){
                      Get.back();
                    },
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Text(
                    StringContent.settings,
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 14,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w600,
                        letterSpacing: 2
                    ),
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Container(
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(width: 3)
                ],
              ),

              Row(
                children: [
                  SizedBox(width: 24),
                  Text(
                    StringContent.account,
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                        fontSize: 16,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1
                    ),
                  ),
                ],
              ),

              SizedBox(height: 10),

              Container(
                margin: EdgeInsets.only(left: 24, right: 24),
                width: double.maxFinite,
                decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),
                child: Column(
                  children: [
                    Container(
                      height: 45,
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Center(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_wallet.png"
                                      : "assets/icons/ic_black_wallet.png", height: 22, width: 22),

                                  SizedBox(width: 8),

                                  Text(
                                    StringContent.accounts,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                          : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowNormal",
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1,
                                      decorationThickness: 1.5,
                                    ),
                                  ),

                                  Expanded(child: SizedBox(), flex: 1),

                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                      : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                      width: themeValue.themeType == 0 ? 22 : 16),
                                  SizedBox(width: 8),
                                ],
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                Get.to(() => AccountView(), arguments: {
                                  "user_list" : _settingController.userModelList,
                                  "selected_user" : _settingController.selectedUserModel,
                                  "selected_ledger" : _settingController.selectedLedger
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 45,
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Center(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  Container(
                                    width: 22,
                                    height: 22,
                                    child: Center(
                                      child: SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_wallet_connect.svg" :
                                      "assets/icons/ic_black_wallet_connect.svg", height: 22, width: 22),
                                    ),
                                  ),

                                  SizedBox(width: 8),

                                  Text(
                                    StringContent.wallet_connect,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                          : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowNormal",
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1,
                                      decorationThickness: 1.5,
                                    ),
                                  ),

                                  Expanded(child: SizedBox(), flex: 1),

                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                      : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                      width: themeValue.themeType == 0 ? 22 : 16),
                                  SizedBox(width: 8),
                                ],
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                Get.to(() => WalletConnectSettingView(), arguments: {
                                  "user_list" : _settingController.userModelList,
                                  "selected_user" : _settingController.selectedUserModel,
                                  "selected_ledger" : _settingController.selectedLedger
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 24),
              Row(
                children: [
                  SizedBox(width: 24),
                  Text(
                    StringContent.display,
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                        fontSize: 16,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1
                    ),
                  ),
                ],
              ),

              SizedBox(height: 10),

              Container(
                margin: EdgeInsets.only(left: 24, right: 24),
                width: double.maxFinite,
                decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),
                child: Column(
                  children: [
                    Container(
                      height: 45,
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Center(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_security.svg" :
                                  "assets/icons/ic_black_security.svg", height: 22, width: 22),

                                  SizedBox(width: 8),

                                  Text(
                                    StringContent.security,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                          : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowNormal",
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1,
                                      decorationThickness: 1.5,
                                    ),
                                  ),

                                  Expanded(child: SizedBox(), flex: 1),

                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                      : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                      width: themeValue.themeType == 0 ? 22 : 16),

                                  SizedBox(width: 8),
                                ],
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                Get.to(() => SecurityView(), arguments: {
                                  "user_list" : _settingController.userModelList,
                                  "selected_user" : _settingController.selectedUserModel,
                                  "selected_ledger" : _settingController.selectedLedger
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 45,
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Center(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_notifications.svg" :
                                  "assets/icons/ic_black_notifications.svg", height: 22, width: 22),

                                  SizedBox(width: 8),

                                  Text(
                                    StringContent.notifications,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                          : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowNormal",
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1,
                                      decorationThickness: 1.5,
                                    ),
                                  ),

                                  Expanded(child: SizedBox(), flex: 1),

                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                      : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                      width: themeValue.themeType == 0 ? 22 : 16),

                                  SizedBox(width: 8),
                                ],
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                Get.to(() => NotificationSettingView(), arguments: {
                                  "user_list" : _settingController.userModelList,
                                  "selected_user" : _settingController.selectedUserModel,
                                  "selected_ledger" : _settingController.selectedLedger
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 45,
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Center(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_currency.svg" : "assets/icons/ic_black_currency.svg", height: 22, width: 22),

                                  SizedBox(width: 8),

                                  Text(
                                    StringContent.currency,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                          : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowNormal",
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1,
                                      decorationThickness: 1.5,
                                    ),
                                  ),

                                  Expanded(child: SizedBox(), flex: 1),

                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                      : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                      width: themeValue.themeType == 0 ? 22 : 16),

                                  SizedBox(width: 8),
                                ],
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                Get.to(() => CurrencySettingView(), arguments: {
                                  "user_list" : _settingController.userModelList,
                                  "selected_user" : _settingController.selectedUserModel,
                                  "selected_ledger" : _settingController.selectedLedger
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 45,
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Center(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_language.svg" :
                                  "assets/icons/ic_black_language.svg", height: 22, width: 22),

                                  SizedBox(width: 8),

                                  Text(
                                    StringContent.language,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                          : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowNormal",
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1,
                                      decorationThickness: 1.5,
                                    ),
                                  ),

                                  Expanded(child: SizedBox(), flex: 1),

                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                      : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                      width: themeValue.themeType == 0 ? 22 : 16),

                                  SizedBox(width: 8),
                                ],
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                Get.to(() => LanguageSettingView(), arguments: {
                                  "user_list" : _settingController.userModelList,
                                  "selected_user" : _settingController.selectedUserModel,
                                  "selected_ledger" : _settingController.selectedLedger
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 45,
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Center(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_theme.svg" :
                                  "assets/icons/ic_black_theme.svg", height: 22, width: 22),

                                  SizedBox(width: 8),

                                  Text(
                                    StringContent.theme,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                          : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowNormal",
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1,
                                      decorationThickness: 1.5,
                                    ),
                                  ),

                                  Expanded(child: SizedBox(), flex: 1),

                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                      : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                      width: themeValue.themeType == 0 ? 22 : 16),

                                  SizedBox(width: 8),
                                ],
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                Get.to(() => ThemeSettingView(), arguments: {
                                  "user_list" : _settingController.userModelList,
                                  "selected_user" : _settingController.selectedUserModel,
                                  "selected_ledger" : _settingController.selectedLedger
                                })?.then((value) => {
                                  _settingController.loadUser()
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 24),
              Row(
                children: [
                  SizedBox(width: 24),
                  Text(
                    StringContent.support,
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                        fontSize: 16,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1
                    ),
                  ),
                ],
              ),

              SizedBox(height: 10),

              Container(
                margin: EdgeInsets.only(left: 24, right: 24),
                width: double.maxFinite,
                decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),
                child: Column(
                  children: [
                    Container(
                      height: 45,
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Center(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_about_us.svg" :
                                  "assets/icons/ic_black_about_us.svg", height: 22, width: 22),

                                  SizedBox(width: 8),

                                  Text(
                                    StringContent.about_us,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                          : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowNormal",
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1,
                                    ),
                                  ),

                                  Expanded(child: SizedBox(), flex: 1),

                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                      : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                      width: themeValue.themeType == 0 ? 22 : 16),

                                  SizedBox(width: 8),
                                ],
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                Get.to(() => AboutUsView(), arguments: {
                                  "user_list" : _settingController.userModelList,
                                  "selected_user" : _settingController.selectedUserModel,
                                  "selected_ledger" : _settingController.selectedLedger
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 45,
                      child: Material(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        child: Stack(
                          children: [
                            Center(
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_faq.svg" :
                                  "assets/icons/ic_black_faq.svg", height: 22, width: 22),

                                  SizedBox(width: 8),

                                  Text(
                                    StringContent.faq,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                          : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowNormal",
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1,
                                    ),
                                  ),

                                  Expanded(child: SizedBox(), flex: 1),

                                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                      : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                      width: themeValue.themeType == 0 ? 22 : 16),

                                  SizedBox(width: 8),
                                ],
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                Get.to(() => FaqView(), arguments: {
                                  "user_list" : _settingController.userModelList,
                                  "selected_user" : _settingController.selectedUserModel,
                                  "selected_ledger" : _settingController.selectedLedger
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
