import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';

import '../controllers/scanner_controller.dart';

class ScannerView extends GetView<ScannerController> {

  ScannerController scannerController = Get.put(ScannerController());

  @override
  Widget build(BuildContext context) {
    scannerController.buildContext = context;
    return Scaffold(
        body: Stack(
          children: [
            scannerController.buildQrView(context),
            Container(
              margin: EdgeInsets.only(top: 60),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 24),
                  Expanded(child: Text(
                    'Scan QR Code',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: "PoppinsMedium",
                      fontSize: 20,
                    ),
                  ), flex: 1),
                  SizedBox(width: 24),
                ],
              ),
            ),
            Positioned.fill(child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.maxFinite,
                margin: EdgeInsets.only(left: 24, right: 24),
                height: 37,
                child: Stack(
                  children: [
                    CustomButton(37, double.maxFinite, BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.topRight,
                          colors: [
                            ThemeConstant.assetDark1,
                            ThemeConstant.assetDark1
                          ]
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ), StringContent.cancel, TextStyle(
                        color: Colors.white,
                        fontFamily: "BarlowRegular",
                        fontSize: 12,
                        letterSpacing: 2
                    )),

                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        onTap: (){
                          Get.back();
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
              bottom: 24,
            )
          ],
        ));
  }
}
