import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:waagmiswap/app/callback/scanner_callback.dart';

class ScannerController extends GetxController {

  late ScannerCallback scannerCallback;

  final qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? controller;
  late BuildContext buildContext;

  bool isScanned = false;

  @override
  void onInit() {
    scannerCallback = Get.arguments["scanner_callback"];
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Widget buildQrView(BuildContext context) => QRView(
    key: qrKey,
    onQRViewCreated: onQRViewCreated,
    overlay: QrScannerOverlayShape(
        cutOutSize: MediaQuery.of(context).size.width * 0.8,
        borderWidth: 5.0,
        borderRadius: 10,
        borderColor: Colors.white
    ),
  );

  void onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      if(!isScanned){
        isScanned = true;
        Get.back();
        scannerCallback.onScanned(scanData.code ?? "");
      }
    });
  }
}
