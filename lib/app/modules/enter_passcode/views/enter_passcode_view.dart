import 'dart:io';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';

import '../controllers/enter_passcode_controller.dart';

class EnterPasscodeView extends GetView<EnterPasscodeController> {

  EnterPasscodeController _enterPasscodeController = Get.put(EnterPasscodeController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(!_enterPasscodeController.fromResume),
      child: Scaffold(
        body: GetBuilder<EnterPasscodeController>(
          id: "theme",
          init: EnterPasscodeController(),
          builder: (themeValue) => Container(
            width: double.maxFinite,
            height: double.maxFinite,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                      themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                    ]
                )
            ),
            child: Column(
              children: [
                SizedBox(height: 24),

                GetBuilder<EnterPasscodeController>(
                    id: "back",
                    init: EnterPasscodeController(),
                    builder: (value) => Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            SizedBox(width: 3),
                            GestureDetector(
                              child: Container(
                                color: Colors.transparent,
                                width: 55,
                                height: 55,
                                child: Center(
                                  child: value.savedPasscode.isEmpty && !value.fromResume || value.from == 5 && !value.fromResume ||
                                      value.from == 3 && !value.fromResume || value.isFromUnlink && !value.fromResume || value.from == 10 ||
                                      value.from == 0 && value.fromSecurity ? Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                                      "assets/icons/ic_black_back.png", width: 24,
                                      height: 24, fit: BoxFit.contain) : SizedBox(),
                                ),
                              ),
                              onTap: (){
                                if(value.savedPasscode.isEmpty || value.from == 5 ||
                                    value.from == 3 || value.isFromUnlink || value.from == 10
                                    || value.from == 0 && value.fromSecurity ){
                                  Get.back();
                                }
                              },
                            )
                          ],
                        ),
                        Expanded(child: SizedBox(),flex: 1),
                        Visibility(child: Image.asset("assets/images/img_hori_messe.png", width: 182,
                            height: 23, fit: BoxFit.contain), visible: _enterPasscodeController.from != 3),
                        Expanded(child: SizedBox(),flex: 1),
                        Row(
                          children: [
                            Container(
                              width: 55,
                              height: 55,
                            ),
                            SizedBox(width: 3)
                          ],
                        )
                      ],
                    )),

                SizedBox(height: 18),

                GetBuilder<EnterPasscodeController>(
                    id: "passcode",
                    init: EnterPasscodeController(),
                    builder: (value) => Text(
                      value.savedPasscode.isEmpty ? value.stepNumber == 1 ? value.from == 5 ? StringContent.enter_new_passcode :
                      StringContent.enter_passcode : value.isPasscodeCorrect ? StringContent.re_enter_passcode :
                      StringContent.wrong_password : value.isPasscodeCorrect ? StringContent.enter_passcode :
                      value.savedPasscode.isNotEmpty ? StringContent.password_incorrect : StringContent.wrong_password,
                      style: TextStyle(color: value.isPasscodeCorrect ? themeValue.themeType == 0 ?
                          ThemeConstant.white : ThemeConstant.blackNavy : Colors.red,
                          fontSize: 14,
                          fontFamily: "BarlowNormal",
                          fontWeight: FontWeight.w600,
                          letterSpacing: 2
                      ),
                    )),

                SizedBox(height: 16),

                GetBuilder<EnterPasscodeController>(
                    id: "passcode",
                    init: EnterPasscodeController(),
                    builder: (value) => Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          radius: 5,
                          backgroundColor: value.numberLists.length >= 1 ? Color.fromRGBO(255, 197, 48, 1.0)
                              : themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          child: CircleAvatar(
                            radius: 4.5,
                            backgroundColor: value.numberLists.length >= 1 ? Color.fromRGBO(255, 197, 48, 1.0)
                                : themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                          ),
                        ),

                        SizedBox(width: 8),

                        CircleAvatar(
                          radius: 5,
                          backgroundColor: value.numberLists.length >= 2 ? Color.fromRGBO(255, 197, 48, 1.0)
                              : themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          child: CircleAvatar(
                            radius: 4.5,
                            backgroundColor: value.numberLists.length >= 2 ? Color.fromRGBO(255, 197, 48, 1.0)
                                : themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                          ),
                        ),

                        SizedBox(width: 8),

                        CircleAvatar(
                          radius: 5,
                          backgroundColor: value.numberLists.length >= 3 ? Color.fromRGBO(255, 197, 48, 1.0)
                              : themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          child: CircleAvatar(
                            radius: 4.5,
                            backgroundColor: value.numberLists.length >= 3 ? Color.fromRGBO(255, 197, 48, 1.0)
                                : themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                          ),
                        ),

                        SizedBox(width: 8),

                        CircleAvatar(
                          radius: 5,
                          backgroundColor: value.numberLists.length >= 4 ? Color.fromRGBO(255, 197, 48, 1.0)
                              : themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          child: CircleAvatar(
                            radius: 4.5,
                            backgroundColor: value.numberLists.length >= 4 ? Color.fromRGBO(255, 197, 48, 1.0)
                                : themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                          ),
                        ),

                        SizedBox(width: 8),

                        CircleAvatar(
                          radius: 5,
                          backgroundColor: value.numberLists.length >= 5 ? Color.fromRGBO(255, 197, 48, 1.0)
                              : themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          child: CircleAvatar(
                            radius: 4.5,
                            backgroundColor: value.numberLists.length >= 5 ? Color.fromRGBO(255, 197, 48, 1.0)
                                : themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                          ),
                        ),

                        SizedBox(width: 8),

                        CircleAvatar(
                          radius: 5,
                          backgroundColor: value.numberLists.length == 6 ? Color.fromRGBO(255, 197, 48, 1.0)
                              : themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          child: CircleAvatar(
                            radius: 4.5,
                            backgroundColor: value.numberLists.length == 6 ? Color.fromRGBO(255, 197, 48, 1.0)
                                : themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                          ),
                        )
                      ],
                    )),

                SizedBox(height: 55),

                Row(
                  children: [
                    SizedBox(width: 55),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "1",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(1);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "2",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(2);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "3",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(3);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    SizedBox(width: 55),
                  ],
                ),

                SizedBox(height: 36),

                Row(
                  children: [
                    SizedBox(width: 55),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "4",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(4);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "5",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(5);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "6",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(6);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    SizedBox(width: 55),
                  ],
                ),

                SizedBox(height: 36),

                Row(
                  children: [
                    SizedBox(width: 55),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "7",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(7);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "8",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(8);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "9",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(9);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    SizedBox(width: 55),
                  ],
                ),

                SizedBox(height: 36),

                Row(
                  children: [
                    SizedBox(width: 55),

                    Expanded(child: SizedBox()),

                    Expanded(child: Container(
                      child: Center(
                        child: Stack(
                          children: [
                            Center(child: CircleAvatar(
                              radius: 31,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              child: CircleAvatar(
                                radius: 30.5,
                                backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              ),
                            )),
                            Center(child: ElevatedButton(
                              child: CircleAvatar(
                                radius: 31,
                                backgroundColor: Colors.transparent,
                                child: CircleAvatar(
                                  radius: 30.5,
                                  backgroundColor: Colors.transparent,
                                  child: Material(
                                    color: Colors.transparent,
                                    shape : CircleBorder(),
                                    clipBehavior : Clip.hardEdge,
                                    child: CircleAvatar(
                                      radius: 30.5,
                                      backgroundColor: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Text(
                                              "0",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 20,
                                                fontFamily: "BarlowSemiBold",
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              style: ButtonStyle(
                                  elevation: MaterialStateProperty.all(0),
                                  backgroundColor: MaterialStateProperty.all(Colors.transparent),
                                  shape: MaterialStateProperty.all(CircleBorder()),
                                  overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                              ),
                              onPressed: (){
                                _enterPasscodeController.enterNumber(0);
                              },
                            ))
                          ],
                        ),
                      ),
                    )),

                    Expanded(child: SizedBox(), flex: 1),

                    SizedBox(width: 55),
                  ],
                ),

                Row(
                  children: [
                    Expanded(child: SizedBox(), flex: 1),

                    ElevatedButton(
                      child: CircleAvatar(
                        radius: 31,
                        backgroundColor: Colors.transparent,
                        child: CircleAvatar(
                          radius: 30.5,
                          backgroundColor: Colors.transparent,
                          child: Material(
                            color: Colors.transparent,
                            shape : CircleBorder(),
                            clipBehavior : Clip.hardEdge,
                            child: CircleAvatar(
                              radius: 30.5,
                              backgroundColor: Colors.transparent,
                              child: Stack(
                                children: [
                                  Center(
                                    child: Text(
                                      StringContent.delete,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                          fontSize: 12,
                                          fontFamily: "BarlowRegular",
                                          fontWeight: FontWeight.w400,
                                          letterSpacing: 1,
                                          height: 1.5
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      style: ButtonStyle(
                          elevation: MaterialStateProperty.all(0),
                          backgroundColor: MaterialStateProperty.all(Colors.transparent),
                          shape: MaterialStateProperty.all(CircleBorder()),
                          overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.5))
                      ),
                      onPressed: (){
                        _enterPasscodeController.remove();
                      },
                    ),

                    SizedBox(width: 55),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
