import 'dart:async';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:get/get.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/callback/passcode_callback.dart';
import 'package:waagmiswap/app/callback/reveal_mnemonic_callback.dart';
import 'package:waagmiswap/app/callback/security_callback.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/faceid/views/faceid_view.dart';
import 'package:waagmiswap/app/modules/home/views/home_view.dart';
import 'package:waagmiswap/app/modules/unlink_confirmation/views/unlink_confirmation_view.dart';
import 'package:waagmiswap/app/widgets/custom_toast.dart';

class EnterPasscodeController extends GetxController {

  MnemonicCallback? mnemonicCallback;

  late List<int> numberLists;
  late DatabaseDAO databaseDAO;

  int stepNumber = 1;
  int from = 0;

  int themeType = 0;

  List<String>? mnemonicLists;
  String publicAddress = "";
  Account? account;

  bool isPasscodeCorrect = true;
  bool faceId = false;
  bool fromResume = false;

  String firstPasscode = "";
  String secondPasscode = "";
  String savedPasscode = "";

  UserModel? userModel;
  bool isFromUnlink = false;
  bool fromWelcome = false;

  PasscodeCallback? passcodeCallback;
  SecurityCallback? securityCallback;

  int securityType = 0;
  bool fromSecurity = false;

  @override
  void onInit() {
    databaseDAO = DatabaseDAO();
    numberLists = [];
    getArgument();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getArgument(){
    if(Get.arguments != null){
      if(Get.arguments["mnemonic_callback"] != null){
        mnemonicCallback = Get.arguments["mnemonic_callback"];
      }
      if(Get.arguments["public_address"] != null){
        publicAddress = Get.arguments["public_address"] ?? "";
      }
      if(Get.arguments["mnemonic_list"] != null){
        mnemonicLists = Get.arguments["mnemonic_list"];
      }
      if(Get.arguments["account"] != null){
        account = Get.arguments["account"];
      }
      if(Get.arguments["user_model"] != null){
        userModel = Get.arguments["user_model"];
      }
      if(Get.arguments["from_unlink"] != null){
        isFromUnlink = Get.arguments["from_unlink"];
      }
      if(Get.arguments["from_resume"] != null){
        fromResume = Get.arguments["from_resume"];
      }
      if(Get.arguments["from_welcome"] != null){
        fromWelcome = Get.arguments["from_welcome"];
      }
      if(Get.arguments["passcode_callback"] != null){
        passcodeCallback = Get.arguments["passcode_callback"];
      }
      if(Get.arguments["security_callback"] != null){
        securityCallback = Get.arguments["security_callback"];
      }
      if(Get.arguments["security_type"] != null){
        securityType = Get.arguments["security_type"];
      }
      if(Get.arguments["from_security"] != null){
        fromSecurity = Get.arguments["from_security"];
      }
      from = Get.arguments["from"] ?? 0;
    }

    loadUser();
  }

  void enterNumber(int number){
    if(numberLists.length < 6){
      numberLists.add(number);
      update(["passcode"]);
    }
    if(numberLists.length == 6){
      if(from != 3){
        if(stepNumber == 1){
          Timer(Duration(milliseconds: 100), () async {
            firstPasscode = parsePasscode();
            if(savedPasscode.isNotEmpty){
              if(!fromSecurity){
                isPasscodeCorrect = firstPasscode == savedPasscode;
                if(isPasscodeCorrect){
                  if(from != 5){
                    if(from == 10){
                      Get.back();
                      securityCallback?.onSecurityChanged(securityType);
                    }
                    else{
                      if(isFromUnlink){
                        Get.off(() => UnlinkConfirmationView(), arguments: {
                          "user_model" : userModel});
                      }
                      else{
                        Get.offAll(() => HomeView());
                      }
                    }
                  }
                  else{
                    savedPasscode = "";
                    numberLists.clear();
                    update(["passcode"]);
                  }
                }
                else{
                  numberLists.clear();
                }
              }
              else{
                stepNumber = 2;
                numberLists.clear();
              }
            }
            else{
              stepNumber = 2;
              numberLists.clear();
            }
            update(["passcode"]);
          });
        }
        else{
          Timer(Duration(milliseconds: 100), () async {
            secondPasscode = parsePasscode();
            isPasscodeCorrect = secondPasscode == firstPasscode;
            if(isPasscodeCorrect){
              if(from != 5){
                if(!fromSecurity){
                  Get.back();
                  Get.to(() => FaceidView(),
                      arguments: {
                        "public_address" : publicAddress,
                        "mnemonic_list" : mnemonicLists,
                        "account" : account,
                        "from" : from,
                        "passcode" : parsePasscode()
                      }
                  );
                }
                else{
                  SharedPreferences.getInstance().then((prefs){
                    prefs.setString("passcode",parsePasscode());
                    Get.back();
                    securityCallback?.onSecurityChanged(securityType);
                  });
                }
              }
              else{
                SharedPreferences.getInstance().then((prefs){
                  prefs.setString("passcode",parsePasscode());
                  Get.back();
                  passcodeCallback?.onPasscodeChanged();
                });
              }
            }
            else{
              numberLists.clear();
            }
            update(["passcode"]);
          });
        }
      }
      else{
        Timer(Duration(milliseconds: 100), () async {
          firstPasscode = parsePasscode();
          isPasscodeCorrect = firstPasscode == savedPasscode;

          if(isPasscodeCorrect){
            Timer(Duration(milliseconds: 100), () async {
              mnemonicCallback?.onRevealed();
              Get.back();
            });
          }
          else{
            numberLists.clear();
            update(["passcode"]);
          }
        });
      }
    }
  }

  void remove(){
    if(numberLists.isNotEmpty){
      numberLists.removeAt(numberLists.length - 1);
    }
    update(["passcode"]);
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      if(!fromSecurity){
        savedPasscode = prefs.getString("passcode") ?? "";
      }
      faceId = prefs.getBool("face_id") ?? false;
      if(from == 3 || savedPasscode.isNotEmpty){
        themeType = prefs.getInt("theme") ?? 0;
        if(from == 0 && fromWelcome || from == 3){
          if(faceId){
            mnemonicCallback?.onRevealed();
            authenticate();
          }
        }
      }
      else{
        if(from == 0 && fromWelcome || from == 3){
          if(faceId){
            mnemonicCallback?.onRevealed();
            authenticate();
          }
        }
      }
      update(["theme","back"]);
    });
  }

  String parsePasscode(){
    String strPasscode = "";
    for(int i = 0; i < numberLists.length ; i++){
      strPasscode = "$strPasscode${numberLists[i]}";
    }
    return strPasscode;
  }

  Future<bool> authenticateWithBiometrics() async {
    bool isAuthenticated = false;

    var androidMessage = AndroidAuthMessages(
      signInTitle: "WAGMISWAP",
      biometricHint: ""
    );

    try{
      final LocalAuthentication localAuthentication = LocalAuthentication();
      isAuthenticated = await localAuthentication.authenticate(
        localizedReason: 'Please complete the biometrics to proceed.',
        androidAuthStrings: androidMessage,
        biometricOnly: true,
      );
    }
    catch(e){
      print(e);
    }

    return isAuthenticated;
  }

  void authenticate() async {
    bool isAuthenticated = await authenticateWithBiometrics();
    if(isAuthenticated){
      if(from == 3){
        Get.back();
      }
      else{
        Get.offAll(() => HomeView());
      }
    }
  }
}
