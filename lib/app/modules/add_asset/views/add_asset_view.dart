import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/modules/send/views/send_view.dart';
import 'package:waagmiswap/app/util/my_util.dart';

import '../controllers/add_asset_controller.dart';

class AddAssetView extends GetView<AddAssetController> {

  final AddAssetController _addAssetController = Get.put(AddAssetController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GetBuilder<AddAssetController>(
        id: "theme",
        init: AddAssetController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Stack(
            children: [
              Container(
                width: double.maxFinite,
                height: double.maxFinite,
                child: Column(
                  children: [
                    SizedBox(height: 30),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(width: 3),
                        GestureDetector(
                          child: Container(
                            color: Colors.transparent,
                            width: 55,
                            height: 55,
                            child: Center(
                              child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                              "assets/icons/ic_black_back.png", width: 24,
                                  height: 24, fit: BoxFit.contain),
                            ),
                          ),
                          onTap: (){
                            Get.back();
                          },
                        ),
                        Expanded(child: SizedBox(),flex: 1),
                        Text(
                          StringContent.choose_asset,
                          style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              fontSize: 14,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w600,
                              letterSpacing: 2
                          ),
                        ),
                        Expanded(child: SizedBox(),flex: 1),
                        Container(
                          width: 55,
                          height: 55,
                        ),
                        SizedBox(width: 3)
                      ],
                    ),

                    SizedBox(height: 18),

                    GetBuilder<AddAssetController>(
                        id: "asset_focus",
                        init: AddAssetController(),
                        builder: (assetValue) => GestureDetector(
                          child: Container(
                            margin: EdgeInsets.only(left: 24, right: 24),
                            padding: EdgeInsets.only(left: 16, right: 16),
                            width: double.maxFinite,
                            height: 35,
                            decoration: BoxDecoration(
                                color: themeValue.themeType == 0 ? Colors.transparent : ThemeConstant.white,
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                border: Border.all(
                                    width: 1,
                                    color: assetValue.isAssetFocus ? themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.lightActiveBorder
                                        : ThemeConstant.secondTextGrayDark
                                )
                            ),
                            child: Center(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SvgPicture.asset("assets/icons/ic_search.svg", height: 16, width: 16),
                                  SizedBox(width: 8),
                                  Expanded(child: TextField(
                                    controller: _addAssetController.textEditingController,
                                    focusNode: assetValue.assetFocus,
                                    keyboardType: TextInputType.number,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 12,
                                      fontFamily: "BarlowRegular",
                                    ),
                                    decoration: InputDecoration.collapsed(hintText: StringContent.search_asset_hint,
                                        hintStyle: TextStyle(
                                          color: Color.fromRGBO(164,164,164,1),
                                          fontSize: 12,
                                          fontFamily: "BarlowRegular",
                                        )
                                    ),
                                    onChanged: (text){
                                      _addAssetController.loadAsset();
                                    },
                                  ), flex: 1)
                                ],
                              ),
                            ),
                          ),
                          onTap: (){
                            assetValue.assetFocus.requestFocus();
                          },
                        )),

                    SizedBox(height: 24),

                    Expanded(child: GetBuilder<AddAssetController>(
                      id: "asset_list",
                      init: AddAssetController(),
                      builder: (assetListValue) => Container(
                        width: double.maxFinite,
                        height: double.maxFinite,
                        child: RefreshIndicator(
                          color: Color.fromRGBO(106, 48, 147, 1.0),
                          onRefresh: ()=> assetListValue.loadAsset(),
                          child: Container(
                            width: double.maxFinite,
                            height: double.maxFinite,
                            child: Stack(
                              children: [

                                Visibility(child: Center(
                                  child: Text(
                                    "No assets",
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowMedium",
                                    ),
                                  ),
                                ), visible: assetListValue.searchedDetailLists.isEmpty && !assetListValue.loadingMap["loading"]),

                                Visibility(child: Center(
                                  child: CircularProgressIndicator(
                                    color: Color.fromRGBO(106, 48, 147, 1.0),
                                  ),
                                ), visible: assetListValue.loadingMap["loading"]),

                                Container(
                                  width: double.maxFinite,
                                  height: double.maxFinite,
                                  child: ListView.builder(
                                    itemBuilder: (context, index){

                                      Map assetMap = assetListValue.searchedDetailLists[index];
                                      AssetsDetailModel assetDetailModel = assetMap["asset_detail"];
                                      bool isAssetExist = assetMap["exist"];
                                      int amount = assetMap["asset_amount"] ?? 0;

                                      return Container(
                                        margin: EdgeInsets.only(left: 24, right: 24, top: index == 0 ? 0 : 16),
                                        width: double.maxFinite,
                                        padding: EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                              begin: Alignment.topCenter,
                                              end: Alignment.bottomCenter,
                                              colors: [
                                                assetListValue.searchedDetailLists[index]["disable"] && assetListValue.searchedDetailLists[
                                                index]["disable_address"] == assetListValue.userModel.address ? Colors.grey.withOpacity(0.5) :
                                                themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,

                                                assetListValue.searchedDetailLists[index]["disable"] && assetListValue.searchedDetailLists[
                                                index]["disable_address"] == assetListValue.userModel.address ? Colors.grey.withOpacity(0.5) :
                                                themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                                              ]
                                          ),
                                          borderRadius: BorderRadius.all(Radius.circular(6)),
                                        ),
                                        child: Row(
                                          children: [
                                            CircleAvatar(
                                                child: assetDetailModel != null ? !assetDetailModel.unitName.contains("-") ? assetDetailModel.index != 0 ? MyUtils.getCuratedIconPath(assetDetailModel.unitName, themeValue.themeType).isEmpty ? SvgPicture.network("https://wallet.wagmiswap.io/testnet/"
                                                    "assets/${assetDetailModel.index}.svg",height: 40, width: 40) : SvgPicture.asset(MyUtils.getCuratedIconPath(assetDetailModel.unitName, themeValue.themeType), height: 34, width: 34) : SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_dark_algo.svg" : "assets/icons/ic_light_algo.svg",
                                                    height: 34, width: 34) : SizedBox() : SizedBox(),
                                                radius: 18,
                                                backgroundColor: assetDetailModel != null ? MyUtils.getCuratedIconPath(assetDetailModel.unitName, themeValue.themeType).isEmpty ? ThemeConstant.secondTextGrayLight : Colors.transparent : Colors.transparent
                                            ),
                                            SizedBox(width: 10),
                                            Expanded(child: Column(
                                              children: [
                                                Row(
                                                  children: [
                                                    Expanded(child: Text(
                                                      assetDetailModel.name,
                                                      style: TextStyle(
                                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                        fontSize: 16,
                                                        fontFamily: "BarlowMedium",
                                                      ),
                                                    ), flex: 1),
                                                  ],
                                                ),
                                                SizedBox(height: 10),
                                                Row(
                                                  children: [
                                                    Text(
                                                      "${assetDetailModel.unitName} (${assetDetailModel.index})",
                                                      style: TextStyle(
                                                        color: ThemeConstant.secondTextGrayDark,
                                                        fontSize: 12,
                                                        fontFamily: "BarlowNormal",
                                                      ),
                                                    )
                                                  ],
                                                )
                                              ],
                                            )),
                                            GestureDetector(
                                              child: Transform.scale(scale: 0.8, child: CupertinoSwitch(
                                                  value: isAssetExist,
                                                  activeColor: assetListValue.searchedDetailLists[index]["disable"] && assetListValue.searchedDetailLists[
                                                  index]["disable_address"] == assetListValue.userModel.address ? Color.fromRGBO(233, 233, 234, 1) :
                                                  Color.fromRGBO(255, 197, 48, 1.0),
                                                  trackColor: Color.fromRGBO(233, 233, 234, 1),
                                                  onChanged: (value){
                                                    if(!assetListValue.searchedDetailLists[index]["disable"] && assetListValue.searchedDetailLists[
                                                    index]["disable_address"] == assetListValue.userModel.address){
                                                      _addAssetController.showAlgoError(false);
                                                      showBottomSheet(context, index, themeValue, assetListValue.searchedDetailLists[index]["asset_detail"],
                                                          assetListValue.searchedDetailLists[index]["exist"], amount);
                                                    }
                                                  })),
                                              onTap: (){
                                                _addAssetController.showAlgoError(false);
                                                if(!assetListValue.searchedDetailLists[index]["disable"] && assetListValue.searchedDetailLists[
                                                index]["disable_address"] == assetListValue.userModel.address){
                                                  showBottomSheet(context, index, themeValue, assetListValue.searchedDetailLists[index]["asset_detail"],
                                                      assetListValue.searchedDetailLists[index]["exist"], amount);
                                                }
                                              },
                                            )
                                          ],
                                        ),
                                      );
                                    },
                                    itemCount: assetListValue.searchedDetailLists.length,
                                    physics: AlwaysScrollableScrollPhysics(),
                                    padding: EdgeInsets.zero,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ))
                  ],
                ),
              ),

              Visibility(child: Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.transparent,
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.3),
                        borderRadius: BorderRadius.all(Radius.circular(8))
                    ),
                    child: Center(
                      child: CircularProgressIndicator(
                        color: Color.fromRGBO(106, 48, 147, 1.0),
                      ),
                    ),
                  ),
                ),
              ), visible: _addAssetController.isLoading.value)
            ],
          ),
        ),
      ),
    );
  }

  showBottomSheet(BuildContext context, int selectedIndex, AddAssetController themeValue, AssetsDetailModel
  assetsDetailModel, bool isExist, int amount){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          themeValue.switchStatus = isExist;
          return Container(
              width: double.maxFinite,
              height: 350.5,
              child: GetBuilder<AddAssetController>(
                id: "theme",
                init: AddAssetController(),
                builder: (value) => Stack(
                  children: [
                    Container(
                      width: double.maxFinite,
                      height: 350.5,
                      decoration: BoxDecoration(
                        color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(24),
                      ),
                    ),
                    Column(
                      children: [
                        Expanded(child: SizedBox(), flex: 1),
                        Container(
                          width: double.maxFinite,
                          height: 350,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                                  themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground
                                ]
                            ),
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                          ),
                          child: Column(
                            children: [
                              SizedBox(height: 16),
                              Text(
                                _addAssetController.switchStatus ? StringContent.removing_asset : StringContent.adding_asset,
                                style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 14,
                                    fontFamily: "BarlowNormal",
                                    fontWeight: FontWeight.w600,
                                    letterSpacing: 2
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 24, right: 24, top: 16),
                                width: double.maxFinite,
                                height: 55,
                                child: Center(
                                  child: Row(
                                    children: [
                                      SizedBox(width: 8),
                                      CircleAvatar(
                                          child: assetsDetailModel != null ? !assetsDetailModel.unitName.contains("-") ? assetsDetailModel.index != 0 ? MyUtils.getCuratedIconPath(assetsDetailModel.unitName, themeValue.themeType).isEmpty ? SvgPicture.network("https://wallet.wagmiswap.io/testnet/"
                                              "assets/${assetsDetailModel.index}.svg",height: 40, width: 40) : SvgPicture.asset(MyUtils.getCuratedIconPath(assetsDetailModel.unitName, themeValue.themeType), height: 40, width: 40) : SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_dark_algo.svg" : "assets/icons/ic_light_algo.svg",
                                              height: 40, width: 40) : SizedBox() : SizedBox(),
                                          radius: 18,
                                          backgroundColor: assetsDetailModel != null ? MyUtils.getCuratedIconPath(assetsDetailModel.unitName, themeValue.themeType).isEmpty ? ThemeConstant.secondTextGrayLight : Colors.transparent : Colors.transparent
                                      ),

                                      SizedBox(width: 8),

                                      Expanded(child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                assetsDetailModel.name,
                                                style: TextStyle(
                                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                    fontSize: 14,
                                                    fontFamily: "BarlowNormal",
                                                    fontWeight: FontWeight.w600
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(height: 4),
                                          Row(
                                            children: [
                                              Text(
                                                "${assetsDetailModel.unitName} (${assetsDetailModel.index})",
                                                style: TextStyle(
                                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                                    fontSize: 12,
                                                    fontFamily: "BarlowNormal",
                                                    fontWeight: FontWeight.w600
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ), flex: 1),
                                      SvgPicture.asset("assets/icons/ic_check_green.svg", height: 22, width: 22),
                                      SizedBox(width: 8),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 16),
                              Row(
                                children: [
                                  SizedBox(width: 24),
                                  Expanded(child: _addAssetController.switchStatus ? RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      text: '',
                                      children: <TextSpan>[
                                        TextSpan(text: amount == 0 ? "You are about to remove " : "To remove ", style: TextStyle(
                                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                            fontSize: 12,
                                            fontFamily: "BarlowNormal",
                                            fontWeight: FontWeight.w400,
                                            letterSpacing: 1,
                                            height: 1.5
                                        )),
                                        TextSpan(text: "${assetsDetailModel.unitName} ", style: TextStyle(
                                            color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                                            fontSize: 12,
                                            fontFamily: "BarlowNormal",
                                            fontWeight: FontWeight.w400,
                                            letterSpacing: 1,
                                            height: 1.5
                                        )),
                                        TextSpan(text: "from ${controller.accountName}. "
                                            "${amount == 0 ? "You will not be able to transact with this asset from this account"
                                            " unless you re-add it." : "The balance must be 0. Please transfer the balance to another "
                                            "account and try again."}", style: TextStyle(
                                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                            fontSize: 12,
                                            fontFamily: "BarlowNormal",
                                            fontWeight: FontWeight.w400,
                                            letterSpacing: 1,
                                            height: 1.5
                                        ))
                                      ],
                                    ),
                                  ) : Text(
                                    StringContent.adding_detail,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                        fontSize: 12,
                                        fontFamily: "BarlowNormal",
                                        fontWeight: FontWeight.w400,
                                        letterSpacing: 1,
                                        height: 1.5
                                    ),
                                  ), flex: 1),
                                  SizedBox(width: 24)
                                ],
                              ),

                              SizedBox(height: 6),

                              themeValue.showError ? Text(
                                "Insufficient Balance. You have 0 Algo.",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.red,
                                    fontSize: 12,
                                    fontFamily: "BarlowNormal",
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 1,
                                    height: 1.5
                                ),
                              ) : SizedBox(),
                              Expanded(child: SizedBox(), flex: 1),
                              Container(
                                margin: EdgeInsets.only(left: 24, right: 24, top: 24),
                                width: double.maxFinite,
                                height: 45,
                                child: Stack(
                                  children: [
                                    CustomButton(45, double.maxFinite, BoxDecoration(
                                      gradient: LinearGradient(
                                          begin: Alignment.topLeft,
                                          end: Alignment.topRight,
                                          colors: [
                                            Color.fromRGBO(106, 48, 147, 1.0),
                                            Color.fromRGBO(160, 68, 255, 1.0)
                                          ]
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                    ), _addAssetController.switchStatus ? amount == 0 ? StringContent.proceed :
                                    StringContent.transfer_balance : StringContent.approve, TextStyle(
                                        color: Colors.white,
                                        fontFamily: "BarlowRegular",
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        letterSpacing: 2
                                    )),

                                    Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        splashColor: Colors.transparent,
                                        onTap: () async {
                                          if(_addAssetController.userModel.amount == 0){
                                            _addAssetController.showAlgoError(true);
                                            return;
                                          }
                                          if(_addAssetController.switchStatus){
                                            if(amount == 0){
                                              _addAssetController.sendTransaction(selectedIndex, assetsDetailModel.index, 0,
                                                  _addAssetController.algorandAccount!, _addAssetController.algorandAccount!.address,
                                                  _addAssetController.algorandAccount!.address,0, context);
                                            }
                                            else{
                                              Get.back();
                                              Get.to(() => SendView(), arguments: {
                                                "user_model" : _addAssetController.userModel,
                                                "ledger_model" : _addAssetController.ledgerModel,
                                                "asset_id" : assetsDetailModel.index
                                              });
                                            }
                                          }
                                          else{
                                            _addAssetController.sendTransaction(selectedIndex, assetsDetailModel.index, 0,
                                                _addAssetController.algorandAccount!, _addAssetController.algorandAccount!.address,
                                                null,0, context);
                                          }
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(left: 24, right: 24, top: 16),
                                width: double.maxFinite,
                                height: 25,
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    splashColor: Colors.transparent,
                                    child: Center(
                                      child: Text(
                                          StringContent.cancel,
                                          style: TextStyle(
                                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                              fontFamily: "BarlowRegular",
                                              fontWeight: FontWeight.w600,
                                              fontSize: 12,
                                              decoration: TextDecoration.underline,
                                              letterSpacing: 2
                                          )
                                      ),
                                    ),
                                    onTap: (){
                                      Get.back();
                                    },
                                  ),
                                ),
                              ),

                              SizedBox(height: 36)
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
          );
        });
  }
}
