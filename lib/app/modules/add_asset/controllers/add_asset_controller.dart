import 'package:algorand_dart/algorand_dart.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sembast/sembast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/algorand/algorand.dart';
import 'package:waagmiswap/app/callback/my_assets_callback.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/repository/api_repository.dart';
import 'package:waagmiswap/app/widgets/custom_toast.dart';

class AddAssetController extends GetxController {

  late TextEditingController textEditingController = TextEditingController();

  late APIRepository apiRepository;
  late DatabaseDAO databaseDAO;
  late AlgorandRepository algorandRepository;
  late Algorand algorand;
  late MyAssetCallback? myAssetCallback;
  late List<Map> holdAssetList;

  Account? algorandAccount;

  int themeType = 0;

  late FocusNode assetFocus;
  bool isAssetFocus = false;

  late List<bool> switchList;
  bool switchStatus = false;

  late List<Map> assetDetailLists;
  late List<Map> searchedDetailLists;

  late UserModel userModel;
  late LedgerModel ledgerModel;

  Map loadingMap = {};

  String assetId = "";
  RxBool isLoading = false.obs;

  int loadingCount = 0;
  String accountName = "";
  late List<AssetsModel> disableList;

  bool showError = false;

  @override
  void onInit() {
    apiRepository = APIRepository(this);
    databaseDAO = DatabaseDAO();
    assetDetailLists = [];
    searchedDetailLists = [];
    holdAssetList = [];
    switchList = [];
    loadingMap["loading"] = false;
    getArguments();
    assetFocus = FocusNode();
    assetFocus.addListener(onAssetFocus);

    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void showAlgoError(bool error){
    showError = error;
    update(["theme"]);
  }

  void initAlgorand(){
    algorandRepository = AlgorandRepository();
    algorand = algorandRepository.getAlgorandConfig(ledgerModel.algodUrl,
        ledgerModel.indexerUrl);
  }

  void getArguments(){
    userModel = Get.arguments["user_model"];
    ledgerModel = Get.arguments["ledger_model"];
    disableList = Get.arguments["disable_list"];
    myAssetCallback = Get.arguments["my_asset_callback"];
    accountName = userModel.userName;

    initAlgorand();
    restoreAccount();
    loadAsset();
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  Future<void> loadAsset() async {
    if(!loadingMap["loading"]){
      loadingMap["loading"] = true;
      assetDetailLists.clear();
      searchedDetailLists.clear();
      holdAssetList.clear();
      update(["asset_list"]);
      assetId = textEditingController.text.toString().trim();
      apiRepository.getAllAssetsDetail(ledgerModel.baseUrl, ledgerModel.algodUrl,
          userModel.address, assetDetailLists, searchedDetailLists, loadingMap,"",
          assetId,holdAssetList,userModel, ledgerModel.ledgerId, disableList);
    }
  }

  void onAssetFocus(){
    isAssetFocus = true;
    update(["asset_focus"]);
  }

  void setAssetList(int index){
    switchList[index] = !switchList[index];
    switchStatus = switchList[index];
    update(["asset_list"]);
  }

  Future<void> restoreAccount() async {
    algorand.restoreAccount(userModel.mnemonic.split(" ")).then((value) {
      algorandAccount = value;
    }).catchError((error, stackTrace) {
      var mnemonicError = error as MnemonicException;
      CustomToast.showToast(mnemonicError.message);
      return null;
    });
  }

  Future<void> sendTransaction(int selectedIndex, int assetId, int amount, Account userAccount,
      Address? receiverAddress, Address? closeToAddress, int addressLength, BuildContext buildContext) async {

    isLoading.value = true;
    update(["theme"]);

    String txId = "";
    try {
      final params = await algorand.getSuggestedTransactionParams();

      AssetTransferTransaction transaction;

      if(closeToAddress == null){
        transaction = await (AssetTransferTransactionBuilder()
          ..assetId = assetId
          ..amount = amount
          ..sender = userAccount.address
          ..receiver = receiverAddress
          ..suggestedParams = params
        ).build();
      }
      else{
        transaction = await (AssetTransferTransactionBuilder()
          ..assetId = assetId
          ..amount = amount
          ..sender = userAccount.address
          ..closeTo = closeToAddress
          ..suggestedParams = params
        ).build();
      }

      txId = transaction.id;

      final signedTransaction = await transaction.sign(userAccount);

      await algorand.sendTransaction(signedTransaction);

      isLoading.value = false;
      update(["theme"]);

      AssetsModel algoAssetsModel = AssetsModel(ledgerPosition: ledgerModel.ledgerId,
          amount: 0, assetId: assetId, userAddress: userModel.address, creator: "",
          isFrozen: false, optedInTtRound: 0, price: userModel.price, currency: "");

      disableList.add(algoAssetsModel);

      Get.back();
      Get.back();

      if(closeToAddress == null){
        onAssetExecuted(txId,selectedIndex,1);
      }
      else{
        var finder = Finder(filter: Filter.and([Filter.equals("user-address", userModel.address),
          Filter.equals("ledger-position", ledgerModel.ledgerId), Filter.equals("asset-id", assetId)]));

        databaseDAO.deleteUserAsset(finder).then((value) {
          onAssetExecuted(txId,selectedIndex,2);
        });
      }

    } on AlgorandException catch (ex) {
      isLoading.value = false;
      update(["theme"]);
      if(ex.cause is DioError){
        var dioError = ex.cause as DioError;
        if(dioError.response != null){
          Map dioResponse = dioError.response!.data;
          String strMessage = dioResponse["message"];
          if(closeToAddress == null && strMessage == "error transaction failed"){
            onAssetExecuted(txId,selectedIndex,1);
          }
          else{
            Get.back();
            showMessageError(this, dioResponse["message"], buildContext);
          }
        }
        else{
          Get.back();
          showMessageError(this, ex.message, buildContext);
        }
      }
      else{
        Get.back();
        showMessageError(this, ex.message, buildContext);
      }
    }
  }

  void onAssetExecuted(String txId, int selectedIndex, int from){
    Get.back();
    Get.back();
    myAssetCallback?.onMyAsset(txId,searchedDetailLists[selectedIndex][
    "asset_model"], searchedDetailLists[selectedIndex]["asset_detail"], from,
        disableList
    );
  }

  showMessageError(AddAssetController themeValue, String message, BuildContext buildContext){
    showModalBottomSheet(
        context: buildContext,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 235.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 235.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset("assets/icons/ic_alert_triangle.svg", height: 35, width: 35),
                                  SizedBox(width: 4),
                                  Text(
                                    "ERROR",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowSemiBold",
                                      decorationThickness: 1.5,
                                    ),
                                  )
                                ]),
                            SizedBox(height: 24),
                            Text(
                              message,
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 32),
                            GestureDetector(
                              child: CustomButton(45, double.maxFinite, BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.topRight,
                                    colors: [
                                      Color.fromRGBO(106, 48, 147, 1.0),
                                      Color.fromRGBO(160, 68, 255, 1.0)
                                    ]
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                              ), StringContent.close, TextStyle(
                                  color: Colors.white,
                                  fontFamily: "BarlowRegular",
                                  fontSize: 12,
                                  letterSpacing: 2
                              )),
                              onTap: (){
                                Get.back();
                              },
                            )
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }
}

