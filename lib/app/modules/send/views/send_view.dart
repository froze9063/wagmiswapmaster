import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/callback/scanner_callback.dart';
import 'package:waagmiswap/app/callback/select_asset_callback.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/scanner/views/scanner_view.dart';
import 'package:waagmiswap/app/modules/select_asset/views/select_asset_view.dart';
import 'package:waagmiswap/app/util/my_util.dart';

import '../controllers/send_controller.dart';

class SendView extends GetView<SendController> implements SelectAssetCallback, ScannerCallback{

  final SendController _sendController = Get.put(SendController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GetBuilder<SendController>(
        id: "theme",
        init: SendController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(height: 30),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: 3),
                      GestureDetector(
                        child: Container(
                          color: Colors.transparent,
                          width: 55,
                          height: 55,
                          child: Center(
                            child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                            "assets/icons/ic_black_back.png", width: 24,
                                height: 24, fit: BoxFit.contain),
                          ),
                        ),
                        onTap: (){
                          Get.back();
                        },
                      ),
                      Expanded(child: SizedBox(),flex: 1),
                      Text(
                        StringContent.send,
                        style: TextStyle(
                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                            fontSize: 14,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w600,
                            letterSpacing: 2
                        ),
                      ),
                      Expanded(child: SizedBox(),flex: 1),
                      Container(
                        width: 55,
                        height: 55,
                      ),
                      SizedBox(width: 3)
                    ],
                  ),

                  SizedBox(height: 18),

                  Expanded(child: sendWidget(themeValue),flex: 1)
                ],
              ),

              Visibility(child: GestureDetector(
                child: Container(
                  width: double.maxFinite,
                  height: double.maxFinite,
                  color: Colors.transparent,
                  child: Column(
                    children: [
                      SizedBox(height: 490),
                      Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.all(10),
                        margin: EdgeInsets.only(left: 24, right: 24, top: 24),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Title",
                              style: TextStyle(
                                  color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                  fontSize: 12,
                                  fontFamily: "BarlowNormal",
                                  decorationThickness: 1.5,
                                  fontWeight: FontWeight.w700,
                                  letterSpacing: 2
                              ),
                            ),
                            SizedBox(height: 8),
                            Text(
                              "Malesuada proin eros nibh lacus, tempor augue aliquam pellentesque vel ultrices faucibus ultricies in pellentesque ipsum odio sapien.",
                              style: TextStyle(
                                  color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                  fontSize: 12,
                                  fontFamily: "BarlowNormal",
                                  decorationThickness: 1.5,
                                  fontWeight: FontWeight.w400,
                                  letterSpacing: 1
                              ),
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                            color: themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            border: Border.all(
                              width: 1,
                              color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                            )
                        ),
                      )
                    ],
                  ),
                ),
                onTap: (){
                  _sendController.setFeeShowed(false);
                },
              ), visible: themeValue.feeShowed),

              Visibility(child: Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.transparent,
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.3),
                        borderRadius: BorderRadius.all(Radius.circular(8))
                    ),
                    child: Center(
                      child: CircularProgressIndicator(
                        color: Color.fromRGBO(106, 48, 147, 1.0),
                      ),
                    ),
                  ),
                ),
              ), visible: themeValue.isLoading)
            ],
          ),
        ),
      ),
    );
  }

  Widget sendWidget(SendController themeValue){
    return Column(
      children: [
        Row(
          children: [
            SizedBox(width: 24),
            Text(
              StringContent.asset,
              style: TextStyle(
                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                  fontSize: 12,
                  fontFamily: "BarlowRegular",
                  height: 1.5,
                  letterSpacing: 2
              ),
            )
          ],
        ),

        !themeValue.assetLoading ? GetBuilder<SendController>(
            id: "selected_asset",
            init: SendController(),
            builder: (assetValue) => GestureDetector(
              child: Container(
                margin: EdgeInsets.only(left: 24, right: 24, top: 16),
                width: double.maxFinite,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                          themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white
                        ]
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    border: Border.all(
                        color: themeValue.themeType == 0 ? Color.fromRGBO(164, 164, 164, 1.0) : ThemeConstant.greyBorder,
                        width: 1
                    )
                ),
                child: Row(
                  children: [
                    CircleAvatar(
                        child: assetValue.selectedAssetsModel != null ? assetValue.selectedAssetsModel!.assetId != 0 ? MyUtils.getCuratedIconPath(assetValue.assetsDetailModel!.unitName, themeValue.themeType).isEmpty ? SizedBox() : SvgPicture.asset(MyUtils.getCuratedIconPath(assetValue.assetsDetailModel!.unitName,
                            themeValue.themeType), height: 34, width: 34) : SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_dark_algo.svg" : "assets/icons/ic_light_algo.svg",
                            height: 34, width: 34) : SvgPicture.asset(MyUtils.getCuratedIconPath("ALGO", themeValue.themeType), height: 34, width: 34),
                        radius: 18,
                        backgroundColor: assetValue.selectedAssetsModel != null ? MyUtils.getCuratedIconPath(assetValue.assetsDetailModel!.unitName, themeValue.themeType).isEmpty ? ThemeConstant.secondTextGrayLight : Colors.transparent : Colors.transparent
                    ),
                    SizedBox(width: 10),
                    Expanded(child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(child: Text(
                              assetValue.selectedAssetsModel == null ? "Algo" : assetValue.assetsDetailModel!.unitName,
                              style: TextStyle(
                                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                  fontSize: 16,
                                  fontFamily: "BarlowNormal",
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 1
                              ),
                            ), flex: 1),
                          ],
                        ),
                      ],
                    )),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          assetValue.selectedAssetsModel == null ? "${_sendController.userModel != null ?
                          MyUtils.parseAlgoToMicro(_sendController.userModel.amount) : 0}" :
                          MyUtils.countAssetAmount(assetValue.selectedAssetsModel!.amount,
                              assetValue.assetsDetailModel!.decimals),
                          style: TextStyle(
                            color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                            fontSize: 16,
                            fontFamily: "BarlowMedium",
                          ),
                        ),

                        Visibility(child: Column(
                          children: [
                            SizedBox(height: 6),

                            Text(
                              assetValue.selectedAssetsModel == null ? "~ ${_sendController.userModel != null ?
                              MyUtils.formatPrice(MyUtils.parseAlgoPrice(double.parse(MyUtils.parseAlgoToMicro(
                                  _sendController.userModel.amount)), double.parse(_sendController.userModel.price.isNotEmpty ?
                              _sendController.userModel.price : "0"))) : 0} ${assetValue.currentCurrency}" : "~ ${MyUtils.formatPrice(MyUtils.countAssetPrice(assetValue.selectedAssetsModel!.price, double.parse(assetValue.selectedAssetsModel!.amount.toString()),
                                  assetValue.assetsDetailModel!.decimals,1))} ${assetValue.currentCurrency}",
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 12,
                                fontFamily: "BarlowNormal",
                              ),
                            )
                          ],
                        ), visible: assetValue.selectedAssetsModel != null ? assetValue.selectedAssetsModel != null && MyUtils.formatPrice(MyUtils.countAssetPrice(assetValue.selectedAssetsModel!.price, double.parse(assetValue.selectedAssetsModel!.amount.toString()),
                            assetValue.assetsDetailModel!.decimals,1)) != "0.00" : _sendController.userModel != null && MyUtils.parseAlgoPrice(_sendController.userModel.amount == 0 ? 0 : double.parse(MyUtils.parseAlgoToMicro(
                            _sendController.userModel.amount)), double.parse(_sendController.userModel.price.isNotEmpty ?
                        _sendController.userModel.price : "0")) != "0.0")
                      ],
                    )
                  ],
                ),
              ),
              onTap: (){
                Get.to(() => SelectAssetView(), arguments: {
                  "user_model" : _sendController.userModel,
                  "ledger_model" : _sendController.ledgerModel,
                  "select_asset_callback" : this
                });
              },
            )) : CircularProgressIndicator(color: Color.fromRGBO(106, 48, 147, 1.0),
        ),

        SizedBox(height: 16),

        Row(
          children: [
            SizedBox(width: 24),
            Expanded(child: Text(
              StringContent.amount,
              style: TextStyle(
                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                  fontSize: 12,
                  fontFamily: "BarlowRegular",
                  height: 1.5,
                  letterSpacing: 2
              ),
            ), flex: 1),
            SizedBox(width: 24)
          ],
        ),

        SizedBox(height: 8),

        GetBuilder<SendController>(
            id: "button_state",
            init: SendController(),
            builder: (value) => Column(
              children: [
                GestureDetector(
                  child: Container(
                    margin: EdgeInsets.only(left: 24, right: 24),
                    padding: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                    width: double.maxFinite,
                    decoration: BoxDecoration(
                        color: themeValue.themeType == 0 ? Colors.transparent : Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        border: Border.all(
                            width: 1,
                            color: value.isAmountFocus ? themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.lightActiveBorder
                                : ThemeConstant.secondTextGrayDark
                        )
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(child: TextField(
                              controller: value.amountTextController,
                              keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
                              ],
                              style: TextStyle(
                                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                  fontSize: 12,
                                  fontFamily: "BarlowRegular",
                                  letterSpacing: 1
                              ),
                              decoration: InputDecoration.collapsed(hintText: StringContent.amount,
                                  hintStyle: TextStyle(
                                    color: Color.fromRGBO(164,164,164,1),
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                  )
                              ),
                              onChanged: (text){
                                _sendController.isMaxPressed = false;
                                _sendController.setAssetPrice(text);
                                _sendController.setButtonState();
                              },
                              focusNode: value.amountFocus,
                            ), flex: 1),

                            Container(
                              width: 1,
                              height: 16,
                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                            ),

                            SizedBox(width: 8),

                            GestureDetector(
                              child: Text(
                                StringContent.max,
                                style: TextStyle(
                                  color: value.isMaxAbleToClicked ? themeValue.themeType == 0 ? ThemeConstant.white :
                                  ThemeConstant.blackNavy : Color.fromRGBO(164,164,164,1),
                                  fontSize: 14,
                                  fontFamily: "BarlowMedium",
                                ),
                              ),
                              onTap: (){
                                if(value.isMaxAbleToClicked){
                                  _sendController.isMaxPressed = true;
                                  _sendController.setMax(false, true);
                                }
                              },
                            )
                          ],
                        ),

                        Container(
                            margin: EdgeInsets.only(top: 8, bottom: 8),
                            width: double.maxFinite,
                            height: 1,
                            color: Color.fromRGBO(164,164,164,1)
                        ),

                        Row(
                          children: [
                            Expanded(child: Text(
                              "${MyUtils.formatPrice(value.assetPrice)} ${value.currentCurrency}",
                              style: TextStyle(
                                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                  fontSize: 12,
                                  fontFamily: "BarlowRegular",
                                  height: 1.5,
                                  letterSpacing: 1
                              ),
                            ), flex: 1),
                            SizedBox(width: 24),
                          ],
                        )
                      ],
                    ),
                  ),
                  onTap: (){
                    value.amountFocus.requestFocus();
                  },
                ),

                Visibility(child: Column(
                  children: [
                    SizedBox(height: 4),
                    Row(
                      children: [
                        SizedBox(width: 24),
                        Expanded(child: Text(
                          value.strError,
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 12,
                              fontFamily: "BarlowRegular",
                              height: 1.5
                          ),
                        ), flex: 1),
                        SizedBox(width: 24)
                      ],
                    )
                  ],
                ), visible: value.isUnSufficientAmount)
              ],
            )),

        SizedBox(height: 16),

        Row(
          children: [
            SizedBox(width: 24),
            Expanded(child: Text(
              StringContent.recipient,
              style: TextStyle(
                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                  fontSize: 12,
                  fontFamily: "BarlowRegular",
                  height: 1.5,
                  letterSpacing: 2
              ),
            ), flex: 1),
            SizedBox(width: 24)
          ],
        ),

        SizedBox(height: 8),

        GetBuilder<SendController>(
          id: "recipient_focus",
          init: SendController(),
          builder: (value) => GestureDetector(
            child: Container(
              margin: EdgeInsets.only(left: 24, right: 24),
              padding: EdgeInsets.only(left: 16, right: 16),
              width: double.maxFinite,
              height: 35,
              decoration: BoxDecoration(
                  color: themeValue.themeType == 0 ? Colors.transparent : Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(
                      width: 1,
                      color: value.isRecipientFocus ? themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.lightActiveBorder
                          : ThemeConstant.secondTextGrayDark
                  )
              ),
              child: Center(
                child: Row(
                  children: [
                    Expanded(child: TextField(
                      controller: _sendController.recipientTextController ,
                      style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 12,
                        fontFamily: "BarlowRegular",
                      ),
                      decoration: InputDecoration.collapsed(hintText: StringContent.recipient_address,
                          hintStyle: TextStyle(
                            color: Color.fromRGBO(164,164,164,1),
                            fontSize: 12,
                            fontFamily: "BarlowRegular",
                          )
                      ),
                      onChanged: (text){
                        _sendController.checkAlgorandAddress();
                        _sendController.setButtonState();
                      },
                      focusNode: _sendController.recipientFocus,
                    ), flex: 1),

                    SizedBox(width: 8),

                    GestureDetector(
                      child: SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_qr.svg" : "assets/icons/ic_black_qr.svg"),
                      onTap: (){
                        Get.to(()=> ScannerView(), arguments: {
                          "scanner_callback" : this
                        });
                      },
                    ),

                    SizedBox(width: 8),

                    Container(
                      width: 1,
                      height: 16,
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                    ),

                    SizedBox(width: 8),

                    GestureDetector(
                      child: Text(
                        StringContent.paste,
                        style: TextStyle(
                          color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          fontSize: 14,
                          fontFamily: "BarlowMedium",
                        ),
                      ),
                      onTap: (){
                        _sendController.paste();
                      },
                    )
                  ],
                ),
              ),
            ),
            onTap: (){
              value.recipientFocus.requestFocus();
            },
          ),
        ),

        themeValue.invalidAddress ? Row(
          children: [
            SizedBox(width: 24),
            Expanded(child: Text(
              StringContent.invalid_address,
              style: TextStyle(
                  color: Colors.red,
                  fontSize: 12,
                  fontFamily: "BarlowRegular",
                  height: 1.5
              ),
            ), flex: 1),
            SizedBox(width: 24)
          ],
        ) : SizedBox(),

        SizedBox(height: 16),

        Row(
          children: [
            SizedBox(width: 24),
            Expanded(child: Text(
              StringContent.note,
              style: TextStyle(
                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                  fontSize: 12,
                  fontFamily: "BarlowRegular",
                  height: 1.5,
                  letterSpacing: 2
              ),
            ), flex: 1),
            SizedBox(width: 24)
          ],
        ),

        SizedBox(height: 8),

        GetBuilder<SendController>(
            id: "note_focus",
            init: SendController(),
            builder: (value) => GestureDetector(
              child: Container(
                margin: EdgeInsets.only(left: 24, right: 24),
                padding: EdgeInsets.only(left: 16, right: 16),
                width: double.maxFinite,
                height: 35,
                decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? Colors.transparent : Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    border: Border.all(
                        width: 1,
                        color: value.isNoteFocus ? themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.lightActiveBorder
                            : ThemeConstant.secondTextGrayDark
                    )
                ),
                child: Center(
                  child: TextField(
                    controller: _sendController.noteTextController,
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                    ),
                    decoration: InputDecoration.collapsed(hintText: StringContent.note,
                        hintStyle: TextStyle(
                          color: Color.fromRGBO(164,164,164,1),
                          fontSize: 12,
                          fontFamily: "BarlowRegular",
                        )
                    ),
                    onChanged: (text){
                      _sendController.setButtonState();
                    },
                    focusNode: _sendController.noteFocus,
                  ),
                ),
              ),
              onTap: (){
                value.noteFocus.requestFocus();
              },
            )),

        SizedBox(height: 24),

        Row(
          children: [
            SizedBox(width: 24),
            Text(
              StringContent.fee,
              style: TextStyle(
                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                  fontSize: 12,
                  fontFamily: "BarlowRegular",
                  height: 1.5,
                  letterSpacing: 2
              ),
            ),
            SizedBox(width: 4),
            GestureDetector(
              child: SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_info.svg" : "assets/icons/ic_black_info.svg", height: 16, width: 16),
              onTap: (){
                _sendController.setFeeShowed(true);
              },
            ),
            Expanded(child: SizedBox(), flex: 1),
            Text(
              "0.001 ALGO",
              style: TextStyle(
                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                  fontSize: 12,
                  fontFamily: "BarlowRegular",
                  height: 1.5,
                  letterSpacing: 1
              ),
            ),
            SizedBox(width: 24),
          ],
        ),

        SizedBox(height: 6),

        Row(
          children: [
            SizedBox(width: 24),
            Text(
              StringContent.total_amount,
              style: TextStyle(
                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                  fontSize: 12,
                  fontFamily: "BarlowRegular",
                  height: 1.5,
                  letterSpacing: 2
              ),
            ),
            Expanded(child: SizedBox(), flex: 1),
            GetBuilder<SendController>(
                id: "total_price",
                init: SendController(),
                builder: (priceValue) => Text(
                  "~ ${MyUtils.formatPrice(priceValue.assetPrice)} ${priceValue.currentCurrency}",
                  style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                      height: 1.5,
                      letterSpacing: 1
                  ),
                )),
            SizedBox(width: 24),
          ],
        ),

        Expanded(child: SizedBox(), flex: 1),

        GetBuilder<SendController>(
            id: "button_state",
            init: SendController(),
            builder: (value) => Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 24, right: 24),
              height: 37,
              child: Stack(
                children: [
                  CustomButton(37, double.maxFinite, BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.topRight,
                        colors: [
                          value.isContinue ? Color.fromRGBO(106, 48, 147, 1.0) : Color.fromRGBO(89, 89, 89, 1.0),
                          value.isContinue ? Color.fromRGBO(160, 68, 255, 1.0) : Color.fromRGBO(89, 89, 89, 1.0)
                        ]
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ), StringContent.confirm, TextStyle(
                      color: value.isContinue ? Colors.white : Color.fromRGBO(151, 151, 151, 1.0),
                      fontFamily: "BarlowRegular",
                      fontSize: 12,
                      letterSpacing: 2,
                      fontWeight: FontWeight.w600
                  )),

                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      splashColor: Colors.transparent,
                      onTap: (){
                        if(value.isContinue){
                          value.sendConfirm();
                        }
                      },
                    ),
                  )
                ],
              ),
            )
        ),

        SizedBox(height: 16),

        GestureDetector(
          child: Container(
            height: 37,
            child: Text(
              StringContent.cancel,
              style: TextStyle(
                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                  fontSize: 14,
                  fontFamily: "BarlowNormal",
                  decorationThickness: 1.5,
                  fontWeight: FontWeight.w500
              ),
            ),
          ),
          onTap: (){
            Get.back();
          },
        ),

        SizedBox(height: StringContent.bottomMargin)
      ],
    );
  }

  @override
  void onAssetSelected(AssetsModel? selectedAssetsModel, AssetsDetailModel? assetsDetailModel,
      UserModel? holdAsset) {
    _sendController.reset();
    _sendController.setSelectedAsset(selectedAssetsModel,assetsDetailModel);
  }

  @override
  void onScanned(String data) {
    _sendController.scanned(data);
  }
}
