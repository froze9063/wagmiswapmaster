import 'dart:io';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/algorand/algorand.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/send_confirm/views/send_confirm_view.dart';
import 'package:waagmiswap/app/repository/api_repository.dart';
import 'package:waagmiswap/app/util/my_util.dart';
import 'package:dio/dio.dart' as dioResponse;

class SendController extends GetxController {

  final count = 0.obs;

  late APIRepository apiRepository;
  late DatabaseDAO databaseDAO;

  bool isContinue = false;
  bool isMaxAbleToClicked = false;
  bool isUnSufficientAmount = false;

  late Algorand algorand;
  late AlgorandRepository algorandRepository;

  late TextEditingController amountTextController;
  late TextEditingController recipientTextController;
  late TextEditingController noteTextController;

  late FocusNode amountFocus;
  late FocusNode recipientFocus;
  late FocusNode noteFocus;

  bool isAmountFocus = false;
  bool isRecipientFocus = false;
  bool isNoteFocus = false;

  int themeType = 0;

  double assetAmount = 0;
  double assetPrice = 0;
  double totalPrice = 0;

  late UserModel userModel;
  late LedgerModel ledgerModel;

  AssetsModel? selectedAssetsModel;
  AssetsDetailModel? assetsDetailModel;

  String currentCurrency = "USD";
  String currentCurrencySymbol = "\$";

  bool feeShowed = false;
  int minBalance = 0;

  String strError = StringContent.insufficient_amount;
  bool minBalanceLoaded = false;
  bool assetLoading = true;

  bool isMaxPressed = false;
  int assetId = 0;

  bool invalidAddress = false;
  bool isLoading = false;

  int userPosition = 0;

  @override
  void onInit() {
    databaseDAO = DatabaseDAO();

    if(Platform.isAndroid){
      FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
    }

    amountTextController = TextEditingController();
    recipientTextController = TextEditingController();
    noteTextController = TextEditingController();

    amountFocus = FocusNode();
    recipientFocus = FocusNode();
    noteFocus = FocusNode();

    amountFocus.addListener(onAmountFocus);
    recipientFocus.addListener(onRecipientFocus);
    noteFocus.addListener(onNoteFocus);

    getArguments();
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void initAlgorand(){
    apiRepository = APIRepository(this);
    algorandRepository = AlgorandRepository();
    algorand = algorandRepository.getAlgorandConfig(ledgerModel.algodUrl,
        ledgerModel.indexerUrl);
    getAccountByAddress();
  }

  Future<void> getAccountByAddress() async {
    dioResponse.Response response = await apiRepository.getPagesResponse(userModel.address,
        ledgerModel.algodUrl, currentCurrency);
    Map responseMap = response.data;
    int extraPages = responseMap["apps-total-extra-pages"] ?? 0;
    int assetHeld = responseMap["assets"] != null ? (responseMap["assets"] as List<dynamic>).length : 0;
    int uint = responseMap["apps-total-schema"]["num-uint"] ?? 0;
    int byteSlice = responseMap["apps-total-schema"]["num-byte-slice"] ?? 0;
    int baseAlgo = 100000;
    int assetMinimum = assetHeld * baseAlgo;
    int appsMinimum = baseAlgo + (28500 * uint) + (50000 * byteSlice);

    if(extraPages == 0){
      minBalance = baseAlgo + assetMinimum + appsMinimum;
    }
    else{
      minBalance = (baseAlgo * (1 + extraPages)) + assetMinimum + appsMinimum;
    }

    minBalanceLoaded = true;
  }

  void setFeeShowed(bool show){
    feeShowed = show;
    update(["theme"]);
  }

  void getArguments(){
    userModel = Get.arguments["user_model"];
    ledgerModel = Get.arguments["ledger_model"];
    assetId = Get.arguments["asset_id"] ?? 0;
    userPosition = Get.arguments["user_position"] ?? 0;

    initAlgorand();
    setMax(true,false);
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      currentCurrency = prefs.getString("currency_name") ?? "USD";
      currentCurrencySymbol = prefs.getString("symbol") ?? "\$";
      if(assetId == 0){
        getAccountBalance();
      }
      else{
        getAssetBalance();
      }

      update(["theme"]);
    });
  }

  void setButtonState(){
    isContinue =
        amountTextController.text.toString().isNotEmpty &&
            !amountTextController.text.toString().contains("-") &&
            recipientTextController.text.toString().isNotEmpty &&
            !isUnSufficientAmount && !invalidAddress;
    update(["button_state"]);
  }

  void onAmountFocus(){
    checkAlgorandAddress();
    isAmountFocus = true;
    isRecipientFocus = false;
    isNoteFocus = false;
    update(["button_state","recipient_focus","note_focus"]);
  }

  void onRecipientFocus(){
    checkAlgorandAddress();
    isRecipientFocus = true;
    isAmountFocus = false;
    isNoteFocus = false;
    update(["recipient_focus","button_state","note_focus"]);
  }

  void onNoteFocus(){
    checkAlgorandAddress();
    isNoteFocus = true;
    isRecipientFocus = false;
    isAmountFocus = false;
    update(["note_focus","recipient_focus","button_state"]);
  }

  void paste(){
    FlutterClipboard.paste().then((value) {
      recipientTextController.text = value;
      checkAlgorandAddress();
      setButtonState();
      update(["recipient_focus"]);
    });
  }

  void scanned(String data){
    recipientTextController.text = data;
    checkAlgorandAddress();
    setButtonState();
    update(["recipient_focus"]);
  }

  void setSelectedAsset(AssetsModel? selectedAssetsModel, AssetsDetailModel? assetsDetailModel){
    this.selectedAssetsModel = selectedAssetsModel;
    this.assetsDetailModel = assetsDetailModel;
    setMax(true,false);
    update(["selected_asset"]);

    if(selectedAssetsModel == null && assetsDetailModel == null){
      getAccountBalance();
    }
  }

  Future<void> setMax(bool isFirst, bool showError) async {
    if(selectedAssetsModel != null){
      isMaxAbleToClicked = selectedAssetsModel!.amount != 0;
      if(!isFirst){
        String totalAsset = MyUtils.countAssetAmount(selectedAssetsModel!.amount,
            assetsDetailModel!.decimals);
        amountTextController.text = totalAsset;
        setAssetPrice(totalAsset);
      }
    }
    else{
      if(!minBalanceLoaded){
        dioResponse.Response response = await apiRepository.getPagesResponse(userModel.address,
            ledgerModel.algodUrl, currentCurrency);
        Map responseMap = response.data;
        int extraPages = responseMap["apps-total-extra-pages"] ?? 0;
        int assetHeld = responseMap["assets"] != null ? (responseMap["assets"] as List<dynamic>).length : 0;
        int uint = responseMap["apps-total-schema"]["num-uint"] ?? 0;
        int byteSlice = responseMap["apps-total-schema"]["num-byte-slice"] ?? 0;
        int baseAlgo = 100000;
        int assetMinimum = assetHeld * baseAlgo;
        int appsMinimum = baseAlgo + (28500 * uint) + (50000 * byteSlice);

        if(extraPages == 0){
          minBalance = baseAlgo + assetMinimum + appsMinimum;
        }
        else{
          minBalance = (baseAlgo * (1 + extraPages)) + assetMinimum + appsMinimum;
        }
        minBalanceLoaded = true;
      }

      int totalMinBalance = userModel.amount - minBalance;

      if(totalMinBalance < minBalance){
        if(showError){
          isUnSufficientAmount = true;
          strError = StringContent.insufficient_minimum_amount;
          update(["button_state","total_price"]);
        }
        return;
      }

      isMaxAbleToClicked = userModel.amount != 0;

      if(!isFirst){
        isUnSufficientAmount = false;
        String microAlgo = MyUtils.parseAlgoToMicro(totalMinBalance).toString();
        amountTextController.text = microAlgo;
        setAssetPrice(microAlgo);
      }
    }

    setButtonState();
    update(["button_state","total_price"]);
  }

  void setAssetPrice(String strAmount){
    if(strAmount.isEmpty){
      strAmount = "0";
    }

    double amount = 0;
    double algorandPrice = MyUtils.parseAlgoPrice(double.parse(MyUtils.parseAlgoToMicro(
        userModel.amount)), double.parse(userModel.price));

    if(selectedAssetsModel == null){
      if(strAmount.isNotEmpty){
        amount = double.parse(strAmount);
      }
    }

    if(selectedAssetsModel != null){
      double defaultAssetPrice = MyUtils.countAssetPrice(selectedAssetsModel!.price,
          double.parse(selectedAssetsModel!.amount.toString()), assetsDetailModel!.decimals,1);

      assetPrice = MyUtils.countAssetPrice(selectedAssetsModel!.price,
          double.parse(strAmount.replaceAll(",", "")), assetsDetailModel!.decimals,2);

      double newAssetPrice = assetPrice;

      isUnSufficientAmount = double.parse(newAssetPrice.toStringAsFixed(2)) >
          double.parse(defaultAssetPrice.toStringAsFixed(2));

      totalPrice = assetPrice;

      if(isUnSufficientAmount){
        strError = StringContent.insufficient_amount;
      }
    }
    else{
      assetPrice = MyUtils.parseAlgoPrice(amount, double.parse(userModel.price));
      isUnSufficientAmount = assetPrice > algorandPrice;
      totalPrice = assetPrice;
      if(isUnSufficientAmount){
        strError = StringContent.insufficient_amount;
      }
      else{
        if(amount != 0.0){
          if(double.parse((double.parse(MyUtils.parseAlgoToMicro(userModel.amount)) - amount
          ).toStringAsFixed(2)) < double.parse(MyUtils.parseAlgoToMicro(minBalance))){
            strError = StringContent.insufficient_minimum_amount;
            isUnSufficientAmount = true;
          }
          else{
            isUnSufficientAmount = false;
          }
        }
      }
    }

    setButtonState();
    update(["button_state","total_price"]);
  }

  void reset(){
    amountTextController.text = "";
    assetPrice = 0;
    totalPrice = 0;
    update(["button_state","total_price"]);
  }

  Future<void> checkAlgorandAddress() async {
    String recipient = recipientTextController.text.toString().trim();
    if(recipient.isNotEmpty){
      try {
        Address.fromAlgorandAddress(address: recipient);
        invalidAddress = false;
        update(["theme"]);
      }catch(e){
        invalidAddress = true;
        update(["theme"]);
      }
    }
    else{
      invalidAddress = false;
      update(["theme"]);
    }
  }

  Future<void> sendConfirm() async {
    isLoading = true;
    update(["theme"]);

    String assetName = assetsDetailModel != null ? assetsDetailModel!.name : "Algorand";
    String assetAmount = amountTextController.text.toString().trim();
    String assetUnitName = assetsDetailModel != null ? assetsDetailModel!.unitName : "Algo";
    String recipient = recipientTextController.text.toString().trim();
    String price = "${MyUtils.formatPrice(totalPrice)} $currentCurrency";
    String note = noteTextController.text.toString().trim();

    try{
      Address.fromAlgorandAddress(address: recipient);

      invalidAddress = false;
      isLoading = false;
      update(["theme"]);

      String strAssetAmount = "";

      if(assetAmount.length == 2){
        if(assetAmount.split("")[1] == "."){
          strAssetAmount = assetAmount.replaceAll(".", "");
        }
        else{
          strAssetAmount = assetAmount;
        }
      }
      else{
        strAssetAmount = assetAmount;
      }

      Map sendMap = {};
      sendMap["asset_id"] = assetsDetailModel != null ? assetsDetailModel!.index : 0;
      sendMap["asset_name"] = assetName;
      sendMap["asset_amount"] = strAssetAmount;
      sendMap["asset_unit_name"] = assetUnitName;
      sendMap["asset_price"] = selectedAssetsModel != null ? selectedAssetsModel!.price : userModel.price;
      sendMap["price"] = price;
      sendMap["recipient"] = recipient;
      sendMap["note"] = note;
      sendMap["decimal"] = selectedAssetsModel != null ? assetsDetailModel!.decimals : 0;
      sendMap["is_max"] = isMaxPressed;
      sendMap["max_amount"] = selectedAssetsModel != null ? selectedAssetsModel!.amount : 0;

      Get.to(() => SendConfirmView(), arguments: {"send_map" : sendMap, "ledger_model" : ledgerModel,
        "user_model" : userModel});
    }
    catch(e){
      invalidAddress = true;
      isLoading = false;
      update(["theme"]);
    }
  }

  Future<void> getAccountBalance() async {
    assetLoading = true;
    update(["theme"]);
    dioResponse.Response userResponse = await apiRepository.getPagesResponse(userModel.address,
        ledgerModel.algodUrl, currentCurrency);
    int amount = userResponse.data["amount"] ?? 0;
    String price = userResponse.data["price"] ?? "";
    userModel.amount = amount;
    userModel.price = price;
    assetLoading = false;
    update(["theme"]);
  }

  Future<void> getAssetBalance() async {
    assetLoading = true;
    dioResponse.Response userResponse = await apiRepository.getPagesResponse(userModel.address,
        ledgerModel.algodUrl, currentCurrency);
    Map dataMap = userResponse.data;

    if(dataMap["assets"] != null){
      List<dynamic> responseAssetList = dataMap["assets"];
      for(int i = 0 ; i < responseAssetList.length; i++) {
        Map<String, dynamic> responseMap = responseAssetList[i];
        responseMap["ledger-position"] = ledgerModel.ledgerId;
        responseMap["user-address"] = userModel.address;

        AssetsModel assetsModel = AssetsModel.fromMap(responseMap);
        assetsModel.currency = currentCurrency;

        if(assetsModel.assetId == assetId){
          selectedAssetsModel = assetsModel;
          break;
        }
      }
    }

    dioResponse.Response assetDetailResponse = await apiRepository.getAssetDetailResponse(ledgerModel.baseUrl,
        currentCurrency, assetId);
    Map<String,dynamic> assetDetailMap = assetDetailResponse.data;
    assetsDetailModel = AssetsDetailModel.fromMap(assetDetailMap,ledgerModel.ledgerId);

    assetLoading = false;
    update(["theme"]);
  }
}
