import 'package:get/get.dart';

import '../controllers/unlink_confirmation_controller.dart';

class UnlinkConfirmationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UnlinkConfirmationController>(
      () => UnlinkConfirmationController(),
    );
  }
}
