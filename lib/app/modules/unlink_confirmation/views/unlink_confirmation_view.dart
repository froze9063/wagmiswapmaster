import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/util/my_util.dart';

import '../controllers/unlink_confirmation_controller.dart';

class UnlinkConfirmationView extends GetView<UnlinkConfirmationController> {

  UnlinkConfirmationController unlinkConfirmationController =
  Get.put(UnlinkConfirmationController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<UnlinkConfirmationController>(
        id: "theme",
        init: UnlinkConfirmationController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Column(
            children: [
              SizedBox(height: 30),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 3),
                  GestureDetector(
                    child: Container(
                      color: Colors.transparent,
                      width: 55,
                      height: 55,
                      child: Center(
                        child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                        "assets/icons/ic_black_back.png", width: 24,
                            height: 24, fit: BoxFit.contain),
                      ),
                    ),
                    onTap: (){
                      Get.back();
                    },
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Text(
                    StringContent.unlink_confirmation,
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 16,
                        fontFamily: "BarlowMedium",
                        letterSpacing: 1
                    ),
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Container(
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(width: 3)
                ],
              ),
              SizedBox(height: 18),
              Row(
                children: [
                  SizedBox(width: 24),
                  Expanded(child: Text(
                    "Are you sure want to unlink your account? \n\nThis will remove your account ${MyUtils.parseAddress(
                        unlinkConfirmationController.userModel.address)} "
                        "from WAGMIswap wallet.\n\nIMPORTANT! In case you need to restore this account in the future, "
                        "make sure you backed up the mnemonic.",
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 12,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w400,
                        letterSpacing: 1,
                        height: 1.5
                    ),
                  )),
                  SizedBox(width: 24),
                ],
              ),
              Expanded(child: SizedBox(), flex: 1),
              Container(
                width: double.maxFinite,
                margin: EdgeInsets.only(left: 24, right: 24),
                height: 37,
                child: Stack(
                  children: [
                    CustomButton(37, double.maxFinite, BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.topRight,
                          colors: [
                            Color.fromRGBO(106, 48, 147, 1.0),
                            Color.fromRGBO(160, 68, 255, 1.0)
                          ]
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ), StringContent.unlink, TextStyle(
                        color: Colors.white,
                        fontFamily: "BarlowRegular",
                        fontSize: 12,
                        letterSpacing: 2
                    )),

                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        onTap: (){
                          unlinkConfirmationController.unlink();
                        },
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: StringContent.bottomMargin)
            ],
          ),
        ),
      ),
    );
  }
}
