import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/select_option/views/select_option_view.dart';

class UnlinkConfirmationController extends GetxController {

  final count = 0.obs;
  int themeType = 0;

  DatabaseDAO databaseDAO = DatabaseDAO();
  late UserModel userModel;

  @override
  void onInit() {
    userModel = Get.arguments["user_model"];
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  void unlink(){
    databaseDAO.delete(userModel).then((value) {
      if(userModel.accountType == "standard"){
        checkAccount();
      }
      else{
        deleteManaged();
      }
    });
  }

  void deleteManaged(){
    databaseDAO.deleteSubAccounts(userModel.address).then((value) {
      checkAccount();
    });
  }

  void checkAccount(){
    bool isStandardNotEmpty = false;
    databaseDAO.getAllSortedNoLimit("account_type", "standard").then((value) {
      isStandardNotEmpty = value.isNotEmpty;
      SharedPreferences.getInstance().then((prefs){
        prefs.setInt("user_position", 0);
        if(isStandardNotEmpty){
          Get.offNamedUntil('home', (route) => false);
        }
        else{
          SharedPreferences.getInstance().then((value) {
            value.remove("passcode");
            value.remove("face_id");
            value.remove("theme");
            value.remove("notification");
            value.remove("notification");
            value.remove("currency_index");
            value.remove("currency_name");
            value.remove("symbol");
            value.remove("user_position");
            value.clear();
            Get.offAll(() => SelectOptionView(), arguments: {"is_from_unlink" : true});
          });
        }
      });
    });
  }
}
