import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/enter_passcode/views/enter_passcode_view.dart';

import '../controllers/select_option_controller.dart';

class SelectOptionView extends GetView<SelectOptionController> {

  SelectOptionController selectOptionController = Get.put(SelectOptionController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(25, 29, 52, 1.0),
                  Color.fromRGBO(16, 15, 31, 1.0)
                ]
            )
        ),
        child: contentChild(),
      ),
    );
  }

  Widget contentChild(){
    return Column(
      children: [
        SizedBox(height: 40),

        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset("assets/images/img_hori_messe.png", width: 182,
                height: 23, fit: BoxFit.contain)
          ],
        ),

        SizedBox(height: 24),

        Text(
          StringContent.welcome_messge_wallet,
          style: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontFamily: "BarlowNormal",
              letterSpacing: 2,
              fontWeight: FontWeight.w600
          ),
        ),

        SizedBox(height: 6),

        Row(
          children: [
            SizedBox(width: 24),
            Expanded(child: Text(
              StringContent.welcome_description,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1.0),
                  fontSize: 12,
                  fontFamily: "BarlowNormal",
                  letterSpacing: 1,
                  fontWeight: FontWeight.w400,
                  height: 1.5
              ),
            ), flex: 1),
            SizedBox(width: 24)
          ],
        ),

        SizedBox(height: 50),

        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset("assets/images/img_select_option.png", width: 300,
                height: 244.77, fit: BoxFit.contain)
          ],
        ),

        Expanded(child: SizedBox(),flex: 1),

        Row(
          children: [
            SizedBox(width: 24),
            Text(
              StringContent.select_an_option,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontFamily: "BarlowNormal",
                  fontWeight: FontWeight.w600,
                  letterSpacing: 2
              ),
            )
          ],
        ),

        SizedBox(height: 16),

        Container(
          width: double.maxFinite,
          margin: EdgeInsets.only(left: 24, right: 24),
          height: 37,
          child: Stack(
            children: [
              CustomButton(37, double.maxFinite, BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.topRight,
                    colors: [
                      Color.fromRGBO(106, 48, 147, 1.0),
                      Color.fromRGBO(160, 68, 255, 1.0)
                    ]
                ),
                borderRadius: BorderRadius.all(Radius.circular(8)),
              ), StringContent.create_new_wallet, TextStyle(
                  color: Colors.white,
                  fontFamily: "BarlowRegular",
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 2
              )),

              Material(
                color: Colors.transparent,
                child: InkWell(
                  splashColor: Colors.transparent,
                  onTap: (){
                    if(selectOptionController.isFromUnlink){
                      selectOptionController.initAlgorand();
                    }
                    else{
                      Get.to(() => EnterPasscodeView(),
                          arguments: {
                            "public_address" : selectOptionController.publicAddress,
                            "mnemonic_list" : selectOptionController.mnemonicLists,
                            "account" : selectOptionController.account
                          }
                      );
                    }
                  },
                ),
              )
            ],
          ),
        ),

        SizedBox(height: 16),

        Container(
          width: double.maxFinite,
          margin: EdgeInsets.only(left: 24, right: 24),
          height: 37,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              splashColor: Colors.transparent,
              child: CustomButton(37, double.maxFinite, BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(
                      width: 1,
                      color: Color.fromRGBO(184, 115, 255, 1.0)
                  )
              ), StringContent.import_account, TextStyle(
                  color: Color.fromRGBO(184, 115, 255, 1.0),
                  fontFamily: "BarlowRegular",
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 2
              )),
              onTap: (){
                Get.to(() => EnterPasscodeView(), arguments: {"from" : 1});
              },
            ),
          ),
        ),

        SizedBox(height: StringContent.bottomMargin)
      ],
    );
  }
}
