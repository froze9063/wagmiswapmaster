import 'package:algorand_dart/algorand_dart.dart';
import 'package:get/get.dart';
import 'package:waagmiswap/app/algorand/algorand.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/modules/enter_passcode/views/enter_passcode_view.dart';

class SelectOptionController extends GetxController {

  final count = 0.obs;

  late List<String> mnemonicLists;
  late Account account;

  String publicAddress = "";
  bool isFromUnlink = false;

  @override
  void onInit() {
    initArguments();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void initArguments(){
    publicAddress = Get.arguments["public_address"] ?? "";
    mnemonicLists = Get.arguments["mnemonic_list"];
    account = Get.arguments["account"];
    isFromUnlink = Get.arguments["is_from_unlink"] ?? false;
  }

  void initAlgorand() {
    AlgorandRepository algorandRepository = AlgorandRepository();
    var algorand = algorandRepository.getAlgorandConfig("${MyConstant.USED_URL}/algod",
        "${MyConstant.USED_URL}/idx");
    algorand.createAccount().then((value) {
      final publicAddress = value.publicAddress;
      value.seedPhrase.then((wordValue) {
        Get.to(() => EnterPasscodeView(),
            arguments: {
              "public_address" : publicAddress,
              "mnemonic_list" : wordValue,
              "account" : value
            }
        );
      });
    });
  }
}
