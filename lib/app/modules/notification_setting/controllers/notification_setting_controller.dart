import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';

class NotificationSettingController extends GetxController {

  final count = 0.obs;

  bool switched = true;
  int themeType = 0;

  int tabPosition = 1;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late LedgerModel selectedLedger;

  @override
  void onInit() {
    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void setSwitch(){
    SharedPreferences.getInstance().then((prefs){
      this.switched = !this.switched;
      prefs.setBool("notification", switched);
      update(["notification"]);
    });
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      this.switched = prefs.getBool("notification") ?? true;
      update(["theme","notification"]);
    });
  }
}
