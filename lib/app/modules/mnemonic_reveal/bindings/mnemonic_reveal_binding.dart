import 'package:get/get.dart';

import '../controllers/mnemonic_reveal_controller.dart';

class MnemonicRevealBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MnemonicRevealController>(
      () => MnemonicRevealController(),
    );
  }
}
