import 'dart:io';

import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenshot_callback/flutter_screenshot_callback.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';

class MnemonicRevealController extends GetxController implements IScreenshotCallback{
  //TODO: Implement MnemonicRevealController

  final count = 0.obs;
  int from = 0;

  bool isRevealed = false;
  bool isCopyTapped = false;
  bool isCopied = false;

  late ScreenshotCallback _screenshotCallback;

  late UserModel userModel;
  late List<String> mnemonicLists;

  BuildContext? buildContext;
  int themeType = 0;

  int tabPosition = 1;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late LedgerModel selectedLedger;

  @override
  void onInit() {
    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];
    loadUser();
    mnemonicLists = [];
    if(Get.arguments != null){
      if(Get.arguments["from"] != null){
        from = Get.arguments["from"];
      }

      userModel = Get.arguments["user_model"];
      mnemonicLists = userModel.mnemonic.split(" ");
    }

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void initCallback(){
    _screenshotCallback = ScreenshotCallback();
    _screenshotCallback.setInterfaceScreenshotCallback(this);
  }

  showBottomSheet(){
    if(buildContext != null){
      if(isRevealed){
        showModalBottomSheet(
            context: buildContext!,
            backgroundColor: Colors.transparent,
            isScrollControlled: true,
            isDismissible: false,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
            ),
            builder: (BuildContext context) {
              return Container(
                  width: double.maxFinite,
                  height: 340.5,
                  child: Stack(
                    children: [
                      Container(
                        width: double.maxFinite,
                        height: 340.5,
                        decoration: BoxDecoration(
                          color: ThemeConstant.blueBorder,
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(24),
                        ),
                      ),
                      Column(
                        children: [
                          Expanded(child: SizedBox(), flex: 1),
                          Container(
                            width: double.maxFinite,
                            height: 340,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    ThemeConstant.darkThemeBackground1,
                                    ThemeConstant.darkThemeBackground1
                                  ]
                              ),
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                            ),
                            child: Column(
                              children: [
                                SizedBox(height: 16),
                                Text(
                                  StringContent.screenshot_detected,
                                  style: TextStyle(
                                    color: ThemeConstant.white,
                                    fontSize: 18,
                                    fontFamily: "BarlowMedium",
                                  ),
                                ),
                                SizedBox(height: 24),
                                SvgPicture.asset("assets/icons/ic_alert_triangle.svg", height: 75, width: 75),
                                SizedBox(height: 24),
                                Row(
                                  children: [
                                    SizedBox(width: 24),
                                    Expanded(child: Text(
                                      StringContent.sreenshot_detail,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: ThemeConstant.white,
                                          fontSize: 12,
                                          fontFamily: "BarlowNormal",
                                          height: 1.5
                                      ),
                                    ), flex: 1),
                                    SizedBox(width: 24)
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 24, right: 24, top: 40),
                                  width: double.maxFinite,
                                  height: 45,
                                  child: Stack(
                                    children: [
                                      CustomButton(45, double.maxFinite, BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.topRight,
                                            colors: [
                                              Color.fromRGBO(106, 48, 147, 1.0),
                                              Color.fromRGBO(160, 68, 255, 1.0)
                                            ]
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                      ),  StringContent.accept, TextStyle(
                                          color: Colors.white,
                                          fontFamily: "BarlowRegular",
                                          fontSize: 12,
                                          letterSpacing: 2
                                      )),

                                      Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          splashColor: Colors.transparent,
                                          onTap: (){
                                            Get.back();
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  )
              );
            });
      }
    }
  }

  void copied(){
    isCopyTapped = true;
    FlutterClipboard.copy(userModel.address);
    update(["copy"]);
  }

  void setCopied(){
    isCopied = true;
    FlutterClipboard.copy(userModel.mnemonic);
    update(["reveal"]);
  }

  void setReveal(){
    isRevealed = true;
    initCallback();
    if(Platform.isAndroid){
      FlutterWindowManager.addFlags(FlutterWindowManager.FLAG_SECURE);
    }
    update(["reveal"]);
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  @override
  deniedPermission() {

  }

  @override
  screenshotCallback(String data) {
    showBottomSheet();
  }
}
