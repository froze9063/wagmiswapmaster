import 'package:get/get.dart';

import '../controllers/currency_setting_controller.dart';

class CurrencySettingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CurrencySettingController>(
      () => CurrencySettingController(),
    );
  }
}
