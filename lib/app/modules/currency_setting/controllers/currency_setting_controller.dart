import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/repository/api_repository.dart';

class CurrencySettingController extends GetxController {

  final count = 0.obs;
  int selectedIndex = -1;

  late List<Map> currencyList;
  int themeType = 0;

  int tabPosition = 1;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late LedgerModel selectedLedger;

  late APIRepository apiRepository;

  @override
  void onInit() {
    apiRepository = APIRepository(this);
    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];

    loadUser();
    currencyList = [];
    initExampleCurrency();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void initExampleCurrency(){
    Map map1 = Map();
    map1["id"] = 1;
    map1["name"] = "US Dollar";
    map1["currency"] = "USD";
    map1["symbol"] = "\$";

    Map map2 = Map();
    map2["id"] = 2;
    map2["name"] = "Indian Rupee";
    map2["currency"] = "INR";
    map2["symbol"] = "₹";

    Map map3 = Map();
    map3["id"] = 3;
    map3["name"] = "Indonesian Rupiah";
    map3["currency"] = "IDR";
    map3["symbol"] = "RP";

    Map map4 = Map();
    map4["id"] = 4;
    map4["name"] = "Malaysian Ringgit";
    map4["currency"] = "MYR";
    map4["symbol"] = "RM";

    Map map5 = Map();
    map5["id"] = 5;
    map5["name"] = "Philippine Peso";
    map5["currency"] = "PHP";
    map5["symbol"] = "₱";

    Map map6 = Map();
    map6["id"] = 6;
    map6["name"] = "Thai Baht";
    map6["currency"] = "THB";
    map6["symbol"] = "฿";

    currencyList.add(map1);
    currencyList.add(map2);
    currencyList.add(map3);
    currencyList.add(map4);
    currencyList.add(map5);
    currencyList.add(map6);
  }

  void setSelected(int index){
    selectedIndex = index;
    saveCurrency(index,currencyList[index]["currency"],currencyList[index]["symbol"]);
    update(["currency"]);
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      selectedIndex = prefs.getInt("currency_index") ?? 0;
      update(["theme"]);
    });
  }

  void saveCurrency(int index, String currency, String symbol){
    SharedPreferences.getInstance().then((prefs){
      prefs.setInt("currency_index", index);
      prefs.setString("currency_name", currency);
      prefs.setString("symbol", symbol);
    });
  }
}
