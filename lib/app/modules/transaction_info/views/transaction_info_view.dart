import 'dart:math';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/util/my_util.dart';

import '../controllers/transaction_info_controller.dart';

class TransactionInfoView extends GetView<TransactionInfoController> {

  final TransactionInfoController _transactionInfoController = Get.put(TransactionInfoController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<TransactionInfoController>(
        id: "theme",
        init: TransactionInfoController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Column(
            children: [
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 3),
                  GestureDetector(
                    child: Container(
                      color: Colors.transparent,
                      width: 55,
                      height: 55,
                      child: Center(
                        child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                        "assets/icons/ic_black_back.png", width: 24,
                            height: 24, fit: BoxFit.contain),
                      ),
                    ),
                    onTap: (){
                      Get.back();
                    },
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Text(
                    StringContent.transaction_info,
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 14,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w600,
                        letterSpacing: 2
                    ),
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Container(
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(width: 3)
                ],
              ),

              SizedBox(height: 18),

              Expanded(child: Container(
                margin: EdgeInsets.only(left: 24, right: 24),
                padding: EdgeInsets.all(8),
                width: double.maxFinite,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(child: Text(
                            StringContent.date,
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          ), flex: 1),

                          Text(
                            "${_transactionInfoController.transactionModel.createdAt} | ${_transactionInfoController.transactionModel.createdTime}",
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          )
                        ],
                      ),

                      SizedBox(height: 24),

                      Row(
                        children: [
                          Expanded(child: Text(
                            StringContent.transaction_type,
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          ), flex: 1),

                          Text(
                            _transactionInfoController.transactionModel.type,
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          )
                        ],
                      ),

                      SizedBox(height: 24),

                      _transactionInfoController.transactionModel.type == "App Call" ? appColumn(themeValue)
                          : notAppColumn(themeValue)
                    ],
                  ),
                ),
              ), flex: 1),
            ],
          ),
        ),
      ),
    );
  }

  Widget notAppColumn(TransactionInfoController themeValue){
    return Column(
      children: [
        Row(
          children: [
            Expanded(child: Text(
              StringContent.asset,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ), flex: 1),

            Text(
              _transactionInfoController.transactionModel.assetName,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            )
          ],
        ),

        SizedBox(height: 24),

        Row(
          children: [
            Expanded(child: Text(
              StringContent.amount,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ), flex: 1),

            Text(
              "${_transactionInfoController.transactionModel.assetName == "ALGO" ? MyUtils.doubleWithoutDecimalToInt(Algo.fromMicroAlgos(_transactionInfoController.transactionModel.amount))
                  : MyUtils.doubleWithoutDecimalToInt(_transactionInfoController.transactionModel.amount / pow(10, _transactionInfoController.transactionModel.decimal))} ${_transactionInfoController.transactionModel.assetName} | "
                  "${_transactionInfoController.transactionModel.assetName == "ALGO" ? MyUtils.formatPrice(MyUtils.parseAlgoPrice(double.parse(MyUtils.parseAlgoToMicro(
                  _transactionInfoController.transactionModel.amount)), double.parse(_transactionInfoController.transactionModel.price.isNotEmpty ?
              _transactionInfoController.transactionModel.price : "0")))
                  : MyUtils.formatPrice(MyUtils.countAssetPrice(_transactionInfoController.transactionModel.price, double.parse(_transactionInfoController.transactionModel.amount.toString()),
                  _transactionInfoController.transactionModel.decimal,1))} ${themeValue.currentCurrency}",
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            )
          ],
        ),

        SizedBox(height: 24),

        Row(
          children: [
            Expanded(child: Text(
              StringContent.from,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ), flex: 1),

            Text(
              MyUtils.parseAddress(_transactionInfoController.transactionModel.from),
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            )
          ],
        ),

        Visibility(child: Column(
          children: [
            SizedBox(height: 24),

            Row(
              children: [
                Expanded(child: Text(
                  StringContent.to,
                  style: TextStyle(
                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                    fontSize: 12,
                    fontFamily: "BarlowNormal",
                    fontWeight: FontWeight.w400,
                    letterSpacing: 1,
                    decorationThickness: 1.5,
                  ),
                ), flex: 1),

                Text(
                  MyUtils.parseAddress(_transactionInfoController.transactionModel.address),
                  style: TextStyle(
                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                    fontSize: 12,
                    fontFamily: "BarlowNormal",
                    fontWeight: FontWeight.w400,
                    letterSpacing: 1,
                    decorationThickness: 1.5,
                  ),
                )
              ],
            ),
          ],
        ), visible: themeValue.transactionModel.type != "Receive"),

        SizedBox(height: 24),

        Row(
          children: [
            Text(
              StringContent.notes,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ),

            SizedBox(width: 100),

            Expanded(child: Text(
              _transactionInfoController.transactionModel.note.isEmpty ? "-" : _transactionInfoController.transactionModel.note,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
              textAlign: TextAlign.end,
            ), flex: 1),
          ],
        ),

        SizedBox(height: 24),

        Row(
          children: [
            Text(
              StringContent.fee,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ),

            SizedBox(width: 4),

            SvgPicture.asset("assets/icons/ic_grey_info.svg", height: 16, width: 16),

            Expanded(child: SizedBox(), flex: 1),

            Text(
              "${NumberFormat('#,##0.000', 'en_US').format(Algo.fromMicroAlgos(
                  themeValue.transactionModel.fee))} ALGOS",
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            )
          ],
        ),

        SizedBox(height: 24),

        Row(
          children: [
            Expanded(child: Text(
              StringContent.total_amount,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ), flex: 1),

            Text(
              "~ ${_transactionInfoController.transactionModel.assetName == "ALGO" ? MyUtils.formatPrice(MyUtils.parseAlgoPrice(double.parse(MyUtils.parseAlgoToMicro(
                  _transactionInfoController.transactionModel.amount)),
                  double.parse(_transactionInfoController.transactionModel.price.isNotEmpty ?
                  _transactionInfoController.transactionModel.price : "0")))
                  : MyUtils.formatPrice(MyUtils.countAssetPrice(_transactionInfoController.transactionModel.price, double.parse(_transactionInfoController.transactionModel.amount.toString()),
                  _transactionInfoController.transactionModel.decimal,1))} ${themeValue.currentCurrency}",
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            )
          ],
        ),

        SizedBox(height: 24),

        Row(
          children: [
            Expanded(child: Text(
              StringContent.status,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ), flex: 1),

            Text(
              "Complete",
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.greenDark : ThemeConstant.greenLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            )
          ],
        ),

        SizedBox(height: 24),

        Row(
          children: [
            Text(
              StringContent.tx_id,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ),
            SizedBox(width: 24),
            Expanded(child: GestureDetector(
              child: Container(
                color: Colors.transparent,
                child: Text(
                  themeValue.transactionModel.transaction_id,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: Colors.blue,
                    decoration: TextDecoration.underline,
                    fontSize: 12,
                    fontFamily: "BarlowNormal",
                    fontWeight: FontWeight.w400,
                    letterSpacing: 1,
                    decorationThickness: 1.5,
                  ),
                ),
              ),
              onTap: (){
                _transactionInfoController.openBrowserTab();
              },
            ), flex: 1)
          ],
        ),
        SizedBox(height: 36),
      ],
    );
  }

  Widget appColumn(TransactionInfoController themeValue){
    return Column(
      children: [
        Row(
          children: [
            Expanded(child: Text(
              StringContent.appId,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ), flex: 1),

            Text(
              "${_transactionInfoController.transactionModel.applicationId}",
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            )
          ],
        ),

        SizedBox(height: 24),

        Row(
          children: [
            Text(
              StringContent.fee,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ),

            SizedBox(width: 4),

            SvgPicture.asset("assets/icons/ic_grey_info.svg", height: 16, width: 16),

            Expanded(child: SizedBox(), flex: 1),

            Text(
              "${NumberFormat('#,##0.000', 'en_US').format(Algo.fromMicroAlgos(
                  themeValue.transactionModel.fee))} ALGOS",
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            )
          ],
        ),

        SizedBox(height: 24),

        Row(
          children: [
            Expanded(child: Text(
              StringContent.status,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ), flex: 1),

            Text(
              "Complete",
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.greenDark : ThemeConstant.greenLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            )
          ],
        ),

        SizedBox(height: 24),

        Row(
          children: [
            Text(
              StringContent.tx_id,
              style: TextStyle(
                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                fontSize: 12,
                fontFamily: "BarlowNormal",
                fontWeight: FontWeight.w400,
                letterSpacing: 1,
                decorationThickness: 1.5,
              ),
            ),
            SizedBox(width: 24),
            Expanded(child: GestureDetector(
              child: Container(
                color: Colors.transparent,
                child: Text(
                  themeValue.transactionModel.transaction_id,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 12,
                    fontFamily: "BarlowNormal",
                    fontWeight: FontWeight.w400,
                    decoration: TextDecoration.underline,
                    letterSpacing: 1,
                    decorationThickness: 1.5,
                  ),
                ),
              ),
              onTap: (){
                _transactionInfoController.openBrowserTab();
              },
            ), flex: 1)
          ],
        ),
        SizedBox(height: 36),
      ],
    );
  }
}
