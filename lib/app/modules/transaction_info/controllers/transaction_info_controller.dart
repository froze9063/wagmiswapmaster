import 'package:get/get.dart';
import 'package:sembast/sembast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:waagmiswap/app/connection/my_connection.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/transaction_model.dart';
import 'package:waagmiswap/app/repository/api_repository.dart';
import 'package:waagmiswap/app/widgets/custom_toast.dart';

class TransactionInfoController extends GetxController {

  late APIRepository apiRepository;
  late DatabaseDAO databaseDAO;

  int themeType = 0;

  late TransactionModel transactionModel;

  String indexerUrl = "";
  String algodUrl = "";

  String currentCurrency = "USD";
  String currentCurrencySymbol = "\$";
  String userAddress = "";

  int selectedLedgerPosition = 0;
  bool fromNotification = false;

  @override
  void onInit() {
    apiRepository = APIRepository(this);
    databaseDAO = DatabaseDAO();

    if(Get.arguments != null){
      transactionModel = Get.arguments["transaction_model"];
      userAddress = Get.arguments["user_address"];

      if(Get.arguments["from_notification"] != null){
        fromNotification = Get.arguments["from_notification"];
      }
      loadUser();
    }
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      currentCurrency = prefs.getString("currency_name") ?? "USD";
      currentCurrencySymbol = prefs.getString("symbol") ?? "\$";
      selectedLedgerPosition = prefs.getInt("selected_ledger_pos") ?? 0;

      algodUrl = MyConnection().getLedgerNetworkList()[selectedLedgerPosition].algodUrl;
      indexerUrl = MyConnection().getLedgerNetworkList()[selectedLedgerPosition].indexerUrl;

      checkPrice();
      update(["theme"]);
    });
  }

  void checkPrice(){
    var assetFinder = Finder(filter: Filter.and([Filter.equals("asset-id", transactionModel.assetId
    ),Filter.equals("currency", currentCurrency)]), limit: 1);

    var assetDetailFinder = Finder(filter: Filter.and([Filter.equals("index",
        transactionModel.assetId)]), limit: 1);

    databaseDAO.getAllAssetBySorted(assetFinder).then((assetValue) {
      if(assetValue.isEmpty){
        getAssetDetail();
      }
      else{
        if(transactionModel.assetId != 0){
          databaseDAO.getAllAssetDetailSorted(assetDetailFinder).then((assetDetailValue) {
            if(assetDetailValue.isEmpty){
              getAssetDetail();
            }
            else{
              transactionModel.price = assetValue[0].price;
              transactionModel.assetName = assetDetailValue[0].unitName;
              transactionModel.decimal = assetDetailValue[0].decimals;
              update(["theme"]);
            }
          });
        }
        else{
          transactionModel.price = assetValue[0].price;
          transactionModel.assetName = "ALGO";
          update(["theme"]);
        }
      }
    });
  }

  Future<void> getAssetDetail() async {
    apiRepository.getSingleAssetDetail(algodUrl,indexerUrl,transactionModel,0,
        0,0,userAddress,currentCurrency,null,0);
  }

  openBrowserTab() async {
    String site = "";

    if(fromNotification){
      site = transactionModel.ledgerId == 0 ? "https://algoexplorer.io/tx/${transactionModel.transaction_id}" :
      "https://testnet.algoexplorer.io/tx/${transactionModel.transaction_id}";
    }
    else{
      site = selectedLedgerPosition == 0 ? "https://algoexplorer.io/tx/${transactionModel.transaction_id}" :
      "https://testnet.algoexplorer.io/tx/${transactionModel.transaction_id}";
    }

    if (await canLaunch(site)) {
      await launch(site);
    } else {
      CustomToast.showToast("Cant launch, invalid url!");
    }
  }
}
