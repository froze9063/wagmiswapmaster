import 'package:get/get.dart';

import '../controllers/transaction_info_controller.dart';

class TransactionInfoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TransactionInfoController>(
      () => TransactionInfoController(),
    );
  }
}
