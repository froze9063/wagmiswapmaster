import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';

class LanguageSettingController extends GetxController {
  //TODO: Implement LanguageSettingController

  final count = 0.obs;
  int selectedIndex = -1;

  late List<Map> languageList;
  int themeType = 0;

  int tabPosition = 1;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late LedgerModel selectedLedger;

  @override
  void onInit() {
    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];
    loadUser();
    languageList = [];
    initExampleLanguage();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void initExampleLanguage(){
    Map map1 = Map();
    map1["name"] = "English";
    map1["currency"] = "English";
    map1["icon"] = "assets/icons/ic_usa.png";

 /*   Map map2 = Map();
    map2["name"] = "French";
    map2["currency"] = "Français";
    map2["icon"] = "assets/icons/ic_french.png";

    Map map3 = Map();
    map3["name"] = "Japanese";
    map3["currency"] = "日本";
    map3["icon"] = "assets/icons/ic_japanese.png";

    Map map4 = Map();
    map4["name"] = "Korean";
    map4["currency"] = "한국인";
    map4["icon"] = "assets/icons/ic_koren.png";*/

    languageList.add(map1);
   /* languageList.add(map2);
    languageList.add(map3);
    languageList.add(map4);*/
  }

  void setSelected(int index){
    this.selectedIndex = index;
    update(["language"]);
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }
}
