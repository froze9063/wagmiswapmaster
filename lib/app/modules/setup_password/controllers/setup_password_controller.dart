import 'package:algorand_dart/algorand_dart.dart';
import 'package:get/get.dart';

class SetupPasswordController extends GetxController {

  final count = 0.obs;

  late List<String> mnemonicLists;
  String publicAddress = "";
  late Account account;

  @override
  void onInit() {
    initArguments();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void initArguments(){
    publicAddress = Get.arguments["public_address"] ?? "";
    mnemonicLists = Get.arguments["mnemonic_list"];
    account = Get.arguments["account"];
  }
}
