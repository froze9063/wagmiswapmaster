import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/enter_passcode/views/enter_passcode_view.dart';

import '../controllers/setup_password_controller.dart';

class SetupPasswordView extends GetView<SetupPasswordController> {

  SetupPasswordController setupPasswordController = Get.put(SetupPasswordController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(25, 29, 52, 1.0),
                  Color.fromRGBO(16, 15, 31, 1.0)
                ]
            )
        ),
        child: Column(
          children: [
            SizedBox(height: 17),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(width: 3),
              GestureDetector(
                child: Container(
                  color: Colors.transparent,
                  width: 55,
                  height: 55,
                  child: Center(
                    child: Image.asset("assets/icons/ic_white_back.png", width: 24,
                        height: 24, fit: BoxFit.contain),
                  ),
                ),
                onTap: (){
                  Get.back();
                },
              ),
              Expanded(child: SizedBox(),flex: 1),
              Image.asset("assets/images/img_hori_messe.png", width: 182,
                  height: 20, fit: BoxFit.contain),
              Expanded(child: SizedBox(),flex: 1),
              Container(
                width: 55,
                height: 55,
              ),
              SizedBox(width: 3)
            ],
          ),

            SizedBox(height: 20),

            Text(
              StringContent.protect_wallet,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontFamily: "BarlowNormal",
                  letterSpacing: 2,
                  fontWeight: FontWeight.w600
              ),
            ),

            SizedBox(height: 6),

            Row(
              children: [
                SizedBox(width: 24),
                Expanded(child: Text(
                  StringContent.ASSETS_SAFE,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1.0),
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      letterSpacing: 1,
                      fontWeight: FontWeight.w400,
                      height: 1.5
                  ),
                ), flex: 1),
                SizedBox(width: 24)
              ],
            ),

            SizedBox(height: 100),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/img_passcode.png", width: 280,
                    height: 263, fit: BoxFit.contain)
              ],
            ),

            Expanded(child: SizedBox(),flex: 1),

            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 24, right: 24),
              height: 37,
              child: Stack(
                children: [
                  CustomButton(37, double.maxFinite, BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color.fromRGBO(106, 48, 147, 1.0),
                          Color.fromRGBO(160, 68, 255, 1.0)
                        ]
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ), StringContent.setup_passcode, TextStyle(
                      color: Colors.white,
                      fontFamily: "BarlowRegular",
                      fontSize: 12,
                      letterSpacing: 2
                  )),

                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      splashColor: Colors.transparent,
                      onTap: (){
                        Get.to(() => EnterPasscodeView(),
                            arguments: {
                              "public_address" : setupPasswordController.publicAddress,
                              "mnemonic_list" : setupPasswordController.mnemonicLists,
                              "account" : setupPasswordController.account
                            }
                        );
                      },
                    ),
                  )
                ],
              ),
            ),

            SizedBox(height: StringContent.bottomMargin)
          ],
        ),
      ),
    );
  }
}
