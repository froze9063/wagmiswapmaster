import 'package:get/get.dart';

import '../controllers/setup_password_controller.dart';

class SetupPasswordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SetupPasswordController>(
      () => SetupPasswordController(),
    );
  }
}
