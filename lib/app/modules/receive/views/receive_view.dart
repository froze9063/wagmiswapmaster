import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';

import '../controllers/receive_controller.dart';

class ReceiveView extends GetView<ReceiveController> {

  final ReceiveController _receiveController = Get.put(ReceiveController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ReceiveController>(
        id: "theme",
        init: ReceiveController(),
        builder: (themeValue) => Container(
            width: double.maxFinite,
            height: double.maxFinite,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                      themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                    ]
                )
            ),
            child: Column(
              children: [
                SizedBox(height: 30),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(width: 3),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: 55,
                        height: 55,
                        child: Center(
                          child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                          "assets/icons/ic_black_back.png", width: 24,
                              height: 24, fit: BoxFit.contain),
                        ),
                      ),
                      onTap: (){
                        Get.back();
                      },
                    ),
                    Expanded(child: SizedBox(),flex: 1),
                    Text(
                      StringContent.receive,
                      style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 16,
                        fontFamily: "BarlowMedium",
                      ),
                    ),
                    Expanded(child: SizedBox(),flex: 1),
                    Container(
                      width: 55,
                      height: 55,
                    ),
                    SizedBox(width: 3)
                  ],
                ),

                SizedBox(height: 45),

                QrImage(
                  data: _receiveController.holdAddress.isEmpty ? _receiveController.userModel.address : _receiveController.holdAddress,
                  version: QrVersions.auto,
                  size: 200.0,
                  foregroundColor: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                ),

                SizedBox(height: 45),

                Row(
                  children: [
                    SizedBox(width: 24),
                    Text(
                      StringContent.account_address,
                      style: TextStyle(
                          color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          fontSize: 12,
                          fontFamily: "BarlowNormal",
                          letterSpacing: 2
                      ),
                    )
                  ],
                ),

                Container(
                  width: double.maxFinite,
                  margin: EdgeInsets.only(left: 24,right: 24,top: 8, bottom: 16),
                  height: 1,
                  color: Color.fromRGBO(151, 151, 151, 1.0),
                ),

                Row(
                  children: [
                    SizedBox(width: 24),
                    Expanded(child: Text(
                      _receiveController.holdAddress.isEmpty ? _receiveController.userModel.address : _receiveController.holdAddress,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.lightPurple,
                        fontSize: 14,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w500,
                      ),
                    ), flex: 1),
                    SizedBox(width: 24),
                  ],
                ),

                SizedBox(height: 16),

                GetBuilder<ReceiveController>(
                  id: "copy",
                  init: ReceiveController(),
                  builder: (value) => GestureDetector(
                    child: Text(
                      !value.isCopyTapped ? StringContent.tap_to_copy : StringContent.copied,
                      style: TextStyle(
                          color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                          fontSize: 14,
                          fontFamily: "BarlowNormal",
                          height: 1.5,
                          fontWeight: FontWeight.w600,
                          decoration: TextDecoration.underline,
                          decorationThickness: 1.5,
                          letterSpacing: 1
                      ),
                    ),
                    onTap: (){
                      value.copied();
                    },
                  ),
                ),

                Container(
                  width: double.maxFinite,
                  margin: EdgeInsets.only(left: 24,right: 24,top: 10),
                  height: 1,
                  color: Color.fromRGBO(151, 151, 151, 1.0),
                ),

                Expanded(child: SizedBox(),flex: 1),

                Container(
                  width: double.maxFinite,
                  margin: EdgeInsets.only(left: 24, right: 24),
                  height: 37,
                  child: Stack(
                    children: [
                      CustomButton(37, double.maxFinite, BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.topRight,
                            colors: [
                              Color.fromRGBO(106, 48, 147, 1.0),
                              Color.fromRGBO(160, 68, 255, 1.0)
                            ]
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      ), StringContent.share_address, TextStyle(
                          color: Colors.white,
                          fontFamily: "BarlowRegular",
                          fontWeight: FontWeight.w600,
                          fontSize: 12,
                          letterSpacing: 2
                      )),

                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: Colors.transparent,
                          onTap: (){
                            _receiveController.share();
                          },
                        ),
                      )
                    ],
                  ),
                ),

                SizedBox(height: StringContent.bottomMargin)
              ],
            )
        ),
      ),
    );
  }
}
