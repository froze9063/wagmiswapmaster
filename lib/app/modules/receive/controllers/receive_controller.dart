import 'package:clipboard/clipboard.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/repository/api_repository.dart';

class ReceiveController extends GetxController {

  final count = 0.obs;

  bool isCopyTapped = false;

  int themeType = 0;

  late UserModel userModel;
  late LedgerModel ledgerModel;

  AssetsModel? selectedAssetsModel;
  AssetsDetailModel? assetsDetailModel;

  String currentCurrency = "USD";
  String currentCurrencySymbol = "\$";

  late APIRepository apiRepository;
  late DatabaseDAO databaseDAO;

  bool assetLoading = true;
  String holdAddress = "";

  @override
  void onInit() {
    apiRepository = APIRepository(this);
    databaseDAO = DatabaseDAO();
    getArguments();
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void getArguments(){
    userModel = Get.arguments["user_model"];
    ledgerModel = Get.arguments["ledger_model"];
  }

  void copied(){
    isCopyTapped = true;
    FlutterClipboard.copy(userModel.address);
    update(["copy"]);
  }

  Future<void> share() async {
    FlutterShare.share(title: userModel.address, text: userModel.address);
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      currentCurrency = prefs.getString("currency_name") ?? "USD";
      currentCurrencySymbol = prefs.getString("symbol") ?? "\$";
      update(["theme"]);
    });
  }
}
