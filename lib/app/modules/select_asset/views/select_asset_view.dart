import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/util/my_util.dart';

import '../controllers/select_asset_controller.dart';

class SelectAssetView extends GetView<SelectAssetController> {

  final SelectAssetController _selectAssetController = Get.put(SelectAssetController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GetBuilder<SelectAssetController>(
        id: "theme",
        init: SelectAssetController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Column(
            children: [
              SizedBox(height: 30),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 3),
                  GestureDetector(
                    child: Container(
                      color: Colors.transparent,
                      width: 55,
                      height: 55,
                      child: Center(
                        child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                        "assets/icons/ic_black_back.png", width: 24,
                            height: 24, fit: BoxFit.contain),
                      ),
                    ),
                    onTap: (){
                      Get.back();
                    },
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Text(
                    StringContent.select_asset,
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 16,
                      fontFamily: "BarlowMedium",
                    ),
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Container(
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(width: 3)
                ],
              ),

              SizedBox(height: 24),

              GetBuilder<SelectAssetController>(
                  id: "assets",
                  init: SelectAssetController(),
                  builder: (assetValue) => Expanded(child: RefreshIndicator(
                    color: Color.fromRGBO(106, 48, 147, 1.0),
                    onRefresh: ()=> assetValue.getAssets(assetValue.userModel),
                    child: Container(
                      height: double.maxFinite,
                      width: double.maxFinite,
                      child: Stack(
                        children: [
                          Visibility(child: Center(
                            child: Text(
                              "No assets",
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowMedium",
                              ),
                            ),
                          ), visible: assetValue.assetMapList.isEmpty && !assetValue.loadingMap[
                          "loading"]),

                          Visibility(child: Center(
                            child: CircularProgressIndicator(
                              color: Color.fromRGBO(106, 48, 147, 1.0),
                            ),
                          ), visible: assetValue.loadingMap["loading"]),

                          Container(
                            width: double.maxFinite,
                            height: double.maxFinite,
                            child: ListView.builder(
                              itemBuilder: (context, index){
                                Map assetMap = assetValue.assetMapList[index];
                                AssetsModel assetModel = assetMap["asset_model"];
                                AssetsDetailModel? assetDetailModel;

                                if(assetMap["asset_detail_model"] != null){
                                  assetDetailModel = assetMap["asset_detail_model"];
                                }

                                return GestureDetector(
                                  child: Container(
                                    margin: EdgeInsets.only(left: 24, right: 24, top: index == 0 ? 0 : 16),
                                    width: double.maxFinite,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                            themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                                            themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white
                                          ]
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(6)),
                                    ),
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                            child: assetDetailModel != null ? !assetDetailModel.unitName.contains("-") ? assetDetailModel.index != 0 ? MyUtils.getCuratedIconPath(assetDetailModel.unitName, themeValue.themeType).isEmpty ? SvgPicture.network("https://wallet.wagmiswap.io/testnet/"
                                                "assets/${assetDetailModel.index}.svg",height: 34, width: 34) : SvgPicture.asset(MyUtils.getCuratedIconPath(assetDetailModel.unitName, themeValue.themeType), height: 34, width: 34) : SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_dark_algo.svg" : "assets/icons/ic_light_algo.svg",
                                                height: 34, width: 34) : SizedBox() : SizedBox(),
                                            radius: 18,
                                            backgroundColor: assetDetailModel != null ? MyUtils.getCuratedIconPath(assetDetailModel.unitName, themeValue.themeType).isEmpty ? ThemeConstant.secondTextGrayLight : Colors.transparent : Colors.transparent
                                        ),
                                        SizedBox(width: 10),
                                        Expanded(child: Row(
                                          children: [
                                            Expanded(child: Text(
                                              assetDetailModel != null ? assetDetailModel.unitName : "",
                                              style: TextStyle(
                                                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                  fontSize: 16,
                                                  fontFamily: "BarlowNormal",
                                                  fontWeight: FontWeight.w600,
                                                  letterSpacing: 1
                                              ),
                                            ), flex: 1),
                                          ],
                                        )),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: [
                                            Text(
                                              assetDetailModel != null ? assetModel.assetId != 0 ? MyUtils.countAssetAmount(assetModel.amount,
                                                  assetDetailModel.decimals) : MyUtils.parseAlgoToMicro(assetModel.amount) : "",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                                                fontSize: 16,
                                                fontFamily: "BarlowMedium",
                                              ),
                                            ),

                                            Visibility(child: Column(
                                              children: [
                                                SizedBox(height: 6),

                                                Text(
                                                  assetDetailModel != null ? assetModel.amount != 0
                                                      ? "~ ${ assetModel.assetId != 0 ? MyUtils.formatPrice(MyUtils.countAssetPrice(assetModel.price, double.parse(assetModel.amount.toString()),
                                                      assetDetailModel.decimals,1)) : MyUtils.formatPrice(MyUtils.parseAlgoPrice(double.parse(MyUtils.parseAlgoToMicro(
                                                      assetModel.amount)), double.parse(assetModel.price.isNotEmpty ? assetModel.price : "0")))} ${assetValue.currentCurrency}" : "": "",
                                                  style: TextStyle(
                                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                    fontSize: 12,
                                                    fontFamily: "BarlowNormal",
                                                  ),
                                                )
                                              ],
                                            ), visible: assetDetailModel != null && MyUtils.formatPrice(MyUtils.countAssetPrice(assetModel.price, double.parse(assetModel.amount.toString()),
                                                assetDetailModel.decimals,1)) != "0.00")
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    if(assetValue.assetMapList[index]["user_hold_list"] != null){
                                      List<Map> userHoldList = assetValue.assetMapList[index]["user_hold_list"];
                                      if(userHoldList.isNotEmpty){
                                        for(int i = 0 ; i < userHoldList.length; i++){
                                          Map holdMap = userHoldList[i];
                                          int amount = holdMap["amount"];

                                          for(int k = 0 ; k < userHoldList.length - 1; k++){
                                            Map holdMap1 = userHoldList[k + 1];
                                            int amount1 = holdMap1["amount"];

                                            if(amount1 > amount){
                                              userHoldList[k+1]["amount"] = amount;
                                              userHoldList[i]["amount"] = amount1;
                                            }
                                          }
                                        }
                                      }
                                    }

                                    Get.back();
                                    if(assetValue.assetMapList[index]["asset_model"].assetId != 0){
                                      _selectAssetController.selectAssetCallback.onAssetSelected(
                                          assetValue.assetMapList[index]["asset_model"],assetValue.assetMapList[
                                      index]["asset_detail_model"], null);
                                    }
                                    else{
                                      _selectAssetController.selectAssetCallback.onAssetSelected(null,null,null);
                                    }
                                  },
                                );
                              },
                              itemCount: assetValue.assetMapList.length,
                              physics: AlwaysScrollableScrollPhysics(),
                              padding: EdgeInsets.zero,
                            ),
                          )
                        ],
                      ),
                    ),
                  )))
            ],
          ),
        ),
      ),
    );
  }
}
