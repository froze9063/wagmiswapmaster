import 'package:get/get.dart';

import '../controllers/select_asset_controller.dart';

class SelectAssetBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SelectAssetController>(
      () => SelectAssetController(),
    );
  }
}
