import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/callback/select_asset_callback.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/repository/api_repository.dart';

class SelectAssetController extends GetxController {

  late DatabaseDAO databaseDAO;
  late APIRepository apiRepository;
  late FocusNode selectFocus;

  bool isSelectFocus = false;

  int themeType = 0;

  late UserModel userModel;
  late LedgerModel ledgerModel;

  late List<Map> assetMapList;
  late List<Map> searchedAssetMapList;

  late SelectAssetCallback selectAssetCallback;
  Map loadingMap = Map();

  String currentCurrency = "USD";
  String currentCurrencySymbol = "\$";
  bool isManaged = false;

  @override
  void onInit() {
    databaseDAO = DatabaseDAO();

    loadingMap["is_loading"] = false;
    loadingMap["loading"] = true;
    loadingMap["loading_count"] = 0;
    loadingMap["percentage_count"] = 0;

    apiRepository = APIRepository(this);
    selectFocus = FocusNode();
    selectFocus.addListener(onSelectFocus);
    assetMapList = [];
    searchedAssetMapList = [];
    getArguments();
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getArguments(){
    userModel = Get.arguments["user_model"];
    ledgerModel = Get.arguments["ledger_model"];
    selectAssetCallback = Get.arguments["select_asset_callback"];
    isManaged = userModel.accountType != "standard";
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      currentCurrency = prefs.getString("currency_name") ?? "USD";
      currentCurrencySymbol = prefs.getString("symbol") ?? "\$";
      getAssets(userModel);
      update(["theme"]);
    });
  }

  void onSelectFocus(){
    isSelectFocus = true;
    update(["search_focus"]);
  }

  Future<void> getAssets(UserModel userModel) async {
    assetMapList.clear();
    searchedAssetMapList.clear();

    loadingMap["is_loading"] = false;
    loadingMap["loading"] = true;
    loadingMap["loading_count"] = 0;
    loadingMap["percentage_count"] = 0;

    update(["assets"]);
    apiRepository.getSearchAssets(userModel, ledgerModel.ledgerId,
        ledgerModel.algodUrl, ledgerModel.indexerUrl, assetMapList, searchedAssetMapList,
        0, loadingMap, currentCurrency);
  }
}
