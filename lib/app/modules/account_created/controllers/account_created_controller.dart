import 'package:get/get.dart';

class AccountCreatedController extends GetxController {
  //TODO: Implement AccountCreatedController

  final count = 0.obs;
  int from = 0;

  @override
  void onInit() {
    getArgument();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void getArgument(){
    if(Get.arguments != null){
      from = Get.arguments["from"] ?? 0;
    }
  }
}
