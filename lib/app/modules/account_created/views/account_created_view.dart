import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/home/views/home_view.dart';

import '../controllers/account_created_controller.dart';

class AccountCreatedView extends GetView<AccountCreatedController> {

  final AccountCreatedController _accountCreatedController = Get.put(
      AccountCreatedController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(25, 29, 52, 1.0),
                  Color.fromRGBO(16, 15, 31, 1.0)
                ]
            )
        ),
        child: Column(
          children: [
            SizedBox(height: 40),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(child: SizedBox(),flex: 1),
                Image.asset("assets/images/img_hori_messe.png", width: 182,
                    height: 23, fit: BoxFit.contain),
                Expanded(child: SizedBox(),flex: 1),
              ],
            ),

            SizedBox(height: 18),

            Text(
              _accountCreatedController.from == 0 ? StringContent.account_created
                  : StringContent.account_imported,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontFamily: "BarlowNormal",
                  fontWeight: FontWeight.w600,
                  letterSpacing: 2
              ),
            ),

            SizedBox(height: 6),

            Row(
              children: [
                SizedBox(width: 24),
                Expanded(child: Text(
                  _accountCreatedController.from == 0 ? StringContent.account_created_phrase
                      : StringContent.account_created_phrase2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1.0),
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      fontWeight: FontWeight.w400,
                      letterSpacing: 1,
                      height: 1.5
                  ),
                ), flex: 1),
                SizedBox(width: 24)
              ],
            ),

            SizedBox(height: 100),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/img_wallet_lock.png", width: 280,
                    height: 263, fit: BoxFit.contain)
              ],
            ),

            Expanded(child: SizedBox(),flex: 1),

            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 24, right: 24),
              height: 37,
              child: Stack(
                children: [
                  CustomButton(37, double.maxFinite, BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color.fromRGBO(106, 48, 147, 1.0),
                          Color.fromRGBO(160, 68, 255, 1.0)
                        ]
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ), StringContent.start_using_wallet, TextStyle(
                      color: Colors.white,
                      fontFamily: "BarlowRegular",
                      fontSize: 12,
                      letterSpacing: 2
                  )),

                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      splashColor: Colors.transparent,
                      onTap: (){
                        Get.offAll(() => HomeView());
                      },
                    ),
                  )
                ],
              ),
            ),

            SizedBox(height: StringContent.bottomMargin)
          ],
        ),
      ),
    );
  }
}

