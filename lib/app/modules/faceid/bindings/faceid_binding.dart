import 'package:get/get.dart';

import '../controllers/faceid_controller.dart';

class FaceidBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FaceidController>(
      () => FaceidController(),
    );
  }
}
