import 'dart:io';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/import_account/views/import_account_view.dart';
import 'package:waagmiswap/app/modules/login_face/views/login_face_view.dart';
import 'package:waagmiswap/app/modules/mnemonic_checklist/views/mnemonic_checklist_view.dart';

import '../controllers/faceid_controller.dart';

class FaceidView extends GetView<FaceidController> {

  FaceidController _faceidController = Get.put(FaceidController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(25, 29, 52, 1.0),
                  Color.fromRGBO(16, 15, 31, 1.0)
                ]
            )
        ),
        child: Column(
          children: [
            SizedBox(height: 24),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(width: 3),
                GestureDetector(
                  child: Container(
                    color: Colors.transparent,
                    width: 55,
                    height: 55,
                    child: Center(
                      child: Image.asset("assets/icons/ic_white_back.png", width: 24,
                          height: 24, fit: BoxFit.contain),
                    ),
                  ),
                  onTap: (){
                    Get.back();
                  },
                ),
                Expanded(child: SizedBox(),flex: 1),
                Image.asset("assets/images/img_hori_messe.png", width: 182,
                    height: 23, fit: BoxFit.contain),
                Expanded(child: SizedBox(),flex: 1),
                Container(
                  width: 55,
                  height: 55,
                ),
                SizedBox(width: 3)
              ],
            ),

            SizedBox(height: 18),

            Text(
              StringContent.protect_wallet,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontFamily: "BarlowNormal",
                  letterSpacing: 2,
                  fontWeight: FontWeight.w600
              ),
            ),

            SizedBox(height: 6),

            Row(
              children: [
                SizedBox(width: 24),
                Expanded(child: Text(
                  StringContent.layer_extra,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1.0),
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      letterSpacing: 1,
                      fontWeight: FontWeight.w400,
                      height: 1.5
                  ),
                ), flex: 1),
                SizedBox(width: 24)
              ],
            ),

            SizedBox(height: 75),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/img_protect.png", width: 280,
                    height: 263, fit: BoxFit.contain)
              ],
            ),

            Expanded(child: SizedBox(),flex: 1),

            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 24, right: 24),
              height: 37,
              child: Stack(
                children: [
                  CustomButton(45, double.maxFinite, BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color.fromRGBO(106, 48, 147, 1.0),
                          Color.fromRGBO(160, 68, 255, 1.0)
                        ]
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ), Platform.isIOS ? StringContent.setup_face : StringContent.setup_touch_id, TextStyle(
                      color: Colors.white,
                      fontFamily: "BarlowRegular",
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 2
                  )),

                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      splashColor: Colors.transparent,
                      onTap: (){
                        if(Platform.isIOS){
                          Get.to(() => LoginFaceView(),  arguments: {
                            "public_address" : _faceidController.publicAddress,
                            "mnemonic_list" : _faceidController.mnemonicLists,
                            "account" : _faceidController.account,
                            "passcode" : _faceidController.passcode,
                            "from" : _faceidController.from
                          });
                        }
                        else{
                          _faceidController.authenticate();
                        }
                      },
                    ),
                  )
                ],
              ),
            ),

            SizedBox(height: 16),

            GestureDetector(
              child: Container(
                height: 37,
                child: Center(
                  child: Text(
                    StringContent.later,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontFamily: "BarlowSemiBold",
                      decoration: TextDecoration.underline,
                      decorationThickness: 1.5,
                    ),
                  ),
                ),
              ),
              onTap: (){
                if(_faceidController.from == 0){
                  Get.to(() => MnemonicChecklistView(),
                      arguments: {
                        "public_address" : _faceidController.publicAddress,
                        "mnemonic_list" : _faceidController.mnemonicLists,
                        "account" : _faceidController.account,
                        "passcode" : _faceidController.passcode
                      }
                  );
                }
                else{
                  Get.to(() => ImportAccountView(), arguments: {"passcode" : _faceidController.passcode});
                }
              },
            ),

            SizedBox(height: StringContent.bottomMargin)
          ],
        ),
      ),
    );
  }
}
