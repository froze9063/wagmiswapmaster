import 'package:get/get.dart';

import '../controllers/mnemonic_checklist_controller.dart';

class MnemonicChecklistBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MnemonicChecklistController>(
      () => MnemonicChecklistController(),
    );
  }
}
