import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/mnemonic/views/mnemonic_view.dart';

import '../controllers/mnemonic_checklist_controller.dart';

class MnemonicChecklistView extends GetView<MnemonicChecklistController> {

  MnemonicChecklistController mnemonicChecklistController = Get.put(MnemonicChecklistController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<MnemonicChecklistController>(
        id: "theme",
        init: MnemonicChecklistController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Column(
            children: [
              SizedBox(height: 30),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 3),
                  GestureDetector(
                    child: Container(
                      color: Colors.transparent,
                      width: 55,
                      height: 55,
                      child: Center(
                        child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                        "assets/icons/ic_black_back.png", width: 24,
                            height: 24, fit: BoxFit.contain),
                      ),
                    ),
                    onTap: (){
                      Get.back();
                    },
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Image.asset("assets/images/img_hori_messe.png", width: 182,
                      height: 23, fit: BoxFit.contain),
                  Expanded(child: SizedBox(),flex: 1),
                  Container(
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(width: 3)
                ],
              ),

              SizedBox(height: 18),

              Text(
                StringContent.create_account,
                style: TextStyle(
                    color: themeValue.themeType == 0 ?
                    ThemeConstant.white : ThemeConstant.blackNavy,
                    fontSize: 14,
                    fontFamily: "BarlowSemiBold",
                    letterSpacing: 2
                ),
              ),

              SizedBox(height: 55),

              Row(
                children: [
                  SizedBox(width: 24),
                  Text(
                    StringContent.acknowledge,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: themeValue.themeType == 0 ?
                        ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 12,
                        fontFamily: "BarlowMedium",
                        height: 1.5
                    ),
                  ),
                  SizedBox(width: 24),
                ],
              ),

              Expanded(child: GetBuilder<MnemonicChecklistController>(
                  id: "checklist",
                  init: MnemonicChecklistController(),
                  builder: (value) => ListView.builder(
                    itemBuilder: (context,index){
                      Map map = value.checkLists[index];
                      String title = map["title"];
                      bool selected = map["selected"];
                      return Container(
                        margin: EdgeInsets.only(top: 16),
                        width: double.maxFinite,
                        child: Row(
                          children: [
                            SizedBox(width: 24),
                            Expanded(child: Text(
                              title,
                              style: TextStyle(
                                  color: themeValue.themeType == 0 ?
                                  ThemeConstant.white : ThemeConstant.blackNavy,
                                  fontSize: 12,
                                  fontFamily: "BarlowNormal",
                                  fontWeight: FontWeight.w400,
                                  letterSpacing: 1,
                                  height: 1.5
                              ),
                            ), flex: 1),
                            SizedBox(width: 16),
                            GestureDetector(
                              child: Container(
                                width: 26,
                                height: 26,
                                decoration: BoxDecoration(
                                    color: selected ? Color.fromRGBO(184, 115, 255, 1.0) : Colors.transparent,
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    border: Border.all(
                                        width: 2,
                                        color: Color.fromRGBO(184, 115, 255, 1.0)
                                    )
                                ),
                                child: Visibility(
                                  child: Center(
                                    child: Image.asset("assets/icons/ic_check.png", width: 16, height: 16),
                                  ),
                                  visible: selected,
                                ),
                              ),
                              onTap: (){
                                mnemonicChecklistController.setCheck(index);
                              },
                            ),
                            SizedBox(width: 24),
                          ],
                        ),
                      );
                    },
                    itemCount: value.checkLists.length,
                    shrinkWrap: true,
                    padding: EdgeInsets.zero,
                  )), flex: 1),

              Row(
                children: [
                  SizedBox(width: 24),
                  Expanded(child: Text(
                    StringContent.mese_content,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: themeValue.themeType == 0 ?
                      ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 12,
                      height: 1.5,
                      fontFamily: "BarlowRegular",
                    ),
                  ), flex: 1),
                  SizedBox(width: 24)
                ],
              ),

              SizedBox(height: 16),

              GetBuilder<MnemonicChecklistController>(
                  id: "button_state",
                  init: MnemonicChecklistController(),
                  builder: (value) => Container(
                    width: double.maxFinite,
                    margin: EdgeInsets.only(left: 24, right: 24),
                    height: 37,
                    child: Stack(
                      children: [
                        CustomButton(45, double.maxFinite, BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.topRight,
                              colors: [
                                value.isContinue ? Color.fromRGBO(106, 48, 147, 1.0) : Color.fromRGBO(89, 89, 89, 1.0),
                                value.isContinue ? Color.fromRGBO(160, 68, 255, 1.0) : Color.fromRGBO(89, 89, 89, 1.0)
                              ]
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ), StringContent.done, TextStyle(
                            color: value.isContinue ? Colors.white : Color.fromRGBO(151, 151, 151, 1.0),
                            fontFamily: "BarlowRegular",
                            fontSize: 12,
                            letterSpacing: 2
                        )),

                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.transparent,
                            onTap: (){
                              if(mnemonicChecklistController.isContinue){
                                Get.to(() => MnemonicView(),
                                    arguments: {
                                      "public_address" : mnemonicChecklistController.publicAddress,
                                      "mnemonic_list" : mnemonicChecklistController.mnemonicLists,
                                      "account" : mnemonicChecklistController.account,
                                      "passcode" : mnemonicChecklistController.passcode,
                                      "from_home" : mnemonicChecklistController.isFromHome,
                                      "face_id" : mnemonicChecklistController.faceId
                                    }
                                );
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  )
              ),
              SizedBox(height: StringContent.bottomMargin)
            ],
          ),
        ),
      ),
    );
  }
}

