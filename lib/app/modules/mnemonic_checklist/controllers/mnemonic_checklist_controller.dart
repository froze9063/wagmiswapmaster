import 'package:algorand_dart/algorand_dart.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/constant/string_content.dart';

class MnemonicChecklistController extends GetxController {
  //TODO: Implement MnemonicChecklistController

  final count = 0.obs;
  String passcode = "";

  late List<Map> checkLists;
  bool isContinue = false;

  List<String>? mnemonicLists;
  String publicAddress = "";
  Account? account;

  bool isFromHome = false;
  bool faceId = false;

  int themeType = 0;

  @override
  void onInit() {
    checkLists = [];
    initiateExampleList();
    initArguments();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  void initArguments(){
    if(Get.arguments["public_address"] != null){
      publicAddress = Get.arguments["public_address"] ?? "";
    }
    if(Get.arguments["mnemonic_list"] != null){
      mnemonicLists = Get.arguments["mnemonic_list"];
    }
    if(Get.arguments["account"] != null){
      account = Get.arguments["account"];
    }
    if(Get.arguments["passcode"] != null){
      passcode = Get.arguments["passcode"];
    }
    if(Get.arguments["from_home"] != null){
      isFromHome = Get.arguments["from_home"];
    }
    if(Get.arguments["face_id"] != null){
      faceId = Get.arguments["face_id"];
    }

    if(isFromHome){
      loadUser();
    }
  }

  void initiateExampleList(){
    Map map1 = Map();
    map1["title"] = StringContent.acknowledge1;
    map1["selected"] = false;

    Map map2 = Map();
    map2["title"] = StringContent.acknowledge2;
    map2["selected"] = false;

    Map map3 = Map();
    map3["title"] = StringContent.acknowledge3;
    map3["selected"] = false;

    checkLists.add(map1);
    checkLists.add(map2);
    checkLists.add(map3);
  }

  void setCheck(int position){
    checkLists[position]["selected"] = !checkLists[position]["selected"];
    update(["checklist"]);
    setButton();
  }

  void setButton(){
    for(int i = 0; i < checkLists.length; i++){
      Map map = checkLists[i];
      bool selected = map["selected"];
      if(!selected){
        isContinue = false;
        update(["button_state"]);
        return;
      }
    }

    isContinue = true;
    update(["button_state"]);
  }
}
