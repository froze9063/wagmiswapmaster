import 'package:flutter/material.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/modules/receive/views/receive_view.dart';
import 'package:waagmiswap/app/modules/send/views/send_view.dart';

import '../controllers/about_us_controller.dart';

class AboutUsView extends GetView<AboutUsController> {

  AboutUsController aboutUsController = Get.put(AboutUsController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<AboutUsController>(
        id: "theme",
        init: AboutUsController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(height: 30),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: 3),
                      GestureDetector(
                        child: Container(
                          color: Colors.transparent,
                          width: 55,
                          height: 55,
                          child: Center(
                            child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                            "assets/icons/ic_black_back.png", width: 24,
                                height: 24, fit: BoxFit.contain),
                          ),
                        ),
                        onTap: (){
                          Get.back();
                        },
                      ),
                      Expanded(child: SizedBox(),flex: 1),
                      Text(
                        StringContent.about_us.toUpperCase(),
                        style: TextStyle(
                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                            fontSize: 14,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w600,
                            letterSpacing: 2
                        ),
                      ),
                      Expanded(child: SizedBox(),flex: 1),
                      Container(
                        width: 55,
                        height: 55,
                      ),
                      SizedBox(width: 3)
                    ],
                  ),
                  SizedBox(height: 18),
                  Image.asset("assets/images/img_hori_messe.png", width: 155,
                      height: 20, fit: BoxFit.contain),
                  SizedBox(height: 24),
                  Row(
                    children: [
                      SizedBox(width: 24),
                      Expanded(child: Text(
                        StringContent.about_us_detail,
                        style: TextStyle(
                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                            fontSize: 12,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w400,
                            letterSpacing: 1,
                            height: 1.5
                        ),
                      )),
                      SizedBox(width: 24),
                    ],
                  ),
                  Expanded(child: SizedBox(), flex: 1),

                  Container(
                    width: double.maxFinite,
                    height: 1,
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                  ),

                  Container(
                    width: double.maxFinite,
                    height: 55,
                    decoration: BoxDecoration(
                        color: themeValue.themeType == 0 ? ThemeConstant.darkBottomColor : ThemeConstant.lightBottomColor
                    ),
                    child: Row(
                      children: [
                        Expanded(child: Container(
                          width: double.maxFinite,
                          height: double.maxFinite,
                          color: Colors.transparent,
                          child: Stack(
                            children: [
                              Row(
                                children: [
                                  SizedBox(width: 24),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      themeValue.tabPosition == 1 ? Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_balance_white.png"
                                          : "assets/icons/ic_balance_purple.png", width: 17.98, height: 17.99)
                                          : Image.asset("assets/icons/ic_balance_grey.png", width: 17.98, height: 17.99),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.balance,
                                        style: TextStyle(
                                            color: themeValue.tabPosition == 1 ? themeValue.themeType == 0 ? ThemeConstant.white :
                                            ThemeConstant.lightPurple :
                                            Color.fromRGBO(164, 164, 164, 1.0),
                                            wordSpacing: 1,
                                            fontSize: 10,
                                            fontFamily: "BarlowRegular",
                                            letterSpacing: 2
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),

                              Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  splashColor: Colors.transparent,
                                  onTap: (){
                                    Get.offAllNamed("home", arguments: {"tab_position" : 1});
                                  },
                                ),
                              )
                            ],
                          ),
                        ),flex: 1),

                        Expanded(child: Container(
                          width: double.maxFinite,
                          height: double.maxFinite,
                          color: Colors.transparent,
                          child: Stack(
                            children: [
                              Row(
                                children: [
                                  Expanded(child: SizedBox(), flex: 1),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      themeValue.tabPosition == 2 ? Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_transaction_white.png" :
                                      "assets/icons/ic_transaction_purple.png", width: 18.78, height: 18)
                                          : Image.asset("assets/icons/ic_transaction_grey.png", width: 18.78, height: 18),
                                      SizedBox(height: 10),
                                      Text(
                                        StringContent.transaction,
                                        style: TextStyle(
                                            color: themeValue.tabPosition == 2 ? themeValue.themeType == 0 ? ThemeConstant.white :
                                            ThemeConstant.lightPurple : Color.fromRGBO(164, 164, 164, 1.0),
                                            wordSpacing: 1,
                                            fontSize: 10,
                                            fontFamily: "BarlowRegular",
                                            letterSpacing: 2
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(width: 24)
                                ],
                              ),

                              Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  splashColor: Colors.transparent,
                                  onTap: (){
                                    Get.offAllNamed("home", arguments: {"tab_position" : 2});
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),flex: 1),
                      ],
                    ),
                  )
                ],
              ),

              Column(
                children: [
                  Expanded(child: SizedBox(), flex: 1),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        child: Image.asset("assets/images/img_polygon.png", width: 65, height: 65),
                        onTap: (){
                          showBottomSheet(context,themeValue);
                        },
                      )
                    ],
                  ),
                  SizedBox(height: 7)
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  showBottomSheet(BuildContext context, AboutUsController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 150.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 150.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    Expanded(child: SizedBox(), flex: 1),
                    Container(
                      width: double.maxFinite,
                      height: 150,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                              themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                            ]
                        ),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: 24),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                child: Container(
                                  color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Image.asset(themeValue.themeType == 0 ? "assets/images/img_send.png"
                                          : "assets/images/img_send_light.png", height: 65, width: 65),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.send,
                                        style: TextStyle(
                                            color: Color.fromRGBO(255, 197, 48, 1),
                                            fontSize: 16,
                                            fontFamily: "BarlowNormal",
                                            letterSpacing: 0.5
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  Get.back();
                                  Get.to(() => SendView(), arguments: {
                                    "user_model" : themeValue.selectedUserModel,
                                    "ledger_model" : themeValue.selectedLedger
                                  })?.then((value) {
                                    FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
                                  });
                                },
                              ),
                              SizedBox(width: 100),
                              GestureDetector(
                                child: Container(
                                  color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Image.asset(themeValue.themeType == 0 ? "assets/images/img_receive.png"
                                          : "assets/images/img_receive_light.png", height: 65, width: 65),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.receive,
                                        style: TextStyle(
                                            color: Color.fromRGBO(160, 68, 255, 1.0),
                                            fontSize: 16,
                                            fontFamily: "BarlowNormal",
                                            letterSpacing: 0.5
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  Get.back();
                                  Get.to(() => ReceiveView(), arguments: {
                                    "user_model" : themeValue.selectedUserModel,
                                    "ledger_model" : themeValue.selectedLedger
                                  });
                                },
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }
}
