import 'dart:async';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/modules/enter_passcode/views/enter_passcode_view.dart';
import 'package:waagmiswap/app/modules/home/views/home_view.dart';
import 'package:waagmiswap/app/modules/select_option/views/select_option_view.dart';

import '../../../algorand/algorand.dart';

class WelcomeController extends GetxController {

  late DatabaseDAO databaseDAO;

  bool isPasscodeOn = true;
  bool faceId = false;
  bool loggedIn = false;

  @override
  void onInit() {
    databaseDAO = DatabaseDAO();
    setPasscodeSwitch();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void setPasscodeSwitch(){
    SharedPreferences.getInstance().then((prefs){
      isPasscodeOn = prefs.getBool("passcode_status") ?? true;
      faceId = prefs.getBool("face_id") ?? true;
      databaseDAO.getAll().then((value) {
        if(value.isNotEmpty){
          loggedIn = true;
          update(["theme"]);
          if(isPasscodeOn){
            Get.offAll(() => EnterPasscodeView(), arguments: {
              "from_welcome" : true});
          }
          else{
            Get.offAll(() => HomeView());
          }
        }
      });
    });
  }

  Future<void> initAlgorand() async {
    AlgorandRepository algorandRepository = AlgorandRepository();
    var algorand = algorandRepository.getAlgorandConfig("${MyConstant.USED_URL}/algod",
        "${MyConstant.USED_URL}/idx");
    algorand.createAccount().then((value) {
      final publicAddress = value.publicAddress;
      value.seedPhrase.then((wordValue) {
        Get.back();
        Get.off(() => SelectOptionView(),
            arguments: {
              "public_address" : publicAddress,
              "mnemonic_list" : wordValue,
              "account" : value
            }
        );
      });
    });
  }
}
