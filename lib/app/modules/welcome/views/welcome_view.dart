import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';

import '../controllers/welcome_controller.dart';

class WelcomeView extends GetView<WelcomeController> {

  WelcomeController welcomeController = Get.put(WelcomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<WelcomeController>(
        id: "theme",
        init: WelcomeController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          color: Color.fromRGBO(25, 29, 52, 1.0),
          child: Stack(
            children: [
              Image.asset("assets/images/img_welcome_screen.png", width: double.maxFinite,
                  height: double.maxFinite, fit: BoxFit.cover),
              Center(
                child: Image.asset("assets/images/messe_logo.png", width: 217, height: 190, fit: BoxFit.contain),
              ),
              !themeValue.loggedIn ? Column(
                children: [
                  Expanded(child: SizedBox(), flex: 1),
                  Container(
                    width: double.maxFinite,
                    margin: EdgeInsets.only(left: 24, right: 24),
                    height: 37,
                    child: Stack(
                      children: [
                        CustomButton(37, double.maxFinite, BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.topRight,
                              colors: [
                                Color.fromRGBO(106, 48, 147, 1.0),
                                Color.fromRGBO(160, 68, 255, 1.0)
                              ]
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ), StringContent.lets_begin.toUpperCase(), TextStyle(
                            color: Colors.white,
                            fontFamily: "BarlowRegular",
                            fontSize: 12,
                            fontWeight: FontWeight.w600,
                            letterSpacing: 2
                        )),

                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.transparent,
                            onTap: (){
                              welcomeController.initAlgorand();
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 53)
                ],
              ) : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
