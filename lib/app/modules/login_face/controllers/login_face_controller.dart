import 'dart:io';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:get/get.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:waagmiswap/app/modules/home/views/home_view.dart';
import 'package:waagmiswap/app/modules/import_account/views/import_account_view.dart';
import 'package:waagmiswap/app/modules/mnemonic_checklist/views/mnemonic_checklist_view.dart';
import 'package:waagmiswap/app/widgets/custom_toast.dart';

class LoginFaceController extends GetxController {

  final count = 0.obs;

  final LocalAuthentication auth = LocalAuthentication();

  int from = 0;

  List<String>? mnemonicLists;
  Account? account;

  String publicAddress = "";
  String passcode = "";

  @override
  void onInit() {
    super.onInit();
    getArgument();
    authenticate();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void getArgument(){
    if(Get.arguments != null){
      from = Get.arguments["from"] ?? 0;
    }
    if(Get.arguments["public_address"] != null){
      publicAddress = Get.arguments["public_address"] ?? "";
    }
    if(Get.arguments["mnemonic_list"] != null){
      mnemonicLists = Get.arguments["mnemonic_list"];
    }
    if(Get.arguments["account"] != null){
      account = Get.arguments["account"];
    }
    if(Get.arguments["passcode"] != null){
      passcode = Get.arguments["passcode"];
    }
  }

  Future<bool> authenticateWithBiometrics() async {
    bool isAuthenticated = false;

    var androidMessage = AndroidAuthMessages(
        signInTitle: "WAGMISWAP",
        biometricHint: ""
    );

    try{
      final LocalAuthentication localAuthentication = LocalAuthentication();
      isAuthenticated = await localAuthentication.authenticate(
        localizedReason: 'Please complete the biometrics to proceed.',
        androidAuthStrings: androidMessage,
        biometricOnly: true,
      );
    }
    catch(e){
      CustomToast.showToast("No biometrics enrolled on this device");
    }

    return isAuthenticated;
  }

  void authenticate() async {
    try{
      bool isAuthenticated = await authenticateWithBiometrics();
      if(isAuthenticated){
        if(from == 0){
          Get.to(() => MnemonicChecklistView(),
              arguments: {
                "public_address" : publicAddress,
                "mnemonic_list" : mnemonicLists,
                "account" : account,
                "passcode" : passcode,
                "face_id" : true
              }
          );
        }
        else if(from == 10){
          Get.offAll(() => HomeView());
        }
        else{
          Get.to(() => ImportAccountView(), arguments: {
            "passcode" : passcode,
            "face_id" : true});
        }
      }
    }
    catch(e){
      CustomToast.showToast("No biometrics enrolled on this device");
    }
  }
}
