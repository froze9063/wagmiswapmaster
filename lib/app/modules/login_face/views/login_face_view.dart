import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';

import '../controllers/login_face_controller.dart';

class LoginFaceView extends GetView<LoginFaceController> {

  LoginFaceController loginFaceController = Get.put(LoginFaceController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(25, 29, 52, 1.0),
                  Color.fromRGBO(16, 15, 31, 1.0)
                ]
            )
        ),
        child: Stack(
          children: [
            Column(
              children: [
                SizedBox(height: 24),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(width: 3),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: 55,
                        height: 55,
                        child: loginFaceController.from != 10 ? Center(
                          child: Image.asset("assets/icons/ic_white_back.png", width: 24,
                              height: 24, fit: BoxFit.contain),
                        ) : SizedBox(),
                      ),
                      onTap: (){
                        if(loginFaceController.from != 10){
                          Get.back();
                        }
                      },
                    ),
                    Expanded(child: SizedBox(),flex: 1),
                    Image.asset("assets/images/img_hori_messe.png", width: 182,
                        height: 23, fit: BoxFit.contain),
                    Expanded(child: SizedBox(),flex: 1),
                    Container(
                      width: 55,
                      height: 55,
                    ),
                    SizedBox(width: 3)
                  ],
                ),

                SizedBox(height: 18),

                Text(
                  loginFaceController.from != 10 ? StringContent.setup_face : StringContent.face_id.toUpperCase(),
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontFamily: "BarlowSemiBold",
                      letterSpacing: 2
                  ),
                ),
              ],
            ),

            Center(
              child: Image.asset("assets/images/img_login_face.png", height: 60,
                  width: 60),
            )
          ],
        ),
      ),
    );
  }
}
