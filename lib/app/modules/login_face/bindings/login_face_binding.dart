import 'package:get/get.dart';

import '../controllers/login_face_controller.dart';

class LoginFaceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginFaceController>(
      () => LoginFaceController(),
    );
  }
}
