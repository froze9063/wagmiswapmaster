import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';

import '../controllers/mnemonic_verify_controller.dart';

class MnemonicVerifyView extends GetView<MnemonicVerifyController> {

  final MnemonicVerifyController _mnemonicVerifyController = Get.put(MnemonicVerifyController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GetBuilder<MnemonicVerifyController>(
        id: "theme",
        init: MnemonicVerifyController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Column(
            children: [
              SizedBox(height: 24),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 3),
                  GestureDetector(
                    child: Container(
                      color: Colors.transparent,
                      width: 55,
                      height: 55,
                      child: Center(
                        child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                        "assets/icons/ic_black_back.png", width: 24,
                            height: 24, fit: BoxFit.contain),
                      ),
                    ),
                    onTap: (){
                      Get.back();
                    },
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Image.asset("assets/images/img_hori_messe.png", width: 182,
                      height: 23, fit: BoxFit.contain),
                  Expanded(child: SizedBox(),flex: 1),
                  Container(
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(width: 3)
                ],
              ),

              SizedBox(height: 18),

              Text(
                StringContent.your_verify,
                style: TextStyle(
                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                    fontSize: 14,
                    fontFamily: "BarlowSemiBold",
                    letterSpacing: 2
                ),
              ),

              SizedBox(height: 55),

              GetBuilder<MnemonicVerifyController>(
                  id: "focus_node",
                  init: MnemonicVerifyController(),
                  builder: (focusValue) => ListView.builder(itemBuilder: (context, index){
                    Map mnemonicMap = _mnemonicVerifyController.mnemonicMapLists[index];
                    return Container(
                      margin: EdgeInsets.only(top: index == 0 ? 0 : 24),
                      width: double.maxFinite,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              SizedBox(width: 24),
                              Expanded(child: Text(
                                "${StringContent.enter_word} #${mnemonicMap["index"]}",
                                style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    height: 1.5
                                ),
                              ), flex: 1),
                              SizedBox(width: 24)
                            ],
                          ),

                          SizedBox(height: 8),

                          GestureDetector(
                            child: Container(
                              margin: EdgeInsets.only(left: 24, right: 24),
                              padding: EdgeInsets.only(left: 16, right: 16),
                              width: double.maxFinite,
                              height: 35,
                              decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.all(Radius.circular(8)),
                                  border: Border.all(
                                      width: 1,
                                      color: focusValue.focusIndex == index ? themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy : Color.fromRGBO(164,164,164,1)
                                  )
                              ),
                              child: Center(
                                child: TextField(
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                  ),
                                  decoration: InputDecoration.collapsed(hintText: StringContent.enter_word,
                                      hintStyle: TextStyle(
                                        color: Color.fromRGBO(164,164,164,1),
                                        fontSize: 12,
                                        fontFamily: "BarlowRegular",
                                      )
                                  ),
                                  inputFormatters: [FilteringTextInputFormatter.allow(RegExp("[a-z]"))],
                                  controller: mnemonicMap["editing"],
                                  onChanged: (text){
                                    _mnemonicVerifyController.setButtonState();
                                  },
                                  focusNode: mnemonicMap["focus_node"],
                                ),
                              ),
                            ),
                            onTap: (){
                              (_mnemonicVerifyController.mnemonicMapLists[index]["focus_node"] as FocusNode).requestFocus();
                            },
                          ),
                        ],
                      ),
                    );
                  },
                    itemCount: _mnemonicVerifyController.mnemonicMapLists.length,
                    shrinkWrap: true,
                    padding: EdgeInsets.zero,
                  )),

              GetBuilder<MnemonicVerifyController>(
                  id: "wrong",
                  init: MnemonicVerifyController(),
                  builder: (value) => Visibility(child: Container(
                    margin: EdgeInsets.only(left: 24, top: 36, right: 24),
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(6)),
                        border: Border.all(
                            color: Color.fromRGBO(241, 83, 83, 1),
                            width: 1.5
                        )
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Image.asset("assets/icons/ic_alert.png", width: 16, height: 16),
                            SizedBox(width: 4),
                            Text(
                              StringContent.error,
                              style: TextStyle(
                                  color: Color.fromRGBO(241, 83, 83, 1),
                                  fontSize: 14,
                                  fontFamily: "BarlowSemiBold",
                                  letterSpacing: 2
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                        Text(
                          StringContent.youve_entered,
                          style: TextStyle(
                              color: Color.fromRGBO(241, 83, 83, 1),
                              fontSize: 12,
                              fontFamily: "BarlowSemiBold",
                              letterSpacing: 2
                          ),
                        ),
                      ],
                    ),
                  ), visible: value.isWrong)),

              Expanded(child: SizedBox(), flex: 1),

              GetBuilder<MnemonicVerifyController>(
                  id: "button_state",
                  init: MnemonicVerifyController(),
                  builder: (value) => Container(
                    width: double.maxFinite,
                    margin: EdgeInsets.only(left: 24, right: 24),
                    height: 37,
                    child: Stack(
                      children: [
                        CustomButton(45, double.maxFinite, BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.topRight,
                              colors: [
                                value.isContinue ? Color.fromRGBO(106, 48, 147, 1.0) : Color.fromRGBO(89, 89, 89, 1.0),
                                value.isContinue ? Color.fromRGBO(160, 68, 255, 1.0) : Color.fromRGBO(89, 89, 89, 1.0)
                              ]
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ), StringContent.confirm, TextStyle(
                            color: value.isContinue ? Colors.white : Color.fromRGBO(151, 151, 151, 1.0),
                            fontFamily: "BarlowRegular",
                            fontSize: 12,
                            letterSpacing: 2
                        )),

                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.transparent,
                            onTap: (){
                              if(_mnemonicVerifyController.isContinue){
                                _mnemonicVerifyController.setButtonClick();
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  )),

              SizedBox(height: StringContent.bottomMargin)
            ],
          ),
        ),
      ),
    );
  }
}

