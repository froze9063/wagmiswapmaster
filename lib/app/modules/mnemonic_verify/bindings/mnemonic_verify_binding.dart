import 'package:get/get.dart';

import '../controllers/mnemonic_verify_controller.dart';

class MnemonicVerifyBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MnemonicVerifyController>(
      () => MnemonicVerifyController(),
    );
  }
}
