import 'dart:io';
import 'dart:math';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/account_created/views/account_created_view.dart';
import 'package:waagmiswap/app/modules/home/views/home_view.dart';
import 'package:waagmiswap/app/widgets/custom_toast.dart';

class MnemonicVerifyController extends GetxController {

  late DatabaseDAO databaseDAO;

  final count = 0.obs;

  bool isContinue = false;
  bool isWrong = false;

  late List<String> mnemonicLists;
  late List<Map> mnemonicMapLists;

  String publicAddress = "";
  String userName = "";
  String passcode = "";
  String mnemonic = "";
  Account? account;

  int focusIndex = -1;

  bool isFromHome = false;
  bool faceId = false;

  int themeType = 0;
  bool isButtonClicked = false;

  @override
  void onInit() {
    if(Platform.isAndroid){
      FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
    }

    databaseDAO = DatabaseDAO();
    mnemonicMapLists = [];

    if(Get.arguments["public_address"] != null){
      publicAddress = Get.arguments["public_address"] ?? "";
    }
    if(Get.arguments["mnemonic_list"] != null){
      mnemonicLists = Get.arguments["mnemonic_list"];
    }
    if(Get.arguments["account"] != null){
      account = Get.arguments["account"];
    }
    if(Get.arguments["user_name"] != null){
      userName = Get.arguments["user_name"];
    }
    if(Get.arguments["passcode"] != null){
      passcode = Get.arguments["passcode"];
    }
    if(Get.arguments["mnemonic"] != null){
      mnemonic = Get.arguments["mnemonic"];
    }
    if(Get.arguments["from_home"] != null){
      isFromHome = Get.arguments["from_home"];
    }
    if(Get.arguments["face_id"] != null){
      faceId = Get.arguments["face_id"];
    }

    if(isFromHome){
      loadUser();
    }

    generateRandom();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  void generateRandom(){
    var rng = Random();
    var randomizeNumber = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
      20,21,22,23,24,25];

    var firstRandomIndex = rng.nextInt(randomizeNumber.length - 1);
    var index1 = randomizeNumber[firstRandomIndex];
    String name1 = mnemonicLists[index1 - 1];
    randomizeNumber.removeAt(firstRandomIndex);

    var secondRandomIndex = rng.nextInt(randomizeNumber.length - 1);
    var index2 = randomizeNumber[secondRandomIndex];
    String name2 = mnemonicLists[index2 - 1];
    randomizeNumber.removeAt(secondRandomIndex);

    var thirdRandomIndex = rng.nextInt(randomizeNumber.length - 1);
    var index3 = randomizeNumber[thirdRandomIndex];
    String name3 = mnemonicLists[index3 - 1];
    randomizeNumber.removeAt(thirdRandomIndex);

    TextEditingController textEditingController1 = TextEditingController();
    TextEditingController textEditingController2 = TextEditingController();
    TextEditingController textEditingController3 = TextEditingController();

    FocusNode focusNode1 = FocusNode();
    focusNode1.addListener(() {
      focusIndex = 0;
      update(["focus_node"]);
    });

    FocusNode focusNode2 = FocusNode();
    focusNode2.addListener(() {
      focusIndex = 1;
      update(["focus_node"]);
    });

    FocusNode focusNode3 = FocusNode();
    focusNode3.addListener(() {
      focusIndex = 2;
      update(["focus_node"]);
    });

    Map map1 = {};
    map1["index"] = index1;
    map1["name"] = name1;
    map1["editing"] = textEditingController1;
    map1["focus_node"] = focusNode1;

    Map map2 = {};
    map2["index"] = index2;
    map2["name"] = name2;
    map2["editing"] = textEditingController2;
    map2["focus_node"] = focusNode2;

    Map map3 = {};
    map3["index"] = index3;
    map3["name"] = name3;
    map3["editing"] = textEditingController3;
    map3["focus_node"] = focusNode3;

    mnemonicMapLists.add(map1);
    mnemonicMapLists.add(map2);
    mnemonicMapLists.add(map3);
  }

  void setButtonState(){
    for(int i = 0; i < mnemonicMapLists.length; i++){
      Map map = mnemonicMapLists[i];
      TextEditingController textEditingController = map["editing"];
      if(textEditingController.text.isEmpty){
        isContinue = false;
        update(["button_state"]);
        return;
      }
    }
    isContinue = true;

    if(isButtonClicked){
      setWrong();
    }

    update(["button_state"]);
  }

  void setWrong(){
    for(int i = 0; i < mnemonicMapLists.length; i++){
      Map map = mnemonicMapLists[i];
      String name = map["name"] ?? "";
      TextEditingController textEditingController = map["editing"];

      if(textEditingController.text.toString().toLowerCase() != name.toLowerCase()){
        isWrong = true;
        update(["wrong"]);
        return;
      }
    }

    isWrong = false;
    update(["wrong"]);
  }

  void setButtonClick(){
    isButtonClicked = true;
    for(int i = 0; i < mnemonicMapLists.length; i++){
      Map map = mnemonicMapLists[i];
      String name = map["name"] ?? "";
      TextEditingController textEditingController = map["editing"];

      if(textEditingController.text.toString().toLowerCase() != name.toLowerCase()){
        isWrong = true;
        update(["wrong"]);
        return;
      }
    }

    isWrong = false;
    update(["wrong"]);

    saveDatabase();
  }

  void saveDatabase(){
    UserModel userModel = UserModel(userName: userName, address: publicAddress,
        passcode: passcode, mnemonic: mnemonic, url: MyConstant.USED_URL, amount: 0,
        amountWithoutPendingRewards: 0, pendingRewards: 0,rewardBase: 0,rewards: 0,round: 0
        ,status: "",price: "", masterAddress: "", accountType: "standard", isActiveAccount: false,
        backedUp: false
    );

    databaseDAO.getAllSorted("user_name",userName).then((value) {
      if(value.isNotEmpty){
        CustomToast.showToast(StringContent.user_exist);
      }
      else{
        databaseDAO.insert(userModel).then((value) {
          if(isFromHome){
            Get.offAll(()=> HomeView(), arguments: {"import" : true});
          }
          else{
            SharedPreferences.getInstance().then((value) {
              value.setBool("face_id", faceId);
              goToAccountCreated();
            });
          }
        });
      }
    });
  }

  void goToAccountCreated(){
    SharedPreferences.getInstance().then((prefs){
      prefs.setString("passcode",passcode);
      Get.to(() => AccountCreatedView());
    });
  }
}
