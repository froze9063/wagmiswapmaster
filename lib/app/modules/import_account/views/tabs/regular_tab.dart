import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/import_account/controllers/import_account_controller.dart';

class RegularTab extends StatelessWidget {

  final ImportAccountController _importAccountController;
  RegularTab(this._importAccountController);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ImportAccountController>(
      id: "theme",
      init: ImportAccountController(),
      builder: (themeValue) => Container(
      width: double.maxFinite,
      height: double.maxFinite,
      child: Column(
        children: [
          SizedBox(height: 24),

          Row(
            children: [
              SizedBox(width: 24),
              Expanded(child: Text(
                StringContent.account_name,
                style: TextStyle(
                    color: themeValue.themeType == 0 ?
                    ThemeConstant.white : ThemeConstant.blackNavy,
                    fontSize: 12,
                    fontFamily: "BarlowRegular",
                    height: 1.5
                ),
              ), flex: 1),
              SizedBox(width: 24)
            ],
          ),

          SizedBox(height: 8),

          GetBuilder<ImportAccountController>(
              id: "account_focus",
              init: ImportAccountController(),
              builder: (value) => GestureDetector(
                child: Container(
                  margin: EdgeInsets.only(left: 24, right: 24),
                  padding: EdgeInsets.only(left: 16, right: 16),
                  width: double.maxFinite,
                  height: 35,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      border: Border.all(
                          width: 1,
                          color: value.isAccountFocus ? themeValue.themeType == 0 ? Colors.white : ThemeConstant.blackNavy : Color.fromRGBO(164,164,164,1)
                      )
                  ),
                  child: Center(
                    child: TextField(
                      controller: value.accountEditingController,
                      style: TextStyle(
                        color: themeValue.themeType == 0 ? Colors.white : ThemeConstant.blackNavy,
                        fontSize: 12,
                        fontFamily: "BarlowRegular",
                      ),
                      decoration: InputDecoration.collapsed(hintText: StringContent.enter_account_name,
                          hintStyle: TextStyle(
                            color: Color.fromRGBO(164,164,164,1),
                            fontSize: 12,
                            fontFamily: "BarlowRegular",
                          )
                      ),
                      focusNode: _importAccountController.accountFocus,
                    ),
                  ),
                ),
                onTap: (){
                  value.accountFocus.requestFocus();
                },
              )),

          GetBuilder<ImportAccountController>(
              id: "error",
              init: ImportAccountController(),
              builder: (value) => Visibility(child: Column(
                children: [
                  SizedBox(height: 4),
                  Row(
                    children: [
                      SizedBox(width: 24),
                      Expanded(child: Text(
                        value.isSameName ? StringContent.name_used :
                        StringContent.account_name_error,
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 12,
                            fontFamily: "BarlowRegular",
                            height: 1.5
                        ),
                      ), flex: 1),
                      SizedBox(width: 24)
                    ],
                  )
                ],
              ), visible: value.isErrorShowed)),

          SizedBox(height: 24),

          Row(
            children: [
              SizedBox(width: 24),
              Text(
                StringContent.mnemonic,
                style: TextStyle(
                  color: themeValue.themeType == 0 ?
                  ThemeConstant.white : ThemeConstant.blackNavy,
                  fontSize: 12,
                  fontFamily: "BarlowRegular",
                ),
              )
            ],
          ),

          GetBuilder<ImportAccountController>(
              id: "mnemonic",
              init: ImportAccountController(),
              builder: (value) => Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.only(left: 24, right: 24, top: 8),
                padding: EdgeInsets.all(8),
                width: double.maxFinite,
                height: 150,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    border: Border.all(
                        color: themeValue.themeType == 0 ? Colors.white : Color.fromRGBO(106, 48, 147, 1.0),
                        width: 0.5
                    )
                ),
                child: Stack(
                  children: [
                    Visibility(child: Row(
                      children: [
                        Expanded(child: TextField(
                          keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
                          inputFormatters: [FilteringTextInputFormatter.allow(RegExp("[a-z ]"))],
                          maxLines: null,
                          minLines: null,
                          expands: true,
                          style: TextStyle(
                            color: themeValue.themeType == 0 ? Colors.white : ThemeConstant.blackNavy,
                            fontSize: 12,
                            fontFamily: "BarlowRegular",
                          ),
                          decoration: InputDecoration.collapsed(hintText: StringContent.mnemonic_hint,
                              hintStyle: TextStyle(
                                color: Color.fromRGBO(164,164,164,1),
                                fontSize: 12,
                                fontFamily: "BarlowRegular",
                              )
                          ),
                          controller: value.mnemonicEditingController,
                          onChanged: (text){
                            _importAccountController.importAccount(MyConstant.TESTNET_URL);
                          },
                        ),
                        )
                      ],
                    ), visible: value.isPasted),
                    Visibility(child: Center(
                      child: GestureDetector(
                        child: Container(
                          width: double.maxFinite,
                          height: 55,
                          color: Colors.transparent,
                          child: Center(
                            child: Text(
                              StringContent.paste_mnemonic,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: ThemeConstant.secondTextGrayDark,
                                fontSize: 12,
                                fontFamily: "BarlowRegular",
                              ),
                            ),
                          ),
                        ),
                        onTap: (){
                          value.paste();
                        },
                      ),
                    ), visible: !value.isPasted)
                  ],
                ),
              )),

          GetBuilder<ImportAccountController>(
              id: "mnemonic_error",
              init: ImportAccountController(),
              builder: (value) => Visibility(child: Column(
                children: [
                  SizedBox(height: 4),
                  Row(
                    children: [
                      SizedBox(width: 24),
                      Expanded(child: Text(
                        value.mnemonicErrorMessage,
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 12,
                            fontFamily: "BarlowRegular",
                            height: 1.5
                        ),
                      ), flex: 1),
                      SizedBox(width: 24)
                    ],
                  )
                ],
              ), visible: value.mnemonicError)),

          Expanded(child: SizedBox(), flex: 1),

          GetBuilder<ImportAccountController>(
              id: "account",
              init: ImportAccountController(),
              builder: (accountValue) => Container(
                width: double.maxFinite,
                margin: EdgeInsets.only(left: 24, right: 24),
                height: 37,
                child: Stack(
                  children: [
                    CustomButton(37, double.maxFinite, BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.topRight,
                          colors: [
                            accountValue.selectedAccount != null ? Color.fromRGBO(106, 48, 147, 1.0)
                                : Color.fromRGBO(89, 89, 89, 1.0),
                            accountValue.selectedAccount != null ? Color.fromRGBO(160, 68, 255, 1.0)
                                : Color.fromRGBO(89, 89, 89, 1.0)
                          ]
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ), StringContent.import, TextStyle(
                        color: Colors.white,
                        fontFamily: "BarlowRegular",
                        fontSize: 12,
                        letterSpacing: 2
                    )),

                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        onTap: (){
                          _importAccountController.import();
                        },
                      ),
                    )
                  ],
                ),
              )),

          SizedBox(height: StringContent.bottomMargin)
        ],
      ),
    ));
  }
}