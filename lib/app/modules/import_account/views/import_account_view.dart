import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/modules/import_account/views/tabs/regular_tab.dart';

import '../controllers/import_account_controller.dart';

class ImportAccountView extends GetView<ImportAccountController> {

  final ImportAccountController _importAccountController = Get.put(ImportAccountController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GetBuilder<ImportAccountController>(
        id: "theme",
        init: ImportAccountController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: GetBuilder<ImportAccountController>(
              id: "tabs",
              init: ImportAccountController(),
              builder: (value) => Column(
                children: [
                  SizedBox(height: _importAccountController.from == 1 ? 30 : 40),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Visibility(child: Row(
                        children: [
                          SizedBox(width: 3),
                          GestureDetector(
                            child: Container(
                              color: Colors.transparent,
                              width: 55,
                              height: 55,
                              child: Center(
                                child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                                "assets/icons/ic_black_back.png", width: 24,
                                    height: 24, fit: BoxFit.contain),
                              ),
                            ),
                            onTap: (){
                              Get.back();
                            },
                          )
                        ],
                      ), visible: _importAccountController.from == 1),
                      Expanded(child: SizedBox(),flex: 1),
                      Image.asset("assets/images/img_hori_messe.png", width: 182,
                          height: 23, fit: BoxFit.contain),
                      Expanded(child: SizedBox(),flex: 1),
                      Visibility(child: Row(
                        children: [
                          Container(
                            width: 55,
                            height: 55,
                          ),
                          SizedBox(width: 3)
                        ],
                      ), visible: _importAccountController.from == 1)
                    ],
                  ),

                  SizedBox(height: _importAccountController.from == 1 ? 18 : 24),

                  Text(
                    StringContent.import_account,
                    style: TextStyle(
                      color: themeValue.themeType == 0 ?
                      ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 14,
                      fontFamily: "BarlowSemiBold",
                    ),
                  ),

                  Expanded(child: PageView(
                    children: [
                      RegularTab(_importAccountController)
                    ],
                  ),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
