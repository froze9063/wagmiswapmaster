import 'dart:io';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/algorand/algorand.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/account_created/views/account_created_view.dart';
import 'package:waagmiswap/app/modules/home/views/home_view.dart';

class ImportAccountController extends GetxController {

  AlgorandRepository algorandRepository = AlgorandRepository();

  late Algorand algorand;
  late DatabaseDAO databaseDAO;

  final count = 0.obs;

  int tabPosition = 0;

  late FocusNode accountFocus;

  bool isAccountFocus = false;
  bool isPasted = false;
  bool isFileImported = false;

  String mnemonicPhrase = "";
  String strPasscode = "";
  String mnemonicErrorMessage = "";

  bool isSameName = false;
  bool isErrorShowed = false;

  late TextEditingController accountEditingController;
  late TextEditingController mnemonicEditingController;
  late TextEditingController managedMnemonicEditingController;

  int from = 0;

  File? myFile;
  String fileName = "";

  String importedAddress = "";
  String importedMnemonic = "";
  String importedName = "";
  String importedUrl = "";

  Account? selectedAccount;
  bool mnemonicError = false;
  bool faceId = false;

  List<UserModel> subUserModelList = [];

  int themeType = 0;

  @override
  void onInit() {
    algorand = algorandRepository.getAlgorandConfig("${MyConstant.USED_URL}/algod",
        "${MyConstant.USED_URL}/idx");

    if(Get.arguments != null){
      strPasscode = Get.arguments["passcode"];
      from = Get.arguments["from"] ?? 0;
      faceId = Get.arguments["face_id"] ?? false;
    }

    if(from == 1){
      loadUser();
    }

    databaseDAO = DatabaseDAO();
    mnemonicEditingController = TextEditingController();
    accountEditingController = TextEditingController();
    managedMnemonicEditingController = TextEditingController();
    accountFocus = FocusNode();
    accountFocus.addListener(onAccountFocus);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  void onAccountFocus(){
    isAccountFocus = true;
    update(["account_focus"]);
  }

  void paste(){
    FlutterClipboard.paste().then((value) {
      String stra = value.replaceAll("[", "");
      String strb = stra.replaceAll("]", "");
      isPasted = true;
      mnemonicEditingController.text = strb.replaceAll(",", "").toLowerCase().replaceAll(RegExp(r"[^\s\w]"), "");
      importAccount(MyConstant.TESTNET_URL);
      update(["mnemonic"]);
    });
  }

  Future<void> importAccount(String url) async {
    String strPhrase = mnemonicEditingController.text.toString().trim();

    AlgorandRepository algorandRepository = AlgorandRepository();
    var algorand = algorandRepository.getAlgorandConfig("${MyConstant.USED_URL}/algod",
        "${MyConstant.USED_URL}/idx");

    await algorand.restoreAccount(strPhrase.split(" ")).then((value) {
      mnemonicError = false;
      selectedAccount = value;
      update(["account","mnemonic_error"]);

    }).catchError((error, stackTrace) {
      var mnemonicError = error as MnemonicException;
      selectedAccount = null;

      if(mnemonicError.message == "Wrong key length"){
        mnemonicErrorMessage = "Wrong Mnnemonic length";
      }
      else{
        mnemonicErrorMessage = mnemonicError.message;
      }

      this.mnemonicError = true;
      update(["account","mnemonic_error"]);

      return null;
    });
  }

  Future<void> import() async {
    String accountName = accountEditingController.text.toString().trim();
    String strPhrase = mnemonicEditingController.text.toString().trim();

    if(selectedAccount != null){
      if(accountName.length < 5){
        isSameName = false;
        isErrorShowed = true;
        update(["error"]);
        return;
      }
      else{
        var users = await databaseDAO.getAllSorted("user_name",accountName);

        if(users.isNotEmpty){
          isSameName = true;
          isErrorShowed = true;
          update(["error"]);
          return;
        }
        else{
          isSameName = false;
          isErrorShowed = false;
        }
      }

      String publicAddress = selectedAccount!.publicAddress;

      saveDatabase(accountName,publicAddress,strPasscode,strPhrase,MyConstant.USED_URL,"standard");
    }
  }

  Future<void> saveDatabase(String userName, String publicAddress, String passcode,
      String mnemonic, String url, String accountType) async {

    UserModel userModel = UserModel(userName: userName, address: publicAddress,
        passcode: passcode, mnemonic: mnemonic, url: url, amount: 0, amountWithoutPendingRewards: 0,
        pendingRewards: 0,rewardBase: 0,rewards: 0,round: 0,status: "",price: "", masterAddress: "",
        accountType: accountType, isActiveAccount: false, backedUp: false);

    if(accountType == "managed"){
      for(int i = 0 ; i < subUserModelList.length; i++){
        UserModel subUserModel = subUserModelList[i];
        await databaseDAO.insertSubAccount(subUserModel);
      }
    }

    databaseDAO.getAllSorted("address",publicAddress).then((value) {
      if(value.isNotEmpty){
        databaseDAO.update(userModel).then((value) {
          continuePage(passcode);
        });
      }
      else{
        databaseDAO.insert(userModel).then((value) {
          continuePage(passcode);
        });
      }
    });
  }

  void continuePage(String passcode){
    SharedPreferences.getInstance().then((prefs){
      if(from != 1){
        prefs.setString("passcode",passcode);
        prefs.setBool("face_id",faceId);
        Get.to(() => AccountCreatedView(), arguments: {"from" : 1});
      }
      else{
        Get.offAll(() => HomeView(), arguments: {"import" : true});
      }
    });
  }
}
