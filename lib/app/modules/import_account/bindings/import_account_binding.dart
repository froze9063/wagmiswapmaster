import 'package:get/get.dart';

import '../controllers/import_account_controller.dart';

class ImportAccountBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ImportAccountController>(
      () => ImportAccountController(),
    );
  }
}
