import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';

class FaqController extends GetxController {

  int faqSelectedPos = -1;
  int themeType = 0;
  int tabPosition = 1;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late LedgerModel selectedLedger;

  @override
  void onInit() {
    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void setSelectedPos(int index){
    this.faqSelectedPos = index;
    update(["selected_pos"]);
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }
}
