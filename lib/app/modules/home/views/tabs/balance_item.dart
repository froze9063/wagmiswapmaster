import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:waagmiswap/app/callback/my_assets_callback.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/add_asset/views/add_asset_view.dart';
import 'package:waagmiswap/app/modules/home/controllers/home_controller.dart';
import 'package:waagmiswap/app/modules/receive/views/receive_view.dart';
import 'package:waagmiswap/app/util/my_util.dart';

class BalanceItem extends StatelessWidget implements MyAssetCallback{

  HomeController _homeController;
  BalanceItem(this._homeController);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        id: "theme",
        init: HomeController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          child: Column(
            children: [
              SizedBox(height: 24),

              GetBuilder<HomeController>(
                  id: "slider",
                  init: HomeController(),
                  builder: (sliderValue) => Container(
                    width: double.maxFinite,
                    height: 73.0,
                    child: Stack(
                      children: [
                        CarouselSlider(
                          carouselController: sliderValue.carouselController,
                          options: CarouselOptions(
                              height: 73.0,
                              aspectRatio: 100,
                              viewportFraction: 0.9,
                              initialPage: sliderValue.selectedUserPosition,
                              enableInfiniteScroll: false,
                              onPageChanged: (index,carousel){
                                themeValue.changePage(index);
                              }
                          ),
                          items: sliderValue.userModelList.map((i) {
                            UserModel userModel = i;
                            return Builder(
                              builder: (BuildContext context) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(6)),
                                      border: Border.all(
                                          color: Color.fromRGBO(184,115,255,1),
                                          width: 1
                                      )
                                  ),
                                  child: Stack(
                                    children: [
                                      ClipRRect(
                                        child: Image.asset("assets/images/ic_wallet_bg.png", width: double.maxFinite,
                                            height: double.maxFinite, fit: BoxFit.cover),
                                        borderRadius: BorderRadius.all(Radius.circular(6)),
                                      ),
                                      Center(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            SizedBox(height: 6),
                                            Row(
                                              children: [
                                                Expanded(child: SizedBox(), flex: 1),

                                                SvgPicture.asset("assets/icons/ic_folder_management.svg", height: 16, width: 16),

                                                SizedBox(width: 6),

                                                Text(
                                                  userModel.userName,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontFamily: "BarlowNormal",
                                                      fontWeight: FontWeight.w600
                                                  ),
                                                ),

                                                Expanded(child: SizedBox(), flex: 1),
                                              ],
                                            ),

                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  MyUtils.parseAddress(userModel.address),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 12,
                                                      fontFamily: "BarlowNormal",
                                                      fontWeight: FontWeight.w300,
                                                      letterSpacing: 1
                                                  ),
                                                ),

                                                GestureDetector(
                                                  child: Container(
                                                    child: Center(child: SvgPicture.asset("assets/icons/ic_qr_code.svg", height: 16, width: 16)),
                                                    padding: EdgeInsets.only(left: 6, right: 16, top: 12, bottom: 12),
                                                    color: Colors.transparent,
                                                  ),
                                                  onTap: (){
                                                    Get.to(() => ReceiveView(), arguments: {
                                                      "user_model" : _homeController.selectedUserModel,
                                                      "ledger_model" : _homeController.selectedLedger
                                                    });
                                                  },
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              },
                            );
                          }).toList(),
                        )
                      ],
                    ),
                  )),

              SizedBox(height: 6),

              GetBuilder<HomeController>(
                  id: "slider",
                  init: HomeController(),
                  builder: (sliderValue) => Container(
                    width: double.maxFinite,
                    height: 10,
                    child: Center(
                      child: ListView.builder(itemBuilder: (context, index){
                        return Row(
                          children: [
                            SizedBox(width: index == 0 ? 0 : 4),
                            CircleAvatar(
                              radius: 3,
                              backgroundColor: index == sliderValue.selectedUserPosition ?
                              Color.fromRGBO(164, 164, 164, 1.0)
                                  : Color.fromRGBO(79, 79, 79, 1.0),
                            )
                          ],
                        );
                      },
                        itemCount: sliderValue.userModelList.length,
                        shrinkWrap: true,
                        padding: EdgeInsets.zero,
                        primary: false,
                        scrollDirection: Axis.horizontal,
                      ),
                    ),
                  )),

              SizedBox(height: 16),
              Row(
                children: [
                  SizedBox(width: 24),
                  GetBuilder<HomeController>(
                      id: "sort_by",
                      init: HomeController(),
                      builder: (sortValue) => GestureDetector(
                        child: Container(
                          padding: EdgeInsets.only(left: 8, right: 8, top: 6, bottom: 6),
                          decoration: BoxDecoration(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              border:Border.all(
                                  color: themeValue.themeType == 0 ? ThemeConstant.darkPurple
                                      : ThemeConstant.lightPurple,
                                  width: 1
                              )
                          ),
                          child: Row(
                            children: [
                              Text(
                                sortValue.walletNetworkLists.isNotEmpty ? sortValue.walletNetworkLists[sortValue.selectedLedgerPosition][themeValue.selectedUserPosition]["sort_name"].isEmpty ?
                                StringContent.sort_by : sortValue.walletNetworkLists[sortValue.selectedLedgerPosition][themeValue.selectedUserPosition]["sort_name"] : StringContent.sort_by,
                                style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.darkPurple
                                        : ThemeConstant.lightPurple,
                                    fontSize: 12,
                                    fontFamily: "BarlowNormal",
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 1
                                ),
                              ),
                              SizedBox(width: 6),
                              SvgPicture.asset("assets/icons/ic_purple_dropdown.svg", width: 8, height: 8)
                            ],
                          ),
                        ),
                        onTap: (){
                          _homeController.isShowDialog(true,5);
                        },
                      )),
                  Expanded(child: SizedBox(), flex: 1),
                  Container(
                    width: 101,
                    margin: EdgeInsets.only(left: 24, right: 24),
                    height: 25,
                    child: Stack(
                      children: [
                        CustomButton(25, double.maxFinite, BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.topRight,
                              colors: [
                                Color.fromRGBO(106, 48, 147, 1.0),
                                Color.fromRGBO(160, 68, 255, 1.0)
                              ]
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ), StringContent.my_asset, TextStyle(
                            color: Colors.white,
                            fontFamily: "BarlowRegular",
                            fontWeight: FontWeight.w600,
                            fontSize: 12,
                            letterSpacing: 2
                        )),

                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.transparent,
                            onTap: (){
                              Get.to(() => AddAssetView(), arguments: {
                                "user_model" : _homeController.selectedUserModel,
                                "ledger_model" : _homeController.selectedLedger,
                                "disable_list" : _homeController.walletNetworkLists[_homeController.selectedLedgerPosition]
                                [_homeController.selectedUserPosition]["disable_list"],
                                "my_asset_callback" : this
                              });/*?.then((value) {
                                _homeController.getAssets();
                              });*/
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),

              SizedBox(height: 16),

              GetBuilder<HomeController>(
                id: "assets",
                init: HomeController(),
                builder: (assetValue) => Expanded(child: RefreshIndicator(
                  color: Color.fromRGBO(106, 48, 147, 1.0),
                  onRefresh: ()=> assetValue.getAssets(),
                  child: Container(
                    width: double.maxFinite,
                    height: double.maxFinite,
                    child: Stack(
                      children: [
                        Visibility(child: Center(
                          child: Text(
                            "No assets",
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              fontSize: 14,
                              fontFamily: "BarlowMedium",
                            ),
                          ),
                        ), visible: assetValue.walletNetworkLists.isNotEmpty ? assetValue.walletNetworkLists[assetValue.selectedLedgerPosition].isNotEmpty ? assetValue.walletNetworkLists[assetValue.selectedLedgerPosition][_homeController.selectedUserPosition][
                        "searched_list"].isEmpty && !assetValue.walletNetworkLists[assetValue.selectedLedgerPosition][_homeController.selectedUserPosition
                        ]["loading"] : false : false),

                        Visibility(child: Center(
                          child: CircularProgressIndicator(
                            color: Color.fromRGBO(106, 48, 147, 1.0),
                          ),
                        ), visible: assetValue.walletNetworkLists.isNotEmpty ? assetValue.walletNetworkLists[assetValue.selectedLedgerPosition].isNotEmpty ? assetValue.walletNetworkLists[assetValue.selectedLedgerPosition][_homeController.selectedUserPosition][
                        "loading"] : true : true),

                        assetValue.walletNetworkLists.isNotEmpty ? assetValue.walletNetworkLists[assetValue.selectedLedgerPosition].isNotEmpty ? !assetValue.walletNetworkLists[assetValue.selectedLedgerPosition][_homeController.selectedUserPosition]["loading"] ? Container(
                          width: double.maxFinite,
                          height: double.maxFinite,
                          child: ListView.builder(
                            itemBuilder: (context, index){
                              Map assetMap = assetValue.walletNetworkLists[assetValue.selectedLedgerPosition][_homeController.selectedUserPosition][
                              "searched_list"][index];

                              bool isPending = assetMap["is_pending"] ?? false;
                              String statusName = assetMap["status_name"] ?? "";

                              AssetsModel assetModel = assetMap["asset_model"];
                              AssetsDetailModel? assetDetailModel;

                              if(assetMap["asset_detail_model"] != null){
                                assetDetailModel = assetMap["asset_detail_model"];
                              }

                              return !isPending ? Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  splashColor: Colors.transparent,
                                  child: Container(
                                    padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                                    width: double.maxFinite,
                                    decoration: BoxDecoration(
                                      color: Colors.transparent,
                                      borderRadius: BorderRadius.all(Radius.circular(6)),
                                    ),
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                            child: assetDetailModel != null ? !assetDetailModel.unitName.contains("-") ? assetModel.assetId != 0 ? MyUtils.getCuratedIconPath(assetDetailModel.unitName, themeValue.themeType).isEmpty ? SvgPicture.network("https://wallet.wagmiswap.io/testnet/"
                                                "assets/${assetModel.assetId}.svg",height: 40, width: 40) : SvgPicture.asset(MyUtils.getCuratedIconPath(assetDetailModel.unitName, themeValue.themeType), height: 40, width: 40) : SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_dark_algo.svg" : "assets/icons/ic_light_algo.svg",
                                                height: 40, width: 40) : SizedBox() : SizedBox(),
                                            radius: 18,
                                            backgroundColor: assetDetailModel != null ? MyUtils.getCuratedIconPath(assetDetailModel.unitName, themeValue.themeType).isEmpty ? ThemeConstant.secondTextGrayLight : Colors.transparent : Colors.transparent
                                        ),
                                        SizedBox(width: 10),
                                        Expanded(child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(child: Text(
                                                  assetDetailModel != null ? assetDetailModel.unitName : "",
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: themeValue.themeType == 0 ? ThemeConstant.white
                                                          : ThemeConstant.blackNavy,
                                                      fontSize: 16,
                                                      fontFamily: "BarlowNormal",
                                                      fontWeight: FontWeight.w400,
                                                      letterSpacing: 1
                                                  ),
                                                ), flex: 1),

                                                SizedBox(width: 16),

                                                Visibility(child: Text(assetDetailModel != null ? assetModel.assetId != 0 ? MyUtils.countAssetAmount(assetModel.amount,
                                                    assetDetailModel.decimals) : MyUtils.parseAlgoToMicro(assetModel.amount) : "",
                                                  style: TextStyle(
                                                      color: themeValue.themeType == 0 ? ThemeConstant.yellowDark
                                                          : ThemeConstant.yellowLight,
                                                      fontSize: 16,
                                                      fontFamily: "BarlowNormal",
                                                      fontWeight: FontWeight.w400,
                                                      letterSpacing: 1
                                                  ),
                                                ), visible: !isPending)
                                              ],
                                            ),
                                            SizedBox(height: 10),
                                            Row(
                                              children: [
                                                Text(
                                                  !isPending ? assetModel.price.isNotEmpty ? "${MyUtils.formatPrice(
                                                      double.parse(assetModel.price.isNotEmpty ? assetModel.price : "0.0")) == "0.00" ? "<" : ""}${_homeController.currentCurrencySymbol}${MyUtils.formatPrice(
                                                      double.parse(assetModel.price.isNotEmpty ? assetModel.price : "0.0")) == "0.00" ? "0.01" : MyUtils.formatPrice(
                                                      double.parse(assetModel.price.isNotEmpty ? assetModel.price : "0.0"))} " : "-" : statusName,
                                                  style: TextStyle(
                                                      color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark
                                                          : ThemeConstant.secondTextGrayLight,
                                                      fontSize: 14,
                                                      fontFamily: "BarlowNormal",
                                                      fontWeight: FontWeight.w400,
                                                      letterSpacing: 1
                                                  ),
                                                ),

                                                Visibility(child: Text(
                                                  "${assetModel.price.isNotEmpty ? assetMap["percentage"] ?? "" : ""}",
                                                  style: TextStyle(
                                                      color: "${assetMap["percentage"] ?? ""}".startsWith("-") ? Colors.red : Color.fromRGBO(108, 216, 107, 1),
                                                      fontSize: 14,
                                                      fontFamily: "BarlowNormal",
                                                      fontWeight: FontWeight.w400,
                                                      letterSpacing: 1
                                                  ),
                                                ), visible: !isPending),

                                                Expanded(child: SizedBox(), flex: 1),

                                                Text(
                                                  assetDetailModel != null ? assetModel.amount != 0 && assetModel.price.isNotEmpty
                                                      ? "~ ${ assetModel.assetId != 0 ? MyUtils.formatPrice(MyUtils.countAssetPrice(assetModel.price, double.parse(assetModel.amount.toString()),
                                                      assetDetailModel.decimals,1)) : MyUtils.formatPrice(MyUtils.parseAlgoPrice(double.parse(MyUtils.parseAlgoToMicro(
                                                      assetModel.amount)), double.parse(assetModel.price.isNotEmpty ? assetModel.price : "0")))} ${themeValue.currentCurrency}" : "": "",
                                                  style: TextStyle(
                                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                      fontSize: 14,
                                                      fontFamily: "BarlowNormal",
                                                      fontWeight: FontWeight.w400,
                                                      letterSpacing: 1
                                                  ),
                                                )
                                              ],
                                            )
                                          ],
                                        ))
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    themeValue.readMore(3);
                                    _homeController.priceMap["latest_price"] = "0";
                                    _homeController.priceMap["lowest"] = null;
                                    _homeController.priceMap["highest"] = null;

                                    _homeController.tokenMap["circulating_supply"] = assetDetailModel?.circulatingSupply ?? 0;
                                    _homeController.tokenMap["asset_name"] = assetDetailModel?.name ?? "";
                                    _homeController.tokenMap["asset_unit_name"] = assetDetailModel?.unitName ?? "";
                                    _homeController.tokenMap["asset_index"] = assetDetailModel?.index ?? 0;
                                    _homeController.tokenMap["creator"] = assetDetailModel?.creator ?? "";
                                    _homeController.tokenMap["amount"] = assetDetailModel != null ? assetModel.assetId != 0 ? MyUtils.countAssetAmount(assetModel.amount,
                                        assetDetailModel.decimals) : MyUtils.parseAlgoToMicro(assetModel.amount) : "0";
                                    _homeController.tokenMap["price"] = assetModel.assetId != 0 ? MyUtils.formatPrice(MyUtils.countAssetPrice(assetModel.price, double.parse(assetModel.amount.toString()),
                                        assetDetailModel?.decimals ?? 0,1)) : MyUtils.formatPrice(MyUtils.parseAlgoPrice(double.parse(MyUtils.parseAlgoToMicro(
                                        assetModel.amount)), double.parse(assetModel.price.isNotEmpty ? assetModel.price : "0")));
                                    _homeController.tokenMap["created_date"] = assetDetailModel?.createdAtRound ?? 0;

                                    _homeController.setFilterPosition(2);
                                    _homeController.loadAssetPrice(assetDetailModel?.unitName ?? "", "day");
                                    _homeController.setBalanceTye(2,index % 2 == 0 ? 1 : 2);
                                  },
                                ),
                              ) : Container(
                                margin: EdgeInsets.only(left: 24, right: 24, top: 8,
                                    bottom: index != assetValue.walletNetworkLists[assetValue.selectedLedgerPosition][assetValue.selectedUserPosition]["searched_list"
                                        ""].length -1 ? 0 : 55),
                                width: double.maxFinite,
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Row(
                                  children: [
                                    SizedBox(width: 16),
                                    SvgPicture.asset("assets/icons/ic_clock.svg"),
                                    SizedBox(width: 22),
                                    Expanded(child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(child: Text(
                                              assetDetailModel?.name ?? "",
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark
                                                    : ThemeConstant.secondTextGrayLight,
                                                fontSize: 16,
                                                fontFamily: "BarlowNormal",
                                              ),
                                            ), flex: 1)
                                          ],
                                        ),
                                        SizedBox(height: 8),
                                        Row(
                                          children: [
                                            Expanded(child: Text(
                                              "${assetDetailModel?.unitName ?? ""} (${assetDetailModel?.index ?? 0})",
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark
                                                    : ThemeConstant.secondTextGrayLight,
                                                fontSize: 12,
                                                fontFamily: "BarlowNormal",
                                              ),
                                            ), flex: 1)
                                          ],
                                        )
                                      ],
                                    )),
                                    Text(
                                      assetMap["status_name"] ?? "",
                                      style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark
                                            : ThemeConstant.secondTextGrayLight,
                                        fontSize: 12,
                                        fontFamily: "BarlowNormal",
                                      ),
                                    )
                                  ],
                                ),
                              );
                            },
                            itemCount: assetValue.walletNetworkLists[assetValue.selectedLedgerPosition][_homeController.selectedUserPosition][
                            "searched_list"].length,
                            padding: EdgeInsets.zero,
                            physics: AlwaysScrollableScrollPhysics(),
                          ),
                        ) : SizedBox() : SizedBox() : SizedBox()
                      ],
                    ),
                  ),
                )),
              ),
            ],
          ),
        ));
  }

  @override
  void onMyAsset(String transactionId, AssetsModel assetsModel, AssetsDetailModel assetsDetailModel,
      int type, List<AssetsModel> disableList) {
    _homeController.walletNetworkLists[_homeController.selectedLedgerPosition][_homeController.selectedUserPosition]["disable_list"] = disableList;
    if(type == 1){
      Map assetMap = {};
      assetMap["asset_model"] = assetsModel;
      assetMap["is_pending"] = true;
      assetMap["asset_detail_model"] = assetsDetailModel;
      assetMap["status_name"] = "Adding Asset ...";
      _homeController.addMyAsset(transactionId,assetMap);
    }
    else{
      _homeController.removeMyAsset(transactionId, assetsDetailModel.index);
    }
  }
}

