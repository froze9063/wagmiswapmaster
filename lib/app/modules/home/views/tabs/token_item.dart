import 'package:clipboard/clipboard.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/home/controllers/home_controller.dart';
import 'package:waagmiswap/app/util/my_util.dart';
import 'package:waagmiswap/app/widgets/custom_empty_view.dart';

class TokenItem extends StatelessWidget {

  HomeController _homeController;
  TokenItem(this._homeController);

  late BuildContext buildContext;

  @override
  Widget build(BuildContext context) {
    buildContext = context;
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (themeValue) => Container(
      width: double.maxFinite,
      height: double.maxFinite,
      child: GetBuilder<HomeController>(
        id: "token_info",
        init: HomeController(),
        builder: (value) => Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(width: 3),
                GestureDetector(
                  child: Container(
                    color: Colors.transparent,
                    width: 55,
                    height: 55,
                    child: Center(
                      child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                      "assets/icons/ic_black_back.png", width: 24,
                          height: 24, fit: BoxFit.contain),
                    ),
                  ),
                  onTap: (){
                    _homeController.setBalanceTye(1,1);
                  },
                ),
                Expanded(child: SizedBox(),flex: 1),
                Text(
                  value.tokenMap["asset_index"] == 0 ? "Algorand" : MyUtils.parseAddress(value.tokenMap["creator"]),
                  style: TextStyle(
                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 14,
                      fontFamily: "BarlowNormal",
                      letterSpacing: 2,
                      fontWeight: FontWeight.w600
                  ),
                ),
                Expanded(child: SizedBox(),flex: 1),
                Container(
                  width: 55,
                  height: 55,
                ),
                SizedBox(width: 3)
              ],
            ),

            Expanded(child: SingleChildScrollView(child: Column(
              children: [
                Row(
                  children: [
                    SizedBox(width: 16),

                    CircleAvatar(
                      child: value.tokenMap["asset_index"] != null ? !value.tokenMap["asset_unit_name"].contains("-") ? value.tokenMap["asset_index"] != 0 ?
                      MyUtils.getCuratedIconPath(value.tokenMap["asset_unit_name"], themeValue.themeType).isEmpty ? SvgPicture.network("https://wallet.wagmiswap.io/testnet/"
                          "assets/${value.tokenMap["asset_index"]}.svg",height: 28, width: 28) : SvgPicture.asset(MyUtils.getCuratedIconPath(value.tokenMap["asset_unit_name"], themeValue.themeType),
                          height: 28, width: 28) : SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_dark_algo.svg" : "assets/icons/ic_light_algo.svg",
                          height: 28, width: 28) : SizedBox() : SizedBox(),
                      radius: !value.tokenMap["asset_unit_name"].contains("-") ? 18 : 14,
                      backgroundColor: MyUtils.getCuratedIconPath(value.tokenMap["asset_unit_name"], themeValue.themeType).isEmpty ? ThemeConstant.secondTextGrayLight : Colors.transparent
                    ),

                    SizedBox(width: 8),
                    Text(
                      value.tokenMap["asset_name"],
                      style: TextStyle(
                          color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 14,
                        fontFamily: "BarlowNormal",
                        letterSpacing: 2,
                        fontWeight: FontWeight.w600
                      ),
                    ),
                    Expanded(child: SizedBox(), flex: 1),
                    Visibility(child: Text(
                      "${value.tokenMap["price"]} ${themeValue.currentCurrency}",
                      style: TextStyle(
                        color: Color.fromRGBO(255, 197, 48, 1.0),
                          fontSize: 16,
                          fontFamily: "BarlowNormal",
                          letterSpacing: 1,
                          fontWeight: FontWeight.w600
                      ),
                    ), visible: value.tokenMap["price"] != "0.00"),
                    SizedBox(width: 24),
                  ],
                ),

                SizedBox(height: 16),

                Row(
                  children: [
                    value.tokenMap["asset_index"] != 0 ? InkWell(
                      child: Container(
                        child: Row(
                          children: [
                            SizedBox(width: 24),
                            SvgPicture.asset("assets/icons/ic_copy.svg", height: 16, width: 16,
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy),
                            SizedBox(width: 8),
                            Text(
                              "${value.tokenMap["asset_index"]}",
                              style: TextStyle(
                                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                  fontSize: 14,
                                  fontFamily: "BarlowNormal",
                                  letterSpacing: 2,
                                  fontWeight: FontWeight.w600
                              ),
                            ),
                          ],
                        ),
                        color: Colors.transparent,
                      ),
                      onTap: (){
                        FlutterClipboard.copy("${value.tokenMap["asset_index"]}");
                        copiedAssetId(_homeController,"Asset ID copied.");
                      },
                    ) : SizedBox(),

                    Expanded(child: SizedBox(), flex: 1),

                    Text(
                      value.tokenMap["amount"] ?? "0",
                      style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 12,
                        fontFamily: "BarlowNormal",
                        letterSpacing: 1,
                        fontWeight: FontWeight.w600,
                        decorationThickness: 1.5,
                      ),
                    ),
                    SizedBox(width: 24)
                  ],
                ),

                SizedBox(height: 16),

                Row(
                  children: [
                    SizedBox(width: 24),
                    Text(
                        _homeController.priceMap["latest_price"].isNotEmpty ? "${MyUtils.formatPrice(double.parse(_homeController.priceMap["latest_price"] ?? "0")) == "0.00" ? "<0.01" :
                      MyUtils.formatPrice(double.parse(_homeController.priceMap["latest_price"] ?? "0"))} ${themeValue.currentCurrency}" : "",
                      style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 12,
                        fontFamily: "BarlowNormal",
                        letterSpacing: 1,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(width: 4),
                    Text(
                        value.priceMap["lowest"] != null ? value.priceMap["lowest"] != "NaN" ? MyUtils.getPercentagePrice(double.parse(value.priceMap["lowest"] ?? "0.0"),
                            double.parse(value.priceMap["highest"] ?? "0.0")).toString().startsWith("-") ? "${MyUtils.getPercentagePrice(double.parse(value.priceMap["lowest"] ?? "0.0"),
                          double.parse(value.priceMap["highest"] ?? "0.0")).toStringAsFixed(2)}%" : "+${MyUtils.getPercentagePrice(double.parse(value.priceMap["lowest"] ?? "0.0"),
                            double.parse(value.priceMap["highest"] ?? "0.0")).toStringAsFixed(2)}%" : "" : "",
                      style: TextStyle(
                        color: MyUtils.getPercentagePrice(double.parse(value.priceMap["lowest"] ?? "0.0"),
                            double.parse(value.priceMap["highest"] ?? "0.0")).toString().startsWith("-") ? Colors.red
                            : Color.fromRGBO(108, 216, 107, 1),
                        fontSize: 10,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(width: 24),
                    Expanded(child: Text(
                        value.priceMap["highest"] != null ? "${double.parse(value.priceMap["highest"] ?? "0.0")} ${themeValue.currentCurrency}" : "",
                      textAlign: TextAlign.end,
                      style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                        fontSize: 12,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w400,
                      ),
                    ), flex: 1),
                    SizedBox(width: 24),
                  ],
                ),

                SizedBox(height: 24),

                GetBuilder<HomeController>(
                  id: "asset_price_list",
                  init: HomeController(),
                  builder: (priceValue) => Container(
                  margin: EdgeInsets.only(left: 24, right: 24),
                  width: double.maxFinite,
                  height: 150,
                  child: Stack(
                    children: [
                      Visibility(child: Center(child: CustomEmptyView.emptyView("No chart data  ",
                          themeValue)), visible: priceValue.tokenPriceList.isEmpty && !priceValue.priceMap["is_loading"]),

                      Visibility(child: Center(
                        child: CircularProgressIndicator(
                          color: themeValue.themeType == 0 ? ThemeConstant.darkPurple :
                          ThemeConstant.lightPurple,
                        ),
                      ), visible: priceValue.priceMap["is_loading"]),

                      Visibility(child: LineChart(
                        LineChartData(
                            borderData: FlBorderData(show: false),
                            gridData: FlGridData(
                                show: false
                            ),
                            lineBarsData: [
                              LineChartBarData(
                                  isCurved: true,
                                  preventCurveOverShooting: true,
                                  color: Colors.green,
                                  belowBarData: BarAreaData(
                                    show: true,
                                    gradient: LinearGradient(
                                      colors: priceValue.tokenPriceList.isNotEmpty ? [Colors.green.withOpacity(0.1),
                                        Colors.green.withOpacity(0.5)] : [Colors.transparent]
                                    ),
                                  ),
                                  dotData: FlDotData(
                                    show: false,
                                  ),
                                  spots: priceValue.tokenPriceList
                              )
                            ],
                            titlesData: FlTitlesData(
                                bottomTitles: AxisTitles(
                                    sideTitles: SideTitles(
                                        showTitles: false
                                    ),
                                ),
                                topTitles: AxisTitles(
                                  sideTitles: SideTitles(
                                      showTitles: false
                                  ),
                                ),
                                rightTitles: AxisTitles(
                                  sideTitles: SideTitles(
                                      showTitles: false
                                  ),
                                ),
                                leftTitles: AxisTitles(
                                  sideTitles: SideTitles(
                                      showTitles: false
                                  ),
                                )
                            )
                        ),
                      ), visible: priceValue.tokenPriceList.isNotEmpty && !priceValue.priceMap[
                        "is_loading"])
                    ],
                  ),
                )),

                SizedBox(height: 24),

                Row(
                  children: [
                    SizedBox(width: 24),
                    Expanded(child: Text(
                      value.priceMap["lowest"] != null ? "${double.parse(value.priceMap["lowest"] ?? "0.0")} ${themeValue.currentCurrency}" : "",
                      style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                        fontSize: 12,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w400,
                      ),
                    ), flex: 1),
                    SizedBox(width: 24),
                  ],
                ),

                SizedBox(height: 16),

                Row(
                  children: [
                    SizedBox(width: 24),

                    GestureDetector(
                      child: Container(
                        padding: EdgeInsets.only(left: 4, right: 4, top: 1, bottom: 1),
                        child: Text(
                          "1H",
                          style: TextStyle(
                            color: value.filterPosition == 1 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple :
                            ThemeConstant.lightPurple : themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark :
                            ThemeConstant.secondTextGrayLight,
                            fontSize: 14,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w400,
                            decorationThickness: 1.5,
                          ),
                        ),
                        decoration: BoxDecoration(
                            color: value.filterPosition == 1 ? themeValue.themeType == 0 ? Colors.transparent : ThemeConstant.lightPurple.withOpacity(0.1) : Colors.transparent,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            border: Border.all(
                              width: 1,
                              color: value.filterPosition == 1 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple : Colors.transparent,
                            )
                        ),
                      ),
                      onTap: (){
                        value.setFilterPosition(1);

                        if(value.tokenMap["asset_index"] == 0){
                          _homeController.loadAssetPrice("ALGO", "day");
                        }
                        else{
                          _homeController.loadAssetPrice(value.tokenMap["asset_unit_name"], "day");
                        }
                      },
                    ),

                    Expanded(child: SizedBox(), flex: 1),
                    GestureDetector(
                      child: Container(
                        padding: EdgeInsets.only(left: 4, right: 4, top: 1, bottom: 1),
                        child: Text(
                          "1D",
                          style: TextStyle(
                            color: value.filterPosition == 2 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple :
                            ThemeConstant.lightPurple : themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark :
                            ThemeConstant.secondTextGrayLight,
                            fontSize: 14,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w400,
                            decorationThickness: 1.5,
                          ),
                        ),
                        decoration: BoxDecoration(
                            color: value.filterPosition == 2 ? themeValue.themeType == 0 ? Colors.transparent : ThemeConstant.lightPurple.withOpacity(0.1) : Colors.transparent,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            border: Border.all(
                              width: 1,
                              color: value.filterPosition == 2 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple : Colors.transparent,
                            )
                        ),
                      ),
                      onTap: (){
                        value.setFilterPosition(2);

                        if(value.tokenMap["asset_index"] == 0){
                          _homeController.loadAssetPrice("ALGO", "day");
                        }
                        else{
                          _homeController.loadAssetPrice(value.tokenMap["asset_unit_name"], "day");
                        }
                      },
                    ),

                    Expanded(child: SizedBox(), flex: 1),
                    GestureDetector(
                      child: Container(
                        padding: EdgeInsets.only(left: 4, right: 4, top: 1, bottom: 1),
                        child: Text(
                          "1W",
                          style: TextStyle(
                            color: value.filterPosition == 3 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple :
                            ThemeConstant.lightPurple : themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark :
                            ThemeConstant.secondTextGrayLight,
                            fontSize: 14,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w400,
                            decorationThickness: 1.5,
                          ),
                        ),
                        decoration: BoxDecoration(
                            color: value.filterPosition == 3 ? themeValue.themeType == 0 ? Colors.transparent : ThemeConstant.lightPurple.withOpacity(0.1) : Colors.transparent,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            border: Border.all(
                              width: 1,
                              color: value.filterPosition == 3 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple : Colors.transparent,
                            )
                        ),
                      ),
                      onTap: (){
                        value.setFilterPosition(3);

                        if(value.tokenMap["asset_index"] == 0){
                          _homeController.loadAssetPrice("ALGO", "week");
                        }
                        else{
                          _homeController.loadAssetPrice(value.tokenMap["asset_unit_name"], "week");
                        }
                      },
                    ),

                    Expanded(child: SizedBox(), flex: 1),
                    GestureDetector(
                      child: Container(
                        padding: EdgeInsets.only(left: 4, right: 4, top: 1, bottom: 1),
                        child: Text(
                          "1M",
                          style: TextStyle(
                            color: value.filterPosition == 4 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple :
                            ThemeConstant.lightPurple : themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark :
                            ThemeConstant.secondTextGrayLight,
                            fontSize: 14,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w400,
                            decorationThickness: 1.5,
                          ),
                        ),
                        decoration: BoxDecoration(
                            color: value.filterPosition == 4 ? themeValue.themeType == 0 ? Colors.transparent : ThemeConstant.lightPurple.withOpacity(0.1) : Colors.transparent,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            border: Border.all(
                              width: 1,
                              color: value.filterPosition == 4 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple : Colors.transparent,
                            )
                        ),
                      ),
                      onTap: (){
                        value.setFilterPosition(4);

                        if(value.tokenMap["asset_index"] == 0){
                          _homeController.loadAssetPrice("ALGO", "month");
                        }
                        else{
                          _homeController.loadAssetPrice(value.tokenMap["asset_unit_name"], "month");
                        }
                      },
                    ),

                    Expanded(child: SizedBox(), flex: 1),
                    GestureDetector(
                      child: Container(
                        padding: EdgeInsets.only(left: 4, right: 4, top: 1, bottom: 1),
                        child: Text(
                          "1Y",
                          style: TextStyle(
                            color: value.filterPosition == 5 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple :
                            ThemeConstant.lightPurple : themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark :
                            ThemeConstant.secondTextGrayLight,
                            fontSize: 14,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w400,
                            decorationThickness: 1.5,
                          ),
                        ),
                        decoration: BoxDecoration(
                            color: value.filterPosition == 5 ? themeValue.themeType == 0 ? Colors.transparent : ThemeConstant.lightPurple.withOpacity(0.1) : Colors.transparent,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            border: Border.all(
                              width: 1,
                              color: value.filterPosition == 5 ? themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple : Colors.transparent,
                            )
                        ),
                      ),
                      onTap: (){
                        value.setFilterPosition(5);

                        if(value.tokenMap["asset_index"] == 0){
                          _homeController.loadAssetPrice("ALGO", "monthly");
                        }
                        else{
                          _homeController.loadAssetPrice(value.tokenMap["asset_unit_name"], "monthly");
                        }
                      },
                    ),

                    SizedBox(width: 24),
                  ],
                ),

                SizedBox(height: 16),

                Container(
                  margin: EdgeInsets.only(left: 24, right: 24),
                  padding: EdgeInsets.all(themeValue.themeType == 0 ? 0 : 8),
                  width: double.maxFinite,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(child: Text(
                            StringContent.market_cap,
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          ), flex: 1),

                          Text(
                            "${themeValue.tokenMap["asset_index"] == 0 ? "5,624,071,516 ${themeValue.currentCurrency}" : themeValue.tokenMap[
                              "circulating_supply"] == 0 ? "-" : "${MyUtils.formatPrice(double.parse(themeValue.tokenMap["circulating_supply"].toString()) / double.parse(
                                _homeController.priceMap["latest_price"] ?? "0"))} "
                                "${themeValue.currentCurrency}"} ",
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          )
                        ],
                      ),

                      SizedBox(height: 24),

                      Row(
                        children: [
                          Expanded(child: Text(
                            StringContent.in_circulation,
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          ), flex: 1),

                          Text(
                            "${themeValue.tokenMap["asset_index"] == 0 ? "6,616,554,724.8 ${themeValue.tokenMap["asset_unit_name"]}"
                                "" : themeValue.tokenMap["circulating_supply"] == 0 ? "-" : "${MyUtils.formatPrice(double.parse(themeValue.tokenMap["circulating_supply"].toString()))} "
                                "${themeValue.tokenMap["asset_unit_name"]}"} ",
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          )
                        ],
                      ),

                      SizedBox(height: 24),

                      Row(
                        children: [
                          Expanded(child: Text(
                            StringContent.inception_date,
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          ), flex: 1),

                          Text(
                            _homeController.tokenMap["created_date"] != 0 ? DateFormat("MMM dd, yyyy").format(DateTime.fromMillisecondsSinceEpoch(
                                _homeController.tokenMap["created_date"] * 1000)) : "-",
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1,
                              decorationThickness: 1.5,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                    decoration: BoxDecoration(
                        color: themeValue.themeType == 0 ? Colors.transparent : ThemeConstant.white,
                        borderRadius: BorderRadius.all(Radius.circular(8))
                    )
                ),

                SizedBox(height: 16),

                Row(
                  children: [
                    SizedBox(width: 24),
                    Expanded(child: Text(
                      "ABOUT ${value.tokenMap["asset_name"] ?? ""}".toUpperCase(),
                      style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 14,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w700,
                        letterSpacing: 4,
                        height: 1.5
                      ),
                    ), flex: 1),
                    SizedBox(width: 24),
                  ],
                ),

                SizedBox(height: 10),

                Row(
                  children: [
                    SizedBox(width: 24),
                    Expanded(child: Text(
                      themeValue.getAssetDetail(value.tokenMap["asset_unit_name"] ?? ""),
                      maxLines: themeValue.maxLine,
                      style: TextStyle(
                          color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 12,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w400,
                        letterSpacing: 2,
                        height: 1.5
                      ),
                    ), flex: 1),
                    SizedBox(width: 24),
                  ],
                ),

                SizedBox(height: 10),

                themeValue.getAssetDetail(value.tokenMap["asset_unit_name"] ?? "") != "-" ? Row(
                  children: [
                    SizedBox(width: 24),
                    GestureDetector(
                      child: Text(
                        themeValue.maxLine != null ? StringContent.read_more : StringContent.read_less,
                        style: TextStyle(
                          color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                          fontSize: 12,
                          fontFamily: "BarlowNormal",
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1,
                          decorationThickness: 1.5,
                        ),
                      ),
                      onTap: (){
                        if(themeValue.maxLine != null){
                          themeValue.readMore(null);
                        }
                        else{
                          themeValue.readMore(3);
                        }
                      },
                    )
                  ],
                ) : SizedBox(),

                SizedBox(height: 24)
              ],
            )), flex: 1),
          ],
        ),
      ),
    ));
  }

  copiedAssetId(HomeController themeValue, String message){
    showModalBottomSheet(
        context: buildContext,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 235.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 235.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CircleAvatar(
                                    child: themeValue.tokenMap["asset_index"] != null ? !themeValue.tokenMap["asset_unit_name"].contains("-") ? themeValue.tokenMap["asset_index"] != 0 ?
                                    MyUtils.getCuratedIconPath(themeValue.tokenMap["asset_unit_name"], themeValue.themeType).isEmpty ? SvgPicture.network("https://wallet.wagmiswap.io/testnet/"
                                        "assets/${themeValue.tokenMap["asset_index"]}.svg",height: 28, width: 28) : SvgPicture.asset(MyUtils.getCuratedIconPath(themeValue.tokenMap["asset_unit_name"], themeValue.themeType),
                                        height: 28, width: 28) : SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_dark_algo.svg" : "assets/icons/ic_light_algo.svg",
                                        height: 28, width: 28) : SizedBox() : SizedBox(),
                                    radius: !themeValue.tokenMap["asset_unit_name"].contains("-") ? 18 : 14,
                                    backgroundColor: MyUtils.getCuratedIconPath(themeValue.tokenMap["asset_unit_name"], themeValue.themeType).isEmpty ? ThemeConstant.secondTextGrayLight : Colors.transparent
                                  ),
                                  SizedBox(width: 4),
                                  Text(
                                    themeValue.tokenMap["asset_name"],
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowSemiBold",
                                      decorationThickness: 1.5,
                                    ),
                                  )
                                ]),
                            SizedBox(height: 24),
                            Text(
                              message,
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 32),
                            GestureDetector(
                              child: CustomButton(45, double.maxFinite, BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.topRight,
                                    colors: [
                                      Color.fromRGBO(106, 48, 147, 1.0),
                                      Color.fromRGBO(160, 68, 255, 1.0)
                                    ]
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                              ), StringContent.close, TextStyle(
                                  color: Colors.white,
                                  fontFamily: "BarlowRegular",
                                  fontSize: 12,
                                  letterSpacing: 2
                              )),
                              onTap: (){
                                Get.back();
                              },
                            )
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }
}