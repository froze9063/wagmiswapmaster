import 'dart:math';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/model/transaction_model.dart';
import 'package:waagmiswap/app/modules/home/controllers/home_controller.dart';
import 'package:waagmiswap/app/modules/transaction_info/views/transaction_info_view.dart';
import 'package:waagmiswap/app/util/my_util.dart';
import 'package:waagmiswap/app/widgets/custom_empty_view.dart';

class TransactionsItem extends StatelessWidget {

  final HomeController _homeController;
  TransactionsItem(this._homeController);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        id: "theme",
        init: HomeController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          child: Column(
            children: [
              SizedBox(height: 24),
              Row(
                children: [
                  SizedBox(width: 24),
                  GetBuilder<HomeController>(
                      id: "transaction_asset_filter",
                      init: HomeController(),
                      builder: (assetTransactionValue) => GestureDetector(
                        child: Container(
                          padding: EdgeInsets.only(left: 8, right: 8, top: 6, bottom: 6),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              border:Border.all(
                                  color: assetTransactionValue.assetTransactionName.isEmpty ? themeValue.themeType == 0 ?
                                  ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight : themeValue.themeType == 0
                                      ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                  width: 1
                              )
                          ),
                          child: Row(
                            children: [
                              Text(
                                assetTransactionValue.assetTransactionName.isEmpty ? "ASSET " : assetTransactionValue.assetTransactionName,
                                style: TextStyle(
                                    color: assetTransactionValue.assetTransactionName.isEmpty ? themeValue.themeType == 0 ?
                                    ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight : themeValue.themeType == 0
                                        ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                    fontSize: 10,
                                    fontFamily: "BarlowNormal",
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 1
                                ),
                              ),
                              SizedBox(width: 6),
                              SvgPicture.asset(assetTransactionValue.assetTransactionName.isEmpty ? "assets/icons/ic_white_dropdown.svg" :
                              "assets/icons/ic_purple_dropdown.svg", width: 8, height: 8)
                            ],
                          ),
                        ),
                        onTap: (){
                          _homeController.isShowDialog(true,3);
                        },
                      )),

                  SizedBox(width: 8),

                  GetBuilder<HomeController>(
                      id: "transaction_date_filter",
                      init: HomeController(),
                      builder: (dateTransactionValue) => GestureDetector(
                        child: Container(
                          padding: EdgeInsets.only(left: 8, right: 8, top: 6, bottom: 6),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              border:Border.all(
                                  color: dateTransactionValue.assetTransactionDate.isEmpty ? themeValue.themeType == 0 ?
                                  ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight : themeValue.themeType == 0
                                      ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                  width: 1
                              )
                          ),
                          child: Row(
                            children: [
                              Text(
                                dateTransactionValue.assetTransactionDate.isEmpty ? StringContent.date.toUpperCase() : dateTransactionValue.assetTransactionDate,
                                style: TextStyle(
                                    color: dateTransactionValue.assetTransactionDate.isEmpty ? themeValue.themeType == 0 ?
                                    ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight : themeValue.themeType == 0
                                        ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                    fontSize: 10,
                                    fontFamily: "BarlowNormal",
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 1
                                ),
                              ),
                              SizedBox(width: 6),
                              SvgPicture.asset(dateTransactionValue.assetTransactionDate.isEmpty ? "assets/icons/ic_white_dropdown.svg" :
                              "assets/icons/ic_purple_dropdown.svg", width: 8, height: 8)
                            ],
                          ),
                        ),
                        onTap: (){
                          _homeController.isShowDialog(true,4);
                        },
                      )),

                  SizedBox(width: 8),

                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.only(left: 8, right: 8, top: 6, bottom: 6),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          border:Border.all(
                              color: themeValue.themeType == 0 ?
                              ThemeConstant.white : ThemeConstant.blackNavy,
                              width: 1
                          )
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_close_white.svg" :
                          "assets/icons/ic_close_black.svg", height: 12, width: 12),
                          SizedBox(width: 6),
                          Text(
                            StringContent.clear_filter,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: themeValue.themeType == 0 ?
                                ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 10,
                                fontFamily: "BarlowNormal",
                                fontWeight: FontWeight.w400,
                                letterSpacing: 1
                            ),
                          )
                        ],
                      ),
                    ),
                    onTap: (){
                      _homeController.clearFilters();
                    },
                  )
                ],
              ),
              SizedBox(height: 24),
              Expanded(child: GetBuilder<HomeController>(
                id: "transactions",
                init: HomeController(),
                builder: (transactionValue) => RefreshIndicator(
                  color: Color.fromRGBO(106, 48, 147, 1.0),
                  onRefresh: ()=> _homeController.loadTransaction(true),
                  child: Container(
                    width: double.maxFinite,
                    height: double.maxFinite,
                    child: Stack(
                      children: [
                        Visibility(child: Center(
                          child: CustomEmptyView.emptyView("No Transactions", themeValue),
                        ), visible: transactionValue.walletTransactionLists[transactionValue.selectedUserPosition][
                        "trans_searched_list"].length == 0 && !transactionValue.walletTransactionLists[
                        transactionValue.selectedUserPosition]["loading"]),

                        Visibility(child: Center(
                          child: CircularProgressIndicator(
                            color: Color.fromRGBO(106, 48, 147, 1.0),
                          ),
                        ), visible: transactionValue.walletTransactionLists[transactionValue.selectedUserPosition]["loading"]),

                        NotificationListener<ScrollNotification>(
                          onNotification: (ScrollNotification scrollInfo){
                            if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
                              if(!transactionValue.walletTransactionLists[transactionValue.selectedUserPosition]["loading_more"] && transactionValue.walletTransactionLists[transactionValue.selectedUserPosition][
                              "next_token"].isNotEmpty){
                                _homeController.loadTransaction(false);
                              }
                            }
                            return false;
                          },
                          child: Container(
                            width: double.maxFinite,
                            height: double.maxFinite,
                            child: ListView.builder(
                              itemBuilder: (context, index){
                                List<TransactionModel> transactionResultList = transactionValue.walletTransactionLists[transactionValue.selectedUserPosition]["trans_searched_list"][
                                index]["searched_transaction_list"];
                                return Container(
                                    child: Column(
                                      children: [
                                        Visibility(child: SizedBox(height: 24), visible: index != 0),
                                        Row(
                                          children: [
                                            SizedBox(width: 24),
                                            Text(
                                              transactionValue.walletTransactionLists[transactionValue.selectedUserPosition]["trans_searched_list"][
                                              index]["date"],
                                              style: TextStyle(
                                                  color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                                                  fontSize: 14,
                                                  fontFamily: "BarlowNormal",
                                                  fontWeight: FontWeight.w600,
                                                  letterSpacing: 1
                                              ),
                                            )
                                          ],
                                        ),
                                        SizedBox(height: 12),

                                        Container(
                                          width: double.maxFinite,
                                          color: Colors.transparent,
                                          child: ListView.builder(itemBuilder: (itemContext, itemIndex){
                                            TransactionModel transactionModel = transactionResultList[itemIndex];
                                            return Material(
                                              color: Colors.transparent,
                                              child: InkWell(
                                                splashColor: Colors.transparent,
                                                child: Container(
                                                  color: Colors.transparent,
                                                  margin: EdgeInsets.only(left: 24, right: 24),
                                                  padding: EdgeInsets.only(top: 8, bottom: 8),
                                                  width: double.maxFinite,
                                                  child: Column(
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Expanded(child: Text(
                                                            transactionModel.type,
                                                            style: TextStyle(
                                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                                fontSize: 14,
                                                                fontFamily: "BarlowNormal",
                                                                fontWeight: FontWeight.w700,
                                                                letterSpacing: 1
                                                            ),
                                                          ), flex: 1),

                                                          Text(
                                                            transactionModel.type == "App Call" ? "${transactionModel.applicationId}" : transactionModel.type != "Add Asset" && transactionModel.type != "Remove Asset" ? "${transactionModel.assetName == "ALGO"
                                                                ? MyUtils.doubleWithoutDecimalToInt(Algo.fromMicroAlgos(transactionModel.amount))
                                                                : MyUtils.doubleWithoutDecimalToInt(transactionModel.amount / pow(10, transactionModel.decimal))} ${transactionModel.assetName}" : transactionModel.assetName,
                                                            style: TextStyle(
                                                                color: transactionModel.type == "Receive" ? themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple
                                                                    : themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                                                                fontSize: 14,
                                                                fontFamily: "BarlowNormal",
                                                                fontWeight: FontWeight.w700,
                                                                letterSpacing: 1
                                                            ),
                                                          )
                                                        ],
                                                      ),

                                                      SizedBox(height: 4),

                                                      Row(
                                                        children: [
                                                          Expanded(child: Text(
                                                            MyUtils.parseAddress(transactionModel.address),
                                                            style: TextStyle(
                                                                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                                                fontSize: 12,
                                                                fontFamily: "BarlowNormal",
                                                                fontWeight: FontWeight.w400,
                                                                letterSpacing: 1
                                                            ),
                                                          ), flex: 1),

                                                          transactionModel.type != "Add Asset" && transactionModel.type != "Remove Asset" && transactionModel.type != "App Call" ? Text(
                                                            "${transactionModel.assetName == "ALGO" ? MyUtils.formatPrice(MyUtils.parseAlgoPrice(double.parse(MyUtils.parseAlgoToMicro(
                                                                transactionModel.amount)),
                                                                double.parse(transactionModel.price.isNotEmpty ?
                                                                transactionModel.price : "0")))
                                                                : MyUtils.formatPrice(MyUtils.countAssetPrice(transactionModel.price, double.parse(transactionModel.amount.toString()),
                                                                transactionModel.decimal,1))} ${themeValue.currentCurrency}",
                                                            style: TextStyle(
                                                              color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                                              fontSize: 12,
                                                              fontFamily: "BarlowNormal",
                                                            ),
                                                          ) : SizedBox()
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                onTap: (){
                                                  Get.to(() => TransactionInfoView(), arguments: {"transaction_model" :
                                                  transactionResultList[itemIndex], "indexer_url" : _homeController.selectedLedger.indexerUrl,
                                                    "algod_url" : _homeController.selectedLedger.algodUrl,
                                                    "user_address" : _homeController.selectedUserModel?.address ?? ""});
                                                },
                                              ),
                                            );
                                          },
                                            itemCount: transactionResultList.length,
                                            padding: EdgeInsets.zero,
                                            shrinkWrap: true,
                                            primary: false,
                                          ),
                                        ),

                                        SizedBox(height: index == transactionValue.walletTransactionLists[transactionValue.selectedUserPosition]["trans_searched_list"].length - 1
                                            ? 36 : 0),
                                      ],
                                    )
                                );
                              },
                              itemCount: transactionValue.walletTransactionLists[transactionValue.selectedUserPosition]["trans_searched_list"].length,
                              padding: EdgeInsets.zero,
                              primary: true,
                              physics: AlwaysScrollableScrollPhysics(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ), flex: 1),

              themeValue.walletTransactionLists[themeValue.selectedUserPosition]["loading_more"] ? Column(
                children: [
                  SizedBox(height: 24),
                  CircularProgressIndicator(
                    color: Color.fromRGBO(106, 48, 147, 1.0),
                  ),
                  SizedBox(height: 24),
                ],
              ) : SizedBox()
            ],
          ),
        ));
  }
}