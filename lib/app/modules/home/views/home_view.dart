import 'dart:io';

import 'package:fbroadcast/fbroadcast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/callback/dapp_request_callback.dart';
import 'package:waagmiswap/app/callback/scanner_callback.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/model/wallet_transaction_detail.dart';
import 'package:waagmiswap/app/modules/home/views/customized_view/select_asset_dialog.dart';
import 'package:waagmiswap/app/modules/home/views/customized_view/select_date_dialog.dart';
import 'package:waagmiswap/app/modules/home/views/customized_view/select_network_dialog.dart';
import 'package:waagmiswap/app/modules/home/views/customized_view/select_transaction_type_dialog.dart';
import 'package:waagmiswap/app/modules/home/views/tabs/balance_item.dart';
import 'package:waagmiswap/app/modules/home/views/tabs/token_item.dart';
import 'package:waagmiswap/app/modules/home/views/tabs/transactions_item.dart';
import 'package:waagmiswap/app/modules/notification/views/notification_view.dart';
import 'package:waagmiswap/app/modules/receive/views/receive_view.dart';
import 'package:waagmiswap/app/modules/scanner/views/scanner_view.dart';
import 'package:waagmiswap/app/modules/send/views/send_view.dart';
import 'package:waagmiswap/app/modules/setting/views/setting_view.dart';
import 'package:waagmiswap/app/util/my_util.dart';
import 'package:waagmiswap/app/widgets/custom_show_up.dart';

import '../controllers/home_controller.dart';
import 'customized_view/select_sort_by.dart';

class HomeView extends GetView<HomeController> implements ScannerCallback, DAppsRequestCallback{

  final HomeController _homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    _homeController.dAppsRequestCallback = this;
    _homeController.checkWalletConnectSession();
    _homeController.buildContext = context;
    return WillPopScope(child: Scaffold(
      body: GetBuilder<HomeController>(
        id: "theme",
        init: HomeController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: GetBuilder<HomeController>(
              id: "tabs",
              init: HomeController(),
              builder: (value) => Stack(
                children: [
                  Column(
                    children: [
                      SizedBox(height: value.balanceType == 1 || value.tabPosition == 2 ? 45 : 30),

                      Visibility(child: Row(
                        children: [
                          SizedBox(width: 24),
                          GestureDetector(
                            child: SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_new_scan.svg"
                                : "assets/icons/ic_new_scan_black.svg", height: 20, width: 20),
                            onTap: (){
                              Get.to(()=> ScannerView(), arguments: {"scanner_callback" : this});
                            },
                          ),
                          SizedBox(width: 16),
                          Container(height: 20, width: 20),
                          Expanded(child: SizedBox()),
                          GetBuilder<HomeController>(
                              id: "network",
                              init: HomeController(),
                              builder: (networkValue) => Row(
                                children: [
                                  CircleAvatar(
                                    radius: 5,
                                    backgroundColor: networkValue.selectedLedgerPosition == 0
                                        ? Color.fromRGBO(108, 216, 107, 1) : Color.fromRGBO(115, 221, 255, 1),
                                  ),
                                  SizedBox(width: 6),
                                  GestureDetector(
                                    child: Container(
                                      color: Colors.transparent,
                                      height: 20,
                                      child: Center(
                                        child: Text(
                                          networkValue.selectedLedger.name,
                                          style: TextStyle(
                                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                              fontSize: 12,
                                              fontFamily: "BarlowNormal",
                                              fontWeight: FontWeight.w400,
                                              letterSpacing: 1
                                          ),
                                        ),
                                      ),
                                    ),
                                    onTap: (){
                                      _homeController.isShowDialog(true,1);
                                    },
                                  )
                                ],
                              )),
                          Expanded(child: SizedBox()),
                          GetBuilder<HomeController>(
                              id: "notification",
                              init: HomeController(),
                              builder: (notificationValue) => GestureDetector(
                                child: Container(
                                  height: 25,
                                  width: 20,
                                  color: Colors.transparent,
                                  child: Stack(
                                    children: [
                                      SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_notifications.svg" :
                                      "assets/icons/ic_black_notifications.svg", height: double.maxFinite, width: double.maxFinite),

                                      Visibility(child: Row(
                                        children: [
                                          Expanded(child: SizedBox()),
                                          CircleAvatar(
                                            radius: 5,
                                            backgroundColor: Color.fromRGBO(255, 197, 48, 1),
                                          )
                                        ],
                                      ), visible: notificationValue.isYellowDotShowed)
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  Get.to(() => NotificationView(),arguments: {
                                    "indexer_url" : _homeController.selectedLedger.indexerUrl,
                                    "algod_url" : _homeController.selectedLedger.algodUrl,
                                    "user_address" : _homeController.selectedUserModel?.address ?? ""})?.then((value) {
                                    _homeController.loadNotification();
                                  });
                                },
                              )),
                          SizedBox(width: 16),
                          GestureDetector(
                            child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_setting.png"
                                : "assets/icons/ic_black_setting.png", height: 20, width: 20),
                            onTap: (){
                              Get.to(() => SettingView(), arguments: {
                                "user_list" : _homeController.userModelList,
                                "selected_user" : _homeController.selectedUserModel,
                                "selected_ledger" : _homeController.selectedLedger,
                              })?.then((value) {
                                _homeController.onSettingResume();
                              });
                            },
                          ),
                          SizedBox(width: 24),
                        ],
                      ), visible: value.balanceType == 1 || value.tabPosition == 2),

                      Expanded(child: Stack(
                        children: [
                          Visibility(child: BalanceItem(_homeController), visible: value.tabPosition == 1 && value.balanceType == 1),
                          Visibility(child: TokenItem(_homeController), visible: value.tabPosition == 1 && value.balanceType == 2),
                          Visibility(child: TransactionsItem(_homeController), visible: value.tabPosition == 2),
                        ],
                      ), flex: 1),

                      Container(
                        width: double.maxFinite,
                        height: 1,
                        color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                      ),

                      Container(
                        width: double.maxFinite,
                        height: 55,
                        decoration: BoxDecoration(
                            color: themeValue.themeType == 0 ? ThemeConstant.darkBottomColor : ThemeConstant.lightBottomColor
                        ),
                        child: Row(
                          children: [
                            Expanded(child: Container(
                              width: double.maxFinite,
                              height: double.maxFinite,
                              color: Colors.transparent,
                              child: Stack(
                                children: [
                                  Row(
                                    children: [
                                      SizedBox(width: 24),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          value.tabPosition == 1 ? Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_balance_white.png"
                                              : "assets/icons/ic_balance_purple.png", width: 17.98, height: 17.99)
                                              : Image.asset("assets/icons/ic_balance_grey.png", width: 17.98, height: 17.99),
                                          SizedBox(height: 8),
                                          Text(
                                            StringContent.balance,
                                            style: TextStyle(
                                                color: value.tabPosition == 1 ? themeValue.themeType == 0 ? ThemeConstant.white :
                                                ThemeConstant.lightPurple :
                                                Color.fromRGBO(164, 164, 164, 1.0),
                                                wordSpacing: 1,
                                                fontSize: 10,
                                                fontFamily: "BarlowRegular",
                                                letterSpacing: 2
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),

                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.transparent,
                                      onTap: (){
                                        _homeController.changeTab(1);
                                        _homeController.setBalanceTye(1,1);
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),flex: 1),

                            Expanded(child: Container(
                              width: double.maxFinite,
                              height: double.maxFinite,
                              color: Colors.transparent,
                              child: Stack(
                                children: [
                                  Row(
                                    children: [
                                      Expanded(child: SizedBox(), flex: 1),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          value.tabPosition == 2 ? Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_transaction_white.png" :
                                          "assets/icons/ic_transaction_purple.png", width: 18.78, height: 18)
                                              : Image.asset("assets/icons/ic_transaction_grey.png", width: 18.78, height: 18),
                                          SizedBox(height: 10),
                                          Text(
                                            StringContent.transaction,
                                            style: TextStyle(
                                                color: value.tabPosition == 2 ? themeValue.themeType == 0 ? ThemeConstant.white :
                                                ThemeConstant.lightPurple : Color.fromRGBO(164, 164, 164, 1.0),
                                                wordSpacing: 1,
                                                fontSize: 10,
                                                fontFamily: "BarlowRegular",
                                                letterSpacing: 2
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(width: 24)
                                    ],
                                  ),

                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.transparent,
                                      onTap: (){
                                        _homeController.changeTab(2);
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),flex: 1),
                          ],
                        ),
                      )
                    ],
                  ),

                  Column(
                    children: [
                      Expanded(child: SizedBox(), flex: 1),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            child: Image.asset("assets/images/img_polygon.png", width: 65, height: 65),
                            onTap: (){
                              showBottomSheet(context,themeValue);
                            },
                          )
                        ],
                      ),
                      SizedBox(height: 7)
                    ],
                  ),

                  GetBuilder<HomeController>(
                      id: "dialog_showed",
                      init: HomeController(),
                      builder: (dialog_value) =>  Visibility(child: GestureDetector(
                        child: dialogWidgetType(dialog_value.dialogType),
                        onTap: (){
                          _homeController.isShowDialog(false,0);
                        },
                      ),
                          visible: dialog_value.isDialogShowed)),

                  GetBuilder<HomeController>(
                      id: "show_up",
                      init: HomeController(),
                      builder: (showUpValue) => showUpValue.isNotificationOn ? showUpValue.isShowUpNotification ? Container(
                        margin: EdgeInsets.only(top: 35),
                        width: double.maxFinite,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(child: CustomShowUp.showUpNotification(showUpValue.showUpMessage
                                ,_homeController), flex: 1)
                          ],
                        ),
                      ) : SizedBox() : SizedBox()),

                  Visibility(child: Container(
                    width: double.maxFinite,
                    height: double.maxFinite,
                    color: Colors.transparent,
                    child: Center(
                      child: Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.3),
                            borderRadius: BorderRadius.all(Radius.circular(8))
                        ),
                        child: Center(
                          child: CircularProgressIndicator(
                            color: Color.fromRGBO(106, 48, 147, 1.0),
                          ),
                        ),
                      ),
                    ),
                  ), visible: _homeController.isWalletLoading)
                ],
              )),
        ),
      ),
    ), onWillPop: _homeController.onBackPress);
  }

  Widget dialogWidgetType(int type){
    if(type == 1){
      return SelectNetworkDialogView(_homeController);
    }
    else if(type == 2){
      return SelecttransactionTypeDialogView(_homeController);
    }
    else if(type == 3){
      return SelectAssetDialogView(_homeController);
    }
    else if(type == 4){
      return SelectDateDialogView(_homeController);
    }
    else{
      return SelectSortBy(_homeController);
    }
  }

  showBottomSheet(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 150.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 150.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    Expanded(child: SizedBox(), flex: 1),
                    Container(
                      width: double.maxFinite,
                      height: 150,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                              themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                            ]
                        ),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: 24),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                child: Container(
                                  color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Image.asset(themeValue.themeType == 0 ? "assets/images/img_send.png"
                                          : "assets/images/img_send_light.png", height: 65, width: 65),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.send,
                                        style: TextStyle(
                                            color: Color.fromRGBO(255, 197, 48, 1),
                                            fontSize: 16,
                                            fontFamily: "BarlowNormal",
                                            letterSpacing: 0.5
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  Get.back();
                                  Get.to(() => SendView(), arguments: {
                                    "user_model" : _homeController.selectedUserModel,
                                    "ledger_model" : _homeController.selectedLedger
                                  })?.then((value) {
                                    if(Platform.isAndroid){
                                      FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
                                    }
                                  });
                                },
                              ),
                              SizedBox(width: 100),
                              GestureDetector(
                                child: Container(
                                  color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Image.asset(themeValue.themeType == 0 ? "assets/images/img_receive.png"
                                          : "assets/images/img_receive_light.png", height: 65, width: 65),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.receive,
                                        style: TextStyle(
                                            color: Color.fromRGBO(160, 68, 255, 1.0),
                                            fontSize: 16,
                                            fontFamily: "BarlowNormal",
                                            letterSpacing: 0.5
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  Get.back();
                                  Get.to(() => ReceiveView(), arguments: {
                                    "user_model" : _homeController.selectedUserModel,
                                    "ledger_model" : _homeController.selectedLedger
                                  });
                                },
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }

  showDAppsBottomSheet(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return GetBuilder<HomeController>(
              init: HomeController(),
              id: "managed_account",
              builder: (manageValue) => Container(
                width: double.maxFinite,
                height: 475.5,
                child: GetBuilder<HomeController  >(
                  init: HomeController(),
                  id: "request_connect",
                  builder: (connectValue) => Stack(
                    children: [
                      Container(
                        width: double.maxFinite,
                        height: 475.5,
                        decoration: BoxDecoration(
                          color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(24),
                        ),
                      ),
                      Column(
                        children: [
                          SizedBox(height: 1),
                          Expanded(child: SizedBox(child: Container(
                            width: double.maxFinite,
                            height: 475,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                                  ]
                              ),
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                            ),
                            child: Column(
                              children: [
                                Expanded(child: SizedBox(), flex: 1),

                                CircleAvatar(
                                  radius: 36,
                                  backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                                  child: CircleAvatar(
                                    radius: 34,
                                    backgroundImage: Image.network(connectValue.wcPeerMeta?.icons[0] ?? "").image,
                                  ),
                                ),

                                SizedBox(height: 16),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                        "${connectValue.wcPeerMeta?.name ?? ""} ",
                                        style: TextStyle(
                                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                            fontSize: 18,
                                            fontFamily: "BarlowMedium",
                                            height: 1.5
                                        )),
                                    Text(
                                        "wants to",
                                        style: TextStyle(
                                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                            fontSize: 16,
                                            fontFamily: "BarlowRegular",
                                            height: 1.5
                                        )),
                                  ],
                                ),

                                Text(
                                    "connect to your account",
                                    style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                        fontSize: 16,
                                        fontFamily: "BarlowRegular",
                                        height: 1.5
                                    )),

                                SizedBox(height: 16),

                                Text(
                                    connectValue.wcPeerMeta?.url ?? "",
                                    style: TextStyle(
                                        color: Color.fromRGBO(108, 216, 107, 1),
                                        fontSize: 14,
                                        fontFamily: "BarlowMedium",
                                        height: 1.5
                                    )),

                                GestureDetector(
                                  child: Container(
                                    margin: EdgeInsets.only(left: 24, right: 24, top: 32),
                                    width: double.maxFinite,
                                    padding: EdgeInsets.all(16),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                                          width: 1
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(16)),
                                    ),
                                    child: Row(
                                      children: [
                                        Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_wallet.png"
                                            : "assets/icons/ic_black_wallet.png", height: 20, width: 20),
                                        SizedBox(width: 16),
                                        Expanded(child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                    connectValue.selectedWalletUserModel != null ? connectValue.selectedWalletUserModel!.userName.isEmpty ?
                                                    MyUtils.parseAddress(connectValue.selectedWalletUserModel!.address) : connectValue.selectedWalletUserModel!.userName: "",
                                                    style: TextStyle(
                                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                        fontSize: 14,
                                                        fontFamily: "BarlowMedium",
                                                        height: 1.5
                                                    ))
                                              ],
                                            ),
                                            SizedBox(height: 6),
                                            Row(
                                              children: [
                                                Text(
                                                    "${connectValue.selectedWalletUserModel != null ?
                                                    MyUtils.parseAlgoToMicro(connectValue.selectedWalletUserModel!.amount) : 0} ALGO",
                                                    style: TextStyle(
                                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                        fontSize: 12,
                                                        fontFamily: "BarlowRegular",
                                                        height: 1.5
                                                    ))
                                              ],
                                            )
                                          ],
                                        ), flex: 1),
                                        _homeController.userModelList.length > 1 ? Row(
                                          children: [
                                            SizedBox(width: 16),
                                            SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                                : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                                width: themeValue.themeType == 0 ? 22 : 16)
                                          ],
                                        ) : SizedBox()
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    if(_homeController.userModelList.length > 1){
                                      Get.back();
                                      _homeController.apiRepository.getUsersAmount(_homeController.userModelList,
                                          _homeController.selectedLedger.algodUrl, _homeController.currentCurrency,
                                          _homeController.userAmountLoading);
                                      showAccountsBottomSheet(context, themeValue);
                                    }
                                  },
                                ),

                                SizedBox(height: 32),

                                GetBuilder<HomeController>(
                                  init: HomeController(),
                                  id: "connect_button",
                                  builder: (connectValue) => Container(
                                    width: double.maxFinite,
                                    margin: EdgeInsets.only(left: 24, right: 24),
                                    height: 45,
                                    child: Stack(
                                      children: [
                                        CustomButton(45, double.maxFinite, BoxDecoration(
                                          gradient: LinearGradient(
                                              begin: Alignment.topLeft,
                                              end: Alignment.topRight,
                                              colors: [
                                                Color.fromRGBO(106, 48, 147, 1.0) ,
                                                Color.fromRGBO(160, 68, 255, 1.0)
                                              ]
                                          ),
                                          borderRadius: BorderRadius.all(Radius.circular(8)),
                                        ), StringContent.connect, TextStyle(
                                            color: Colors.white,
                                            fontFamily: "BarlowRegular",
                                            fontSize: 12,
                                            letterSpacing: 2
                                        )),

                                        Material(
                                          color: Colors.transparent,
                                          child: InkWell(
                                            splashColor: Colors.transparent,
                                            onTap: (){
                                              _homeController.approve();
                                            },
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),

                                GestureDetector(
                                  child: Container(
                                    padding: EdgeInsets.all(16),
                                    child: Text(
                                      StringContent.close,
                                      style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                        fontSize: 14,
                                        fontFamily: "BarlowSemiBold",
                                        decorationThickness: 1.5,
                                      ),
                                    ),
                                  ),
                                  onTap: (){
                                    _homeController.wcClient?.killSession();
                                    Get.back();
                                  },
                                ),

                                SizedBox(height: StringContent.bottomMargin)
                              ],
                            ),
                          )), flex: 1),
                        ],
                      ),
                    ],
                  ),
                ),
              ));
        });
  }

  showStatusBottomSheet(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 350.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 350.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            CircleAvatar(
                              radius: 36,
                              backgroundColor: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                              child: CircleAvatar(
                                radius: 34,
                                backgroundImage: Image.network(_homeController.wcPeerMeta?.icons[0] ?? "").image,
                              ),
                            ),
                            SizedBox(height: 24),
                            Text(
                              "You are Connected to ${_homeController.wcPeerMeta?.name}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 18,
                                fontFamily: "BarlowSemiBold",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 16),
                            Text(
                              "Please return to ${_homeController.wcPeerMeta?.name} to continue and finish your operation. "
                                  "You can find all connected sessions in the dedicated setting page.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                height: 1.5,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            Expanded(child: SizedBox(), flex: 1),
                            Container(
                              width: double.maxFinite,
                              margin: EdgeInsets.only(left: 24, right: 24),
                              height: 45,
                              child: Stack(
                                children: [
                                  CustomButton(45, double.maxFinite, BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.topRight,
                                        colors: [
                                          Color.fromRGBO(106, 48, 147, 1.0),
                                          Color.fromRGBO(160, 68, 255, 1.0)
                                        ]
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                  ), StringContent.close, TextStyle(
                                      color: Colors.white,
                                      fontFamily: "BarlowRegular",
                                      fontSize: 12,
                                      letterSpacing: 2
                                  )),

                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.transparent,
                                      onTap: (){
                                        Get.back();
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height: 24),
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  showStatusBottomSheetRequest(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 275.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 275.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Text(
                              "TRANSACTION REQUEST",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowSemiBold",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 16),
                            Text(
                              StringContent.transaction_request,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 12,
                                height: 1.5,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            Expanded(child: SizedBox(), flex: 1),
                            Container(
                              width: double.maxFinite,
                              height: 45,
                              child: Stack(
                                children: [
                                  CustomButton(45, double.maxFinite, BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.topRight,
                                        colors: [
                                          Color.fromRGBO(106, 48, 147, 1.0),
                                          Color.fromRGBO(160, 68, 255, 1.0)
                                        ]
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                  ), StringContent.close, TextStyle(
                                      color: Colors.white,
                                      fontFamily: "BarlowRegular",
                                      fontSize: 12,
                                      letterSpacing: 2
                                  )),

                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.transparent,
                                      onTap: (){
                                        Get.back();
                                        if(_homeController.walletTransactionDetailList.length == 1){
                                          _homeController.selectedWalletDetail = _homeController.walletTransactionDetailList[0];
                                          showOptInBottomSheetRequest(context,themeValue);
                                        }
                                        else{
                                          showUnsignedBottomSheetRequest(context,themeValue);
                                        }
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height: 24),
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }

  showOptInBottomSheetRequest(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 375.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 375.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Text(
                              _homeController.apanNumber != - 1 ? "Application Call" : "POSSIBLE ASSETS OPT-IN REQUEST",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowSemiBold",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 36),
                            Text(
                              _homeController.apanNumber != - 1 ? "#${_homeController.appId}" : _homeController.walletTransactionDetailList[0].asset,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                height: 1.5,
                                fontFamily: "BarlowBold",
                                decorationThickness: 1.5,
                              ),
                            ),
                            Text(
                              _homeController.apanNumber != - 1 ? "Application ID" : "${_homeController.walletTransactionDetailList[0].assetId}",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 12,
                                height: 1.5,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 36),
                            Row(
                              children: [
                                Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_wallet.png"
                                    : "assets/icons/ic_black_wallet.png", height: 22, width: 22),
                                SizedBox(width: 16),
                                Text(
                                  MyUtils.parseAddress(_homeController.walletTransactionDetailList[0].from),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    height: 1.5,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 8),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "Network Fee",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 14,
                                    height: 1.5,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Text(
                                  "0.001 ALGO",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    height: 1.5,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 8),
                            Row(
                              children: [
                                GestureDetector(
                                  child: Container(
                                    height: 24,
                                    color: Colors.transparent,
                                    child: Text(
                                      "Show Transaction Details",
                                      style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                        fontSize: 12,
                                        height: 1.5,
                                        fontFamily: "BarlowRegular",
                                        decorationThickness: 1.5,
                                      ),
                                    ),
                                  ),
                                  onTap: (){
                                    Get.back();
                                    _homeController.apanNumber != - 1 ? showApplicationDetailsBottomSheetRequest(context, themeValue)
                                        : showTransactionDetailsBottomSheetRequest(context, themeValue);
                                  },
                                )
                              ],
                            ),
                            SizedBox(height: 32),
                            Container(
                              width: double.maxFinite,
                              height: 45,
                              child: Stack(
                                children: [
                                  CustomButton(45, double.maxFinite, BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.topRight,
                                        colors: [
                                          Color.fromRGBO(106, 48, 147, 1.0),
                                          Color.fromRGBO(160, 68, 255, 1.0)
                                        ]
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                  ), "CONFIRM", TextStyle(
                                      color: Colors.white,
                                      fontFamily: "BarlowRegular",
                                      fontSize: 12,
                                      letterSpacing: 2
                                  )),

                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.transparent,
                                      onTap: (){
                                        Get.back();
                                        showConfirmBottomSheetRequest(_homeController.buildContext, _homeController);
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            GestureDetector(
                              child: Container(
                                padding: EdgeInsets.all(16),
                                child: Text(
                                  StringContent.cancel,
                                  style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowSemiBold",
                                      decorationThickness: 1.5,
                                      decoration: TextDecoration.underline
                                  ),
                                ),
                              ),
                              onTap: (){
                                _homeController.wcClient?.rejectRequest(id: _homeController.requestId);
                                Get.back();
                              },
                            ),
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  showConfirmBottomSheetRequest(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 315.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 315.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Text(
                              "ARE YOU SURE?",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 18,
                                fontFamily: "BarlowSemiBold",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 16),
                            Text(
                              StringContent.confirmation,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                height: 1.5,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 55),
                            Container(
                              width: double.maxFinite,
                              height: 45,
                              child: Stack(
                                children: [
                                  CustomButton(45, double.maxFinite, BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.topRight,
                                        colors: [
                                          Color.fromRGBO(106, 48, 147, 1.0),
                                          Color.fromRGBO(160, 68, 255, 1.0)
                                        ]
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                  ), StringContent.accept, TextStyle(
                                      color: Colors.white,
                                      fontFamily: "BarlowRegular",
                                      fontSize: 12,
                                      letterSpacing: 2
                                  )),

                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.transparent,
                                      onTap: (){
                                        Get.back();
                                        _homeController.signTransaction();
                                        showBeingProcessBottomSheetRequest(context,themeValue);
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            GestureDetector(
                              child: Container(
                                padding: EdgeInsets.all(16),
                                child: Text(
                                  StringContent.cancel,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 14,
                                    fontFamily: "BarlowSemiBold",
                                    decoration: TextDecoration.underline,
                                    decorationThickness: 1.5,
                                  ),
                                ),
                              ),
                              onTap: (){
                                _homeController.wcClient?.rejectRequest(id: _homeController.requestId);
                                Get.back();
                              },
                            ),
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  showBeingProcessBottomSheetRequest(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 255.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 255.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Text(
                              "YOUR TRANSACTION IS BEING PROCESSED",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowSemiBold",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 16),
                            Text(
                              "The transaction has been signed and sent to ${_homeController.dapsName}. "
                                  "Please visit ${_homeController.dapsName} for the remaining process.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                height: 1.5,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            Expanded(child: SizedBox(), flex: 1),
                            Container(
                              width: double.maxFinite,
                              margin: EdgeInsets.only(left: 24, right: 24),
                              height: 45,
                              child: Stack(
                                children: [
                                  CustomButton(45, double.maxFinite, BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.topRight,
                                        colors: [
                                          Color.fromRGBO(106, 48, 147, 1.0),
                                          Color.fromRGBO(160, 68, 255, 1.0)
                                        ]
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                  ), StringContent.close, TextStyle(
                                      color: Colors.white,
                                      fontFamily: "BarlowRegular",
                                      fontSize: 12,
                                      letterSpacing: 2
                                  )),

                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.transparent,
                                      onTap: (){
                                        Get.back();
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height: 24),
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  showUnsignedBottomSheetRequest(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 335.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 335.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Text(
                              "UNSIGNED TRANSACTIONS",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowSemiBold",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 24),
                            Container(
                              width: double.maxFinite,
                              padding: EdgeInsets.all(16),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(8)),
                                  border: Border.all(
                                      width: 1,
                                      color: Colors.white
                                  )
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Multiple Transaction Request",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowSemiBold",
                                      decorationThickness: 1.5,
                                    ),
                                  ),
                                  SizedBox(height: 8),
                                  Text(
                                    "${_homeController.walletTransactionDetailList.length} Txns-Group ID: 85r2..du8f=",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 12,
                                      fontFamily: "BarlowRegular",
                                      decorationThickness: 1.5,
                                    ),
                                  ),
                                  SizedBox(height: 24),
                                  GestureDetector(
                                    child: Container(
                                      height: 24,
                                      color: Colors.transparent,
                                      child: Text(
                                        "Show Transaction Details",
                                        style: TextStyle(
                                          color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                          fontSize: 12,
                                          height: 1.5,
                                          fontFamily: "BarlowRegular",
                                          decorationThickness: 1.5,
                                        ),
                                      ),
                                    ),
                                    onTap: (){
                                      Get.back();
                                      showDetailMultipleBottomSheetRequest(context,themeValue);
                                    },
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height: 24),
                            Container(
                              width: double.maxFinite,
                              height: 45,
                              child: Stack(
                                children: [
                                  CustomButton(45, double.maxFinite, BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.topRight,
                                        colors: [
                                          Color.fromRGBO(106, 48, 147, 1.0),
                                          Color.fromRGBO(160, 68, 255, 1.0)
                                        ]
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                  ), "CONFIRM ALL", TextStyle(
                                      color: Colors.white,
                                      fontFamily: "BarlowRegular",
                                      fontSize: 12,
                                      letterSpacing: 2
                                  )),

                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.transparent,
                                      onTap: (){
                                        Get.back();
                                        showConfirmBottomSheetRequest(_homeController.buildContext, _homeController);
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            GestureDetector(
                              child: Container(
                                padding: EdgeInsets.all(16),
                                child: Text(
                                  StringContent.cancel,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 14,
                                    fontFamily: "BarlowSemiBold",
                                    decorationThickness: 1.5,
                                  ),
                                ),
                              ),
                              onTap: (){
                                _homeController.wcClient?.rejectRequest(id: _homeController.requestId);
                                Get.back();
                              },
                            ),
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  showDetailMultipleBottomSheetRequest(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: double.maxFinite,
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / 3),
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: double.maxFinite,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 16),
                            Row(
                              children: [
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    width: 45,
                                    height: 45,
                                    child: Center(
                                      child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                                      "assets/icons/ic_black_back.png", width: 24,
                                          height: 24, fit: BoxFit.contain),
                                    ),
                                  ),
                                  onTap: (){
                                    Get.back();
                                    showUnsignedBottomSheetRequest(context, themeValue);
                                  },
                                ),
                                Expanded(child: Text(
                                  "MULTIPLE TRANSACTION REQUEST",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 14,
                                    fontFamily: "BarlowSemiBold",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),

                                Container(
                                  color: Colors.transparent,
                                  width: 45,
                                  height: 45,
                                )
                              ],
                            ),
                            Expanded(child: ListView.builder(itemBuilder: (context,index){
                              WalletTransactionDetail walletDetail = _homeController.walletTransactionDetailList[index];
                              return Container(
                                width: double.maxFinite,
                                margin: EdgeInsets.only(top: 24),
                                padding: EdgeInsets.all(16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    border: Border.all(
                                        width: 1,
                                        color: Colors.white
                                    )
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_wallet.png"
                                            : "assets/icons/ic_black_wallet.png", height: 16, width: 16),

                                        SizedBox(width: 8),
                                        Text(
                                          walletDetail.from,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                            fontSize: 12,
                                            fontFamily: "BarlowRegular",
                                            decorationThickness: 1.5,
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 24),
                                    Text(
                                      walletDetail.type != "appl" ? walletDetail.amount :
                                      "Application Call to #${walletDetail.applicationId}",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                        fontSize: 14,
                                        fontFamily: "BarlowSemiBold",
                                        decorationThickness: 1.5,
                                      ),
                                    ),
                                    SizedBox(height: 8),
                                    Text(
                                      walletDetail.type != "appl" ? walletDetail.price.isEmpty ? "-" : walletDetail.type == "pay" ? "${_homeController.currentCurrencySymbol}${MyUtils.formatPrice(MyUtils.parseAlgoPrice(double.parse(MyUtils.parseAlgoToMicro(
                                          walletDetail.amounts)), double.parse(walletDetail.price.isNotEmpty ? walletDetail.price : "0")))}" : "${_homeController.currentCurrencySymbol}${MyUtils.formatPrice(MyUtils.countAssetPrice(
                                          walletDetail.price, double.parse(walletDetail.amounts.toString()), walletDetail.decimals,1))}" : "",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                        fontSize: 12,
                                        fontFamily: "BarlowRegular",
                                        decorationThickness: 1.5,
                                      ),
                                    ),
                                    SizedBox(height: walletDetail.type != "appl" ? 16 : 0),
                                    GestureDetector(
                                      child: Container(
                                        height: 24,
                                        color: Colors.transparent,
                                        child: Text(
                                          "Show Transaction Details",
                                          style: TextStyle(
                                            color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                            fontSize: 12,
                                            height: 1.5,
                                            fontFamily: "BarlowRegular",
                                            decorationThickness: 1.5,
                                          ),
                                        ),
                                      ),
                                      onTap: (){
                                        _homeController.selectedWalletDetail =  _homeController.walletTransactionDetailList[index];
                                        Get.back();
                                        if(walletDetail.type != "appl"){
                                          showTransactionDetailsBottomSheetRequest(context,themeValue);
                                        }
                                        else{
                                          showApplicationDetailsBottomSheetRequest(context,themeValue);
                                        }
                                      },
                                    )
                                  ],
                                ),
                              );
                            },
                              padding: EdgeInsets.zero,
                              shrinkWrap: true,
                              itemCount: _homeController.walletTransactionDetailList.length,
                            ), flex: 1)
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  showTransactionDetailsBottomSheetRequest(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 555.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 555.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 16),
                            Row(
                              children: [
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    height: 45,
                                    child: Center(
                                      child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                                      "assets/icons/ic_black_back.png", width: 24,
                                          height: 24, fit: BoxFit.contain),
                                    ),
                                  ),
                                  onTap: (){
                                    Get.back();
                                    if(_homeController.walletTransactionDetailList.length == 1){
                                      _homeController.selectedWalletDetail = _homeController.walletTransactionDetailList[0];
                                      showOptInBottomSheetRequest(context, themeValue);
                                    }
                                    else{
                                      showDetailMultipleBottomSheetRequest(context, themeValue);
                                    }
                                  },
                                ),
                                Expanded(child: Text(
                                  "TRANSACTION DETAILS",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 14,
                                    fontFamily: "BarlowSemiBold",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),

                                Container(
                                  color: Colors.transparent,
                                  height: 45,
                                )
                              ],
                            ),
                            SizedBox(height: 32),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "From",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Expanded(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_wallet.png"
                                        : "assets/icons/ic_black_wallet.png", height: 18, width: 18),

                                    SizedBox(width: 8),
                                    Text(
                                      _homeController.selectedWalletDetail?.from ?? "",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                        fontSize: 12,
                                        fontFamily: "BarlowRegular",
                                        decorationThickness: 1.5,
                                      ),
                                    )
                                  ],
                                ), flex: 3)
                              ],
                            ),
                            SizedBox(height: 24),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "To",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Expanded(child: Text(
                                  _homeController.selectedWalletDetail?.to ?? "",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 3)
                              ],
                            ),
                            SizedBox(height: 24),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "Balance",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Expanded(child: Text(
                                  _homeController.selectedWalletDetail?.balance ?? "",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 3)
                              ],
                            ),
                            SizedBox(height: 24),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "Asset",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Expanded(child: Text(
                                  _homeController.selectedWalletDetail?.asset ?? "",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 3)
                              ],
                            ),
                            SizedBox(height: 24),
                            Container(
                              width: double.maxFinite,
                              height: 1,
                              color: Colors.white,
                            ),
                            SizedBox(height: 24),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "Amount",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Expanded(child: Text(
                                  _homeController.selectedWalletDetail?.amount ?? "",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 3)
                              ],
                            ),
                            SizedBox(height: 24),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "Fee",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Expanded(child: Text(
                                  _homeController.selectedWalletDetail?.fee ?? "",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 3)
                              ],
                            ),
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  showApplicationDetailsBottomSheetRequest(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 300.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 300.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 16),
                            Row(
                              children: [
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    height: 45,
                                    child: Center(
                                      child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                                      "assets/icons/ic_black_back.png", width: 24,
                                          height: 24, fit: BoxFit.contain),
                                    ),
                                  ),
                                  onTap: (){
                                    Get.back();
                                    if(_homeController.walletTransactionDetailList.length == 1){
                                      _homeController.selectedWalletDetail = _homeController.walletTransactionDetailList[0];
                                      showOptInBottomSheetRequest(context, themeValue);
                                    }
                                    else{
                                      showDetailMultipleBottomSheetRequest(context, themeValue);
                                    }
                                  },
                                ),
                                Expanded(child: Text(
                                  "APPLICATION CALL",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 14,
                                    fontFamily: "BarlowSemiBold",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),

                                Container(
                                  color: Colors.transparent,
                                  height: 45,
                                )
                              ],
                            ),
                            SizedBox(height: 32),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "From",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Expanded(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_wallet.png"
                                        : "assets/icons/ic_black_wallet.png", height: 18, width: 18),

                                    SizedBox(width: 8),
                                    Text(
                                      _homeController.selectedWalletDetail?.from ?? "",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                        fontSize: 12,
                                        fontFamily: "BarlowRegular",
                                        decorationThickness: 1.5,
                                      ),
                                    )
                                  ],
                                ), flex: 3)
                              ],
                            ),
                            SizedBox(height: 24),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "Application ID",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Expanded(child: Text(
                                  "${_homeController.selectedWalletDetail?.applicationId ?? 0}",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 3)
                              ],
                            ),
                            SizedBox(height: 24),
                            Container(
                              width: double.maxFinite,
                              height: 1,
                              color: Colors.white,
                            ),
                            SizedBox(height: 24),
                            Row(
                              children: [
                                Expanded(child: Text(
                                  "Fee",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 1),
                                Expanded(child: Text(
                                  "${_homeController.selectedWalletDetail?.fee}",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    decorationThickness: 1.5,
                                  ),
                                ), flex: 3)
                              ],
                            ),
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  showAccountsBottomSheet(BuildContext context, HomeController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return GetBuilder<HomeController>(
            id: "accounts",
            init: HomeController(),
            builder: (value) => Container(
              width: double.maxFinite,
              height: 655.5,
              child: Stack(
                children: [
                  Container(
                    width: double.maxFinite,
                    height: 655.5,
                    decoration: BoxDecoration(
                      color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(24),
                    ),
                  ),
                  Column(
                    children: [
                      SizedBox(height: 1),
                      Expanded(
                        child: Container(
                          width: double.maxFinite,
                          padding: EdgeInsets.only(left: 24, right: 24),
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                  themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                                ]
                            ),
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                          ),
                          child: Column(
                            children: [
                              SizedBox(height: 16),
                              Row(
                                children: [
                                  GestureDetector(
                                    child: Container(
                                      color: Colors.transparent,
                                      width: 45,
                                      height: 45,
                                      child: Center(
                                        child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                                        "assets/icons/ic_black_back.png", width: 24,
                                            height: 24, fit: BoxFit.contain),
                                      ),
                                    ),
                                    onTap: (){
                                      Get.back();
                                      showDAppsBottomSheet(_homeController.buildContext, _homeController);
                                    },
                                  ),
                                  Expanded(child: Text(
                                    "ACCOUNTS",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowSemiBold",
                                      decorationThickness: 1.5,
                                    ),
                                  ), flex: 1),

                                  Container(
                                    color: Colors.transparent,
                                    width: 45,
                                    height: 45,
                                  )
                                ],
                              ),
                              Expanded(child: !_homeController.userAmountLoading["loading"] ? ListView.builder(itemBuilder: (context,index){
                                UserModel userModel = _homeController.userModelList[index];
                                return GestureDetector(
                                  child: Container(
                                    width: double.maxFinite,
                                    margin: EdgeInsets.only(top: 24, bottom: index == _homeController.userModelList.length - 1 ? 36 : 0),
                                    padding: EdgeInsets.all(16),
                                    decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                        border: Border.all(
                                          width: 1,
                                          color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                                        )
                                    ),
                                    child: Row(
                                      children: [
                                        Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_wallet.png"
                                            : "assets/icons/ic_black_wallet.png", height: 20, width: 20),
                                        SizedBox(width: 16),
                                        Expanded(child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              userModel.userName.isNotEmpty ? userModel.userName : MyUtils.parseAddress(userModel.address),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                fontSize: 14,
                                                fontFamily: "BarlowSemiBold",
                                                decorationThickness: 1.5,
                                              ),
                                            ),
                                            SizedBox(height: 8),
                                            Text(
                                              "${MyUtils.parseAlgoToMicro(userModel.amount)} ALGO",
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                                fontFamily: "BarlowRegular",
                                                decorationThickness: 1.5,
                                              ),
                                            ),
                                          ],
                                        ), flex: 1)
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    Get.back();
                                    _homeController.selectedWalletUserModel = _homeController.userModelList[index];
                                    showDAppsBottomSheet(_homeController.buildContext, _homeController);
                                  },
                                );
                              },
                                padding: EdgeInsets.zero,
                                shrinkWrap: true,
                                itemCount: _homeController.userModelList.length,
                              ) : Container(
                                width: double.maxFinite,
                                height: double.maxFinite,
                                child: Center(
                                  child: CircularProgressIndicator(color: Color.fromRGBO(106, 48, 147, 1.0)),
                                ),
                              ), flex: 1)
                            ],
                          ),
                        ),
                        flex: 1,
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  void onScanned(String data) {
    _homeController.requestClient(data);
  }

  @override
  void onDAppsLoading() {
    Map data = {};
    data["loading"] = true;
    FBroadcast.instance().broadcast(MyConstant.BROADCAST_SETTING, value: data);
    _homeController.showWalletConnectLoading(true);
  }

  @override
  void onDAppsRequested() {
    Map data = {};
    data["loading"] = false;
    FBroadcast.instance().broadcast(MyConstant.BROADCAST_SETTING, value: data);
    _homeController.showWalletConnectLoading(false);
    showDAppsBottomSheet(_homeController.buildContext, _homeController);
  }

  @override
  void onDAppsFailed(String message) {
    Map data = {};
    data["loading"] = false;
    FBroadcast.instance().broadcast(MyConstant.BROADCAST_SETTING, value: data);
    _homeController.showWalletConnectLoading(false);
    if(message.isNotEmpty){
      if(message == "Connected"){
        showStatusBottomSheet(_homeController.buildContext, _homeController);
      }
      else if(message == "AlgoSign"){
        showStatusBottomSheetRequest(_homeController.buildContext, _homeController);
      }
    }
  }
}

