import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/home/controllers/home_controller.dart';
import 'package:waagmiswap/app/util/my_util.dart';

class SelectDateDialogView extends StatelessWidget {

  HomeController _homeController;
  SelectDateDialogView(this._homeController);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        id: "theme",
        init: HomeController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          color: Color.fromRGBO(20, 21, 40, 0.7),
          child: Column(
            children: [
              SizedBox(height: 100),
              GetBuilder<HomeController>(
                  id: "date",
                  init: HomeController(),
                  builder: (value) => Stack(
                    children: [
                      Visibility(child: Container(
                        margin: EdgeInsets.only(left: 24, right: 24),
                        width: double.maxFinite,
                        height: 510,
                        child: Column(
                          children: [
                            SizedBox(height: 16),
                            Text(
                              StringContent.date.toUpperCase(),
                              style: TextStyle(
                                  color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                  wordSpacing: 1,
                                  fontSize: 14,
                                  fontFamily: "BarlowMedium",
                                  letterSpacing: 2
                              ),
                            ),
                            SizedBox(height: 16),
                            Container(
                              width: double.maxFinite,
                              height: 1,
                              color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                            ),

                            Expanded(child: SizedBox(), flex: 1),

                            Container(
                              width: double.maxFinite,
                              height: 405,
                              child: Column(
                                children: [
                                  GetBuilder<HomeController>(
                                      id: "month_year",
                                      init: HomeController(),
                                      builder: (value) => Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          GestureDetector(
                                            child: GestureDetector(
                                              child: Container(
                                                height: 55,
                                                width: 55,
                                                color: Colors.transparent,
                                                child: Center(
                                                  child: Image.asset("assets/icons/ic_left_purple.png", width: 22,height: 22),
                                                ),
                                              ),
                                              onTap: (){
                                                value.prevDate();
                                              },
                                            ),
                                          ),
                                          GestureDetector(
                                            child: Container(
                                              color: Colors.transparent,
                                              child: Row(
                                                children: [
                                                  SizedBox(width: 16),
                                                  Text(value.currentMonth, style: TextStyle(
                                                      fontSize: 14,
                                                      color: ThemeConstant.darkPurple,
                                                      fontFamily: "BarlowBold"
                                                  )),
                                                  SizedBox(width: 4),
                                                  Text(value.currentyear, style: TextStyle(
                                                      fontSize: 14,
                                                      color: ThemeConstant.darkPurple,
                                                      fontFamily: "BarlowBold"
                                                  )),
                                                  SizedBox(width: 16)
                                                ],
                                              ),
                                            ),
                                            onTap: (){
                                              value.showCalendarFilter(1);
                                            },
                                          ),
                                          GestureDetector(
                                            child: Container(
                                              height: 55,
                                              width: 55,
                                              color: Colors.transparent,
                                              child: Center(
                                                child: Image.asset("assets/icons/ic_right_purple.png", width: 22,height: 22),
                                              ),
                                            ),
                                            onTap: (){
                                              value.nextDate();
                                            },
                                          )
                                        ],
                                      )),

                                  CalendarCarousel<Event>(
                                    targetDateTime: value.targetDateTime,
                                    onDayPressed: (DateTime date, List<Event> events) {
                                      value.setSelectedDate(date);
                                    },
                                    firstDayOfWeek: 1,
                                    minSelectedDate: DateFormat("yyyy-MM-dd").parse(
                                        "1800-01-01"),
                                    weekendTextStyle: TextStyle(
                                      fontFamily: "BarlowRegular",
                                      color: Color.fromRGBO(144, 144, 144, 1.0),
                                    ),
                                    weekDayFormat: WeekdayFormat.short,
                                    weekdayTextStyle: TextStyle(
                                      fontFamily: "BarlowRegular",
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    ),
                                   /* selectedDayTextStyle: TextStyle(
                                      fontFamily: "BarlowRegular",
                                      color: Colors.white,
                                    ),*/
                                    todayButtonColor: Colors.transparent,
                                   /* todayTextStyle: TextStyle(
                                      fontFamily: "BarlowRegular",
                                      color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                    ),*/
                                    prevDaysTextStyle: TextStyle(
                                      fontFamily: "BarlowRegular",
                                      color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark.withOpacity(0.5) : ThemeConstant.secondTextGrayLight.withOpacity(0.5),
                                    ),
                                    daysTextStyle: TextStyle(
                                      fontFamily: "BarlowRegular",
                                      color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                    ),
                                    nextDaysTextStyle: TextStyle(
                                      fontFamily: "BarlowRegular",
                                      color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark.withOpacity(0.5) : ThemeConstant.secondTextGrayLight.withOpacity(0.5),
                                    ),
                                    todayBorderColor: Colors.transparent,
                                    selectedDayButtonColor: Colors.transparent,
                                    selectedDayBorderColor: Colors.transparent,
                                    showHeader: false,
                                    customDayBuilder: (
                                        bool isSelectable,
                                        int index,
                                        bool isSelectedDay,
                                        bool isToday,
                                        bool isPrevMonthDay,
                                        TextStyle textStyle,
                                        bool isNextMonthDay,
                                        bool isThisMonthDay,
                                        DateTime day,
                                        ) {
                                      if(isToday && MyUtils.parseSingleDate(day, "yyyy-MM-dd") == MyUtils.parseSingleDate(value.startDate, "yyyy-MM-dd")){
                                        return Material(
                                          elevation: 5,
                                          color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                          borderRadius: BorderRadius.all(Radius.circular(25)),
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                                borderRadius: BorderRadius.all(Radius.circular(25))
                                            ),
                                            child: Center(
                                              child: Text("${day.day}", style: TextStyle(
                                                color: Colors.white,
                                              )),
                                            ),
                                          ),
                                        );
                                      }
                                      else{
                                        if(day == value.startDate) {
                                          if(isToday && day != value.startDate){
                                            return null;
                                          }
                                          else{
                                            return Material(
                                              elevation: 5,
                                              color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                              borderRadius: BorderRadius.all(Radius.circular(25)),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                    color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                                    borderRadius: BorderRadius.all(Radius.circular(25))
                                                ),
                                                child: Center(
                                                  child: Text("${day.day}", style: TextStyle(
                                                    color: Colors.white,
                                                  )),
                                                ),
                                              ),
                                            );
                                          }
                                        }
                                        else{
                                          if(value.endDate != null){
                                            if(day.millisecondsSinceEpoch >= value.startDate.millisecondsSinceEpoch &&
                                                day.millisecondsSinceEpoch <= value.endDate!.millisecondsSinceEpoch){
                                              return Material(
                                                elevation: 5,
                                                color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                                borderRadius: BorderRadius.all(Radius.circular(25)),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                                      borderRadius: BorderRadius.all(Radius.circular(25))
                                                  ),
                                                  child: Center(
                                                    child: Text("${day.day}", style: TextStyle(
                                                      color: Colors.white,
                                                    )),
                                                  ),
                                                ),
                                              );
                                            }
                                            else{
                                              return null;
                                            }
                                          }
                                          else{
                                            return null;
                                          }
                                        }
                                      }
                                    },
                                    weekFormat: false,
                                    height: 340,
                                    daysHaveCircularBorder: false,
                                    selectedDateTime: value.selectedDateTime,
                                    onCalendarChanged: (date){
                                      value.setCurrentYearMonth(date);
                                    },
                                  )
                                ],
                              ),
                            ),

                            Row(
                              children: [
                                SizedBox(width: 12),

                                Expanded(child: Container(
                                  width: double.maxFinite,
                                  height: 37,
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.transparent,
                                      child: CustomButton(37, double.maxFinite, BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius: BorderRadius.all(Radius.circular(8)),
                                          border: Border.all(
                                            width: 1,
                                            color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                          )
                                      ), StringContent.cancel, TextStyle(
                                          color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                          fontFamily: "BarlowRegular",
                                          fontSize: 12,
                                          letterSpacing: 2
                                      )),
                                      onTap: (){
                                        _homeController.isShowDialog(false,0);
                                      },
                                    ),
                                  ),
                                ), flex: 1),

                                SizedBox(width: 16),

                                Expanded(child: Container(
                                  width: double.maxFinite,
                                  height: 37,
                                  child: Stack(
                                    children: [
                                      CustomButton(37, double.maxFinite, BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.topRight,
                                            colors: [
                                              Color.fromRGBO(106, 48, 147, 1.0),
                                              Color.fromRGBO(160, 68, 255, 1.0)
                                            ]
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                      ), StringContent.set_date, TextStyle(
                                          color: Colors.white,
                                          fontFamily: "BarlowRegular",
                                          fontSize: 12,
                                          letterSpacing: 2
                                      )),

                                      Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          splashColor: Colors.transparent,
                                          onTap: (){
                                            _homeController.filterByDate();
                                            _homeController.isShowDialog(false,0);
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ), flex: 1),

                                SizedBox(width: 12),
                              ],
                            ),

                            SizedBox(height: 12)
                          ],
                        ),
                        decoration: BoxDecoration(
                            color: themeValue.themeType == 0 ? ThemeConstant.selectHomeBackgroundDark : ThemeConstant.lightThemeBackground,
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            border: Border.all(
                                color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                                width: 1
                            )
                        ),
                      ), visible: value.isCalendarShowed),

                      Visibility(child: Container(
                        margin: EdgeInsets.only(left: 24, right: 24),
                        width: double.maxFinite,
                        height: 215,
                        decoration: BoxDecoration(
                            color: themeValue.themeType == 0 ? ThemeConstant.selectHomeBackgroundDark : ThemeConstant.lightThemeBackground,
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            border: Border.all(
                                color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                                width: 1
                            )
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    width: 55,
                                    height: 40,
                                    child: Center(
                                      child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                                      "assets/icons/ic_black_back.png", width: 24,
                                          height: 24, fit: BoxFit.contain),
                                    ),
                                  ),
                                  onTap: (){
                                    value.showCalendarFilter(0);
                                  },
                                ),
                                Expanded(child: Text(
                                  StringContent.date.toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      wordSpacing: 1,
                                      fontSize: 14,
                                      fontFamily: "BarlowMedium",
                                      letterSpacing: 2
                                  ),
                                ), flex: 1),
                                Container(
                                  color: Colors.transparent,
                                  width: 55,
                                  height: 40,
                                )
                              ],
                            ),
                            Container(
                              width: double.maxFinite,
                              height: 1,
                              color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                            ),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    padding: EdgeInsets.only(left: 24, right: 16,top: 16,bottom: 16),
                                    child: SvgPicture.asset("assets/icons/ic_left_purple.svg", height: 10, width: 10),
                                  ),
                                  onTap: (){
                                    value.setPrevNextMonth(0);
                                  },
                                ),
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    padding: EdgeInsets.all(16),
                                    child: Text("${value.currentyear}", style: TextStyle(
                                      fontSize: 14,
                                      color: ThemeConstant.darkPurple,
                                      fontFamily: "BarlowMedium",
                                      decoration: TextDecoration.underline,
                                    )),
                                  ),
                                  onTap: (){
                                    value.showCalendarFilter(2);
                                  },
                                ),
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    padding: EdgeInsets.only(left: 16, right: 24,top: 16,bottom: 16),
                                    child: SvgPicture.asset(value.currentyear == DateTime.now().year.toString() ? "assets/icons/ic_right_grey.svg" :
                                    "assets/icons/ic_right_purple.svg", height: 10, width: 10),
                                  ),
                                  onTap: (){
                                    value.setPrevNextMonth(1);
                                  },
                                )
                              ],
                            ),

                            Container(
                              margin: EdgeInsets.only(left: 8, right: 8),
                              width: double.maxFinite,
                              height: 115,
                              child: GridView.count(
                                crossAxisCount: 4,
                                childAspectRatio: 2.5,
                                padding: EdgeInsets.zero,
                                children: List.generate(_homeController.monthList.length, (index) {
                                  Map monthMap = _homeController.monthList[index];
                                  return GestureDetector(
                                    child: Container(
                                        child: Center(
                                          child: Text(
                                            monthMap["name"],
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: ThemeConstant.white,
                                                fontFamily: "BarlowMedium"
                                            ),
                                          ),
                                        ),
                                        decoration: BoxDecoration(
                                            color: value.selectedMonthIndex == index ? ThemeConstant.walletLightPurple  : Colors.transparent,
                                            borderRadius: BorderRadius.all(Radius.circular(25))
                                        )
                                    ),
                                    onTap: (){
                                      value.setSelectedMonth(index);
                                    },
                                  );
                                }),
                              ),
                            ),
                          ],
                        ),
                      ), visible: value.isMonthShowed),

                      Visibility(child: Container(
                        margin: EdgeInsets.only(left: 24, right: 24),
                        width: double.maxFinite,
                        height: 205,
                        decoration: BoxDecoration(
                            color: themeValue.themeType == 0 ? ThemeConstant.selectHomeBackgroundDark : ThemeConstant.lightThemeBackground,
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            border: Border.all(
                                color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                                width: 1
                            )
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    width: 55,
                                    height: 40,
                                    child: Center(
                                      child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                                      "assets/icons/ic_black_back.png", width: 24,
                                          height: 24, fit: BoxFit.contain),
                                    ),
                                  ),
                                  onTap: (){
                                    value.showCalendarFilter(1);
                                  },
                                ),
                                Expanded(child: Text(
                                  StringContent.date.toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      wordSpacing: 1,
                                      fontSize: 14,
                                      fontFamily: "BarlowMedium",
                                      letterSpacing: 2
                                  ),
                                ), flex: 1),
                                Container(
                                  color: Colors.transparent,
                                  width: 55,
                                  height: 40,
                                )
                              ],
                            ),
                            Container(
                              width: double.maxFinite,
                              height: 1,
                              color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                            ),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    padding: EdgeInsets.only(left: 24,top: 16,bottom: 16,right: 16),
                                    child: SvgPicture.asset("assets/icons/ic_left_purple.svg", height: 10, width: 10),
                                  ),
                                  onTap: (){
                                    value.setPrevNextYear(0);
                                  },
                                ),
                                Container(
                                  child: Text("${_homeController.yearList[0]} - ${_homeController.yearList[_homeController.yearList.length - 1]}",
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: ThemeConstant.white,
                                          fontFamily: "BarlowMedium",
                                          letterSpacing: 1.5
                                      )),
                                ),
                                GestureDetector(
                                  child: Container(
                                    color: Colors.transparent,
                                    padding: EdgeInsets.only(left: 16,top: 16,bottom: 16,right: 24),
                                    child: SvgPicture.asset(_homeController.yearList[_homeController.yearList.length - 1] == DateTime.now().year ? "assets/icons/ic_right_grey.svg" :
                                    "assets/icons/ic_right_purple.svg", height: 10, width: 10),
                                  ),
                                  onTap: (){
                                    if(_homeController.yearList[_homeController.yearList.length - 1] != DateTime.now().year){
                                      value.setPrevNextYear(1);
                                    }
                                  },
                                ),
                              ],
                            ),

                            Container(
                              margin: EdgeInsets.only(left: 8, right: 8),
                              width: double.maxFinite,
                              height: 115,
                              child: GridView.count(
                                crossAxisCount: 4,
                                childAspectRatio: 2.5,
                                padding: EdgeInsets.zero,
                                children: List.generate(_homeController.yearList.length, (index) {
                                  return GestureDetector(
                                    child: Container(
                                      child: Center(
                                        child: Text(
                                          "${_homeController.yearList[index]}",
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: ThemeConstant.white,
                                              fontFamily: "BarlowMedium"
                                          ),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                          color: value.selectedYearIndex == index ? ThemeConstant.walletLightPurple  : Colors.transparent,
                                          borderRadius: BorderRadius.all(Radius.circular(25))
                                      ),
                                    ),
                                    onTap: (){
                                      value.setSelectedYear(index);
                                    },
                                  );
                                }),
                              ),
                            ),
                          ],
                        ),
                      ), visible: value.isYearShowed)
                    ],
                  )),
            ],
          ),
        ));
  }
}

