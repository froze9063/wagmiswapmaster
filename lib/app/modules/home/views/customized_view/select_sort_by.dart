import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/home/controllers/home_controller.dart';

class SelectSortBy extends StatelessWidget {

  HomeController _homeController;
  SelectSortBy(this._homeController);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        id: "theme",
        init: HomeController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          color: Color.fromRGBO(20, 21, 40, 0.7),
          child: Column(
            children: [
              SizedBox(height: 100),
              Container(
                margin: EdgeInsets.only(left: 24, right: 24),
                width: double.maxFinite,
                height: 325,
                child: Column(
                  children: [
                    SizedBox(height: 16),
                    Text(
                      StringContent.sort_by.toUpperCase(),
                      style: TextStyle(
                          color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          wordSpacing: 1,
                          fontSize: 14,
                          fontFamily: "BarlowNormal",
                          fontWeight: FontWeight.w600,
                          letterSpacing: 2
                      ),
                    ),
                    SizedBox(height: 16),
                    Container(
                      width: double.maxFinite,
                      height: 1,
                      color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    ),
                    ListView.builder(itemBuilder: (context, index){
                      String name = _homeController.sortByList[index];
                      return GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(left: 12, right: 12, top: 12),
                          width: double.maxFinite,
                          height: 46,
                          decoration: BoxDecoration(
                              color: themeValue.themeType == 0 ? themeValue.walletNetworkLists[themeValue.selectedLedgerPosition][themeValue.selectedUserPosition]["sort_name"] == name ? ThemeConstant.selectedHomeDark : ThemeConstant.selectHomeDark
                                  : themeValue.walletNetworkLists[themeValue.selectedLedgerPosition][themeValue.selectedUserPosition]["sort_name"] == name ? ThemeConstant.white : ThemeConstant.white,
                              borderRadius: BorderRadius.all(Radius.circular(6)),
                              border: Border.all(
                                  color: themeValue.walletNetworkLists[themeValue.selectedLedgerPosition][themeValue.selectedUserPosition]["sort_name"] == name ? themeValue.themeType == 0 ?
                                  ThemeConstant.darkPurple : ThemeConstant.lightPurple : Colors.transparent
                              )
                          ),
                          child: Center(
                            child: Row(
                              children: [
                                SizedBox(width: 12),
                                SizedBox(width: 12),
                                Expanded(child: Text(
                                  name,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 16,
                                    fontFamily: "BarlowMedium",
                                  ),
                                ), flex: 1),
                                SizedBox(width: 12),
                              ],
                            ),
                          ),
                        ),
                        onTap: (){
                          _homeController.setSortByFilter(_homeController.sortByList[index],index);
                          _homeController.isShowDialog(false,0);
                        },
                      );
                    },
                      itemCount: _homeController.sortByList.length,
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                    ),

                    Expanded(child: SizedBox(), flex: 1),

                    Container(
                      width: double.maxFinite,
                      margin: EdgeInsets.only(left: 12, right: 12),
                      height: 37,
                      child: Stack(
                        children: [
                          CustomButton(37, double.maxFinite, BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.topRight,
                                colors: [
                                  Color.fromRGBO(106, 48, 147, 1.0),
                                  Color.fromRGBO(160, 68, 255, 1.0)
                                ]
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                          ), StringContent.close, TextStyle(
                              color: Colors.white,
                              fontFamily: "BarlowRegular",
                              fontSize: 12,
                              letterSpacing: 2
                          )),

                          Material(
                            color: Colors.transparent,
                            child: InkWell(
                              splashColor: Colors.transparent,
                              onTap: (){
                                _homeController.isShowDialog(false,0);
                              },
                            ),
                          )
                        ],
                      ),
                    ),

                    SizedBox(height: 12)
                  ],
                ),
                decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.selectHomeBackgroundDark : ThemeConstant.lightThemeBackground,
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    border: Border.all(
                        color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                        width: 1
                    )
                ),
              ),
            ],
          ),
        ));
  }
}