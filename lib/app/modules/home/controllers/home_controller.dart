import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:dio/dio.dart';
import 'package:fbroadcast/fbroadcast.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:sembast/sembast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';
import 'package:waagmiswap/app/algorand/algorand.dart';
import 'package:waagmiswap/app/callback/dapp_request_callback.dart';
import 'package:waagmiswap/app/connection/my_connection.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/transaction_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/model/wallet_connect_model.dart';
import 'package:waagmiswap/app/model/wallet_transaction_detail.dart';
import 'package:waagmiswap/app/modules/enter_passcode/views/enter_passcode_view.dart';
import 'package:waagmiswap/app/repository/api_repository.dart';
import 'package:waagmiswap/app/util/my_util.dart';
import 'package:wallet_connect/wallet_connect.dart';

class HomeController extends GetxController with WidgetsBindingObserver{

  late MyConnection myConnection;
  late APIRepository apiRepository;

  final count = 0.obs;

  late DatabaseDAO databaseDAO;

  int tabPosition =  1;

  bool isDialogShowed = false;

  // 1 = network, 2 = transaction type, 3 = asset type, 4 = date
  int dialogType = 0;

  // Token info
  int fromToken = 1;
  int balanceType = 1;

  int themeType = 0;

  DateTime today = DateTime.now();
  DateTime selectedDateTime = DateTime.now();
  DateTime targetDateTime = DateTime.now();
  String selectedDate = "";

  DateTime startDate = DateTime.now();
  DateTime? endDate;

  String currentMonth = "";
  String currentyear = "";

  int selectedLedgerPosition = 0;
  int selectedUserPosition = 0;
  int currentUserAlgo = 0;

  String strStartDate = "";
  String strEndDate = "";
  String nextToken = "";
  int? assetId;

  late List<List<Map>> walletNetworkLists;
  late List<UserModel> userModelList;
  late List<LedgerModel> ledgerNetworkList;
  late List<AssetsDetailModel> assetTransactionList;

  late LedgerModel selectedLedger;
  late CarouselController carouselController;

  bool isFromImportAccount = false;
  bool isFromTransaction = false;

  UserModel? selectedUserModel;
  UserModel? selectedWalletUserModel;
  AssetsDetailModel? selectedAssetDetailmodel;

  //TRANSACTION FILTER
  late List<Map> walletTransactionLists;

  String transactionType = "";
  String assetTransactionName = "";
  String assetTransactionDate = "";

  String showUpMessage = "";
  bool isShowUpNotification = false;
  bool isYellowDotShowed = false;

  Map tokenMap = {};
  Map priceMap = {};
  Map algoMap = {};

  List<FlSpot> tokenPriceList = [];

  int filterPosition = 2;

  late List<Map> monthList;
  late List<int> yearList;

  bool isCalendarShowed = true;
  bool isMonthShowed = false;
  bool isYearShowed = false;

  int selectedYearIndex = 0;
  int selectedMonthIndex = 0;

  bool isNotificationOn = true;

  late List<String> sortByList;
  late List<String> transType;

  WCClient? wcClient;
  WCPeerMeta? wcPeerMeta;

  String currentCurrency = "USD";
  String currentCurrencySymbol = "\$";

  int? maxLine = 3;
  String connectionUri = "";

  int requestId = 0;
  bool isFromSetting = false;

  List<String> transactionsBase64 = [];
  List<WalletTransactionDetail> walletTransactionDetailList = [];

  WalletTransactionDetail? selectedWalletDetail;
  bool isWalletLoading = false;

  bool isPaused = false;
  bool gotTransaction = false;
  List<dynamic>? assignTransactions;

  DAppsRequestCallback? dAppsRequestCallback;
  String sndMnemonic = "";

  late BuildContext buildContext;

  String startUri = "algorand-wc:";
  StreamSubscription? _sub;
  Object? _err;
  Uri? _latestUri;
  Uri? _initialUri;
  bool _initialUriIsHandled = false;

  String dapsName = "";
  bool handleLink = false;
  bool gotoPasscode = false;

  int apanNumber = -1;
  int appId = 0;

  late Map userAmountLoading;

  @override
  onInit() {
    if(Get.arguments == null){
      SharedPreferences.getInstance().then((prefs) {
        prefs.setInt("selected_ledger_pos", 0);
        prefs.setInt("user_position",0);
      });
    }

    WidgetsBinding.instance?.addObserver(this);

    if(Platform.isAndroid){
      FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
    }

    priceMap["is_loading"] = false;

    if(Get.arguments != null){
      isFromImportAccount = Get.arguments["import"] ?? false;
      isFromTransaction = Get.arguments["transaction"] ?? false;

      if(isFromTransaction){
        showShowup(Get.arguments["message"] ?? "");
      }
    }

    myConnection = MyConnection();
    apiRepository = APIRepository(this);

    userAmountLoading = {};
    userAmountLoading["loading"] = true;

    ledgerNetworkList = myConnection.getLedgerNetworkList();
    walletNetworkLists = [];
    walletTransactionLists = [];
    transType = [];
    sortByList = [];
    monthList = [];
    yearList = [];
    assetTransactionList = [];
    userModelList = [];
    databaseDAO = DatabaseDAO();
    carouselController = CarouselController();
    selectedLedger = ledgerNetworkList[selectedLedgerPosition];

    transType.add("Send");
    transType.add("Receive");

    setBroadcast();
    setSortBy();
    loadNotification();
    loadUsersAccount(true);
    setToday();
    addMonth();
    addYear();

    selectedYearIndex = yearList.length - 1;
    selectedMonthIndex = DateTime.now().month - 1;

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void increment() => count.value++;

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      SharedPreferences.getInstance().then((prefs){
        bool isPasscodeOn = prefs.getBool("passcode_status") ?? true;
        if(isPasscodeOn){
          if(isPaused){
            if(!gotoPasscode){
              if(Get.currentRoute != "/ScannerView"){
                gotoPasscode = true;
                Get.to(() => EnterPasscodeView(), arguments: {
                  "from" : 3,
                  "from_resume" : true
                })?.then((value) {
                  resetGotoPasscode();
                  if(gotTransaction){
                    gotTransaction = false;
                    isPaused = false;
                    _initialUriIsHandled = false;
                    onTransactions();
                  }
                  else{
                    if(_latestUri != null){
                      isPaused = false;
                      if(_latestUri.toString().startsWith("wc:") || _latestUri.toString().startsWith(startUri)){
                        _initialUriIsHandled = false;
                        if(_latestUri.toString().startsWith("wc:")){
                          requestClient(_latestUri.toString());
                        }
                        else{
                          requestClient(_latestUri!.queryParameters["uri"] ?? "");
                        }
                      }
                    }
                  }
                });
              }
            }
          }
        }
        else{
          if(gotTransaction){
            gotTransaction = false;
            isPaused = false;
            _initialUriIsHandled = false;
            onTransactions();
          }
          else{
            if(_latestUri != null){
              isPaused = false;
              if(_latestUri.toString().startsWith("wc:") || _latestUri.toString().startsWith(startUri)){
                _initialUriIsHandled = false;
                if(_latestUri.toString().startsWith("wc:")){
                  requestClient(_latestUri.toString());
                }
                else{
                  requestClient(_latestUri!.queryParameters["uri"] ?? "");
                }
              }
            }
          }
        }
      });
    }
    else if(state == AppLifecycleState.paused){
      isPaused = true;
    }
  }

  void _handleIncomingLinks() {
    if (!kIsWeb) {
      _sub = uriLinkStream.listen((Uri? uri) {
        _latestUri = uri;
        _err = null;
      }, onError: (Object err) {
        _latestUri = null;
        if (err is FormatException) {
          _err = err;
        } else {
          _err = null;
        }
      });
    }
  }

  Future<void> _handleInitialUri() async {
    if (!_initialUriIsHandled) {
      _initialUriIsHandled = true;
      try {
        final uri = await getInitialUri();
        if (uri == null) {
          print('no initial uri');
        } else {
          _initialUri = uri;
          print('got initial uri: $_latestUri');
          if(!isPaused){
            if(_latestUri != null){
              if(_latestUri.toString().startsWith("wc:") || _latestUri.toString().startsWith(startUri)){
                _initialUriIsHandled = false;
                if(_latestUri.toString().startsWith("wc:")){
                  requestClient(_latestUri.toString());
                }
                else{
                  requestClient(_latestUri!.queryParameters["uri"] ?? "");
                }
              }
            }
            else{
              if(_initialUri != null){
                if(_initialUri.toString().startsWith("wc:") || _initialUri.toString().startsWith(startUri)){
                  _initialUriIsHandled = false;
                  if(_initialUri.toString().startsWith("wc:")){
                    requestClient(_initialUri.toString());
                  }
                  else{
                    requestClient(_initialUri!.queryParameters["uri"] ?? "");
                  }
                }
              }
            }
          }
        }
      } on PlatformException {
        print('falied to get initial uri');
      } on FormatException catch (err) {
        print('malformed initial uri');
        _err = err;
      }
    }
  }

  void resetGotoPasscode(){
    Timer(Duration(seconds: 2), () async {
      gotoPasscode = false;
    });
  }

  void setBroadcast(){
    FBroadcast.instance().unregister(this);
    FBroadcast.instance().register(MyConstant.BROADCAST, (value, callback){
      String key = value["key"] ?? "";
      requestClient(key);
    });
  }

  void showWalletConnectLoading(bool loading){
    isWalletLoading = loading;
    update(["theme"]);
  }

  Future<void> onTransactions() async {
    gotTransaction = false;
    _latestUri = null;
    await onAlgoTransaction(requestId,assignTransactions);
    dAppsRequestCallback?.onDAppsFailed("AlgoSign");
  }

  void checkWalletConnectSession() async {
    var finder = Finder(filter: Filter.and([Filter.equals("is_connected", true)]), limit: 1);
    List<WalletConnectModel> walletConnectModelList = await databaseDAO.getAllWalletConnectFilter(finder);
    if(walletConnectModelList.isNotEmpty){
      dapsName = walletConnectModelList[0].walletName;
      wcClient = WCClient(
        onAlgoSign: (id,transactions) async {
          gotTransaction = true;
          transactionsBase64.clear();
          walletTransactionDetailList.clear();
          requestId = id;
          assignTransactions?.clear();
          assignTransactions = transactions;
          if(!isPaused){
            onTransactions();
          }
        },
        onEthSendTransaction: (index,eth){
          print("onEthSendTransaction");
        },
        onEthSign: (index,eth){
          print("onEthSign");
        },
        onEthSignTransaction: (index,eth){
          print("onEthSignTransaction");
        },
        onCustomRequest: (index,text){
          print("onCustomRequest");
        },
        onConnect: () {

        },
        onDisconnect: (code, reason) {
          print("onDisconnect");
          dAppsRequestCallback?.onDAppsFailed(reason ?? "Disconnected");
        },
        onFailure: (error) {
          print("onFailure");
          dAppsRequestCallback?.onDAppsFailed(error);
        },
        onSessionRequest: (id, peerMeta) {
          print("onSessionRequest");
          wcPeerMeta = peerMeta;
          dAppsRequestCallback?.onDAppsRequested();
          update(["request_connect","connect_button"]);
        },
      );

      sndMnemonic = walletConnectModelList[0].sndMnemonic;
      wcClient?.connectFromSessionStore(WCSessionStore.fromJson(jsonDecode(walletConnectModelList[0].sessionJson)));
    }
  }

  Future<bool> onBackPress() {
    if(tabPosition == 2 && balanceType == 1 || tabPosition == 1 && balanceType == 2){
      tabPosition = 1;
      balanceType = 1;
      update(["theme"]);
    }
    return Future.value(false);
  }

  String getAssetDetail(String unitName){
    if(unitName == "ALGO"){
      return StringContent.about_algo;
    }
    else if(unitName == "ARCC"){
      return StringContent.about_arcc;
    }
    else if(unitName == "USDC"){
      return StringContent.about_usdc;
    }
    else if(unitName == "USDt"){
      return StringContent.about_usdt;
    }
    else if(unitName == "Planets"){
      return StringContent.about_planet;
    }
    else if(unitName == "YLDY"){
      return StringContent.about_yldy;
    }
    else if(unitName == "OPUL"){
      return StringContent.about_opul;
    }
    else{
      return "-";
    }
  }

  void readMore(int? raedMore){
    maxLine = raedMore;
    update(["theme"]);
  }

  void setFilterPosition(int position){
    filterPosition = position;
    update(["token_info"]);
  }

  void setSortBy(){
    sortByList.add("Name");
    sortByList.add("Balance");
    sortByList.add("24H Price (%)");
  }

  void setSortByFilter(String sortName, int index){
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["sort_name"] = sortName;

    if(index == 0){
      filterAssetByName();
    }
    else if (index == 1){
      filterAssetByAmount();
    }
    else{
      filterAssetByPercentage();
    }

    update(["sort_by","theme"]);
  }

  void addMonth(){
    Map janMap = {};
    janMap["name"] = "Jan";
    janMap["index"] = 1;

    Map janMap2 = {};
    janMap2["name"] = "Feb";
    janMap2["index"] = 2;

    Map janMap3 = {};
    janMap3["name"] = "Mar";
    janMap3["index"] = 3;

    Map janMap4 = {};
    janMap4["name"] = "April";
    janMap4["index"] = 4;

    Map janMap5 = {};
    janMap5["name"] = "May";
    janMap5["index"] = 5;

    Map janMap6 = {};
    janMap6["name"] = "Jun";
    janMap6["index"] = 6;

    Map janMap7 = {};
    janMap7["name"] = "Jul";
    janMap7["index"] = 7;

    Map janMap8 = {};
    janMap8["name"] = "Aug";
    janMap8["index"] = 8;

    Map janMap9 = {};
    janMap9["name"] = "Sep";
    janMap9["index"] = 9;

    Map janMap10 = {};
    janMap10["name"] = "Oct";
    janMap10["index"] = 10;

    Map janMap11 = {};
    janMap11["name"] = "Nov";
    janMap11["index"] = 11;

    Map janMap12 = {};
    janMap12["name"] = "Des";
    janMap12["index"] = 12;

    monthList.add(janMap);
    monthList.add(janMap2);
    monthList.add(janMap3);
    monthList.add(janMap4);
    monthList.add(janMap5);
    monthList.add(janMap6);
    monthList.add(janMap7);
    monthList.add(janMap8);
    monthList.add(janMap9);
    monthList.add(janMap10);
    monthList.add(janMap11);
    monthList.add(janMap12);
  }

  void addYear(){
    int currentYear = DateTime.now().year;
    yearList.add(currentYear - 11);
    yearList.add(currentYear - 10);
    yearList.add(currentYear - 9);
    yearList.add(currentYear - 8);
    yearList.add(currentYear - 7);
    yearList.add(currentYear - 6);
    yearList.add(currentYear - 5);
    yearList.add(currentYear - 4);
    yearList.add(currentYear - 3);
    yearList.add(currentYear - 2);
    yearList.add(currentYear - 1);
    yearList.add(currentYear);
  }

  void setPrevNextYear(int from){
    for(int i = 0; i < yearList.length; i++){
      yearList[i] = from == 0 ? yearList[i] - 11 : yearList[i] + 11;
    }
    update(["date"]);
  }

  void setPrevNextMonth(int from){
    if(from == 0){
      currentyear = "${int.parse(currentyear) - 1}";
    }
    else{
      if(int.parse(currentyear) < DateTime.now().year){
        currentyear = "${int.parse(currentyear) + 1}";
      }
    }
    update(["date"]);
  }

  void setSelectedYear(int index){
    selectedYearIndex = index;
    currentyear = "${yearList[index]}";
    update(["date"]);
  }

  void setSelectedMonth(int index){
    selectedMonthIndex = index;
    update(["date"]);
  }

  void showCalendarFilter(int from){
    if(from == 0){
      isCalendarShowed = true;
      isMonthShowed = false;
      isYearShowed = false;
      setSelectedMonthYearFilter();
    }
    else if(from == 1){
      isCalendarShowed = false;
      isMonthShowed = true;
      isYearShowed = false;
    }
    else{
      isCalendarShowed = false;
      isMonthShowed = false;
      isYearShowed = true;
    }
    update(["date"]);
  }

  void setSelectedMonthYearFilter(){
    String strSelectedDate = "$currentyear ${selectedMonthIndex + 1} ${MyUtils.parseSingleDate(selectedDateTime, "dd")}";
    selectedDateTime = MyUtils.parseDateTime(strSelectedDate,"yyyy MM dd");
    targetDateTime = MyUtils.parseDateTime(strSelectedDate,"yyyy MM dd");
    currentMonth = MyUtils.parseSingleDate(selectedDateTime, "MMMM");
    currentyear = MyUtils.parseSingleDate(selectedDateTime, "yyyy");
    update(["date"]);
  }

  void loadUser(bool isLoadTransaction){
    SharedPreferences.getInstance().then((prefs) async {
      themeType = prefs.getInt("theme") ?? 0;
      isNotificationOn = prefs.getBool("notification") ?? true;
      currentCurrency = prefs.getString("currency_name") ?? "USD";
      currentCurrencySymbol = prefs.getString("symbol") ?? "\$";

      if(Get.arguments != null){
        selectedLedgerPosition = prefs.getInt("selected_ledger_pos") ?? 0;
        selectedLedger = ledgerNetworkList[selectedLedgerPosition];
        selectedUserPosition = prefs.getInt("user_position") ?? 0;
      }

      walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["currency"] = currentCurrency;
      walletTransactionLists[selectedUserPosition]["currency"] = currentCurrency;

      if(isLoadTransaction){
        if(isFromSetting){
          isFromSetting = false;
          loadLocalAsset(userModelList[selectedUserPosition].address);
        }
        else{
          getAssets();
        }
        getTransactionAsset();
        loadTransaction(true);
      }

      if(Get.arguments != null){
        tabPosition = Get.arguments["tab_position"] ?? 1;
      }

      if(!handleLink){
        handleLink = true;
        _handleIncomingLinks();
        _handleInitialUri();
      }

      update(["theme","network"]);
    });
  }

  Future<void> loadNotification() async {
    await databaseDAO.getAllTransactionSorted("read", false).then((value) {
      isYellowDotShowed = value.isNotEmpty;
      update(["notification"]);
    });
  }

  void setTransactionFilter(String name){
    transactionType = name;
    filterTransaction();
    update(["transaction_type_filter"]);
  }

  void setAssetFilter(String name, AssetsDetailModel selectedAssetDetailmodel){
    for(int i = 0 ; i < walletTransactionLists.length; i++){
      if(i != selectedUserPosition){
        walletTransactionLists[i]["next_token"] = "";
        walletTransactionLists[i]["is_first"] = false;
      }
    }

    assetTransactionName = name;
    this.selectedAssetDetailmodel = selectedAssetDetailmodel;
    assetId = selectedAssetDetailmodel.index;
    loadTransaction(true);
    update(["transaction_asset_filter"]);
  }

  void setSelectedDate(DateTime selectedDate){
    selectedDateTime = selectedDate;
    this.selectedDate = MyUtils.parseSingleDate(selectedDateTime, "yyyy-MM-dd");

    if(endDate != null){
      startDate = selectedDate;
      endDate = null;
    }
    else{
      if(selectedDate.millisecondsSinceEpoch < startDate.millisecondsSinceEpoch){
        startDate = selectedDate;
      }
      else{
        endDate = selectedDate;
      }
    }

    update(["date","transaction_date_filter"]);
  }

  void changeTab(int position){
    tabPosition = position;
    update(["tabs"]);
  }

  void isShowDialog(bool isShowed, int dialogType){
    isDialogShowed = isShowed;
    this.dialogType = dialogType;
    update(["dialog_showed"]);
  }

  void setBalanceTye(int balanceTabType, int fromToken){
    balanceType = balanceTabType;
    this.fromToken = fromToken;
    update(["tabs","token_info"]);
  }

  void setToday(){
    DateFormat monthDateFormat = DateFormat("MMMM");
    DateFormat yearDateFormat = DateFormat("yyyy");
    currentMonth = monthDateFormat.format(DateTime.now());
    currentyear = yearDateFormat.format(DateTime.now());
  }

  void setCurrentYearMonth(DateTime dateTime){
    DateFormat monthDateFormat = DateFormat("MMMM");
    DateFormat yearDateFormat =  DateFormat("yyyy");
    currentMonth = monthDateFormat.format(dateTime);
    currentyear = yearDateFormat.format(dateTime);
    update(["month_year"]);
  }

  void nextDate(){
    targetDateTime = DateTime(targetDateTime.year, targetDateTime.month + 1);
    update(["date"]);
  }

  void prevDate(){
    targetDateTime = DateTime(targetDateTime.year, targetDateTime.month - 1);
    update(["date"]);
  }

  void initiateCurrentDate(){
    DateTime nowDate = DateTime.now();
    DateFormat parseDate = DateFormat("yyyy-MM-dd");
    selectedDate = parseDate.format(nowDate);
  }

  Future<void> changePage(int index) async {
    var sharedPrefs = await SharedPreferences.getInstance();
    sharedPrefs.setInt("user_position", index);

    selectedUserPosition = index;

    selectedUserModel = userModelList[selectedUserPosition];
    selectedWalletUserModel = userModelList[selectedUserPosition];

    if(!isFromImportAccount){
      if(walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].isEmpty || walletNetworkLists[selectedLedgerPosition][
      selectedUserPosition]["currency"] != currentCurrency){
        walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["currency"] = currentCurrency;
        getAssets();
      }
      if(!walletTransactionLists[selectedUserPosition]["is_first"] || walletTransactionLists[
      selectedUserPosition]["currency"] != currentCurrency){
        walletTransactionLists[selectedUserPosition]["is_first"] = true;
        walletTransactionLists[selectedUserPosition]["currency"] = currentCurrency;
        loadTransaction(true);
      }
      update(["theme","assets","slider"]);
    }
    else{
      isFromImportAccount = false;
      if(walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].isEmpty){
        walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["currency"] = currentCurrency;
        getAssets();
      }
      if(!walletTransactionLists[selectedUserPosition]["is_first"]){
        walletTransactionLists[selectedUserPosition]["is_first"] = true;
        walletTransactionLists[selectedUserPosition]["currency"] = currentCurrency;
        loadTransaction(true);
      }
      update(["theme","assets","slider"]);
    }
  }

  Future<void> filterTransaction() async {
    walletTransactionLists[selectedUserPosition]["trans_searched_list"] = walletTransactionLists[selectedUserPosition]["trans_original_list"];
    List<Map> filteredSearched = [];
    for(int i = 0 ; i < walletTransactionLists[selectedUserPosition]["trans_searched_list"].length; i++){
      List<TransactionModel> transactionResultList = walletTransactionLists[selectedUserPosition]["trans_searched_list"][i]["transaction_list"];
      List<TransactionModel> searchedResultList = [];

      for(int j = 0; j < transactionResultList.length; j++){
        TransactionModel transactionModel = transactionResultList[j];
        if(transactionType.isNotEmpty && selectedAssetDetailmodel != null){
          if(transactionModel.type.toLowerCase() == transactionType.toLowerCase() &&
              transactionModel.assetId == selectedAssetDetailmodel?.index){
            searchedResultList.add(transactionModel);
          }
        }
        else if(transactionType.isNotEmpty){
          if(transactionModel.type.toLowerCase() == transactionType.toLowerCase()){
            searchedResultList.add(transactionModel);
          }
        }
        else{
          if(selectedAssetDetailmodel != null){
            if(transactionModel.assetId == selectedAssetDetailmodel?.index){
              searchedResultList.add(transactionModel);
            }
          }
        }
      }

      if(searchedResultList.isNotEmpty){
        Map filteredSearchedMap = {};
        filteredSearchedMap["transaction_list"] = walletTransactionLists[selectedUserPosition]["trans_searched_list"][i]["transaction_list"];
        filteredSearchedMap["searched_transaction_list"] = searchedResultList;
        filteredSearchedMap["date"] = walletTransactionLists[selectedUserPosition]["trans_searched_list"][i]["date"];
        filteredSearched.add(filteredSearchedMap);
      }
    }

    walletTransactionLists[selectedUserPosition]["trans_searched_list"] = filteredSearched;
    update(["transactions"]);
  }

  void changeLedgerNetwork(LedgerModel ledgerModel, int position){
    SharedPreferences.getInstance().then((prefs){
      prefs.setInt("selected_ledger_pos", position);
      selectedLedgerPosition = position;
      selectedLedger = ledgerModel;

      for(int i = 0 ; i < walletNetworkLists[selectedLedgerPosition].length ; i++){
        walletNetworkLists[selectedLedgerPosition][i]["loading"] = false;
        walletNetworkLists[selectedLedgerPosition][i]["loading_count"] = 0;
        walletNetworkLists[selectedLedgerPosition][i]["percentage_count"] = 0;
        walletNetworkLists[selectedLedgerPosition][i]["currency"] = currentCurrency;
        walletNetworkLists[selectedLedgerPosition][i]["original_list"].clear();
        walletNetworkLists[selectedLedgerPosition][i]["searched_list"].clear();
      }

      for(int i = 0 ; i < walletTransactionLists.length ; i++){
        walletTransactionLists[i]["loading"] = false;
        walletTransactionLists[i]["loading_more"] = false;
        walletTransactionLists[i]["is_first"] = false;
        walletTransactionLists[i]["next_token"] = "";
        walletTransactionLists[i]["trans_original_list"].clear();
        walletTransactionLists[i]["trans_searched_list"].clear();
      }

      if(walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"].isEmpty){
        getAssets();
      }

      loadTransaction(true);
      update(["network","slider"]);
    });
  }

  void loadUsersAccount(bool isLoadTransaction){
    SharedPreferences.getInstance().then((prefs) async {
      userModelList.clear();
      walletNetworkLists.clear();
      walletTransactionLists.clear();

      List<Map> mainNetworkList = [];
      List<Map> testNetworkList = [];

      databaseDAO.getAll().then((value) {
        for (int i = 0; i < value.length; i++) {
          userModelList.add(value[i]);

          List<Map> originalList = [];
          List<Map> searchedList = [];
          List<AssetsModel> disableList = [];

          Map walletMap = {};
          walletMap["loading"] = false;
          walletMap["loading_count"] = 0;
          walletMap["percentage_count"] = 0;
          walletMap["disable_list"] = disableList;
          walletMap["original_list"] = originalList;
          walletMap["searched_list"] = searchedList;
          walletMap["sort_name"] = "";

          List<Map> transOriginalList = [];
          List<Map> transSearchedList = [];

          Map transactionMap = {};
          transactionMap["loading"] = false;
          transactionMap["loading_more"] = false;
          transactionMap["is_first"] = false;
          transactionMap["next_token"] = "";
          transactionMap["trans_original_list"] = transOriginalList;
          transactionMap["trans_searched_list"] = transSearchedList;

          mainNetworkList.add(walletMap);

          walletTransactionLists.add(transactionMap);
        }

        for (int i = 0; i < value.length; i++) {
          List<Map> originalList = [];
          List<Map> searchedList = [];
          List<AssetsModel> disableList = [];

          Map walletMap = {};
          walletMap["loading"] = false;
          walletMap["loading_count"] = 0;
          walletMap["percentage_count"] = 0;
          walletMap["disable_list"] = disableList;
          walletMap["original_list"] = originalList;
          walletMap["searched_list"] = searchedList;
          walletMap["sort_name"] = "";

          List<Map> transOriginalList = [];
          List<Map> transSearchedList = [];

          Map transactionMap = {};
          transactionMap["loading"] = false;
          transactionMap["loading_more"] = false;
          transactionMap["is_first"] = false;
          transactionMap["next_token"] = "";
          transactionMap["trans_original_list"] = transOriginalList;
          transactionMap["trans_searched_list"] = transSearchedList;

          testNetworkList.add(walletMap);
        }

        walletNetworkLists.add(mainNetworkList);
        walletNetworkLists.add(testNetworkList);

        if (isFromImportAccount) {
          selectedUserPosition = userModelList.length - 1;
          carouselController.animateToPage(selectedUserPosition);
        }
        else {
          selectedUserPosition = prefs.getInt("user_position") ?? 0;
          carouselController.animateToPage(selectedUserPosition);
        }

        selectedUserModel = userModelList[selectedUserPosition];
        selectedWalletUserModel = userModelList[selectedUserPosition];

        loadUser(isLoadTransaction);
        update(["slider", "assets"]);
      });
    });
  }

  Future<void> getAssets() async {
    if(!walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["loading"]){
      walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"].clear();
      walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].clear();

      walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["loading_count"] = 0;
      walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["percentage_count"] = 0;
      walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["loading"] = true;
      walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["sort_name"] = "";

      update(["assets","slider","theme","sort_by"]);

      apiRepository.getAssets(userModelList[selectedUserPosition], selectedLedgerPosition,
          selectedLedger.algodUrl, selectedLedger.indexerUrl, [], [], currentUserAlgo,
          {}, currentCurrency, walletNetworkLists, selectedUserPosition);
    }
  }

  Future<void> getTransactionAsset() async {
    apiRepository.getAllAsset(selectedLedger.baseUrl, assetTransactionList);
  }

  Future<void> onSettingResume() async {
    var prefs = await SharedPreferences.getInstance();
    var value = await databaseDAO.getAll();

    for (int i = 0; i < value.length; i++) {
      userModelList[i].userName = value[i].userName;
    }

    themeType = prefs.getInt("theme") ?? 0;
    isNotificationOn = prefs.getBool("notification") ?? true;
    selectedLedgerPosition = prefs.getInt("selected_ledger_pos") ?? 0;
    selectedLedger = ledgerNetworkList[selectedLedgerPosition];
    currentCurrency = prefs.getString("currency_name") ?? "USD";
    currentCurrencySymbol = prefs.getString("symbol") ?? "\$";
    selectedUserPosition = prefs.getInt("user_position") ?? 0;

    update(["theme","network"]);

    if(currentCurrency != walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["currency"]){
      walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["currency"] = currentCurrency;
      getAssets();
      loadTransaction(true);
    }
  }

  void loadLocalAsset(String address) async {
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"].clear();
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].clear();

    var finder = Finder(filter: Filter.and([Filter.equals("ledger-position",
        selectedLedgerPosition), Filter.equals("user-address", address)]));

    databaseDAO.getAllAssetBySorted(finder).then((assetValue) async {
      if(assetValue.isNotEmpty){
        for(int i = 0; i < assetValue.length; i++){
          Map assetMap = {};
          assetMap["asset_model"] = assetValue[i];

          var finder2 = Finder(filter: Filter.and([Filter.equals("ledger-position", selectedLedgerPosition),
            Filter.equals("index", assetValue[i].assetId)]),limit: 1);

          var pricingFinder = Finder(filter: Filter.and([Filter.equals("ledger-position", selectedLedgerPosition),
            Filter.equals("asset-id", assetValue[i].assetId), Filter.equals("currency", currentCurrency)]), limit: 1);

          var pricingList = await databaseDAO.getAssetPricingWithFinder(pricingFinder);

          if(pricingList.isEmpty){
            getAssets();
            break;
          }
          else{
            databaseDAO.getAllAssetDetailSorted(finder2).then((assetDetailValue) {
              assetMap["asset_detail_model"] = assetDetailValue[0];

              databaseDAO.getAssetPricingWithFinder(pricingFinder).then((assetPricing) {
                String assetPrice = assetPricing[0].price;
                double latestPrice = double.parse(assetPricing[0].price.isEmpty ? "0.0" : assetPricing[0].price);
                double percentagePrice = double.parse(assetPricing[0].percentagePrice.isEmpty ? "0.0" : assetPricing[0].percentagePrice);
                String percentage = assetPricing[0].percentage;

                if((assetMap["asset_model"] as AssetsModel).assetId == 0){
                  selectedUserModel?.amount = (assetMap["asset_model"] as AssetsModel).amount;
                  selectedUserModel?.price = (assetMap["asset_model"] as AssetsModel).price;
                }

                (assetMap["asset_model"] as AssetsModel).price = assetPrice;
                assetMap["latest_price"] = latestPrice;
                assetMap["percentage_price"] = percentagePrice;
                assetMap["percentage"] = percentage;

                walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"].add(assetMap);
                walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].add(assetMap);

                update(["assets","slider"]);
              }
              );
            });
          }
        }
      }
      else{
        getAssets();
      }
    });
  }

  void addMyAsset(String txId, Map assetMap){
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].add(assetMap);
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"].add(assetMap);

    apiRepository.getAssetPending(txId, userModelList[selectedUserPosition], selectedLedgerPosition,
        selectedLedger.algodUrl, selectedLedger.indexerUrl, null, null, currentUserAlgo,
        "my_asset", "", currentCurrency, {},walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].length - 1, 1, (
            assetMap["asset_model"] as AssetsModel).assetId, selectedLedger.baseUrl,walletNetworkLists,selectedUserPosition);

    update(["assets","slider"]);
  }

  void removeMyAsset(String txId, int assetIndex){
    int selectedAssetPosition = 0;
    int selectedPosition = 0;

    for(int i = 0; i < walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"].length; i++){
      AssetsModel assetModel = walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"][i]["asset_model"];
      if(assetModel.assetId == assetIndex){
        selectedAssetPosition = i;
        break;
      }
    }

    for(int i = 0; i < walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].length; i++){
      AssetsModel assetModel = walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"][i]["asset_model"];
      if(assetModel.assetId == assetIndex){
        selectedPosition = i;
        break;
      }
    }

    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"][selectedAssetPosition]["is_pending"] = true;
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"][selectedAssetPosition]["status_name"] = "Removing Asset ...";

    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"][selectedPosition]["is_pending"] = true;
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"][selectedPosition]["status_name"] = "Removing Asset ...";

    apiRepository.getAssetPending(txId, userModelList[selectedUserPosition], selectedLedgerPosition,
        selectedLedger.algodUrl, selectedLedger.indexerUrl, null, null, currentUserAlgo,"my_asset",
        "", currentCurrency, {}, selectedPosition, 2, assetIndex, selectedLedger.baseUrl, walletNetworkLists,selectedUserPosition);

    update(["assets","slider"]);
  }

  Future<void> loadTransaction(bool fromRefresh) async {
    if(fromRefresh){
      walletTransactionLists[selectedUserPosition]["next_token"] = "";
    }
    apiRepository.getAllTransaction(selectedLedger.algodUrl,selectedLedger.indexerUrl,
        userModelList[selectedUserPosition].address,currentCurrency,strStartDate,strEndDate,
        assetId,walletTransactionLists,selectedUserPosition);
  }

  void filterByDate(){
    String strSelectedDate = "${MyUtils.parseSingleDate(startDate, "dd")} ${monthList[selectedMonthIndex]
    ["index"]} ${yearList[selectedYearIndex]}";
    startDate = MyUtils.parseDateTime(strSelectedDate, "dd MM yyyy");

    for(int i = 0 ; i < walletTransactionLists.length; i++){
      if(i != selectedUserPosition){
        walletTransactionLists[i]["next_token"] = "";
        walletTransactionLists[i]["is_first"] = false;
      }
    }

    if(endDate != null){
      assetTransactionDate = "${MyUtils.parseSingleDate(startDate, "dd")} - "
          "${MyUtils.parseSingleDate(endDate!, "dd")}";
    }
    else{
      assetTransactionDate = MyUtils.parseSingleDate(startDate, "dd-MM-yyyy");
    }

    update(["date","transaction_date_filter","transactions"]);

    strStartDate = MyUtils.parseSingleDate(startDate, "yyyy-MM-dd");

    if(endDate != null){
      strEndDate = MyUtils.parseSingleDate(endDate!, "yyyy-MM-dd");
    }

    loadTransaction(true);
  }

  void clearFilters(){
    for(int i = 0 ; i < walletTransactionLists.length; i++){
      if(i != selectedUserPosition){
        walletTransactionLists[i]["next_token"] = "";
        walletTransactionLists[i]["is_first"] = false;
      }
    }

    transactionType = "";
    assetTransactionName = "";
    assetTransactionDate = "";
    targetDateTime = DateTime.now();
    startDate = DateTime.now();
    endDate = null;
    selectedAssetDetailmodel = null;
    assetId = null;
    strStartDate = "";
    strEndDate = "";
    loadTransaction(true);
    update(["transactions","date","transaction_type_filter","transaction_asset_filter",
      "transaction_date_filter"]);
  }

  void showShowup(String message){
    showUpMessage = message;
    isShowUpNotification = true;
    update(["show_up"]);

    Timer(Duration(seconds: 4), () async {
      databaseDAO.getAll().then((value) {
        isShowUpNotification = false;
        isFromTransaction = false;
        update(["show_up"]);
      });
    });
  }

  Future<void> loadAssetPrice(String assetUnitName, String priceType) async {
    if(!priceMap["is_loading"]){
      apiRepository.getTokenPrice(tokenPriceList, assetUnitName, priceType,
          priceMap, currentCurrency);
      apiRepository.getLatestPrice(assetUnitName, "latest", priceMap, currentCurrency);
    }
  }

  Future<void> filterAssetByName() async {
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].sort((a, b) => (a["asset_detail_model"] as AssetsDetailModel
    ).name.compareTo((b["asset_detail_model"] as AssetsDetailModel).name));
    update(["assets","slider"]);
  }

  Future<void> filterAssetByAmount() async {
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].clear();

    List<Map> filteredAssetList = [];

    for(int i = 0; i < walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"].length ; i++){
      Map assetMap = walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"][i];
      filteredAssetList.add(assetMap);
    }

    for(int i = 0 ; i < filteredAssetList.length ; i++){
      for(int j = 0; j < filteredAssetList.length; j++){
        if(j < filteredAssetList.length - 1){
          Map map1 = filteredAssetList[j];
          Map map2 = filteredAssetList[j + 1];

          int position1 = (filteredAssetList[j]["asset_model"] as AssetsModel).amount;
          int position2 = (filteredAssetList[j + 1]["asset_model"] as AssetsModel).amount;

          if(position1 < position2){
            filteredAssetList[j] = map2;
            filteredAssetList[j + 1] =  map1;
          }
        }
      }
    }

    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"] = filteredAssetList;
    update(["assets","slider"]);
  }

  Future<void> filterAssetByPercentage() async {
    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"].clear();
    List<Map> filteredAssetList = [];

    for(int i = 0; i < walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"].length ; i++){
      Map assetMap = walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["original_list"][i];
      filteredAssetList.add(assetMap);
    }

    for(int i = 0 ; i < filteredAssetList.length ; i++){
      for(int j = 0; j < filteredAssetList.length; j++){
        if(j < filteredAssetList.length - 1){
          Map map1 = filteredAssetList[j];
          Map map2 = filteredAssetList[j + 1];

          double position1 = filteredAssetList[j]["percentage_price"] ?? 0;
          double position2 = filteredAssetList[j + 1]["percentage_price"] ?? 0;

          if(position1 < position2){
            filteredAssetList[j] = map2;
            filteredAssetList[j + 1] =  map1;
          }
        }
      }
    }

    walletNetworkLists[selectedLedgerPosition][selectedUserPosition]["searched_list"] = filteredAssetList;
    update(["assets","slider"]);
  }

  Future<void> requestClient(String uri) async {
    if(uri.startsWith("wc:") || uri.startsWith(startUri)){
      dAppsRequestCallback?.onDAppsLoading();
      connectionUri = uri;
      _latestUri = null;
      wcClient = WCClient(
        onAlgoSign: (id,transactions) async {
          gotTransaction = true;
          transactionsBase64.clear();
          walletTransactionDetailList.clear();
          requestId = id;
          assignTransactions?.clear();
          assignTransactions = transactions;
          if(!isPaused){
            onTransactions();
          }
        },
        onEthSendTransaction: (index,eth){
          print("onEthSendTransaction");
        },
        onEthSign: (index,eth){
          print("onEthSign");
        },
        onEthSignTransaction: (index,eth){
          print("onEthSignTransaction");
        },
        onCustomRequest: (index,text){
          print("onCustomRequest");
        },
        onConnect: () {
          print("onConnect");
          dAppsRequestCallback?.onDAppsFailed("Connected");
        },
        onDisconnect: (code, reason) {
          print("onDisconnect");
          dAppsRequestCallback?.onDAppsFailed(reason ?? "Disconnected");
        },
        onFailure: (error) {
          print("onFailure");
          dAppsRequestCallback?.onDAppsFailed(error);
        },
        onSessionRequest: (id, peerMeta) {
          print("onSessionRequest");
          wcPeerMeta = peerMeta;
          dapsName = peerMeta.name;
          dAppsRequestCallback?.onDAppsRequested();
          update(["request_connect","connect_button"]);
        },
      );

      final session = WCSession.from(uri);

      final peerMeta = WCPeerMeta(
          name: '',
          url: '',
          description: ''
      );

      await wcClient?.connectNewSession(session: session, peerMeta: peerMeta);
    }
    else{
      showQRError(this);
    }
  }

  void approve(){
    Get.back();

    transactionsBase64.clear();
    walletTransactionDetailList.clear();

    showWalletConnectLoading(true);

    List<String> addressList = [];

    sndMnemonic = selectedWalletUserModel?.mnemonic ?? "";

    addressList.add(selectedWalletUserModel?.address ?? "");
    wcClient?.approveSession(accounts: addressList, chainId: 1);
    saveNetworkHistory(addressList[0]);

    showWalletConnectLoading(false);
  }

  Future<void> saveNetworkHistory(String address) async {
    var finder = Finder(filter: Filter.and([Filter.equals("wallet_name", wcPeerMeta?.name ?? ""),
      Filter.equals("wallet_url", wcPeerMeta?.url), Filter.equals("wallet_connect_address", address)]),
        limit: 1);

    String now = MyUtils.parseSingleDate(DateTime.now(), "MMMM dd, yyyy - HH:mm");

    List<WalletConnectModel> walletConnectModelList = await databaseDAO.getAllWalletConnects();

    for(int i = 0; i < walletConnectModelList.length; i++){
      walletConnectModelList[i].isConnected = false;
      await databaseDAO.updateWalletConnects(walletConnectModelList[i]);
    }

    databaseDAO.getAllWalletConnectFilter(finder).then((value) {
      if(value.isEmpty){
        databaseDAO.insertWalletConnect(WalletConnectModel(walletName: wcPeerMeta?.name ?? "",
            walletUrl: wcPeerMeta?.url ?? "",walletDesc: wcPeerMeta?.description ?? "",
            walletImage: wcPeerMeta?.icons[0] ?? "" , walletDate: now,walletConnectAddress:
            address, uri: connectionUri,chainId: wcClient?.sessionStore.chainId ?? 0,
            peerId: wcClient?.sessionStore.peerId ?? "", remotePeerId: wcClient?.sessionStore.remotePeerId ?? ""
            ,isConnected: true,sessionJson: jsonEncode(wcClient?.sessionStore.toJson()),
            sndMnemonic: sndMnemonic));

        Timer(Duration(seconds: 1), () async {
          FBroadcast.instance().broadcast(MyConstant.BROADCAST_SETTING, value: {});
        });
      }
      else{
        databaseDAO.updateWalletConnect(WalletConnectModel(walletName: wcPeerMeta?.name ?? "",
            walletUrl: wcPeerMeta?.url ?? "",walletDesc: wcPeerMeta?.description ?? "",
            walletImage: wcPeerMeta?.icons[0] ?? "" , walletDate: now,walletConnectAddress:
            address, uri: connectionUri,chainId: wcClient?.sessionStore.chainId ?? 0,
            peerId: wcClient?.sessionStore.peerId ?? "", remotePeerId: wcClient?.sessionStore.remotePeerId ?? ""
            ,isConnected: true,sessionJson: jsonEncode(wcClient?.sessionStore.toJson()),
            sndMnemonic: sndMnemonic), finder);

        Timer(Duration(seconds: 1), () async {
          FBroadcast.instance().broadcast(MyConstant.BROADCAST_SETTING, value: {});
        });
      }
    });
  }

  Future<void> onAlgoTransaction(int id, List<dynamic>? transactions) async {
    AlgorandRepository algorandRepository = AlgorandRepository();
    Algorand algorand = algorandRepository.getAlgorandConfig(selectedLedger.algodUrl,
        selectedLedger.indexerUrl);
    Account masterAccount = await algorand.restoreAccount(sndMnemonic.split(" "));

    transactionsBase64.clear();
    walletTransactionDetailList.clear();

    if(transactions != null){
      if(transactions.isNotEmpty){
        apanNumber = -1;
        for(int j = 0 ; j < transactions.length; j++){
          List<dynamic> transList = transactions[j];
          if(transList.isNotEmpty){
            for(int i = 0; i < transList.length ; i++){
              Map transactionMap = transList[i];
              String txn = transactionMap["txn"];
              var decodedBase64 = base64Decode(txn);
              Map decodedTxn = Encoder.decodeMessagePack(decodedBase64);
              String type = decodedTxn["type"];

              if(type == "pay"){
                PaymentTransaction paymentTransaction = decodeToPaymentTransaction(
                    txn);

                var signedTx = await paymentTransaction.sign(masterAccount);

                var pricingFinder = Finder(filter: Filter.and([Filter.equals("ledger-position", selectedLedgerPosition),
                  Filter.equals("asset-id", 0), Filter.equals("currency", currentCurrency)]), limit: 1);

                var pricingList = await databaseDAO.getAssetPricingWithFinder(pricingFinder);

                String price = "";

                if(pricingList.isNotEmpty){
                  price = pricingList[0].price;
                }
                else{
                  var response = await apiRepository.getAssetPricingResponse("ALGO",currentCurrency);

                  if(response.data is Map){
                    Map responseMap = response.data;
                    price = responseMap["ALGO/$currentCurrency"] ?? "";
                  }
                }

                String from = MyUtils.parseAddress(paymentTransaction.sender?.encodedAddress ?? "");
                String to = paymentTransaction.receiver?.encodedAddress ?? "";
                String balance = "${MyUtils.parseAlgoToMicro(selectedUserModel?.amount ?? 0)} ALGO";
                String amount = "${MyUtils.doubleWithoutDecimalToInt(Algo.fromMicroAlgos(
                    paymentTransaction.amount ?? 0))} ALGO";
                String asset = "ALGO";
                String fee = "${NumberFormat('#,##0.000', 'en_US').format(Algo.fromMicroAlgos(
                    paymentTransaction.fee ?? 0))} ALGO";
                String type = "pay";
                String applicationId = "";

                WalletTransactionDetail walletTransactionDetail = WalletTransactionDetail(0, from, to,
                    balance, asset, amount, fee, type, applicationId, price,0,paymentTransaction.amount ?? 0);

                walletTransactionDetailList.add(walletTransactionDetail);
                transactionsBase64.add(signedTx.toBase64());
              }
              else if(type == "axfer"){
                AssetTransferTransaction assetTransaction = assetTransferTransaction(
                    txn);

                var signedTx = await assetTransaction.sign(masterAccount);

                var response = await apiRepository.getAssetDetailResponse(selectedLedger.baseUrl, currentCurrency,
                    assetTransaction.assetId ?? 0);

                Map dataResponse = response.data;
                String unitName = dataResponse["asset"]["params"]["unit-name"] ?? "";
                int decimals = dataResponse["asset"]["params"]["decimals"] ?? 0;

                var pricingFinder = Finder(filter: Filter.and([Filter.equals("ledger-position", selectedLedgerPosition),
                  Filter.equals("asset-id", assetTransaction.assetId ?? 0), Filter.equals("currency", currentCurrency)]),
                    limit: 1);

                var pricingList = await databaseDAO.getAssetPricingWithFinder(pricingFinder);

                String price = "";

                if(pricingList.isNotEmpty){
                  price = pricingList[0].price;
                }
                else{
                  var response = await apiRepository.getAssetPricingResponse(unitName,currentCurrency);

                  if(response.data is Map){
                    Map responseMap = response.data;
                    price = responseMap["$unitName/$currentCurrency"] ?? "";
                  }
                }

                String from = MyUtils.parseAddress(assetTransaction.sender?.encodedAddress ?? "");
                String to = assetTransaction.receiver?.encodedAddress ?? "";
                String balance = "${MyUtils.countAssetAmount(assetTransaction.amount ?? 0,
                    decimals)} $unitName";
                String amount = "${MyUtils.doubleWithoutDecimalToInt((assetTransaction.amount ?? 0) / pow(10, decimals))} $unitName";
                String asset = unitName;
                String fee = "${NumberFormat('#,##0.000', 'en_US').format(Algo.fromMicroAlgos(
                    assetTransaction.fee ?? 0))} ALGO";
                String type = "axfer";
                String applicationId = "";

                WalletTransactionDetail walletTransactionDetail = WalletTransactionDetail(assetTransaction.assetId ?? 0,
                    from, to, balance, asset, amount, fee, type, applicationId,price,decimals,assetTransaction.amount ?? 0);

                walletTransactionDetailList.add(walletTransactionDetail);
                transactionsBase64.add(signedTx.toBase64());
              }
              else if(type == "appl"){
                Map decodedApplicationCall = Encoder.decodeMessagePack(decodedBase64);
                int applicationId = decodedApplicationCall["apid"] ?? 0;
                appId = applicationId;

                int fee = decodedApplicationCall["fee"] ?? 0;
                int fv = decodedApplicationCall["fv"] ?? 0;
                int lv = decodedApplicationCall["lv"] ?? 0;
                String gen = decodedApplicationCall["gen"] ?? "";

                Address? snd;
                if(decodedApplicationCall["snd"] != null){
                  snd = Address.fromAlgorandAddress(address: Address.encodeAddress(decodedApplicationCall["snd"]));
                }

                Uint8List? grpList;
                if(decodedApplicationCall["grp"] != null){
                  grpList = decodedApplicationCall["grp"];
                }

                Uint8List? ghList;
                if(decodedApplicationCall["gh"] != null){
                  ghList = decodedApplicationCall["gh"];
                }

                //apaa
                List<Uint8List> argumentList = [];
                if(decodedApplicationCall["apaa"] != null){
                  List<dynamic> apaaList =  decodedApplicationCall["apaa"];
                  for(int i = 0 ; i < apaaList.length; i++){
                    var apaa = apaaList[i] as Uint8List;
                    argumentList.add(apaa);
                  }
                }

                //apat
                List<Address> apatAccounts = [];
                if(decodedApplicationCall["apat"] != null){
                  List<dynamic> apatList = decodedApplicationCall["apat"] ?? [];
                  for(int i = 0 ; i < apatList.length; i++){
                    var apat = apatList[i] as Uint8List;
                    apatAccounts.add(Address.fromAlgorandAddress(address: Address.encodeAddress(apat)));
                  }
                }

                //apas
                List<int> foreignAssets = [];
                if(decodedApplicationCall["apas"] != null){
                  List<dynamic> apasList = decodedApplicationCall["apas"];
                  for(int i = 0 ; i < apasList.length; i++){
                    var apas = apasList[i] as int;
                    foreignAssets.add(apas);
                  }
                }

                //apfa
                List<int> foreignApps = [];
                if(decodedApplicationCall["apfa"] != null){
                  List<dynamic> apfaList = decodedApplicationCall["apfa"] ?? [];
                  for(int i = 0 ; i < apfaList.length; i++){
                    var apfa = apfaList[i] as int;
                    foreignApps.add(apfa);
                  }
                }

                var transaction;

                if(decodedApplicationCall["apan"] != null){
                  apanNumber = decodedApplicationCall["apan"];
                  transaction = await (ApplicationCallTransactionBuilder()
                    ..onCompletion = OnCompletion.values[decodedApplicationCall["apan"]]
                    ..sender = snd
                    ..applicationId = applicationId
                    ..arguments = argumentList.isNotEmpty ? argumentList : null
                    ..foreignAssets = foreignAssets.isNotEmpty ? foreignAssets : null
                    ..foreignApps = foreignApps.isNotEmpty ? foreignApps : null
                    ..firstValid = fv
                    ..lastValid = lv
                    ..accounts = apatAccounts.isNotEmpty ? apatAccounts : null
                    ..genesisHash = ghList
                    ..genesisId = gen
                    ..group = grpList)
                      .build();
                }
                else{
                  transaction = await (ApplicationCallTransactionBuilder()
                    ..sender = snd
                    ..applicationId = applicationId
                    ..arguments = argumentList.isNotEmpty ? argumentList : null
                    ..foreignAssets = foreignAssets.isNotEmpty ? foreignAssets : null
                    ..foreignApps = foreignApps.isNotEmpty ? foreignApps : null
                    ..firstValid = fv
                    ..lastValid = lv
                    ..accounts = apatAccounts.isNotEmpty ? apatAccounts : null
                    ..genesisHash = ghList
                    ..genesisId = gen
                    ..group = grpList)
                      .build();
                }

                transaction.fee = fee;
                var signedTx = await transaction.sign(masterAccount);

                String from = MyUtils.parseAddress(transaction.sender?.encodedAddress ?? "");
                String to = "";
                String balance = "";
                String amount = "";
                String asset = "";
                String txFee = "${NumberFormat('#,##0.000', 'en_US').format(Algo.fromMicroAlgos(
                    transaction.fee ?? 0))} ALGO";
                String type = "appl";
                String txApplicationId = "${transaction.applicationId}";

                WalletTransactionDetail walletTransactionDetail = WalletTransactionDetail(0, from, to,
                    balance, asset, amount, txFee, type, txApplicationId,"",0,0);

                walletTransactionDetailList.add(walletTransactionDetail);
                transactionsBase64.add(signedTx.toBase64());
              }
            }
          }
        }
      }
    }
  }

  Future<void> signTransaction() async {
    try{
      wcClient?.approveRequest(id: requestId, result: transactionsBase64);
    }
    on AlgorandException catch (ex) {
      if(ex.cause is DioError){
        print((ex.cause as DioError).response);
      }
      else{
        print((ex).message);
      }
      print(ex);
    }
  }

  PaymentTransaction decodeToPaymentTransaction(String txn) {
    var decodedBase64 = base64Decode(txn);
    return PaymentTransaction.fromJson(Encoder.decodeMessagePack(decodedBase64));
  }

  AssetTransferTransaction assetTransferTransaction(String txn) {
    var decodedBase64 = base64Decode(txn);
    return AssetTransferTransaction.fromJson(Encoder.decodeMessagePack(decodedBase64));
  }

  RawTransaction decodeRawTransaction(String txn) {
    var decodedBase64 = base64Decode(txn);
    return RawTransaction.fromJson(Encoder.decodeMessagePack(decodedBase64));
  }

  showQRError(HomeController themeValue){
    showModalBottomSheet(
        context: buildContext,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 235.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 235.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset("assets/icons/ic_alert_triangle.svg", height: 35, width: 35),
                                  SizedBox(width: 4),
                                  Text(
                                    "ERROR",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowSemiBold",
                                      decorationThickness: 1.5,
                                    ),
                                  )
                                ]),
                            SizedBox(height: 24),
                            Text(
                              "Invalid QR Code. Try a new one or try again.",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 32),
                            GestureDetector(
                              child: CustomButton(45, double.maxFinite, BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.topRight,
                                    colors: [
                                      Color.fromRGBO(106, 48, 147, 1.0),
                                      Color.fromRGBO(160, 68, 255, 1.0)
                                    ]
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                              ), StringContent.close, TextStyle(
                                  color: Colors.white,
                                  fontFamily: "BarlowRegular",
                                  fontSize: 12,
                                  letterSpacing: 2
                              )),
                              onTap: (){
                                Get.back();
                              },
                            )
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }
}

