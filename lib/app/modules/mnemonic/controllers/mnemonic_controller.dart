import 'dart:io';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenshot_callback/flutter_screenshot_callback.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/modules/mnemonic_verify/views/mnemonic_verify_view.dart';
import 'package:waagmiswap/app/util/my_util.dart';

class MnemonicController extends GetxController implements IScreenshotCallback{

  late TextEditingController textEditingController;
  late ScreenshotCallback _screenshotCallback;
  late DatabaseDAO databaseDAO;

  final count = 0.obs;

  bool isRevealed = false;
  bool isCopied = false;

  late FocusNode accountFocus;

  late List<String> mnemonicLists;
  String publicAddress = "";
  late Account account;
  String passcode = "";

  bool isAccountFocus = false;
  bool isErrorShowed = false;
  bool isSameName = false;
  bool isDialogShowed = false;
  bool isFromHome = false;
  bool faceId = false;

  BuildContext? buildContext;
  int themeType = 0;

  @override
  void onInit() {
    initCallback();

    if(Platform.isAndroid){
      FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
    }

    databaseDAO = DatabaseDAO();
    textEditingController = TextEditingController();
    accountFocus = FocusNode();
    accountFocus.addListener(onAccountFocus);
    mnemonicLists = [];
    publicAddress = Get.arguments["public_address"] ?? "";
    mnemonicLists = Get.arguments["mnemonic_list"];
    account = Get.arguments["account"];

    if(Get.arguments["face_id"] != null){
      faceId = Get.arguments["face_id"];
    }

    if(Get.arguments["passcode"] != null){
      passcode = Get.arguments["passcode"];
    }

    if(Get.arguments["from_home"] != null){
      isFromHome = Get.arguments["from_home"];
    }

    if(isFromHome){
      loadUser();
    }

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void increment() => count.value++;

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  void initCallback(){
    _screenshotCallback = ScreenshotCallback();
    _screenshotCallback.setInterfaceScreenshotCallback(this);
  }

  void setReveal(){
    isRevealed = true;
    if(Platform.isAndroid){
      FlutterWindowManager.addFlags(FlutterWindowManager.FLAG_SECURE);
    }
    update(["reveal"]);
  }

  void setCopied(){
    isCopied = true;
    FlutterClipboard.copy(mnemonicLists.toString());
    update(["reveal"]);
  }

  void onAccountFocus(){
    isAccountFocus = true;
    update(["account_focus"]);
  }

  Future<void> setError() async {
    var users = await databaseDAO.getAllSorted("user_name",textEditingController.text);

    if(users.isNotEmpty){
      isSameName = true;
      isErrorShowed = true;
      update(["error"]);
    }
    else{
      isSameName = false;
      isErrorShowed = textEditingController.text.length < 5;
      update(["error"]);
    }

    if(isErrorShowed){
      return;
    }

    Get.off(() => MnemonicVerifyView(),
        arguments: {
          "user_name" : textEditingController.text,
          "public_address" : publicAddress,
          "mnemonic_list" : mnemonicLists,
          "account" : account,
          "passcode" : passcode,
          "mnemonic" : MyUtils.parseMnemonic(mnemonicLists),
          "from_home" : isFromHome,
          "face_id" : faceId,
        }
    );
  }

  showBottomSheet(){
    if(buildContext != null){
      if(isRevealed){
        showModalBottomSheet(
            context: buildContext!,
            backgroundColor: Colors.transparent,
            isScrollControlled: true,
            isDismissible: false,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
            ),
            builder: (BuildContext context) {
              return Container(
                  width: double.maxFinite,
                  height: 340.5,
                  child: Stack(
                    children: [
                      Container(
                        width: double.maxFinite,
                        height: 340.5,
                        decoration: BoxDecoration(
                          color: ThemeConstant.blueBorder,
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(24),
                        ),
                      ),
                      Column(
                        children: [
                          Expanded(child: SizedBox(), flex: 1),
                          Container(
                            width: double.maxFinite,
                            height: 340,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    ThemeConstant.darkThemeBackground1,
                                    ThemeConstant.darkThemeBackground1
                                  ]
                              ),
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                            ),
                            child: Column(
                              children: [
                                SizedBox(height: 16),
                                Text(
                                  StringContent.screenshot_detected,
                                  style: TextStyle(
                                    color: ThemeConstant.white,
                                    fontSize: 18,
                                    fontFamily: "BarlowMedium",
                                  ),
                                ),
                                SizedBox(height: 24),
                                SvgPicture.asset("assets/icons/ic_alert_triangle.svg", height: 75, width: 75),
                                SizedBox(height: 24),
                                Row(
                                  children: [
                                    SizedBox(width: 24),
                                    Expanded(child: Text(
                                      StringContent.sreenshot_detail,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: ThemeConstant.white,
                                          fontSize: 12,
                                          fontFamily: "BarlowNormal",
                                          height: 1.5
                                      ),
                                    ), flex: 1),
                                    SizedBox(width: 24)
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 24, right: 24, top: 40),
                                  width: double.maxFinite,
                                  height: 45,
                                  child: Stack(
                                    children: [
                                      CustomButton(45, double.maxFinite, BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.topRight,
                                            colors: [
                                              Color.fromRGBO(106, 48, 147, 1.0),
                                              Color.fromRGBO(160, 68, 255, 1.0)
                                            ]
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                      ),  StringContent.accept, TextStyle(
                                          color: Colors.white,
                                          fontFamily: "BarlowRegular",
                                          fontSize: 12,
                                          letterSpacing: 2
                                      )),

                                      Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          splashColor: Colors.transparent,
                                          onTap: (){
                                            Get.back();
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  )
              );
            });
      }
    }
  }

  @override
  deniedPermission() {

  }

  @override
  screenshotCallback(String data) {
    showBottomSheet();
  }
}
