import 'package:get/get.dart';

import '../controllers/mnemonic_controller.dart';

class MnemonicBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MnemonicController>(
      () => MnemonicController(),
    );
  }
}
