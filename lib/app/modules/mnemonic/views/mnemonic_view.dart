import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';

import '../controllers/mnemonic_controller.dart';

class MnemonicView extends GetView<MnemonicController> {

  MnemonicController mnemonicController = Get.put(MnemonicController());

  @override
  Widget build(BuildContext context) {
    mnemonicController.buildContext = context;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GetBuilder<MnemonicController>(
        id: "theme",
        init: MnemonicController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1
                        : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2
                        : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Column(
            children: [
              SizedBox(height: 24),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 3),
                  GestureDetector(
                    child: Container(
                      color: Colors.transparent,
                      width: 55,
                      height: 55,
                      child: Center(
                        child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                        "assets/icons/ic_black_back.png", width: 24,
                            height: 24, fit: BoxFit.contain),
                      ),
                    ),
                    onTap: (){
                      Get.back();
                    },
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Image.asset("assets/images/img_hori_messe.png", width: 182,
                      height: 23, fit: BoxFit.contain),
                  Expanded(child: SizedBox(),flex: 1),
                  Container(
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(width: 3)
                ],
              ),

              SizedBox(height: 14),

              Text(
                StringContent.your_mnemonic,
                style: TextStyle(
                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                    fontSize: 14,
                    fontFamily: "BarlowNormal",
                    fontWeight: FontWeight.w400,
                    letterSpacing: 2
                ),
              ),

              SizedBox(height: 6),

              Row(
                children: [
                  SizedBox(width: 24),
                  Expanded(child: Text(
                    StringContent.your_mnemonic_desc,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 12,
                        fontFamily: "BarlowRegular",
                        letterSpacing: 1,
                        fontWeight: FontWeight.w400,
                        height: 1.5
                    ),
                  ), flex: 1),
                  SizedBox(width: 24)
                ],
              ),

              SizedBox(height: 16),

              Row(
                children: [
                  SizedBox(width: 24),
                  Expanded(child: Text(
                    StringContent.account_name,
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 12,
                        fontFamily: "BarlowRegular",
                        fontWeight: FontWeight.w400,
                        letterSpacing: 2,
                        height: 1.5
                    ),
                  ), flex: 1),
                  SizedBox(width: 24)
                ],
              ),

              SizedBox(height: 8),

              GetBuilder<MnemonicController>(
                  id: "account_focus",
                  init: MnemonicController(),
                  builder: (value) => GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(left: 24, right: 24),
                      padding: EdgeInsets.only(left: 16, right: 16),
                      width: double.maxFinite,
                      height: 35,
                      decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          border: Border.all(
                              width: 1,
                              color: value.isAccountFocus ? themeValue.themeType == 0 ? Colors.white : ThemeConstant.blackNavy : Color.fromRGBO(164,164,164,1)
                          )
                      ),
                      child: Center(
                        child: TextField(
                          controller: mnemonicController.textEditingController,
                          style: TextStyle(
                            color: themeValue.themeType == 0 ? Colors.white : ThemeConstant.blackNavy,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            letterSpacing: 2,
                            fontFamily: "BarlowRegular",
                          ),
                          decoration: InputDecoration.collapsed(hintText: StringContent.enter_account_name,
                              hintStyle: TextStyle(
                                color: Color.fromRGBO(164,164,164,1),
                                fontSize: 12,
                                fontFamily: "BarlowRegular",
                              )
                          ),
                          focusNode: mnemonicController.accountFocus,
                        ),
                      ),
                    ),
                    onTap: (){
                      value.accountFocus.requestFocus();
                    },
                  )),

              GetBuilder<MnemonicController>(
                  id: "error",
                  init: MnemonicController(),
                  builder: (value) => Visibility(child: Column(
                    children: [
                      Row(
                        children: [
                          SizedBox(width: 24),
                          Expanded(child: Text(
                            value.isSameName ? StringContent.name_used :
                            StringContent.account_name_error,
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 12,
                                fontFamily: "BarlowRegular",
                                height: 1.5
                            ),
                          ), flex: 1),
                          SizedBox(width: 24)
                        ],
                      )
                    ],
                  ), visible: value.isErrorShowed)),

              GetBuilder<MnemonicController>(
                  id: "reveal",
                  init: MnemonicController(),
                  builder: (value) => !value.isRevealed ? Container(
                    margin: EdgeInsets.all(24),
                    padding: EdgeInsets.all(24),
                    width: double.maxFinite,
                    height: 315,
                    decoration: BoxDecoration(
                        color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.lightPurple,
                        borderRadius: BorderRadius.all(Radius.circular(4))
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          StringContent.store_mnemonic,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.blackNavy : ThemeConstant.white,
                              fontSize: 12,
                              fontFamily: "BarlowNormal",
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1,
                              height: 1.5
                          ),
                        ),

                        SizedBox(height: 16),

                        GestureDetector(
                          child: Text(
                            StringContent.tap_reveal_mnemonic,
                            style: TextStyle(
                              color: Colors.transparent,
                              shadows: [
                                Shadow(
                                    color: themeValue.themeType == 0 ? ThemeConstant.blackNavy :
                                    ThemeConstant.white,
                                    offset: Offset(0, -5))
                              ],
                              fontSize: 14,
                              fontFamily: "BarlowMedium",
                              height: 1.5,
                              decorationColor: Colors.black,
                              decoration: TextDecoration.underline,
                              decorationThickness: 1.5,
                            ),
                          ),
                          onTap: (){
                            mnemonicController.setReveal();
                          },
                        )
                      ],
                    ),
                  ) : Container(
                    margin: EdgeInsets.all(24),
                    padding: EdgeInsets.all(16),
                    width: double.maxFinite,
                    height: 325,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        border: Border.all(
                            color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.lightPurple,
                            width: 1
                        )
                    ),
                    child: Column(
                      children: [
                        Expanded(child: GridView.count(
                            crossAxisCount: 3,
                            childAspectRatio: 3.5,
                            padding: EdgeInsets.zero,
                            children: List.generate(value.mnemonicLists.length, (index) {
                              return Text(
                                "${index + 1}. ${value.mnemonicLists[index]}",
                                style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.yellowDark :
                                    ThemeConstant.lightPurple,
                                    fontSize: 12,
                                    fontFamily: "BarlowRegular",
                                    height: 1.5
                                ),
                              );
                            })
                        ), flex: 1),

                        GestureDetector(
                          child: Text(
                            !value.isCopied ? StringContent.tap_to_copy : StringContent.copied,
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                              fontSize: 14,
                              fontFamily: "BarlowMedium",
                              height: 1.5,
                              decoration: TextDecoration.underline,
                              decorationThickness: 1.5,
                            ),
                          ),
                          onTap: (){
                            mnemonicController.setCopied();
                          },
                        )
                      ],
                    ),
                  )),

              GetBuilder<MnemonicController>(
                id: "reveal",
                init: MnemonicController(),
                builder: (buttonValue) => Visibility(child: Row(
                  children: [
                    SizedBox(width: 16),
                    Expanded(child: Text(
                      StringContent.make_sure,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        height: 1.5,
                        color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark
                            : ThemeConstant.secondTextGrayLight,
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 1,
                        fontFamily: "BarlowNormal",
                      ),
                    ), flex: 1),
                    SizedBox(width: 16),
                  ],
                ), visible: buttonValue.isRevealed),
              ),

              Expanded(child: SizedBox(),flex: 1),

              GetBuilder<MnemonicController>(
                  id: "reveal",
                  init: MnemonicController(),
                  builder: (value) => Visibility(child: Container(
                    width: double.maxFinite,
                    margin: EdgeInsets.only(left: 24, right: 24),
                    height: 37,
                    child: Stack(
                      children: [
                        CustomButton(45, double.maxFinite, BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.topRight,
                              colors: [
                                Color.fromRGBO(106, 48, 147, 1.0),
                                Color.fromRGBO(160, 68, 255, 1.0)
                              ]
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ), StringContent.verify, TextStyle(
                            color: Colors.white,
                            fontFamily: "BarlowRegular",
                            fontSize: 12,
                            letterSpacing: 2
                        )),

                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.transparent,
                            onTap: (){
                              mnemonicController.setError();
                            },
                          ),
                        )
                      ],
                    ),
                  ), visible: value.isRevealed)),

              SizedBox(height: StringContent.bottomMargin)
            ],
          ),
        ),
      ),
    );
  }
}
