import 'dart:io';
import 'dart:math';

import 'package:algorand_dart/algorand_dart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/algorand/algorand.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/transaction_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/repository/api_repository.dart';
import 'package:waagmiswap/app/util/my_util.dart';
import 'package:waagmiswap/app/widgets/custom_toast.dart';

class SendConfirmController extends GetxController {

  final count = 0.obs;
  int themeType = 0;

  late Map sendMap;
  late Algorand algorand;

  late DatabaseDAO databaseDAO;
  late APIRepository apiRepository;
  late AlgorandRepository algorandRepository;

  late LedgerModel ledgerModel;
  late UserModel userModel;

  String receiverUserName = "";
  String sendMessage = "";
  String receivedMessage = "";

  RxBool isLoading = false.obs;
  bool isRecipientInLocal = false;
  bool isFirstState = true;

  Account? algorandAccount;
  String currentCurrency = "USD";

  late BuildContext buildContext;
  int userPosition = 0;

  @override
  void onInit() {
    if(Platform.isAndroid){
      FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
    }

    databaseDAO = DatabaseDAO();

    if(Get.arguments != null){
      sendMap = Get.arguments["send_map"];
      ledgerModel = Get.arguments["ledger_model"];
      userModel = Get.arguments["user_model"];
      userPosition = Get.arguments["user_position"] ?? 0;
      loadUsersAccount();
    }

    initAlgorand();
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void increment() => count.value++;

  void loadUsersAccount(){
    databaseDAO.getAllSorted("address", sendMap["recipient"]).then((value) async {
      if(value.isNotEmpty){
        isRecipientInLocal = true;

        receiverUserName = value[0].userName;

        sendMessage = "${sendMap["asset_amount"]} ${sendMap["asset_unit_name"]} "
            "sent to ${receiverUserName.isEmpty ? MyUtils.parseAddress(sendMap["recipient"])
            : receiverUserName} by ${userModel.userName.isEmpty ? MyUtils.parseAddress(userModel.address) :
        userModel.userName}";

        receivedMessage = "You received ${sendMap["asset_amount"]} ${sendMap["asset_unit_name"]} from "
            "${userModel.userName.isEmpty ? MyUtils.parseAddress(userModel.address) : userModel.userName}";
      }
      else{
        isRecipientInLocal = false;

        sendMessage = "${sendMap["asset_amount"]} ${sendMap["asset_unit_name"]} "
            "sent to ${MyUtils.parseAddress(sendMap["recipient"])} by ${userModel.userName.isEmpty ?
        MyUtils.parseAddress(userModel.address) :
        userModel.userName}";
      }
    });
  }

  void saveTransaction(String transactionId){
    DateTime now = DateTime.now();

    String createdDate = DateFormat("MMM dd, yyyy").format(now);
    String createdTime = DateFormat("HH:mm a").format(now);
    String assetUnitName = sendMap["asset_unit_name"];

    int assetAmount = assetUnitName.toUpperCase() == "ALGO" ? Algo.toMicroAlgos(
        double.parse(sendMap["asset_amount"])) : (double.parse(sendMap["asset_amount"]) * pow(10, sendMap[
    "decimal"])).toInt();

    if(sendMap["recipient"] == userModel.address && sendMap["asset_amount"] == "0"){
      TransactionModel transactionModel = TransactionModel(transaction_id: transactionId,
          type: "Add Asset", address: sendMap["recipient"], assetId: sendMap["asset_id"],
          assetName: assetUnitName.toUpperCase(), amount: assetAmount, price: sendMap["asset_price"],
          createdAt: createdDate, note: sendMap["note"], createdTime: createdTime,
          notificationMessage: sendMessage, read: false,confirmedRound: 0, decimal: sendMap["decimal"] ?? 0,
          from: userModel.address, ledgerId: ledgerModel.ledgerId, applicationId: 0, fee: 1000);

      databaseDAO.insertTransaction(transactionModel);
    }
    else{
      if(isRecipientInLocal){
        TransactionModel transactionModel = TransactionModel(transaction_id: transactionId,
            type: "Send", address: sendMap["recipient"], assetId: sendMap["asset_id"],
            assetName: assetUnitName.toUpperCase(), amount: assetAmount, price: sendMap["asset_price"],
            createdAt: createdDate, note: sendMap["note"], createdTime: createdTime,
            notificationMessage: sendMessage, read: false,confirmedRound: 0, decimal: sendMap["decimal"] ?? 0,
            from: userModel.address, ledgerId: ledgerModel.ledgerId, applicationId: 0, fee: 1000);

        TransactionModel transactionModel2 = TransactionModel(transaction_id: transactionId,
            type: "Receive", address: sendMap["recipient"], assetId: sendMap["asset_id"],
            assetName: assetUnitName.toUpperCase(), amount: assetAmount, price: sendMap["asset_price"],
            createdAt: createdDate, note: sendMap["note"], createdTime: createdTime,
            notificationMessage: receivedMessage, read: false,confirmedRound: 0, decimal: sendMap["decimal"] ?? 0,
            from: userModel.address, ledgerId: ledgerModel.ledgerId, applicationId: 0, fee: 1000);

        databaseDAO.insertTransaction(transactionModel);
        databaseDAO.insertTransaction(transactionModel2);
      }
      else{
        TransactionModel transactionModel = TransactionModel(transaction_id: transactionId,
            type: "Send", address: sendMap["recipient"], assetId: sendMap["asset_id"],
            assetName: assetUnitName.toUpperCase(), amount: assetAmount, price: sendMap["asset_price"],
            createdAt: createdDate, note: sendMap["note"], createdTime: createdTime,
            notificationMessage: sendMessage, read: false,confirmedRound: 0, decimal: sendMap["decimal"] ?? 0,
            from: userModel.address, ledgerId: ledgerModel.ledgerId, applicationId: 0, fee: 1000);

        databaseDAO.insertTransaction(transactionModel);
      }
    }
  }

  void initAlgorand(){
    apiRepository = APIRepository(this);
    algorandRepository = AlgorandRepository();
    algorand = algorandRepository.getAlgorandConfig(ledgerModel.algodUrl,
        ledgerModel.indexerUrl);
    restoreAccount();
  }

  Future<void> restoreAccount() async {
    algorand.restoreAccount(userModel.mnemonic.split(" ")).then((value) async {
      algorandAccount = value;
    }).catchError((error, stackTrace) {
      var mnemonicError = error as MnemonicException;
      CustomToast.showToast(mnemonicError.message);
      return null;
    });
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  Future<void> sendAsset() async {
    isLoading.value = true;
    update(["theme"]);

    Address receiver = Address.fromAlgorandAddress(address: sendMap["recipient"]);
    int assetId = sendMap["asset_id"];
    String assetAmount = sendMap["asset_amount"];
    String notes = sendMap["note"];

    Account signer = await algorand.restoreAccount(userModel.mnemonic.split(" "));
    Address sender = Address.fromAlgorandAddress(address: signer.publicAddress);

    _sendTransaction(signer,sender,receiver,assetId,assetAmount,null,notes, "send");
  }

  Future<void> _sendTransaction(Account signer, Address sender, Address receiver, int assetId, String amount,
      Address? rekey, String notes, String type) async {
    try{
      final params = await algorand.getSuggestedTransactionParams();
      String txId = "";

      RawTransaction transaction;

      if(assetId != 0){
        transaction = await (AssetTransferTransactionBuilder()
          ..assetId = assetId
          ..sender = sender
          ..note = notes.toBytes()
          ..amount = !sendMap["is_max"] ? (double.parse(amount) * pow(10, sendMap[
          "decimal"])).toInt() : sendMap["max_amount"]
          ..receiver = receiver
          ..rekeyTo = rekey
          ..suggestedParams = params).build();
      }
      else{
        transaction = await (PaymentTransactionBuilder()
          ..sender = sender
          ..note = notes.toBytes()
          ..amount = Algo.toMicroAlgos(double.parse(amount))
          ..receiver = receiver
          ..rekeyTo = rekey
          ..suggestedParams = params).build();
      }

      txId = await transaction.id;

      final signedTx = await transaction.sign(signer);

      await algorand.sendTransaction(signedTx);

      if(type == "send"){
        saveTransaction(txId);
      }

      await apiRepository.getAssetPending(txId, userModel, ledgerModel.ledgerId, ledgerModel.algodUrl,
          ledgerModel.indexerUrl, null, null, 0, type, sendMessage, currentCurrency, {}, 0, 0, 0, "",
          null, 0);

    } on AlgorandException catch (ex) {
      isLoading.value = false;
      update(["theme"]);

      if(ex.message == "recipient has not opted in or added the ASSET in their own wallet"){
        showQRError(this,"Recipient has not added (opted in to) the ${sendMap["asset_unit_name"]} asset.");
      }
      else{
        showQRError(this,ex.message);
      }
    }
  }

  showQRError(SendConfirmController themeValue, String message){
    showModalBottomSheet(
        context: buildContext,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 235.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 235.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset("assets/icons/ic_alert_triangle.svg", height: 35, width: 35),
                                  SizedBox(width: 4),
                                  Text(
                                    "ERROR",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowSemiBold",
                                      decorationThickness: 1.5,
                                    ),
                                  )
                                ]),
                            SizedBox(height: 24),
                            Text(
                              message,
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 32),
                            GestureDetector(
                              child: CustomButton(45, double.maxFinite, BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.topRight,
                                    colors: [
                                      Color.fromRGBO(106, 48, 147, 1.0),
                                      Color.fromRGBO(160, 68, 255, 1.0)
                                    ]
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                              ), StringContent.close, TextStyle(
                                  color: Colors.white,
                                  fontFamily: "BarlowRegular",
                                  fontSize: 12,
                                  letterSpacing: 2
                              )),
                              onTap: (){
                                Get.back();
                              },
                            )
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }
}

