import 'package:get/get.dart';

import '../controllers/send_confirm_controller.dart';

class SendConfirmBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SendConfirmController>(
      () => SendConfirmController(),
    );
  }
}
