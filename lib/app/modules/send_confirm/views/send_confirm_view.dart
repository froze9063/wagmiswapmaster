import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/util/my_util.dart';

import '../controllers/send_confirm_controller.dart';

class SendConfirmView extends GetView<SendConfirmController> {

  final SendConfirmController _sendConfirmController = Get.put(SendConfirmController());

  @override
  Widget build(BuildContext context) {
    _sendConfirmController.buildContext = context;
    return Scaffold(
      body: GetBuilder<SendConfirmController>(
        id: "theme",
        init: SendConfirmController(),
        builder: (themeValue) => _sendConfirmController.isFirstState ? Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(height: 30),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: 3),
                      GestureDetector(
                        child: Container(
                          color: Colors.transparent,
                          width: 55,
                          height: 55,
                          child: Center(
                            child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                            "assets/icons/ic_black_back.png", width: 24,
                                height: 24, fit: BoxFit.contain),
                          ),
                        ),
                        onTap: (){
                          Get.back();
                        },
                      ),
                      Expanded(child: SizedBox(),flex: 1),
                      Text(
                        StringContent.send,
                        style: TextStyle(
                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                            fontSize: 14,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w600,
                            letterSpacing: 2
                        ),
                      ),
                      Expanded(child: SizedBox(),flex: 1),
                      Container(
                        width: 55,
                        height: 55,
                      ),
                      SizedBox(width: 3)
                    ],
                  ),

                  SizedBox(height: 18),

                  Expanded(child: confirmWidget(themeValue),flex: 1)
                ],
              ),

              Visibility(child: Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.transparent,
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.3),
                        borderRadius: BorderRadius.all(Radius.circular(8))
                    ),
                    child: Center(
                      child: CircularProgressIndicator(
                        color: Color.fromRGBO(106, 48, 147, 1.0),
                      ),
                    ),
                  ),
                ),
              ), visible: _sendConfirmController.isLoading.value)
            ],
          ),
        ) : SizedBox(),
      ),
    );
  }

  Widget confirmWidget(SendConfirmController themeValue){
    return Column(
      children: [

        Container(
          margin: EdgeInsets.only(left: 24, right: 24),
          padding: EdgeInsets.all(8),
          width: double.maxFinite,
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(child: Text(
                    StringContent.transaction_type,
                    style: TextStyle(
                      color: ThemeConstant.secondTextGrayDark,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  ), flex: 1),

                  Text(
                    "Send",
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  )
                ],
              ),

              SizedBox(height: 24),

              Row(
                children: [
                  Expanded(child: Text(
                    StringContent.asset,
                    style: TextStyle(
                      color: ThemeConstant.secondTextGrayDark,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  ), flex: 1),

                  Text(
                    "${_sendConfirmController.sendMap["asset_unit_name"]}".toUpperCase(),
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  )
                ],
              ),

              SizedBox(height: 24),

              Row(
                children: [
                  Expanded(child: Text(
                    StringContent.amount,
                    style: TextStyle(
                      color: ThemeConstant.secondTextGrayDark,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  ), flex: 1),

                  Text(
                    "${_sendConfirmController.sendMap["asset_amount"]} ${(_sendConfirmController.sendMap[
                    "asset_unit_name"] as String).toUpperCase()} "
                        "| ${_sendConfirmController.sendMap["price"]}",
                    style: TextStyle(
                      color: ThemeConstant.yellowDark,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  )
                ],
              ),

              SizedBox(height: 24),

              Row(
                children: [
                  Expanded(child: Text(
                    StringContent.recipient,
                    style: TextStyle(
                      color: ThemeConstant.secondTextGrayDark,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  ), flex: 1),

                  Text(
                    MyUtils.parseAddress(_sendConfirmController.sendMap["recipient"]),
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  )
                ],
              ),

              SizedBox(height: 24),

              Row(
                children: [
                  Text(
                    StringContent.notes,
                    style: TextStyle(
                      color: ThemeConstant.secondTextGrayDark,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  ),
                  SizedBox(width: 24),
                  Expanded(child: Text(
                    (_sendConfirmController.sendMap["note"] as String).isEmpty ?
                    "-" : _sendConfirmController.sendMap["note"],
                    textAlign: TextAlign.end,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  ), flex: 1),
                ],
              ),

              SizedBox(height: 24),

              Row(
                children: [
                  Text(
                    StringContent.fee,
                    style: TextStyle(
                      color: ThemeConstant.secondTextGrayDark,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  ),

                  SizedBox(width: 4),

                  SvgPicture.asset("assets/icons/ic_grey_info.svg", height: 16, width: 16),

                  Expanded(child: SizedBox(), flex: 1),

                  Text(
                    "0.001 ALGOS",
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  )
                ],
              ),

              SizedBox(height: 24),

              Row(
                children: [
                  Expanded(child: Text(
                    StringContent.total_amount,
                    style: TextStyle(
                      color: ThemeConstant.secondTextGrayDark,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  ), flex: 1),

                  Text(
                    "~ ${_sendConfirmController.sendMap["price"]}",
                    style: TextStyle(
                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                      decorationThickness: 1.5,
                    ),
                  )
                ],
              )
            ],
          ),
        ),

        Expanded(child: SizedBox(), flex: 1),

        Row(
          children: [
            SizedBox(width: 24),

            Expanded(child: Container(
              width: double.maxFinite,
              height: 37,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  splashColor: Colors.transparent,
                  child: CustomButton(37, double.maxFinite, BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      border: Border.all(
                          width: 1,
                          color: Color.fromRGBO(184, 115, 255, 1.0)
                      )
                  ), StringContent.reject, TextStyle(
                      color: Color.fromRGBO(184, 115, 255, 1.0),
                      fontFamily: "BarlowRegular",
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 2
                  )),
                  onTap: (){
                    Get.back();
                  },
                ),
              ),
            ), flex: 1),

            SizedBox(width: 16),

            Expanded(child: Container(
              width: double.maxFinite,
              height: 37,
              child: Stack(
                children: [
                  CustomButton(37, double.maxFinite, BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color.fromRGBO(106, 48, 147, 1.0),
                          Color.fromRGBO(160, 68, 255, 1.0)
                        ]
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ), StringContent.sign, TextStyle(
                      color: Colors.white,
                      fontFamily: "BarlowRegular",
                      fontWeight: FontWeight.w600,
                      fontSize: 12,
                      letterSpacing: 2
                  )),

                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      splashColor: Colors.transparent,
                      onTap: (){
                        _sendConfirmController.sendAsset();
                      },
                    ),
                  )
                ],
              ),
            ), flex: 1),

            SizedBox(width: 24),
          ],
        ),

        SizedBox(height: StringContent.bottomMargin)
      ],
    );
  }
}

