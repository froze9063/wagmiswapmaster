import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/enter_passcode/views/enter_passcode_view.dart';

class StandartAccountDetailController extends GetxController {

  DatabaseDAO databaseDAO = DatabaseDAO();

  bool isContinue = false;

  late FocusNode nameFocus;
  late UserModel userModel;

  bool isNameFocus = false;

  int themeType = 0;
  int counterText = 0;
  bool isSameName = false;

  TextEditingController nameController = TextEditingController();

  int tabPosition = 1;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late LedgerModel selectedLedger;

  @override
  void onInit() {
    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];
    loadUser();
    userModel = Get.arguments["user_model"];
    nameController.text = userModel.userName;
    isContinue = userModel.userName.isNotEmpty;
    counterText = userModel.userName.length;
    nameFocus = FocusNode();
    nameFocus.addListener(onNameFocus);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> checkContinue(String text) async {
    var users = await databaseDAO.getAllSorted("user_name", text);

    if(users.isNotEmpty){
      isSameName = true;
      isContinue = false;
    }
    else{
      isSameName = false;
      isContinue = isContinue = text.length > 4;
    }

    update(["button_state"]);
  }

  void onNameFocus(){
    isNameFocus = true;
    update(["name_focus"]);
  }

  void countText(String text){
    counterText = text.length;
    checkContinue(text);
    update(["counter_text"]);
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  void confirmUnlink() {
    Get.to(() => EnterPasscodeView(), arguments: {
      "user_model" : userModel, "from_unlink" : true});
  }

  void saveChanges(){
    userModel.userName = nameController.text.toString().trim();
    databaseDAO.update(userModel).then((value) {
      Get.back();
    });
  }
}
