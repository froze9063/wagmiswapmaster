import 'package:get/get.dart';

import '../controllers/standart_account_detail_controller.dart';

class StandartAccountDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StandartAccountDetailController>(
      () => StandartAccountDetailController(),
    );
  }
}
