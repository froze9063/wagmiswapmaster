import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/mnemonic_reveal/views/mnemonic_reveal_view.dart';
import 'package:waagmiswap/app/util/my_util.dart';

import '../controllers/standart_account_detail_controller.dart';

class StandartAccountDetailView extends GetView<StandartAccountDetailController> {

  final StandartAccountDetailController _standartAccountDetailController = Get.put(
      StandartAccountDetailController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GetBuilder<StandartAccountDetailController>(
        id: "theme",
        init: StandartAccountDetailController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Column(
            children: [
              SizedBox(height: 30),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 3),
                  GestureDetector(
                    child: Container(
                      color: Colors.transparent,
                      width: 55,
                      height: 55,
                      child: Center(
                        child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                        "assets/icons/ic_black_back.png", width: 24,
                            height: 24, fit: BoxFit.contain),
                      ),
                    ),
                    onTap: (){
                      Get.back();
                    },
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_folder.svg" :
                  "assets/icons/ic_black_folder.svg", height: 22, width: 22),
                  SizedBox(width: 6),
                  Text(
                    _standartAccountDetailController.userModel.userName.isEmpty ? MyUtils.parseAddress(_standartAccountDetailController.userModel.address) : _standartAccountDetailController.userModel.userName,
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 16,
                        fontFamily: "BarlowMedium",
                        letterSpacing: 1
                    ),
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Container(
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(width: 3)
                ],
              ),
              SizedBox(height: 18),
              Row(
                children: [
                  SizedBox(width: 24),
                  Text(
                    StringContent.name,
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                        fontSize: 16,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  SizedBox(width: 24),
                  Expanded(child: GetBuilder<StandartAccountDetailController>(
                    id: "name_focus",
                    init: StandartAccountDetailController(),
                    builder: (value) => GestureDetector(
                      child: Container(
                        width: double.maxFinite,
                        height: 40,
                        padding: EdgeInsets.only(left: 8, right: 8),
                        decoration: BoxDecoration(
                            color: themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            border: Border.all(
                                width: 1,
                                color: value.isNameFocus ? themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.lightActiveBorder
                                    : Colors.transparent
                            )
                        ),
                        child: Center(
                          child: TextField(
                            controller: _standartAccountDetailController.nameController,
                            maxLength: 20,
                            style: TextStyle(
                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                              fontSize: 12,
                              fontFamily: "BarlowRegular",
                            ),
                            decoration: InputDecoration.collapsed(hintText: _standartAccountDetailController.userModel.userName.isEmpty ? MyUtils.parseAddress(_standartAccountDetailController.userModel.address) : _standartAccountDetailController.userModel.userName,
                              hintStyle: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                fontSize: 12,
                                fontFamily: "BarlowRegular",
                              ),
                            ),
                            buildCounter: (BuildContext context, {int? currentLength, int? maxLength, bool? isFocused}) => null,
                            onChanged: (text){
                              _standartAccountDetailController.countText(text);
                            },
                            focusNode: _standartAccountDetailController.nameFocus,
                          ),
                        ),
                      ),
                      onTap: (){
                        value.nameFocus.requestFocus();
                      },
                    ),
                  ), flex: 1),
                  SizedBox(width: 16),
                  GetBuilder<StandartAccountDetailController>(
                    id: "name_focus",
                    init: StandartAccountDetailController(),
                    builder: (value) => GestureDetector(
                      child: Container(
                        padding: EdgeInsets.all(8),
                        child: Row(
                          children: [
                            SvgPicture.asset("assets/icons/ic_red_chain.svg", height: 18, width: 18),
                            SizedBox(width: 8),
                            Text(
                              StringContent.unlink,
                              style: TextStyle(
                                color: Color.fromRGBO(241, 83, 83, 1.0),
                                fontSize: 12,
                                fontFamily: "BarlowMedium",
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            border: Border.all(
                              width: 1,
                              color: Color.fromRGBO(241, 83, 83, 1.0),
                            )
                        ),
                      ),
                      onTap: (){
                        _standartAccountDetailController.confirmUnlink();
                      },
                    ),
                  ),
                  SizedBox(width: 24),
                ],
              ),
              SizedBox(height: 4),
              GetBuilder<StandartAccountDetailController>(
                  id: "counter_text",
                  init: StandartAccountDetailController(),
                  builder: (counterValue) => Text(
                    counterValue.counterText < 5 ? StringContent.account_name_error : !counterValue.isSameName ?
                    "${counterValue.counterText}/20 Characters Remaining" : StringContent.name_used,
                    style: TextStyle(
                      color: counterValue.counterText < 5 ? Colors.red : counterValue.isSameName ? Colors.red :
                      themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                      fontSize: 12,
                      fontFamily: "BarlowNormal",
                    ),
                  )),
              SizedBox(height: 24),
              Row(
                children: [
                  SizedBox(width: 24),
                  Text(
                    StringContent.accounts,
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                        fontSize: 16,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1
                    ),
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                ],
              ),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.only(left: 24, right: 24),
                width: double.maxFinite,
                decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),
                child: ListView.builder(itemBuilder: (context,index){
                  return Container(
                    height: 45,
                    child: Material(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Colors.transparent,
                      child: Stack(
                        children: [
                          Center(
                            child: Row(
                              children: [
                                SizedBox(width: 8),
                                Text(
                                  MyUtils.parseAddress(_standartAccountDetailController.userModel.address),
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 14,
                                    fontFamily: "BarlowNormal",
                                  ),
                                ),
                                SizedBox(width: 8),
                                SvgPicture.asset("assets/icons/ic_check_green.svg", height: 22, width: 22),
                                Expanded(child: SizedBox(), flex: 1),
                                SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                    : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                    width: themeValue.themeType == 0 ? 22 : 16),
                                SizedBox(width: 8),
                              ],
                            ),
                          ),
                          InkWell(
                            splashColor: Colors.transparent,
                            onTap: (){
                              Get.to(() => MnemonicRevealView(), arguments: {"from" : 1,
                                "user_model" : _standartAccountDetailController.userModel,
                                "user_list" : _standartAccountDetailController.userModelList,
                                "selected_user" : _standartAccountDetailController.selectedUserModel,
                                "selected_ledger" : _standartAccountDetailController.selectedLedger
                              });
                            },
                          )
                        ],
                      ),
                    ),
                  );
                },
                  itemCount: 1,
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  primary: false,
                ),
              ),
              Expanded(child: SizedBox(), flex: 1),
              GetBuilder<StandartAccountDetailController>(
                  id: "button_state",
                  init: StandartAccountDetailController(),
                  builder: (value) => Container(
                    width: double.maxFinite,
                    margin: EdgeInsets.only(left: 24, right: 24),
                    height: 37,
                    child: Stack(
                      children: [
                        CustomButton(37, double.maxFinite, BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.topRight,
                              colors: [
                                value.isContinue ? Color.fromRGBO(106, 48, 147, 1.0) : Color.fromRGBO(89, 89, 89, 1.0),
                                value.isContinue ? Color.fromRGBO(160, 68, 255, 1.0) : Color.fromRGBO(89, 89, 89, 1.0)
                              ]
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ), StringContent.save, TextStyle(
                            color: value.isContinue ? Colors.white : Color.fromRGBO(151, 151, 151, 1.0),
                            fontFamily: "BarlowRegular",
                            fontSize: 12,
                            letterSpacing: 2
                        )),

                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.transparent,
                            onTap: (){
                              if(value.isContinue){
                                _standartAccountDetailController.saveChanges();
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  )
              ),
              SizedBox(height: StringContent.bottomMargin)
            ],
          ),
        ),
      ),
    );
  }
}
