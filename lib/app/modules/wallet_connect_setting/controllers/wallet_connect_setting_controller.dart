import 'dart:convert';
import 'package:fbroadcast/fbroadcast.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/connection/my_connection.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/model/wallet_connect_model.dart';
import 'package:waagmiswap/app/repository/api_repository.dart';
import 'package:wallet_connect/wallet_connect.dart';
import 'package:wallet_connect/wc_client.dart';

class WalletConnectSettingController extends GetxController {
  late MyConnection myConnection;
  late APIRepository apiRepository;

  final count = 0.obs;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late DatabaseDAO databaseDAO;
  late List<WalletConnectModel> walletConnectList;

  UserModel? selectedWalletUserModel;

  int themeType = 0;

  WCClient? wcClient;

  int disconnectPosition = -1;

  int tabPosition = 1;

  late LedgerModel selectedLedger;

  String currentCurrency = "";
  String currentCurrencySymbol = "";

  bool isWalletLoading = false;

  @override
  void onInit() {
    myConnection = MyConnection();
    apiRepository = APIRepository(this);

    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];
    selectedWalletUserModel = selectedUserModel;
    walletConnectList = [];
    databaseDAO = DatabaseDAO();

    loadUser();
    loadConnectList();
    setBroadcast();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      currentCurrency = prefs.getString("currency_name") ?? "USD";
      currentCurrencySymbol = prefs.getString("symbol") ?? "\$";
      update(["theme"]);
    });
  }

  void setDisconnectPosition(int position){
    disconnectPosition = position;
    update(["wallet_connect"]);
  }

  Future<void> loadConnectList() async {
    walletConnectList.clear();
    databaseDAO.getAllWalletConnect().then((value) {
      walletConnectList = value;
      update(["wallet_connect"]);
    });
  }

  Future<void> disconnect(WalletConnectModel walletConnectModel, int position) async {
    try{
      wcClient = WCClient(
        onConnect: () {},
        onDisconnect: (code, reason) {},
        onFailure: (error) {},
        onSessionRequest: (id, peerMeta) {},
      );

      await wcClient?.connectFromSessionStore(WCSessionStore.fromJson(jsonDecode(walletConnectModel.sessionJson)));
      await wcClient?.killSession();

      await databaseDAO.deleteWalletConnection(walletConnectModel);
      walletConnectList.removeAt(position);

      update(["wallet_connect"]);
    }
    catch(e){
      print(e);
    }
  }

  void setBroadcast(){
    FBroadcast.instance().unregister(this);
    FBroadcast.instance().register(MyConstant.BROADCAST_SETTING, (value, callback){
      isWalletLoading = value["loading"] ?? false;
      loadConnectList();
      update(["theme"]);
    });
  }
}
