import 'package:get/get.dart';

import '../controllers/wallet_connect_setting_controller.dart';

class WalletConnectSettingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WalletConnectSettingController>(
      () => WalletConnectSettingController(),
    );
  }
}
