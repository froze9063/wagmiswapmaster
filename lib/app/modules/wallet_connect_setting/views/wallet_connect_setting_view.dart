import 'dart:io';

import 'package:fbroadcast/fbroadcast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/callback/scanner_callback.dart';
import 'package:waagmiswap/app/constant/api_constant.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/model/wallet_connect_model.dart';
import 'package:waagmiswap/app/modules/receive/views/receive_view.dart';
import 'package:waagmiswap/app/modules/scanner/views/scanner_view.dart';
import 'package:waagmiswap/app/modules/send/views/send_view.dart';
import 'package:waagmiswap/app/util/my_util.dart';

import '../controllers/wallet_connect_setting_controller.dart';

class WalletConnectSettingView extends GetView<WalletConnectSettingController> implements
    ScannerCallback {

  WalletConnectSettingController walletConnectSettingController = Get.put(WalletConnectSettingController());

  late BuildContext buildContext;

  @override
  Widget build(BuildContext context) {
    buildContext = context;
    return Scaffold(
      body: GetBuilder<WalletConnectSettingController>(
        id: "theme",
        init: WalletConnectSettingController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )),
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(height: 30),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: 3),
                      GestureDetector(
                        child: Container(
                          color: Colors.transparent,
                          width: 55,
                          height: 55,
                          child: Center(
                            child: Image.asset( themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                            "assets/icons/ic_black_back.png", width: 24,
                                height: 24, fit: BoxFit.contain),
                          ),
                        ),
                        onTap: (){
                          Get.back();
                        },
                      ),
                      Expanded(child: SizedBox(),flex: 1),
                      Text(
                        StringContent.wallet_connect.toUpperCase(),
                        style: TextStyle(
                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                            fontSize: 14,
                            fontFamily: "BarlowNormal",
                            fontWeight: FontWeight.w600,
                            letterSpacing: 2
                        ),
                      ),
                      Expanded(child: SizedBox(),flex: 1),

                      GestureDetector(
                        child: Container(
                          width: 55,
                          height: 55,
                          color: Colors.transparent,
                          child: Center(
                            child: SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_new_scan.svg"
                                : "assets/icons/ic_new_scan_black.svg", height: 20, width: 20),
                          ),
                        ),
                        onTap: (){
                          Get.to(()=> ScannerView(), arguments: {"scanner_callback" : this});
                        },
                      ),

                      SizedBox(width: 3)
                    ],
                  ),
                  SizedBox(height: 10),
                  GetBuilder<WalletConnectSettingController>(
                      id: "wallet_connect",
                      init: WalletConnectSettingController(),
                      builder: (value) => Expanded(child: RefreshIndicator(
                        onRefresh: ()=> walletConnectSettingController.loadConnectList(),
                        child: Container(
                          width: double.maxFinite,
                          height: double.maxFinite,
                          child: Stack(
                            children: [
                              Visibility(child: Center(
                                child: Text(
                                  "No connection history",
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 16,
                                    fontFamily: "BarlowMedium",
                                  ),
                                ),
                              ), visible: value.walletConnectList.isEmpty),

                              Container(
                                width: double.maxFinite,
                                height: double.maxFinite,
                                child: ListView.builder(itemBuilder: (context,index){
                                  WalletConnectModel walletConnectModel = value.walletConnectList[index];
                                  return Container(
                                    height: 130,
                                    margin: EdgeInsets.only(left: 24, right: 24),
                                    child: Material(
                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                      color: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(width: 8),

                                                CircleAvatar(
                                                  child: Image.network(walletConnectModel.walletImage,height: 40, width: 40),
                                                  radius: 18,
                                                  backgroundColor: ThemeConstant.secondTextGrayLight,
                                                ),

                                                SizedBox(width: 16),

                                                Expanded(child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Expanded(child: Text(
                                                          walletConnectModel.walletName,
                                                          maxLines: 1,
                                                          overflow: TextOverflow.ellipsis,
                                                          style: TextStyle(
                                                              color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                              fontSize: 14,
                                                              fontFamily: "BarlowNormal",
                                                              fontWeight: FontWeight.w600,
                                                              letterSpacing: 1
                                                          ),
                                                        ), flex: 1)
                                                      ],
                                                    ),
                                                    SizedBox(height: 8),
                                                    Row(
                                                      children: [
                                                        Expanded(child: Text(
                                                          walletConnectModel.walletDesc,
                                                          maxLines: 2,
                                                          overflow: TextOverflow.ellipsis,
                                                          style: TextStyle(
                                                            color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                                            fontSize: 12,
                                                            fontFamily: "BarlowNormal",
                                                            fontWeight: FontWeight.w500,
                                                          ),
                                                        ), flex: 1)
                                                      ],
                                                    ),
                                                    SizedBox(height: 8),
                                                    Row(
                                                      children: [
                                                        Expanded(child: Text(
                                                          walletConnectModel.walletDate,
                                                          maxLines: 1,
                                                          overflow: TextOverflow.ellipsis,
                                                          style: TextStyle(
                                                            color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark : ThemeConstant.secondTextGrayLight,
                                                            fontSize: 12,
                                                            fontFamily: "BarlowNormal",
                                                            fontWeight: FontWeight.w500,
                                                          ),
                                                        ), flex: 1)
                                                      ],
                                                    ),
                                                    SizedBox(height: 8),
                                                    Container(
                                                      padding: EdgeInsets.only(left: 6, right: 6, top: 3, bottom: 3),
                                                      child: Text(
                                                        "Connected with ${MyUtils.parseAddress(walletConnectModel.walletConnectAddress)}",
                                                        style: TextStyle(
                                                          color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                                          fontSize: 12,
                                                          fontFamily: "BarlowNormal",
                                                          fontWeight: FontWeight.w400,
                                                          decorationThickness: 1.5,
                                                        ),
                                                      ),
                                                      decoration: BoxDecoration(
                                                          color: Colors.transparent,
                                                          borderRadius: BorderRadius.all(Radius.circular(16)),
                                                          border: Border.all(
                                                            width: 1,
                                                            color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                                          )
                                                      ),
                                                    )
                                                  ],
                                                ), flex: 1),

                                                GestureDetector(
                                                  child: Container(
                                                    height: 35,
                                                    width: 35,
                                                    alignment: Alignment.topLeft,
                                                    color: Colors.transparent,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                      children: [
                                                        CircleAvatar(
                                                          radius: 2,
                                                          backgroundColor: Colors.white,
                                                        ),
                                                        SizedBox(width: 1),
                                                        CircleAvatar(
                                                          radius: 2,
                                                          backgroundColor: Colors.white,
                                                        ),
                                                        SizedBox(width: 1),
                                                        CircleAvatar(
                                                          radius: 2,
                                                          backgroundColor: Colors.white,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  onTap: (){
                                                    value.setDisconnectPosition(index);
                                                  },
                                                ),

                                                SizedBox(width: 8),
                                              ],
                                            ),
                                          ),

                                          Visibility(child: GestureDetector(
                                            child: Container(
                                              width: double.maxFinite,
                                              height: double.maxFinite,
                                              color: Colors.transparent,
                                              alignment: Alignment.topLeft,
                                              child: Row(
                                                children: [
                                                  Expanded(child: SizedBox(), flex: 1),
                                                  GestureDetector(
                                                    child: Container(
                                                      margin: EdgeInsets.only(top: 16),
                                                      padding: EdgeInsets.only(left: 12, right: 12, top: 3, bottom: 3),
                                                      child: Text(
                                                        "Disconnect",
                                                        style: TextStyle(
                                                          color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                                          fontSize: 12,
                                                          fontFamily: "BarlowNormal",
                                                          fontWeight: FontWeight.w400,
                                                          decorationThickness: 1.5,
                                                        ),
                                                      ),
                                                      decoration: BoxDecoration(
                                                          color: themeValue.themeType == 0 ? ThemeConstant.selectedHomeDark : ThemeConstant.white,
                                                          borderRadius: BorderRadius.all(Radius.circular(8)),
                                                          border: Border.all(
                                                            width: 1,
                                                            color: themeValue.themeType == 0 ? ThemeConstant.darkPurple : ThemeConstant.lightPurple,
                                                          )
                                                      ),
                                                    ),
                                                    onTap: (){
                                                      value.setDisconnectPosition(-1);
                                                      value.disconnect(walletConnectModel,index);
                                                    },
                                                  )
                                                ],
                                              ),
                                            ),
                                            onTap: (){
                                              value.setDisconnectPosition(-1);
                                            },
                                          ), visible: index == value.disconnectPosition)
                                        ],
                                      ),
                                    ),
                                  );
                                },
                                  itemCount: value.walletConnectList.length,
                                  physics: AlwaysScrollableScrollPhysics(),
                                  padding: EdgeInsets.zero,
                                  primary: false,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),flex: 1)),

                  Container(
                    width: double.maxFinite,
                    height: 1,
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                  ),

                  Container(
                    width: double.maxFinite,
                    height: 55,
                    decoration: BoxDecoration(
                        color: themeValue.themeType == 0 ? ThemeConstant.darkBottomColor : ThemeConstant.lightBottomColor
                    ),
                    child: Row(
                      children: [
                        Expanded(child: Container(
                          width: double.maxFinite,
                          height: double.maxFinite,
                          color: Colors.transparent,
                          child: Stack(
                            children: [
                              Row(
                                children: [
                                  SizedBox(width: 24),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      themeValue.tabPosition == 1 ? Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_balance_white.png"
                                          : "assets/icons/ic_balance_purple.png", width: 17.98, height: 17.99)
                                          : Image.asset("assets/icons/ic_balance_grey.png", width: 17.98, height: 17.99),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.balance,
                                        style: TextStyle(
                                            color: themeValue.tabPosition == 1 ? themeValue.themeType == 0 ? ThemeConstant.white :
                                            ThemeConstant.lightPurple :
                                            Color.fromRGBO(164, 164, 164, 1.0),
                                            wordSpacing: 1,
                                            fontSize: 10,
                                            fontFamily: "BarlowRegular",
                                            letterSpacing: 2
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),

                              Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  splashColor: Colors.transparent,
                                  onTap: (){
                                    Get.offAllNamed("home", arguments: {"tab_position" : 1});
                                  },
                                ),
                              )
                            ],
                          ),
                        ),flex: 1),

                        Expanded(child: Container(
                          width: double.maxFinite,
                          height: double.maxFinite,
                          color: Colors.transparent,
                          child: Stack(
                            children: [
                              Row(
                                children: [
                                  Expanded(child: SizedBox(), flex: 1),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      themeValue.tabPosition == 2 ? Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_transaction_white.png" :
                                      "assets/icons/ic_transaction_purple.png", width: 18.78, height: 18)
                                          : Image.asset("assets/icons/ic_transaction_grey.png", width: 18.78, height: 18),
                                      SizedBox(height: 10),
                                      Text(
                                        StringContent.transaction,
                                        style: TextStyle(
                                            color: themeValue.tabPosition == 2 ? themeValue.themeType == 0 ? ThemeConstant.white :
                                            ThemeConstant.lightPurple : Color.fromRGBO(164, 164, 164, 1.0),
                                            wordSpacing: 1,
                                            fontSize: 10,
                                            fontFamily: "BarlowRegular",
                                            letterSpacing: 2
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(width: 24)
                                ],
                              ),

                              Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  splashColor: Colors.transparent,
                                  onTap: (){
                                    Get.offAllNamed("home", arguments: {"tab_position" : 2});
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),flex: 1),
                      ],
                    ),
                  )
                ],
              ),

              Column(
                children: [
                  Expanded(child: SizedBox(), flex: 1),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        child: Image.asset("assets/images/img_polygon.png", width: 65, height: 65),
                        onTap: (){
                          showBottomSheet(context,themeValue);
                        },
                      )
                    ],
                  ),
                  SizedBox(height: 7)
                ],
              ),

              Visibility(child: Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.transparent,
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.3),
                        borderRadius: BorderRadius.all(Radius.circular(8))
                    ),
                    child: Center(
                      child: CircularProgressIndicator(
                        color: Color.fromRGBO(106, 48, 147, 1.0),
                      ),
                    ),
                  ),
                ),
              ), visible: walletConnectSettingController.isWalletLoading)
            ],
          ),
        ),
      ),
    );
  }

  showBottomSheet(BuildContext context, WalletConnectSettingController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 150.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 150.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    Expanded(child: SizedBox(), flex: 1),
                    Container(
                      width: double.maxFinite,
                      height: 150,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                              themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                            ]
                        ),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: 24),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                child: Container(
                                  color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Image.asset(themeValue.themeType == 0 ? "assets/images/img_send.png"
                                          : "assets/images/img_send_light.png", height: 65, width: 65),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.send,
                                        style: TextStyle(
                                            color: Color.fromRGBO(255, 197, 48, 1),
                                            fontSize: 16,
                                            fontFamily: "BarlowNormal",
                                            letterSpacing: 0.5
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  Get.back();
                                  Get.to(() => SendView(), arguments: {
                                    "user_model" : walletConnectSettingController.selectedUserModel,
                                    "ledger_model" : walletConnectSettingController.selectedLedger
                                  })?.then((value) {
                                    if(Platform.isAndroid){
                                      FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
                                    }
                                  });
                                },
                              ),
                              SizedBox(width: 100),
                              GestureDetector(
                                child: Container(
                                  color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Image.asset(themeValue.themeType == 0 ? "assets/images/img_receive.png"
                                          : "assets/images/img_receive_light.png", height: 65, width: 65),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.receive,
                                        style: TextStyle(
                                            color: Color.fromRGBO(160, 68, 255, 1.0),
                                            fontSize: 16,
                                            fontFamily: "BarlowNormal",
                                            letterSpacing: 0.5
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  Get.back();
                                  Get.to(() => ReceiveView(), arguments: {
                                    "user_model" : walletConnectSettingController.selectedUserModel,
                                    "ledger_model" : walletConnectSettingController.selectedLedger
                                  });
                                },
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }

  @override
  void onScanned(String key) {
    Map data = {};
    data["key"] = key;
    FBroadcast.instance().broadcast(MyConstant.BROADCAST, value: data);
  }
}
