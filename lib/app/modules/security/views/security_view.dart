import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/callback/passcode_callback.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/enter_passcode/views/enter_passcode_view.dart';
import 'package:waagmiswap/app/modules/receive/views/receive_view.dart';
import 'package:waagmiswap/app/modules/send/views/send_view.dart';

import '../controllers/security_controller.dart';

class SecurityView extends GetView<SecurityController> implements PasscodeCallback{

  final SecurityController _securityController = Get.put(SecurityController());
  late BuildContext buildContext;

  @override
  Widget build(BuildContext context) {
    buildContext = context;
    return Scaffold(
      body: GetBuilder<SecurityController>(
        id: "theme",
        init: SecurityController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Stack(
            children: [
              Column(children: [
                SizedBox(height: 30),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(width: 3),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: 55,
                        height: 55,
                        child: Center(
                          child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                          "assets/icons/ic_black_back.png", width: 24,
                              height: 24, fit: BoxFit.contain),
                        ),
                      ),
                      onTap: (){
                        Get.back();
                      },
                    ),
                    Expanded(child: SizedBox(),flex: 1),
                    Text(
                      StringContent.security,
                      style: TextStyle(
                          color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                          fontSize: 14,
                          fontFamily: "BarlowNormal",
                          fontWeight: FontWeight.w600,
                          letterSpacing: 2
                      ),
                    ),
                    Expanded(child: SizedBox(),flex: 1),
                    Container(
                      width: 55,
                      height: 55,
                    ),
                    SizedBox(width: 3)
                  ],
                ),
                SizedBox(height: 10),

                Container(
                  margin: EdgeInsets.only(left: 24, right: 24),
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                      color: themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                      borderRadius: BorderRadius.all(Radius.circular(8))
                  ),
                  child: Column(
                    children: [
                      Container(
                        height: 45,
                        child: Material(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          color: Colors.transparent,
                          child: Center(
                            child: Row(
                              children: [
                                SizedBox(width: 8),
                                SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_passcode.svg" :
                                "assets/icons/ic_black_passcode.svg", height: 22, width: 22),

                                SizedBox(width: 8),

                                Text(
                                  StringContent.passcode,
                                  style: TextStyle(
                                    color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                    fontSize: 14,
                                    fontFamily: "BarlowNormal",
                                    fontWeight: FontWeight.w600,
                                    decorationThickness: 1.5,
                                  ),
                                ),

                                Expanded(child: SizedBox(), flex: 1),

                                GetBuilder<SecurityController>(
                                    id: "passcode",
                                    init: SecurityController(),
                                    builder: (set_value) => GestureDetector(
                                      child: Container(
                                        color: Colors.transparent,
                                        child: Row(
                                          children: [
                                            Transform.scale(scale: 0.8, child: CupertinoSwitch(
                                                value: set_value.passcodeSwitched,
                                                activeColor: Color.fromRGBO(255, 197, 48, 1.0),
                                                trackColor: Color.fromRGBO(233, 233, 234, 1),
                                                onChanged: (value){
                                                  set_value.setPasscodeSwitch();
                                                })),
                                          ],
                                        ),
                                      ),
                                      onTap: (){
                                        set_value.setPasscodeSwitch();
                                      },
                                    )),
                                SizedBox(width: 8),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Visibility(child: Column(
                        children: [
                          SizedBox(height: 10),
                          Container(
                            height: 45,
                            child: Material(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              color: Colors.transparent,
                              child: Stack(
                                children: [
                                  Center(
                                    child: Row(
                                      children: [
                                        SizedBox(width: 8),

                                        Text(
                                          StringContent.edit_passcode,
                                          style: TextStyle(
                                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                            fontSize: 14,
                                            fontFamily: "BarlowNormal",
                                            fontWeight: FontWeight.w600,
                                            decorationThickness: 1.5,
                                          ),
                                        ),

                                        Expanded(child: SizedBox(), flex: 1),

                                        SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_right.svg"
                                            : "assets/icons/ic_black_right.svg", height: themeValue.themeType == 0 ? 22 : 16,
                                            width: themeValue.themeType == 0 ? 22 : 16),
                                        SizedBox(width: 8),
                                      ],
                                    ),
                                  ),
                                  InkWell(
                                    splashColor: Colors.transparent,
                                    onTap: (){
                                      Get.to(() => EnterPasscodeView(), arguments: {
                                        "from" : 5,
                                        "passcode_callback" : this
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ), visible: themeValue.passcodeSwitched)
                    ],
                  ),
                ),
                Visibility(child: Column(
                  children: [
                    SizedBox(height: 24),
                    Container(
                      margin: EdgeInsets.only(left: 24, right: 24),
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                          color: themeValue.themeType == 0 ? ThemeConstant.selectHomeDark : ThemeConstant.white,
                          borderRadius: BorderRadius.all(Radius.circular(8))
                      ),
                      child: Column(
                        children: [
                          Container(
                            height: 45,
                            child: Material(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              color: Colors.transparent,
                              child: Center(
                                child: Row(
                                  children: [
                                    SizedBox(width: 8),
                                    SvgPicture.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_face_id.svg" :
                                    "assets/icons/ic_black_face_id.svg", height: 22, width: 22),

                                    SizedBox(width: 8),

                                    Text(
                                      Platform.isIOS ? StringContent.face_id : StringContent.face_finger,
                                      style: TextStyle(
                                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                        fontSize: 14,
                                        fontFamily: "BarlowNormal",
                                        fontWeight: FontWeight.w600,
                                        decorationThickness: 1.5,
                                      ),
                                    ),

                                    Expanded(child: SizedBox(), flex: 1),

                                    GetBuilder<SecurityController>(
                                        id: "face_id",
                                        init: SecurityController(),
                                        builder: (set_value) => GestureDetector(
                                          child: Container(
                                            color: Colors.transparent,
                                            child: Row(
                                              children: [
                                                Transform.scale(scale: 0.8, child: CupertinoSwitch(
                                                    value: set_value.faceSwitched,
                                                    activeColor: Color.fromRGBO(255, 197, 48, 1.0),
                                                    trackColor: Color.fromRGBO(233, 233, 234, 1),
                                                    onChanged: (value){
                                                      set_value.setFaceSwitch();
                                                    })),
                                              ],
                                            ),
                                          ),
                                          onTap: (){
                                            set_value.setFaceSwitch();
                                          },
                                        )),
                                    SizedBox(width: 8),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ), visible: themeValue.passcodeSwitched),

                Expanded(child: SizedBox(), flex: 1),

                Container(
                  width: double.maxFinite,
                  height: 1,
                  color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                ),

                Container(
                  width: double.maxFinite,
                  height: 55,
                  decoration: BoxDecoration(
                      color: themeValue.themeType == 0 ? ThemeConstant.darkBottomColor : ThemeConstant.lightBottomColor
                  ),
                  child: Row(
                    children: [
                      Expanded(child: Container(
                        width: double.maxFinite,
                        height: double.maxFinite,
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Row(
                              children: [
                                SizedBox(width: 24),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    themeValue.tabPosition == 1 ? Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_balance_white.png"
                                        : "assets/icons/ic_balance_purple.png", width: 17.98, height: 17.99)
                                        : Image.asset("assets/icons/ic_balance_grey.png", width: 17.98, height: 17.99),
                                    SizedBox(height: 8),
                                    Text(
                                      StringContent.balance,
                                      style: TextStyle(
                                          color: themeValue.tabPosition == 1 ? themeValue.themeType == 0 ? ThemeConstant.white :
                                          ThemeConstant.lightPurple :
                                          Color.fromRGBO(164, 164, 164, 1.0),
                                          wordSpacing: 1,
                                          fontSize: 10,
                                          fontFamily: "BarlowRegular",
                                          letterSpacing: 2
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),

                            Material(
                              color: Colors.transparent,
                              child: InkWell(
                                splashColor: Colors.transparent,
                                onTap: (){
                                  Get.offAllNamed("home", arguments: {"tab_position" : 1});
                                },
                              ),
                            )
                          ],
                        ),
                      ),flex: 1),

                      Expanded(child: Container(
                        width: double.maxFinite,
                        height: double.maxFinite,
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Row(
                              children: [
                                Expanded(child: SizedBox(), flex: 1),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    themeValue.tabPosition == 2 ? Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_transaction_white.png" :
                                    "assets/icons/ic_transaction_purple.png", width: 18.78, height: 18)
                                        : Image.asset("assets/icons/ic_transaction_grey.png", width: 18.78, height: 18),
                                    SizedBox(height: 10),
                                    Text(
                                      StringContent.transaction,
                                      style: TextStyle(
                                          color: themeValue.tabPosition == 2 ? themeValue.themeType == 0 ? ThemeConstant.white :
                                          ThemeConstant.lightPurple : Color.fromRGBO(164, 164, 164, 1.0),
                                          wordSpacing: 1,
                                          fontSize: 10,
                                          fontFamily: "BarlowRegular",
                                          letterSpacing: 2
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(width: 24)
                              ],
                            ),

                            Material(
                              color: Colors.transparent,
                              child: InkWell(
                                splashColor: Colors.transparent,
                                onTap: (){
                                  Get.offAllNamed("home", arguments: {"tab_position" : 2});
                                },
                              ),
                            ),
                          ],
                        ),
                      ),flex: 1),
                    ],
                  ),
                )
              ]),

              Column(
                children: [
                  Expanded(child: SizedBox(), flex: 1),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        child: Image.asset("assets/images/img_polygon.png", width: 65, height: 65),
                        onTap: (){
                          showBottomSheet(context,themeValue);
                        },
                      )
                    ],
                  ),
                  SizedBox(height: 7)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  showBottomSheet(BuildContext context, SecurityController themeValue){
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 150.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 150.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    Expanded(child: SizedBox(), flex: 1),
                    Container(
                      width: double.maxFinite,
                      height: 150,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                              themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                            ]
                        ),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: 24),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                child: Container(
                                  color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Image.asset(themeValue.themeType == 0 ? "assets/images/img_send.png"
                                          : "assets/images/img_send_light.png", height: 65, width: 65),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.send,
                                        style: TextStyle(
                                            color: Color.fromRGBO(255, 197, 48, 1),
                                            fontSize: 16,
                                            fontFamily: "BarlowNormal",
                                            letterSpacing: 0.5
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  Get.back();
                                  Get.to(() => SendView(), arguments: {
                                    "user_model" : themeValue.selectedUserModel,
                                    "ledger_model" : themeValue.selectedLedger
                                  })?.then((value) {
                                    FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
                                  });
                                },
                              ),
                              SizedBox(width: 100),
                              GestureDetector(
                                child: Container(
                                  color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Image.asset(themeValue.themeType == 0 ? "assets/images/img_receive.png"
                                          : "assets/images/img_receive_light.png", height: 65, width: 65),
                                      SizedBox(height: 8),
                                      Text(
                                        StringContent.receive,
                                        style: TextStyle(
                                            color: Color.fromRGBO(160, 68, 255, 1.0),
                                            fontSize: 16,
                                            fontFamily: "BarlowNormal",
                                            letterSpacing: 0.5
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: (){
                                  Get.back();
                                  Get.to(() => ReceiveView(), arguments: {
                                    "user_model" : themeValue.selectedUserModel,
                                    "ledger_model" : themeValue.selectedLedger
                                  });
                                },
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }

  showPasscodeChanged(SecurityController themeValue, String message){
    showModalBottomSheet(
        context: buildContext,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (BuildContext context) {
          return Container(
            width: double.maxFinite,
            height: 235.5,
            child: Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 235.5,
                  decoration: BoxDecoration(
                    color: themeValue.themeType == 0 ? ThemeConstant.blueBorder : ThemeConstant.greyBorder,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(24),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 1),
                    Expanded(
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.only(left: 24, right: 24),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white,
                                themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.white
                              ]
                          ),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 24),
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset("assets/icons/ic_check_green.svg", height: 24, width: 24),
                                  SizedBox(width: 4),
                                  Text(
                                    "PASSCODE",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                      fontSize: 14,
                                      fontFamily: "BarlowSemiBold",
                                      decorationThickness: 1.5,
                                    ),
                                  )
                                ]),
                            SizedBox(height: 24),
                            Text(
                              message,
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                                fontSize: 14,
                                fontFamily: "BarlowRegular",
                                decorationThickness: 1.5,
                              ),
                            ),
                            SizedBox(height: 32),
                            GestureDetector(
                              child: CustomButton(45, double.maxFinite, BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.topRight,
                                    colors: [
                                      Color.fromRGBO(106, 48, 147, 1.0),
                                      Color.fromRGBO(160, 68, 255, 1.0)
                                    ]
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                              ), StringContent.close, TextStyle(
                                  color: Colors.white,
                                  fontFamily: "BarlowRegular",
                                  fontSize: 12,
                                  letterSpacing: 2
                              )),
                              onTap: (){
                                Get.back();
                              },
                            )
                          ],
                        ),
                      ),
                      flex: 1,
                    )
                  ],
                )
              ],
            ),
          );
        });
  }

  @override
  void onPasscodeChanged() {
    showPasscodeChanged(_securityController,"Passcode successfully changed.");
  }
}
