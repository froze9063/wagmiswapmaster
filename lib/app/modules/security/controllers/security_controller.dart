import 'package:get/get.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/callback/security_callback.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/enter_passcode/views/enter_passcode_view.dart';
import 'package:waagmiswap/app/widgets/custom_toast.dart';

class SecurityController extends GetxController implements SecurityCallback{

  final count = 0.obs;

  bool passcodeSwitched = true;
  bool faceSwitched = false;

  int themeType = 0;

  int tabPosition = 1;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late LedgerModel selectedLedger;

  @override
  void onInit() {
    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void setPasscodeSwitch(){
    if(!passcodeSwitched){
      Get.to(() => EnterPasscodeView(), arguments: {
        "security_type" : 1,
        "from_security" : true,
        "security_callback" : this
      });
    }
    else{
      Get.to(() => EnterPasscodeView(), arguments: {
        "from" : 10,
        "security_type" : 1,
        "security_callback" : this
      });
    }
  }

  void setFaceSwitch(){
    if(faceSwitched){
      Get.to(() => EnterPasscodeView(), arguments: {
        "from" : 10,
        "security_type" : 2,
        "security_callback" : this
      });
    }
    else{
      authenticate();
    }
  }

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      passcodeSwitched = prefs.getBool("passcode_status") ?? true;
      faceSwitched = prefs.getBool("face_id") ?? true;
      update(["theme","passcode"]);
    });
  }

  Future<bool> authenticateWithBiometrics() async {
    bool isAuthenticated = false;

    var androidMessage = AndroidAuthMessages(
        signInTitle: "WAGMISWAP",
        biometricHint: ""
    );

    try{
      final LocalAuthentication localAuthentication = LocalAuthentication();
      isAuthenticated = await localAuthentication.authenticate(
        localizedReason: 'Please complete the biometrics to proceed.',
        androidAuthStrings: androidMessage,
        biometricOnly: true,
      );
    }
    catch(e){
      CustomToast.showToast("No biometrics enrolled on this device");
    }

    return isAuthenticated;
  }

  void authenticate() async {
    bool isAuthenticated = await authenticateWithBiometrics();
    if(isAuthenticated){
      SharedPreferences.getInstance().then((prefs){
        faceSwitched = true;
        prefs.setBool("face_id", faceSwitched);
        update(["face_id","theme"]);
      });
    }
    else{
      faceSwitched = false;
      update(["face_id","theme"]);
    }
  }

  @override
  void onSecurityChanged(int securityType) {
   if(securityType == 1){
     SharedPreferences.getInstance().then((prefs){
       passcodeSwitched = !passcodeSwitched;
       prefs.setBool("passcode_status", passcodeSwitched);
       update(["passcode","theme"]);
     });
   }
   else{
     SharedPreferences.getInstance().then((prefs){
       faceSwitched = !faceSwitched;
       prefs.setBool("face_id", faceSwitched);
       update(["face_id","theme"]);
     });
   }
  }
}
