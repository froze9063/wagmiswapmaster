import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/algorand/algorand.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/modules/mnemonic_checklist/views/mnemonic_checklist_view.dart';

class AccountController extends GetxController {

  final count = 0.obs;
  int themeType = 0;

  late DatabaseDAO databaseDAO;

  late List<UserModel> standardModelList;

  int tabPosition = 1;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late LedgerModel selectedLedger;

  @override
  void onInit() {
    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];
    databaseDAO = DatabaseDAO();
    standardModelList = [];
    loadStandardUser();
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  void loadStandardUser(){
    databaseDAO.getAllSortedNoLimit("account_type", "standard").then((value) {
      standardModelList.clear();
      for(int i = 0; i < value.length; i++){
        standardModelList.add(value[i]);
      }
      update(["standard_account"]);
    });
  }

  Future<void> initAlgorand() async {
    AlgorandRepository algorandRepository = AlgorandRepository();
    var algorand = algorandRepository.getAlgorandConfig("https://wallet.wagmiswap.io/testnet/api/algod",
        "https://wallet.wagmiswap.io/testnet/api/idx");
    algorand.createAccount().then((value) {
      final publicAddress = value.publicAddress;
      value.seedPhrase.then((wordValue) {
        Get.to(() => MnemonicChecklistView(),
            arguments: {
              "public_address" : publicAddress,
              "mnemonic_list" : wordValue,
              "account" : value,
              "from_home" : true
            }
        );
      });
    });
  }
}
