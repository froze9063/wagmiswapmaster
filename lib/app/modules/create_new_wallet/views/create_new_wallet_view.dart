import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/custom_widgets/custom_button.dart';
import 'package:waagmiswap/app/modules/login_face/views/login_face_view.dart';

import '../controllers/create_new_wallet_controller.dart';

class CreateNewWalletView extends GetView<CreateNewWalletController> {

  CreateNewWalletController createNewWalletController = Get.put(CreateNewWalletController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color.fromRGBO(29, 28, 28, 1.0),
                  Color.fromRGBO(52, 52, 52, 1.0)
                ]
            )
        ),
        child: Column(
          children: [
            SizedBox(height: 20),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(width: 3),
                GestureDetector(
                  child: Container(
                    color: Colors.transparent,
                    width: 55,
                    height: 55,
                    child: Center(
                      child: Image.asset("assets/icons/ic_white_back.png", width: 24,
                          height: 24, fit: BoxFit.contain),
                    ),
                  ),
                  onTap: (){
                    Get.back();
                  },
                ),
                Expanded(child: SizedBox(),flex: 1),
                Image.asset("assets/images/img_hori_messe.png", width: 182,
                    height: 18, fit: BoxFit.contain),
                Expanded(child: SizedBox(),flex: 1),
                Container(
                  width: 55,
                  height: 55,
                ),
                SizedBox(width: 3)
              ],
            ),

            SizedBox(height: 30),

            Row(
              children: [
                SizedBox(width: 24),
                Text(
                  StringContent.create_passcode,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontFamily: "BarlowSemiBold",
                      letterSpacing: 2
                  ),
                )
              ],
            ),

            SizedBox(height: 24),

            Row(
              children: [
                SizedBox(width: 24),
                Text(
                  StringContent.new_passcode,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                      letterSpacing: 2
                  ),
                ),
                Expanded(child: SizedBox(),flex: 1),
                GestureDetector(
                  child: Text(
                    StringContent.show,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        fontFamily: "BarlowRegular",
                        letterSpacing: 2
                    ),
                  ),
                  onTap: (){
                    createNewWalletController.setPassword();
                  },
                ),
                SizedBox(width: 24),
              ],
            ),

            SizedBox(height: 8),

            GetBuilder<CreateNewWalletController>(
              id: "password",
              init: CreateNewWalletController(),
              builder: (value) => Container(
              margin: EdgeInsets.only(left: 24, right: 24),
              padding: EdgeInsets.only(left: 16, right: 16),
              width: double.maxFinite,
              height: 35,
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(
                      width: 1,
                      color: Color.fromRGBO(124, 124, 124, 1.0)
                  )
              ),
              child: Center(
                child: TextField(
                  obscureText: value.isPasswordShow,
                  style: TextStyle(
                      color: Colors.white
                  ),
                  decoration: InputDecoration.collapsed(hintText: "",
                      hintStyle: TextStyle(
                          color: Colors.white
                      )
                  ),
                ),
              ),
            )),

            SizedBox(height: 24),

            Row(
              children: [
                SizedBox(width: 24),
                Text(
                  StringContent.reenter_pass,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                      letterSpacing: 2
                  ),
                )
              ],
            ),

            SizedBox(height: 8),

            GetBuilder<CreateNewWalletController>(
                id: "password",
                init: CreateNewWalletController(),
                builder: (value) => Container(
                  margin: EdgeInsets.only(left: 24, right: 24),
                  padding: EdgeInsets.only(left: 16, right: 16),
                  width: double.maxFinite,
                  height: 35,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      border: Border.all(
                          width: 1,
                          color: Color.fromRGBO(124, 124, 124, 1.0)
                      )
                  ),
                  child: Center(
                    child: TextField(
                      obscureText: value.isPasswordShow,
                      style: TextStyle(
                          color: Colors.white
                      ),
                      decoration: InputDecoration.collapsed(hintText: "",
                          hintStyle: TextStyle(
                              color: Colors.white
                          )
                      ),
                    ),
                  ),
                )),

            SizedBox(height: 24),

            Row(
              children: [
                SizedBox(width: 24),
                Text(
                  StringContent.sign_with_face,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                      letterSpacing: 2
                  ),
                )
              ],
            ),

            SizedBox(height: 8),

            GetBuilder<CreateNewWalletController>(
              id: "checked",
              init: CreateNewWalletController(),
              builder: (check_value) => GestureDetector(
                child: Row(
                  children: [
                    SizedBox(width: 24),
                    CupertinoSwitch(value: check_value.isChecked, onChanged: (value){
                      check_value.setChecked();
                    })
                  ],
                ),
                onTap: (){
                  check_value.setChecked();
                },
              )),

            SizedBox(height: 24),

            Row(
              children: [
                SizedBox(width: 24),
                Container(
                    width: 16,
                    height: 16,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                        border: Border.all(
                            width: 1,
                            color: Colors.white
                        )
                    )
                ),
                SizedBox(width: 10),
                Text(
                  StringContent.tnc_agree,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                      letterSpacing: 2
                  ),
                ),
                SizedBox(width: 4),
                Text(
                  StringContent.tnc,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: "BarlowRegular",
                      letterSpacing: 2,
                      decoration: TextDecoration.underline,
                  ),
                )
              ],
            ),

            Expanded(child: SizedBox(),flex: 1),

            GestureDetector(
              child: CustomButton(45, double.maxFinite, BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(
                      width: 1,
                      color: Color.fromRGBO(124, 124, 124, 1.0)
                  )
              ), StringContent.create_new_wallet, TextStyle(
                  color: Color.fromRGBO(124, 124, 124, 1.0),
                  fontFamily: "BarlowRegular",
                  fontSize: 12,
                  letterSpacing: 2
              )),
              onTap: (){
                Get.to(() => LoginFaceView());
              },
            ),

            SizedBox(height: 55)
          ],
        ),
      ),
    );
  }
}
