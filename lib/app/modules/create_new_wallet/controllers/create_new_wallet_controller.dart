import 'package:get/get.dart';

class CreateNewWalletController extends GetxController {
  //TODO: Implement CreateNewWalletController

  final count = 0.obs;

  bool isPasswordShow = true;
  bool isChecked = false;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void setPassword(){
    this.isPasswordShow = !this.isPasswordShow;
    update(["password"]);
  }

  void setChecked(){
    this.isChecked = !this.isChecked;
    update(["checked"]);
  }
}
