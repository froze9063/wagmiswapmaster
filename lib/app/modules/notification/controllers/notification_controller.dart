import 'package:get/get.dart';
import 'package:sembast/sembast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/dao/database_dao.dart';
import 'package:waagmiswap/app/model/transaction_model.dart';
import 'package:waagmiswap/app/modules/transaction_info/views/transaction_info_view.dart';

class NotificationController extends GetxController {

  late DatabaseDAO databaseDAO;

  int themeType = 0;

  late List<Map> notificationList;

  String indexerUrl = "";
  String algodUrl = "";
  String userAddress = "";

  @override
  void onInit() {
    indexerUrl = Get.arguments["indexer_url"];
    algodUrl = Get.arguments["algod_url"];
    userAddress = Get.arguments["user_address"];
    notificationList = [];
    databaseDAO = DatabaseDAO();
    loadAllNotification();
    loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      themeType = prefs.getInt("theme") ?? 0;
      update(["theme"]);
    });
  }

  Future<void> updateNotification(TransactionModel transactionModel) async {
    transactionModel.read = true;
    final finder = Finder(filter: Filter.byKey(transactionModel.id));
    databaseDAO.updateTransactionNotification(transactionModel, finder).then((value) {
      Get.to(() => TransactionInfoView(), arguments: {
        "transaction_model" : transactionModel,
        "indexer_url" : indexerUrl,
        "algod_url" : algodUrl,
        "from_notification" : true,
        "user_address" : userAddress
      })?.then((value) {
        loadAllNotification();
      });
    });
  }

  Future<void> loadAllNotification() async {
    notificationList.clear();

    await databaseDAO.getAllTransaction().then((value){
       for(int i = 0 ; i < value.length; i++){
         TransactionModel transactionModel = value[i];
         String createdDate = transactionModel.createdAt;

         if(notificationList.isNotEmpty){
           bool isSameDateFound = false;
           int sameDateIndex = 0;

           for(int j = 0; j < notificationList.length; j++){
             Map transactionListMap = notificationList[j];
             String strDate = transactionListMap["date"];

             if(strDate == createdDate){
               isSameDateFound = true;
               sameDateIndex = j;
             }
           }

           if(isSameDateFound){
             (notificationList[sameDateIndex]["notification_list"] as List<TransactionModel>).add(transactionModel);
           }
           else{
             List<TransactionModel> resultTransactionList = [];
             resultTransactionList.add(transactionModel);

             Map transMap = Map();
             transMap["notification_list"]= resultTransactionList;
             transMap["date"] = createdDate;

             notificationList.add(transMap);
           }
         }
         else{
           List<TransactionModel> resultTransactionList = [];
           resultTransactionList.add(transactionModel);

           Map transMap = Map();
           transMap["notification_list"]= resultTransactionList;
           transMap["date"] = createdDate;

           notificationList.add(transMap);
         }
       }

       update(["notifications"]);
    });
  }
}
