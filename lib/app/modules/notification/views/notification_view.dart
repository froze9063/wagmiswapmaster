import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:waagmiswap/app/constant/string_content.dart';
import 'package:waagmiswap/app/constant/theme_constant.dart';
import 'package:waagmiswap/app/model/transaction_model.dart';

import '../controllers/notification_controller.dart';

class NotificationView extends GetView<NotificationController> {

  NotificationController _notificationController = Get.put(NotificationController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<NotificationController>(
        id: "theme",
        init: NotificationController(),
        builder: (themeValue) => Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground1 : ThemeConstant.lightThemeBackground,
                    themeValue.themeType == 0 ? ThemeConstant.darkThemeBackground2 : ThemeConstant.lightThemeBackground
                  ]
              )
          ),
          child: Column(
            children: [
              SizedBox(height: 30),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 3),
                  GestureDetector(
                    child: Container(
                      color: Colors.transparent,
                      width: 55,
                      height: 55,
                      child: Center(
                        child: Image.asset(themeValue.themeType == 0 ? "assets/icons/ic_white_back.png" :
                        "assets/icons/ic_black_back.png", width: 24,
                            height: 24, fit: BoxFit.contain),
                      ),
                    ),
                    onTap: (){
                      Get.back();
                    },
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Text(
                    StringContent.your_notification.toUpperCase(),
                    style: TextStyle(
                        color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                        fontSize: 14,
                        fontFamily: "BarlowNormal",
                        fontWeight: FontWeight.w600,
                        letterSpacing: 2
                    ),
                  ),
                  Expanded(child: SizedBox(),flex: 1),
                  Container(
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(width: 3)
                ],
              ),

              GetBuilder<NotificationController>(
                id: "notifications",
                init: NotificationController(),
                builder: (value) => Expanded(child: RefreshIndicator(
                  color: Color.fromRGBO(106, 48, 147, 1.0),
                  onRefresh: ()=> _notificationController.loadAllNotification(),
                  child: Container(
                    width: double.maxFinite,
                    height: double.maxFinite,
                    child: Stack(children: [

                      Visibility(child: Center(
                        child: Text(
                          "No Notification",
                          style: TextStyle(
                            color: themeValue.themeType == 0 ? ThemeConstant.white : ThemeConstant.blackNavy,
                            fontSize: 16,
                            fontFamily: "BarlowMedium",
                          ),
                        ),
                      ), visible: value.notificationList.isEmpty),

                      Container(
                        width: double.maxFinite,
                        height: double.maxFinite,
                        child: ListView.builder(itemBuilder: (context, index){
                          List<TransactionModel> transactionModelList = value.notificationList[(
                              value.notificationList.length - 1) - index]["notification_list"];
                          return GestureDetector(
                            child: Container(
                              child: Column(
                                children: [
                                  SizedBox(height: 24),
                                  Row(
                                    children: [
                                      SizedBox(width: 24),
                                      Text(
                                        value.notificationList[(value.notificationList.length - 1) - index]["date"],
                                        style: TextStyle(
                                            color: themeValue.themeType == 0 ? ThemeConstant.yellowDark : ThemeConstant.yellowLight,
                                            fontSize: 16,
                                            fontFamily: "BarlowNormal",
                                            fontWeight: FontWeight.w600,
                                            letterSpacing: 1
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 12),
                                  Container(
                                      margin: themeValue.themeType == 0 ? EdgeInsets.all(0): EdgeInsets.only(left: 24, right: 24),
                                      child: ListView.builder(itemBuilder: (itemContext, itemIndex){
                                        TransactionModel transactionModel = transactionModelList[itemIndex];
                                        return Material(
                                          color: Colors.transparent,
                                          child: InkWell(
                                            splashColor: Colors.transparent,
                                            child: Container(
                                              width: double.maxFinite,
                                              color: Colors.transparent,
                                              padding: themeValue.themeType == 0 ? EdgeInsets.only(top: 8, bottom: 8, left: 24, right: 24) : EdgeInsets.all(8),
                                              child: Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      Expanded(child: Text(
                                                        transactionModel.type.toUpperCase(),
                                                        style: TextStyle(
                                                            color: transactionModel.read ? themeValue.themeType == 0 ? ThemeConstant.white :
                                                            ThemeConstant.blackNavy : themeValue.themeType == 0 ? ThemeConstant.yellowDark :
                                                            ThemeConstant.yellowLight,
                                                            fontSize: 14,
                                                            fontFamily: "BarlowNormal",
                                                            fontWeight: FontWeight.w700,
                                                            letterSpacing: 1
                                                        ),
                                                      ), flex: 1),
                                                    ],
                                                  ),

                                                  SizedBox(height: 4),

                                                  Row(
                                                    children: [
                                                      Expanded(child: Text(
                                                        transactionModel.notificationMessage,
                                                        style: TextStyle(
                                                            color: themeValue.themeType == 0 ? ThemeConstant.secondTextGrayDark
                                                                : ThemeConstant.secondTextGrayLight,
                                                            fontSize: 12,
                                                            fontFamily: "BarlowNormal",
                                                            fontWeight: FontWeight.w400,
                                                            letterSpacing: 1
                                                        ),
                                                      ), flex: 1),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                            onTap: (){
                                              _notificationController.updateNotification(transactionModelList[itemIndex]);
                                            },
                                          ),
                                        );
                                      },
                                        itemCount: transactionModelList.length,
                                        shrinkWrap: true,
                                        padding: EdgeInsets.zero,
                                        primary: false,
                                        reverse: true,
                                      ),
                                      decoration: BoxDecoration(
                                          color: themeValue.themeType == 0 ? Colors.transparent : ThemeConstant.white,
                                          borderRadius: BorderRadius.all(Radius.circular(8))
                                      )
                                  ),
                                ],
                              ),
                            ),
                            onTap: (){

                            },
                          );
                        },
                          itemCount: value.notificationList.length,
                          padding: EdgeInsets.zero
                        ),
                      )
                    ]),
                  ),
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
