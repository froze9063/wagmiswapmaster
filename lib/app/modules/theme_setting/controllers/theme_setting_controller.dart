import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waagmiswap/app/model/ledger_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';

class ThemeSettingController extends GetxController {

  final count = 0.obs;
  int selectedIndex = 0;

  int themeType = 0;
  late List<Map> themeList;

  int tabPosition = 1;

  late List<UserModel> userModelList;
  late UserModel selectedUserModel;
  late LedgerModel selectedLedger;

  @override
  void onInit() {
    userModelList = Get.arguments["user_list"];
    selectedUserModel = Get.arguments["selected_user"];
    selectedLedger = Get.arguments["selected_ledger"];
    loadUser();
    themeList = [];
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void loadUser(){
    SharedPreferences.getInstance().then((prefs){
      selectedIndex = prefs.getInt("theme") ?? 0;
      themeType = prefs.getInt("theme") ?? 0;
      initExampleTheme();
      update(["theme"]);
    });
  }

  void initExampleTheme(){
    themeList.clear();

    Map map1 = Map();
    map1["name"] = "Dark";
    map1["icon"] = themeType == 0 ? "assets/icons/ic_white_dark.svg" : "assets/icons/ic_black_dark.svg";

    Map map2 = Map();
    map2["name"] = "Light";
    map2["icon"] = themeType == 0 ? "assets/icons/ic_white_light.svg" : "assets/icons/ic_black_light.svg";

    themeList.add(map1);
    themeList.add(map2);
  }

  void setSelected(int index){
    this.selectedIndex = index;
    SharedPreferences.getInstance().then((prefs){
      prefs.setInt("theme", index);
      loadUser();
    });
    update(["theme"]);
  }
}
