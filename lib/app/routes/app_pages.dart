import 'package:get/get.dart';

import '../modules/about_us/bindings/about_us_binding.dart';
import '../modules/about_us/views/about_us_view.dart';
import '../modules/account/bindings/account_binding.dart';
import '../modules/account/views/account_view.dart';
import '../modules/account_created/bindings/account_created_binding.dart';
import '../modules/account_created/views/account_created_view.dart';
import '../modules/add_asset/bindings/add_asset_binding.dart';
import '../modules/add_asset/views/add_asset_view.dart';
import '../modules/create_new_wallet/bindings/create_new_wallet_binding.dart';
import '../modules/create_new_wallet/views/create_new_wallet_view.dart';
import '../modules/currency_setting/bindings/currency_setting_binding.dart';
import '../modules/currency_setting/views/currency_setting_view.dart';
import '../modules/enter_passcode/bindings/enter_passcode_binding.dart';
import '../modules/enter_passcode/views/enter_passcode_view.dart';
import '../modules/faceid/bindings/faceid_binding.dart';
import '../modules/faceid/views/faceid_view.dart';
import '../modules/faq/bindings/faq_binding.dart';
import '../modules/faq/views/faq_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/import_account/bindings/import_account_binding.dart';
import '../modules/import_account/views/import_account_view.dart';
import '../modules/import_wallet/bindings/import_wallet_binding.dart';
import '../modules/import_wallet/views/import_wallet_view.dart';
import '../modules/language_setting/bindings/language_setting_binding.dart';
import '../modules/language_setting/views/language_setting_view.dart';
import '../modules/login_face/bindings/login_face_binding.dart';
import '../modules/login_face/views/login_face_view.dart';
import '../modules/mnemonic/bindings/mnemonic_binding.dart';
import '../modules/mnemonic/views/mnemonic_view.dart';
import '../modules/mnemonic_checklist/bindings/mnemonic_checklist_binding.dart';
import '../modules/mnemonic_checklist/views/mnemonic_checklist_view.dart';
import '../modules/mnemonic_reveal/bindings/mnemonic_reveal_binding.dart';
import '../modules/mnemonic_reveal/views/mnemonic_reveal_view.dart';
import '../modules/mnemonic_verify/bindings/mnemonic_verify_binding.dart';
import '../modules/mnemonic_verify/views/mnemonic_verify_view.dart';
import '../modules/notification/bindings/notification_binding.dart';
import '../modules/notification/views/notification_view.dart';
import '../modules/notification_setting/bindings/notification_setting_binding.dart';
import '../modules/notification_setting/views/notification_setting_view.dart';
import '../modules/receive/bindings/receive_binding.dart';
import '../modules/receive/views/receive_view.dart';
import '../modules/scanner/bindings/scanner_binding.dart';
import '../modules/scanner/views/scanner_view.dart';
import '../modules/secret_phase/bindings/secret_phase_binding.dart';
import '../modules/secret_phase/views/secret_phase_view.dart';
import '../modules/security/bindings/security_binding.dart';
import '../modules/security/views/security_view.dart';
import '../modules/select_asset/bindings/select_asset_binding.dart';
import '../modules/select_asset/views/select_asset_view.dart';
import '../modules/select_option/bindings/select_option_binding.dart';
import '../modules/select_option/views/select_option_view.dart';
import '../modules/send/bindings/send_binding.dart';
import '../modules/send/views/send_view.dart';
import '../modules/send_confirm/bindings/send_confirm_binding.dart';
import '../modules/send_confirm/views/send_confirm_view.dart';
import '../modules/setting/bindings/setting_binding.dart';
import '../modules/setting/views/setting_view.dart';
import '../modules/setup_password/bindings/setup_password_binding.dart';
import '../modules/setup_password/views/setup_password_view.dart';
import '../modules/standart_account_detail/bindings/standart_account_detail_binding.dart';
import '../modules/standart_account_detail/views/standart_account_detail_view.dart';
import '../modules/theme_setting/bindings/theme_setting_binding.dart';
import '../modules/theme_setting/views/theme_setting_view.dart';
import '../modules/transaction_info/bindings/transaction_info_binding.dart';
import '../modules/transaction_info/views/transaction_info_view.dart';
import '../modules/unlink_confirmation/bindings/unlink_confirmation_binding.dart';
import '../modules/unlink_confirmation/views/unlink_confirmation_view.dart';
import '../modules/wallet_connect_setting/bindings/wallet_connect_setting_binding.dart';
import '../modules/wallet_connect_setting/views/wallet_connect_setting_view.dart';
import '../modules/welcome/bindings/welcome_binding.dart';
import '../modules/welcome/views/welcome_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.WELCOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.WELCOME,
      page: () => WelcomeView(),
      binding: WelcomeBinding(),
    ),
    GetPage(
      name: _Paths.SELECT_OPTION,
      page: () => SelectOptionView(),
      binding: SelectOptionBinding(),
    ),
    GetPage(
      name: _Paths.IMPORT_WALLET,
      page: () => ImportWalletView(),
      binding: ImportWalletBinding(),
    ),
    GetPage(
      name: _Paths.CREATE_NEW_WALLET,
      page: () => CreateNewWalletView(),
      binding: CreateNewWalletBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN_FACE,
      page: () => LoginFaceView(),
      binding: LoginFaceBinding(),
    ),
    GetPage(
      name: _Paths.SECRET_PHASE,
      page: () => SecretPhaseView(),
      binding: SecretPhaseBinding(),
    ),
    GetPage(
      name: _Paths.SETUP_PASSWORD,
      page: () => SetupPasswordView(),
      binding: SetupPasswordBinding(),
    ),
    GetPage(
      name: _Paths.ENTER_PASSCODE,
      page: () => EnterPasscodeView(),
      binding: EnterPasscodeBinding(),
    ),
    GetPage(
      name: _Paths.FACEID,
      page: () => FaceidView(),
      binding: FaceidBinding(),
    ),
    GetPage(
      name: _Paths.MNEMONIC_CHECKLIST,
      page: () => MnemonicChecklistView(),
      binding: MnemonicChecklistBinding(),
    ),
    GetPage(
      name: _Paths.MNEMONIC,
      page: () => MnemonicView(),
      binding: MnemonicBinding(),
    ),
    GetPage(
      name: _Paths.MNEMONIC_VERIFY,
      page: () => MnemonicVerifyView(),
      binding: MnemonicVerifyBinding(),
    ),
    GetPage(
      name: _Paths.ACCOUNT_CREATED,
      page: () => AccountCreatedView(),
      binding: AccountCreatedBinding(),
    ),
    GetPage(
      name: _Paths.IMPORT_ACCOUNT,
      page: () => ImportAccountView(),
      binding: ImportAccountBinding(),
    ),
    GetPage(
      name: _Paths.ADD_ASSET,
      page: () => AddAssetView(),
      binding: AddAssetBinding(),
    ),
    GetPage(
      name: _Paths.SEND,
      page: () => SendView(),
      binding: SendBinding(),
    ),
    GetPage(
      name: _Paths.RECEIVE,
      page: () => ReceiveView(),
      binding: ReceiveBinding(),
    ),
    GetPage(
      name: _Paths.SELECT_ASSET,
      page: () => SelectAssetView(),
      binding: SelectAssetBinding(),
    ),
    GetPage(
      name: _Paths.TRANSACTION_INFO,
      page: () => TransactionInfoView(),
      binding: TransactionInfoBinding(),
    ),
    GetPage(
      name: _Paths.SEND_CONFIRM,
      page: () => SendConfirmView(),
      binding: SendConfirmBinding(),
    ),
    GetPage(
      name: _Paths.NOTIFICATION,
      page: () => NotificationView(),
      binding: NotificationBinding(),
    ),
    GetPage(
      name: _Paths.SETTING,
      page: () => SettingView(),
      binding: SettingBinding(),
    ),
    GetPage(
      name: _Paths.ACCOUNT,
      page: () => AccountView(),
      binding: AccountBinding(),
    ),
    GetPage(
      name: _Paths.UNLINK_CONFIRMATION,
      page: () => UnlinkConfirmationView(),
      binding: UnlinkConfirmationBinding(),
    ),
    GetPage(
      name: _Paths.STANDART_ACCOUNT_DETAIL,
      page: () => StandartAccountDetailView(),
      binding: StandartAccountDetailBinding(),
    ),
    GetPage(
      name: _Paths.MNEMONIC_REVEAL,
      page: () => MnemonicRevealView(),
      binding: MnemonicRevealBinding(),
    ),
    GetPage(
      name: _Paths.SECURITY,
      page: () => SecurityView(),
      binding: SecurityBinding(),
    ),
    GetPage(
      name: _Paths.NOTIFICATION_SETTING,
      page: () => NotificationSettingView(),
      binding: NotificationSettingBinding(),
    ),
    GetPage(
      name: _Paths.CURRENCY_SETTING,
      page: () => CurrencySettingView(),
      binding: CurrencySettingBinding(),
    ),
    GetPage(
      name: _Paths.THEME_SETTING,
      page: () => ThemeSettingView(),
      binding: ThemeSettingBinding(),
    ),
    GetPage(
      name: _Paths.LANGUAGE_SETTING,
      page: () => LanguageSettingView(),
      binding: LanguageSettingBinding(),
    ),
    GetPage(
      name: _Paths.ABOUT_US,
      page: () => AboutUsView(),
      binding: AboutUsBinding(),
    ),
    GetPage(
      name: _Paths.FAQ,
      page: () => FaqView(),
      binding: FaqBinding(),
    ),
    GetPage(
      name: _Paths.SCANNER,
      page: () => ScannerView(),
      binding: ScannerBinding(),
    ),
    GetPage(
      name: _Paths.WALLET_CONNECT_SETTING,
      page: () => WalletConnectSettingView(),
      binding: WalletConnectSettingBinding(),
    )
  ];
}
