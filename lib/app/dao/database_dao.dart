import 'package:sembast/sembast.dart';
import 'package:waagmiswap/app/model/asset_detail_model.dart';
import 'package:waagmiswap/app/model/asset_pricing.dart';
import 'package:waagmiswap/app/model/assets_model.dart';
import 'package:waagmiswap/app/model/transaction_model.dart';
import 'package:waagmiswap/app/model/user_model.dart';
import 'package:waagmiswap/app/model/wallet_connect_model.dart';
import 'package:waagmiswap/app/repository/database_repository.dart';

class DatabaseDAO {
  static const String USER_STORE_NAME = 'user';
  static const String USER_SUB_ACCOUNT = 'user_sub_Account';
  static const String ASSET_STORE_NAME = 'asset';
  static const String ASSET_DISABLE_NAME = 'asset';
  static const String ASSET_DETAIL_STORE_NAME = 'asset_detail';
  static const String TRANSACTION = 'transaction';
  static const String WALLET_CONNECT = 'wallet_connect';
  static const String ASSET_PRICING = 'asset_pricing';
  static const String CURATED_ASSET = 'curated_asset';

  final userStore = intMapStoreFactory.store(USER_STORE_NAME);
  final userSubAccount = intMapStoreFactory.store(USER_SUB_ACCOUNT);
  final assetStore = intMapStoreFactory.store(ASSET_STORE_NAME);
  final assetDetailStore = intMapStoreFactory.store(ASSET_DETAIL_STORE_NAME);
  final transaction = intMapStoreFactory.store(TRANSACTION);
  final walletConnectStore = intMapStoreFactory.store(WALLET_CONNECT);
  final assetPricingStore = intMapStoreFactory.store(ASSET_PRICING);
  final curatedAssetStore = intMapStoreFactory.store(CURATED_ASSET);

  Future<Database> get _db async => await AppDatabase.instance.database;

  Future insertCuratedAsset(AssetsDetailModel curatedAssetModel) async {
    await curatedAssetStore.add(await _db, curatedAssetModel.toMap());
  }

  Future insert(UserModel user) async {
    await userStore.add(await _db, user.toMap());
  }

  Future insertSubAccount(UserModel user) async {
    await userSubAccount.add(await _db, user.toMap());
  }

  Future insertAsset(AssetsModel assetsModel) async {
    await assetStore.add(await _db, assetsModel.toMap());
  }

  Future insertAssetDetail(AssetsDetailModel assetsDetailModel) async {
    await assetDetailStore.add(await _db, assetsDetailModel.toMap());
  }

  Future insertTransaction(TransactionModel transactionModel) async {
    await transaction.add(await _db, transactionModel.toMap());
  }

  Future insertWalletConnect(WalletConnectModel walletConnectModel) async {
    await walletConnectStore.add(await _db, walletConnectModel.toMap());
  }

  Future insertAssetPricing(AssetsPricing assetPricing) async {
    await assetPricingStore.add(await _db, assetPricing.toMap());
  }

  Future<List<UserModel>> getAllSorted(String key, String value) async {
    final finder = Finder(filter: Filter.equals(key, value), limit: 1);

    final recordSnapshots = await userStore.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final user = UserModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<UserModel>> getAllSortedWithFinder(Finder finder) async {
    final recordSnapshots = await userStore.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final user = UserModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<UserModel>> getAllSortedNoLimit(String key, String value) async {
    final finder = Finder(filter: Filter.equals(key, value));

    final recordSnapshots = await userStore.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final user = UserModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<UserModel>> getUserSubSorted(Finder finder) async {
    final recordSnapshots = await userSubAccount.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final user = UserModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<AssetsModel>> getAllAssetBySorted(Finder filterFinder) async {
    final finder = filterFinder;

    final recordSnapshots = await assetStore.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final asset = AssetsModel.fromMap(snapshot.value);
      asset.id = snapshot.key;
      return asset;
    }).toList();
  }

  Future<List<AssetsDetailModel>> getAllAssetDetailSorted(Finder assetFinder) async {
    final finder = assetFinder;

    final recordSnapshots = await assetDetailStore.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final asset = AssetsDetailModel.fromDatabaseMap(snapshot.value);
      asset.id = snapshot.key;
      return asset;
    }).toList();
  }

  Future<List<UserModel>> getAll() async {
    final recordSnapshots = await userStore.find(
      await _db
    );

    return recordSnapshots.map((snapshot) {
      final user = UserModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<AssetsDetailModel>> getAllCurated() async {
    final recordSnapshots = await curatedAssetStore.find(
        await _db
    );

    return recordSnapshots.map((snapshot) {
      final asset = AssetsDetailModel.fromDatabaseMap(snapshot.value);
      asset.id = snapshot.key;
      return asset;
    }).toList();
  }

  Future<List<AssetsDetailModel>> getAllCuratedFinder(Finder assetFinder) async {
    final finder = assetFinder;

    final recordSnapshots = await curatedAssetStore.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final asset = AssetsDetailModel.fromDatabaseMap(snapshot.value);
      asset.id = snapshot.key;
      return asset;
    }).toList();
  }

  Future<List<TransactionModel>> getAllTransaction() async {
    final recordSnapshots = await transaction.find(
        await _db
    );

    return recordSnapshots.map((snapshot) {
      final user = TransactionModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<TransactionModel>> getAllTransactionSorted(String key, var value) async {
    final finder = Finder(filter: Filter.equals(key, value), limit: 1);

    final recordSnapshots = await transaction.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final user = TransactionModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<WalletConnectModel>> getAllWalletConnectFilter(Finder assetFinder) async {
    final recordSnapshots = await walletConnectStore.find(
        await _db,
        finder: assetFinder
    );

    return recordSnapshots.map((snapshot) {
      final user = WalletConnectModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<WalletConnectModel>> getAllWalletConnects() async {
    final recordSnapshots = await walletConnectStore.find(
        await _db
    );

    return recordSnapshots.map((snapshot) {
      final user = WalletConnectModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<WalletConnectModel>> getAllWalletConnect() async {
    final recordSnapshots = await walletConnectStore.find(
        await _db
    );

    return recordSnapshots.map((snapshot) {
      final user = WalletConnectModel.fromMap(snapshot.value);
      user.id = snapshot.key;
      return user;
    }).toList();
  }

  Future<List<AssetsPricing>> getAssetPricingWithFinder(Finder finder) async {
    final recordSnapshots = await assetPricingStore.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final assetPricing = AssetsPricing.fromMap(snapshot.value);
      assetPricing.id = snapshot.key;
      return assetPricing;
    }).toList();
  }

  Future update(UserModel user) async {
    final finder = Finder(filter: Filter.byKey(user.id));
    await userStore.update(
      await _db,
      user.toMap(),
      finder: finder,
    );
  }

  Future updateSubAccount(UserModel user) async {
    final finder = Finder(filter: Filter.byKey(user.id));
    await userSubAccount.update(
      await _db,
      user.toMap(),
      finder: finder,
    );
  }

  Future updateAsset(AssetsModel assetsModel,Finder assetFinder) async {
    final finder = assetFinder;
    await assetStore.update(
      await _db,
      assetsModel.toMap(),
      finder: finder,
    );
  }

  Future updateAssetDetail(AssetsDetailModel assetsDetailModel,Finder assetFinder) async {
    final finder = assetFinder;
    await assetDetailStore.update(
      await _db,
      assetsDetailModel.toMap(),
      finder: finder,
    );
  }

  Future updateTransactionNotification(TransactionModel transactionModel,Finder assetFinder) async {
    final finder = assetFinder;
    await transaction.update(
      await _db,
      transactionModel.toMap(),
      finder: finder,
    );
  }

  Future updateAssetPricing(AssetsPricing assetsPricing,Finder assetFinder) async {
    final finder = assetFinder;
    await assetPricingStore.update(
      await _db,
      assetsPricing.toMap(),
      finder: finder,
    );
  }

  Future updateWalletConnects(WalletConnectModel walletConnectModel) async {
    final finder = Finder(filter: Filter.byKey(walletConnectModel.id));
    await walletConnectStore.update(
      await _db,
      walletConnectModel.toMap(),
      finder: finder,
    );
  }

  Future updateWalletConnect(WalletConnectModel walletConnectModel,Finder assetFinder) async {
    final finder = assetFinder;
    await walletConnectStore.update(
      await _db,
      walletConnectModel.toMap(),
      finder: finder,
    );
  }

  Future updateCuratedAsset(AssetsDetailModel assetsDetailModel) async {
    final finder = Finder(filter: Filter.byKey(assetsDetailModel.id));
    await curatedAssetStore.update(
      await _db,
      assetsDetailModel.toMap(),
      finder: finder,
    );
  }

  Future delete(UserModel user) async {
    final finder = Finder(filter: Filter.byKey(user.id));
    await userStore.delete(
      await _db,
      finder: finder,
    );
  }

  Future deleteAssetById(AssetsModel assetsModel) async {
    final finder = Finder(filter: Filter.byKey(assetsModel.id));
    await assetStore.delete(
      await _db,
      finder: finder,
    );
  }

  Future deleteUserAsset(Finder assetFinder) async {
    final finder = assetFinder;
    await assetStore.delete(
      await _db,
      finder: finder,
    );
  }

  Future deleteSubAccounts(String masterAddress) async {
    var filter = Filter.equals('master_address', masterAddress);
    var finder = Finder(filter: filter);
    await userSubAccount.delete(await _db, finder: finder);
  }

  Future deleteWalletConnection(WalletConnectModel walletConnectModel) async {
    final finder = Finder(filter: Filter.byKey(walletConnectModel.id));
    await walletConnectStore.delete(
      await _db,
      finder: finder,
    );
  }
}