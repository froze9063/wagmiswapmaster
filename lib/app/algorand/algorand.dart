import 'package:algorand_dart/algorand_dart.dart';

class AlgorandRepository {
  Algorand getAlgorandConfig(String algodUrl, String idxUrl){
    var algodClient = AlgodClient(
      apiUrl: algodUrl,
    );
    var indexerClient = IndexerClient(
      apiUrl: idxUrl,
    );
    return Algorand(
      algodClient: algodClient,
      indexerClient: indexerClient,
    );
  }
}

