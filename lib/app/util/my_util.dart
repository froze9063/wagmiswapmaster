import 'dart:math';
import 'package:algorand_dart/algorand_dart.dart';
import 'package:intl/intl.dart';

class MyUtils{
  static String parseTime(String date, String strDateFormat, String strParseFormat,
      bool addingOneDay){
    String strDate = "";

    if(addingOneDay){
      DateFormat dateFormat = DateFormat(strDateFormat);
      DateFormat parseFormat = DateFormat(strParseFormat);
      DateTime dateTime = dateFormat.parse(date);
      var newDate = dateTime.add(Duration(days: 1));
      strDate = parseFormat.format(newDate);
      return strDate;
    }
    else{
      DateFormat dateFormat = DateFormat(strDateFormat);
      DateFormat parseFormat = DateFormat(strParseFormat);
      DateTime dateTime = dateFormat.parse(date);
      strDate = parseFormat.format(dateTime);
      return strDate;
    }
  }

  static String parseSingleDate(DateTime dateTime, String datePattern){
    String strDate = "";
    DateFormat dateFormat = DateFormat(datePattern);
    strDate = dateFormat.format(dateTime);
    return strDate;
  }

  static String parseAddress(String address){
    List<String> addressList = address.split("");
    String strAddress = "";
    if(addressList.length >= 20){
      strAddress = "$strAddress${addressList[0]}$strAddress${addressList[1]}$strAddress${
          addressList[2]}$strAddress${addressList[3]}$strAddress${addressList[4]}$strAddress${addressList[5]}"
          "$strAddress${addressList[6]}$strAddress${addressList[7]}.....$strAddress${addressList[addressList.length - 8]}$strAddress${addressList[addressList.length - 7]}$strAddress${
          addressList[addressList.length - 6]}$strAddress${addressList[addressList.length - 5]}$strAddress${addressList[addressList.length - 4]}$strAddress${addressList[addressList.length - 3]}"
          "$strAddress${addressList[addressList.length - 2]}$strAddress${addressList[addressList.length - 1]}";
      return strAddress;
    }
    else{
      return address;
    }
  }

  static String countAssetAmount(int amount, int decimal) {
    final currencyFormatter = NumberFormat('#,##0.00', 'en_US');

    var assetAmount = amount / pow(10, decimal);

    if(currencyFormatter.format(assetAmount) == "0.00"){
      return "0";
    }
    else{
      return currencyFormatter.format(assetAmount);
    }
  }

  static double countAssetPrice(String price, double amount, int decimal, int type) {
    if(price == ""){
      return 0;
    }
    else{
      double assetAmount = amount / pow(10, decimal);
      double dblPrice = double.parse(price);
      double totalPrice = 0;

      if(type == 1){
        totalPrice = assetAmount * dblPrice;
      }
      else{
        totalPrice = amount * dblPrice;
      }

      if(totalPrice == 0.0){
        return 0;
      }
      else{
        return totalPrice;
      }
    }
  }

  static String parseAlgoToMicro(int amount){
    if(amount == 0){
      return "0";
    }
    final currencyFormatter = NumberFormat('#,##0.00', 'en_US');
    return currencyFormatter.format(Algo.fromMicroAlgos(amount));
  }

  static double parseAlgoPrice(double amount, double price){
    if(amount == 0){
      return 0;
    }

    double totalPrice = price * amount;
    return roundDouble(totalPrice,2);
  }

  static String formatPrice(double price){
    final currencyFormatter = NumberFormat('#,##0.00', 'en_US');
    return currencyFormatter.format(price);
  }

  static double setMicroAlgo(int amount){
    double result = amount / 1000;
    return result;
  }

  static double setAlgoPrice(double amount, double price){
    double totalPrice = price * amount;
    return totalPrice;
  }

  static String setTotalPrice(double price){
    final currencyFormatter = NumberFormat('#,##0', 'en_US');
    String strPrice = currencyFormatter.format(price.round());
    String strSubString = strPrice.substring(0, strPrice.length - 1);

    return strSubString.replaceFirst(",", ".", strSubString.length - 3);
  }

  static double setAssetPrice(String price, double amount, int decimal) {
    if(price == ""){
      return 0;
    }
    else{
      double assetAmount = amount / pow(10, decimal);
      double dblPrice = double.parse(price);
      double totalPrice = ((assetAmount * dblPrice) * amount);
      return totalPrice;
    }
  }

  static double roundDouble(double value, int places){
    double mod = pow(10.0, places).toDouble();
    return ((value * mod).round().toDouble() / mod);
  }

  static DateTime parseDateTime(String date, String pattern){
    DateFormat dateFormat = DateFormat(pattern);
    return dateFormat.parse(date);
  }

  static double getPercentagePrice(double lowestPrice, double highestPrice){
    double percentage = ((highestPrice / lowestPrice) * 100) - 100;
    return percentage;
  }

  static String parseMnemonic(List<String> mnemonicList){
    String value = mnemonicList.toString();
    String stra = value.replaceAll("[", "");
    String strb = stra.replaceAll("]", "");
    return strb.replaceAll(",", "");
  }

  static String getCuratedIconPath(String unitName, int theme){
    if(unitName == "ALGO"){
      if(theme == 0){
        return "assets/icons/ic_dark_algo.svg";
      }
      else{
        return "assets/icons/ic_light_algo.svg";
      }
    }
    else if(unitName == "ARCC"){
      if(theme == 0){
        return "assets/icons/ic_dark_arcc.svg";
      }
      else{
        return "assets/icons/ic_light_arcc.svg";
      }
    }
    else if(unitName == "USDC"){
      if(theme == 0){
        return "assets/icons/ic_dark_usdc.svg";
      }
      else{
        return "assets/icons/ic_light_usdc.svg";
      }
    }
    else if(unitName == "USDt"){
      if(theme == 0){
        return "assets/icons/ic_dark_usdt.svg";
      }
      else{
        return "assets/icons/ic_light_usdt.svg";
      }
    }
    else if(unitName == "Planets"){
      if(theme == 0){
        return "assets/icons/ic_dark_planet.svg";
      }
      else{
        return "assets/icons/ic_light_planet.svg";
      }
    }
    else if(unitName == "YLDY"){
      if(theme == 0){
        return "assets/icons/ic_dark_yldy.svg";
      }
      else{
        return "assets/icons/ic_light_yldy.svg";
      }
    }
    else if(unitName == "OPUL"){
      if(theme == 0){
        return "assets/icons/ic_dark_opul.svg";
      }
      else{
        return "assets/icons/ic_light_opul.svg";
      }
    }
    else{
      return "";
    }
  }

  static num doubleWithoutDecimalToInt(double val) {
    return val % 1 == 0 ? val.toInt() : val;
  }
}