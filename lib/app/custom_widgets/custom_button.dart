import 'package:flutter/cupertino.dart';

class CustomButton extends StatelessWidget {

  late double height;
  late double width;
  late BoxDecoration decoration;
  late String buttonText;
  late TextStyle textStyle;

  CustomButton(this.height, this.width, this.decoration, this.buttonText, this.textStyle);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      width: this.width,
      decoration: this.decoration,
      child: Center(
        child: Text(
          this.buttonText,
          style: this.textStyle,
        ),
      ),
    );
  }
}