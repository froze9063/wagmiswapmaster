class UserModel {

  int id = 0;

  String userName;
  String address;
  String passcode;
  String mnemonic;
  String url;

  int amount;
  int amountWithoutPendingRewards;
  int pendingRewards;
  int rewardBase;
  int rewards;
  int round;

  String status;
  String price;

  String masterAddress;
  String accountType;

  bool isActiveAccount;
  bool backedUp;

  UserModel({required this.userName, required this.address, required this.passcode,
    required this.mnemonic, required this.url, required this.amount, required this.amountWithoutPendingRewards
    , required this.pendingRewards, required this.rewardBase, required this.rewards, required this.round
    , required this.status, required this.price, required this.masterAddress, required this.accountType,
    required this.isActiveAccount,required this.backedUp
  });

  Map<String, dynamic> toMap() {
    return {
      'user_name': userName,
      'address': address,
      'passcode': passcode,
      'mnemonic': mnemonic,
      'url': url,
      'amount': amount,
      'amount-without-pending-rewards': amountWithoutPendingRewards,
      'pending-rewards': pendingRewards,
      'reward-base': rewardBase,
      'rewards': rewards,
      'round': round,
      'status': status,
      'price': price,
      'master_address': masterAddress,
      'account_type': accountType,
      'is_active_Account': isActiveAccount,
      'backed_up': backedUp,
    };
  }

  static UserModel fromMap(Map<String, dynamic> map) {
    return UserModel(
      userName: map['user_name'] ?? "",
      address: map['address'] ?? "",
      passcode: map['passcode'] ?? "",
      mnemonic: map['mnemonic'] ?? "",
      url: map['url'] ?? "",
      amount: map['amount'] ?? 0,
      amountWithoutPendingRewards: map['amount-without-pending-rewards'] ?? 0,
      pendingRewards: map['pending-rewards'] ?? 0,
      rewardBase: map['reward-base'] ?? 0,
      rewards: map['rewards'] ?? 0,
      round: map['round'] ?? 0,
      status: map['status'] ?? "",
      price: map['price'] ?? "",
      masterAddress: map['master_address'] ?? "",
      accountType: map['account_type'] ?? "",
      isActiveAccount: map['is_active_Account'] ?? false,
      backedUp: map['backed_up'] ?? false
    );
  }
}