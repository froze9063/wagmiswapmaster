class AssetsDetailModel {
  int id = 0;
  int ledgerPosition;
  dynamic createdAtRound;
  int index;
  String clawback;
  String creator;
  int decimals;
  String freeze;
  String manager;
  String name;
  String nameB64;
  String reserve;
  dynamic total;
  String unitName;
  String unitNameB64;
  String url;
  String urlB64;
  int currentRound;
  int circulatingSupply;

  AssetsDetailModel({required this.ledgerPosition,required this.createdAtRound, required this.index, required this.clawback,
    required this.creator, required this.decimals, required this.freeze, required this.manager
    , required this.name, required this.nameB64, required this.reserve, required this.total
    , required this.unitName, required this.unitNameB64, required this.url, required this.urlB64
    , required this.currentRound, required this.circulatingSupply});

  Map<String, dynamic> toMap() {
    return {
      'ledger-position': ledgerPosition,
      'created-at-round': createdAtRound,
      'index': index,
      'clawback': clawback,
      'creator': creator,
      'decimals': decimals,
      'freeze': freeze,
      'manager': manager,
      'name': name,
      'name-b64': nameB64,
      'reserve': reserve,
      'total': total,
      'unit-name': unitName,
      'unit-name-b64': unitNameB64,
      'url': url,
      'url-b64': urlB64,
      'current_round': currentRound,
      'circulating-supply': circulatingSupply
    };
  }

  static AssetsDetailModel fromMap(Map<String, dynamic> map, int ledgerPosition) {
    return AssetsDetailModel(
        ledgerPosition: ledgerPosition,
        createdAtRound: map["asset"]['created-at-round'] ?? 0,
        index: map["asset"]['index'] ?? 0,
        clawback: map["asset"]["params"]['clawback'] ?? "",
        creator: map["asset"]["params"]['creator'] ?? "",
        decimals: map["asset"]["params"]['decimals'] ?? 0,
        freeze: map["asset"]["params"]['freeze'] ?? "",
        manager: map["asset"]["params"]['manager'] ?? "",
        name: map["asset"]["params"]['name'] ?? "",
        nameB64: map["asset"]["params"]['name-b64'] ?? "",
        reserve: map["asset"]["params"]['reserve'] ?? "",
        total: map["asset"]["params"]['total'] ?? 0,
        unitName: map["asset"]["params"]['unit-name'] ?? "",
        unitNameB64: map["asset"]["params"]['unit-name-b64'] ?? "",
        url: map["asset"]["params"]['url'] ?? "",
        urlB64: map["asset"]["params"]['url-b64'] ?? "",
        currentRound: map['current-round'] ?? 0,
        circulatingSupply: map['circulating-supply'] ?? 0,
    );
  }

  static AssetsDetailModel fromAssetMap(Map<String, dynamic> map) {
    return AssetsDetailModel(
        ledgerPosition: 0,
        createdAtRound: map["assets"]['created-at-round'],
        index: map['index'] ?? 0,
        clawback: map["assets"]["params"]['clawback'] ?? "",
        creator: map["assets"]["params"]['creator'] ?? "",
        decimals: map["assets"]["params"]['decimals'] ?? 0,
        freeze: map["assets"]["params"]['freeze'] ?? "",
        manager: map["assets"]["params"]['manager'] ?? "",
        name: map["assets"]["params"]['name'] ?? "",
        nameB64: map["assets"]["params"]['name-b64'] ?? "",
        reserve: map["assets"]["params"]['reserve'] ?? "",
        total: map["assets"]["params"]['total'] ?? 0,
        unitName: map["assets"]["params"]['unit-name'] ?? "",
        unitNameB64: map["assets"]["params"]['unit-name-b64'] ?? "",
        url: map["assets"]["params"]['url'] ?? "",
        urlB64: map["assets"]["params"]['url-b64'] ?? "",
        currentRound: 0,
        circulatingSupply: map['circulating-supply'] ?? 0,
    );
  }

  static AssetsDetailModel fromDatabaseMap(Map<String, dynamic> map) {
    return AssetsDetailModel(
        ledgerPosition: map['ledger-position'] ?? 0,
        createdAtRound: map['created-at-round'],
        index: map['index'] ?? 0,
        clawback: map['clawback'] ?? "",
        creator: map['creator'] ?? "",
        decimals: map['decimals'] ?? 0,
        freeze: map['freeze'] ?? "",
        manager: map['manager'] ?? "",
        name: map['name'] ?? "",
        nameB64: map['name-b64'] ?? "",
        reserve: map['reserve'] ?? "",
        total: map['total'] ?? 0,
        unitName: map['unit-name'] ?? "",
        unitNameB64: map['unit-name-b64'] ?? "",
        url: map['url'] ?? "",
        urlB64: map['url-b64'] ?? "",
        currentRound: map['current-round'] ?? 0,
        circulatingSupply: map['circulating-supply'] ?? 0,
    );
  }
}