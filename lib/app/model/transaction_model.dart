class TransactionModel {
  int id = 0;
  String transaction_id = "";
  String type = "";
  String address = "";
  int assetId = 0;
  String assetName = "";
  int amount = 0;
  String price = "";
  String createdAt = "";
  String note = "";
  String createdTime = "";
  String notificationMessage = "";
  bool read = false;
  dynamic confirmedRound = 0;
  int decimal = 0;
  String from = "";
  int ledgerId = 0;
  int applicationId = 0;
  int fee = 0;

  TransactionModel({required this.transaction_id, required this.type, required this.address,
    required this.assetId, required this.assetName, required this.amount,
    required this.price, required this.createdAt, required this.note,
    required this.createdTime, required this.notificationMessage, required this.read,
    required this.confirmedRound, required this.decimal, required this.from, required this.ledgerId,
    required this.applicationId, required this.fee
  });

  Map<String, dynamic> toMap() {
    return {
      'transaction_id': transaction_id,
      'type': type,
      'address': address,
      'asset_id': assetId,
      'asset_name': assetName,
      'amount': amount,
      'price': price,
      'created_at': createdAt,
      'note': note,
      'created_time': createdTime,
      'notification_message': notificationMessage,
      'read': read,
      'confirmed-round': confirmedRound,
      'decimal': decimal,
      'from': from,
      'ledger_id': ledgerId,
      'application_id': applicationId,
      'fee': fee
    };
  }

  static TransactionModel fromMap(Map<String, dynamic> map) {
    return TransactionModel(
        transaction_id: map['transaction_id'] ?? "",
        type: map['type'] ?? "",
        address: map['address'] ?? "",
        assetId: map['asset_id'] ?? 0,
        assetName: map['asset_name'] ?? "",
        amount: map['amount'] ?? 0,
        price: map['price'] ?? "",
        createdAt: map['created_at'] ?? "",
        note: map['note'] ?? "",
        createdTime: map['created_time'] ?? "",
        notificationMessage: map['notification_message'] ?? "",
        read: map['read'] ?? false,
        confirmedRound: map['confirmed-round'] ?? 0,
        decimal: map['decimal'] ?? 0,
        from: map['from'] ?? "",
        ledgerId: map['ledger_id'] ?? 0,
        applicationId: map['application_id'] ?? 0,
        fee: map['fee'] ?? 0
    );
  }
}