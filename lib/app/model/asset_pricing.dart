class AssetsPricing {
  int id = 0;
  int ledgerPosition;
  int assetId;
  String currency;
  String price;
  String percentage;
  String percentagePrice;

  AssetsPricing({required this.ledgerPosition,required this.assetId,required this.currency,
    required this.price, required this.percentage, required this.percentagePrice});

  Map<String, dynamic> toMap() {
    return {
      'ledger-position': ledgerPosition,
      'asset-id': assetId,
      'price': price,
      'currency': currency,
      'percentage': percentage,
      'percentage-price': percentagePrice
    };
  }

  static AssetsPricing fromMap(Map<String, dynamic> map) {
    return AssetsPricing(
        ledgerPosition: map["ledger-position"],
        assetId: map['asset-id'] ?? 0,
        price: map['price'] ?? "",
        currency: map['currency'] ?? "",
        percentage: map['percentage'] ?? "",
        percentagePrice: map['percentage-price'] ?? ""
    );
  }
}