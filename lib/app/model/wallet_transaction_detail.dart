class WalletTransactionDetail {
  int assetId = 0;
  String from = "";
  String to = "";
  String balance = "";
  String asset = "";
  String amount = "";
  String fee = "";
  String type = "";
  String applicationId = "";
  String price = "";
  int decimals = 0;
  int amounts = 0;

  WalletTransactionDetail(this.assetId, this.from, this.to, this.balance, this.asset,this.amount,
      this.fee,this.type,this.applicationId,this.price, this.decimals,this.amounts);
}