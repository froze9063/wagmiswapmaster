class WalletConnectModel {
  int id = 0;
  String walletName;
  String walletUrl;
  String walletDesc;
  String walletImage;
  String walletDate;
  String walletConnectAddress;
  String uri;
  int chainId;
  String peerId;
  String remotePeerId;
  bool isConnected;
  String sessionJson;
  String sndMnemonic;

  WalletConnectModel({required this.walletName,required this.walletUrl,required this.walletDesc,required this.walletImage,
  required this.walletDate, required this.isConnected, required this.walletConnectAddress, required this.chainId
    , required this.peerId, required this.remotePeerId, required this.uri, required this.sessionJson, required this.sndMnemonic
  });

  Map<String, dynamic> toMap() {
    return {
      'wallet_name': walletName,
      'wallet_url': walletUrl,
      'wallet_desc': walletDesc,
      'wallet_image': walletImage,
      'wallet_date': walletDate,
      'wallet_connect_address': walletConnectAddress,
      'uri': uri,
      'chain_id': chainId,
      'peer_id': peerId,
      'remote_peer_id': remotePeerId,
      'is_connected': isConnected,
      'session_json': sessionJson,
      "snd_mnemonic" : sndMnemonic
    };
  }

  static WalletConnectModel fromMap(Map<String, dynamic> map) {
    return WalletConnectModel(
      walletName: map["wallet_name"] ?? "",
      walletUrl: map['wallet_url'] ?? "",
      walletDesc: map["wallet_desc"] ?? "",
      walletImage: map['wallet_image'] ?? "",
      walletDate: map["wallet_date"] ?? "",
      walletConnectAddress: map["wallet_connect_address"] ?? "",
      isConnected: map['is_connected-id'] ?? false,
      chainId: map["chain_id"] ?? 0,
      peerId: map["peer_id"] ?? "",
      remotePeerId: map['remote_peer_id-id'] ?? "",
      uri: map["uri"] ?? "",
      sessionJson: map["session_json"] ?? "",
      sndMnemonic: map["snd_mnemonic"] ?? ""
    );
  }
}