class LedgerModel {
  int ledgerId;
  String name;
  bool isEditable;
  String genesisId;
  String genesisHash;
  String symbol;
  String algodUrl;
  String indexerUrl;
  String headers;
  String baseUrl;

  LedgerModel({required this.ledgerId,required this.name, required this.isEditable, required this.genesisId,
    required this.genesisHash, required this.symbol, required this.algodUrl, required this.indexerUrl,
    required this.headers, required this.baseUrl});

  Map<String, dynamic> toMap() {
    return {
      'ledger_id': ledgerId,
      'name': name,
      'is_editable': isEditable,
      'genesis_id': genesisId,
      'genesis_hash': genesisHash,
      'symbol': symbol,
      'algod_url': algodUrl,
      'indexer_url': indexerUrl,
      'headers': headers,
      'base_url': baseUrl,
    };
  }

  static LedgerModel fromMap(Map<String, dynamic> map) {
    return LedgerModel(
      ledgerId: map['ledger_id'],
      name: map['name'],
      isEditable: map['is_editable'],
      genesisId: map['genesis_id'],
      genesisHash: map['genesis_hash'],
      symbol: map['symbol'],
      algodUrl: map['algod_url'],
      indexerUrl: map['indexer_url'],
      headers: map['headers'],
      baseUrl: map['base_url'],
    );
  }
}