class AssetsModel {
  int id = 0;
  int ledgerPosition;
  int amount;
  int assetId;
  String userAddress;
  String creator;
  bool isFrozen;
  int optedInTtRound;
  String price;
  String currency;

  AssetsModel({required this.ledgerPosition,required this.amount, required this.assetId,required this.userAddress,
    required this.creator, required this.isFrozen, required this.optedInTtRound,
    required this.price,required this.currency});

  Map<String, dynamic> toMap() {
    return {
      'ledger-position': ledgerPosition,
      'amount': amount,
      'asset-id': assetId,
      'user-address': userAddress,
      'creator': creator,
      'is-frozen': isFrozen,
      'opted_in_at_round': optedInTtRound,
      'price': price,
      'currency': currency
    };
  }

  static AssetsModel fromMap(Map<String, dynamic> map) {
    return AssetsModel(
        ledgerPosition: map["ledger-position"],
        amount: map['amount'] ?? 0,
        assetId: map['asset-id'] ?? 0,
        userAddress: map['user-address'] ?? "",
        creator: map['creator'] ?? "",
        isFrozen: map['is-frozen'] ?? false,
        optedInTtRound: map['opted_in_at_round'] ?? 0,
        price: map['price'] ?? "",
        currency: map['currency'] ?? "USD"
    );
  }
}