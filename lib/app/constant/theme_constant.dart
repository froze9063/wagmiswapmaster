import 'package:flutter/material.dart';

class ThemeConstant{
  static Color darkThemeBackground1 = Color.fromRGBO(25, 29, 52, 1.0);
  static Color darkThemeBackground2 = Color.fromRGBO(16, 15, 31, 1.0);
  static Color darkThemeBackground3 = Color.fromRGBO(20, 21, 40, 1.0);
  static Color lightThemeBackground = Color.fromRGBO(243, 243, 248, 1.0);

  static Color selectHomeDark = Color.fromRGBO(34, 33, 57, 1.0);
  static Color selectHomeLight = Color.fromRGBO(246, 246, 254, 1.0);

  static Color selectedHomeDark = Color.fromRGBO(18, 18, 37, 1.0);
  static Color selectHomeBackgroundDark = Color.fromRGBO(20, 21, 40, 1.0);

  static Color assetDark1 = Color.fromRGBO(60, 58, 118, 1.0);
  static Color assetDark2 = Color.fromRGBO(27, 26, 54, 1.0);

  static Color darkBottomColor = Color.fromRGBO(37, 40, 67, 1);
  static Color lightBottomColor = Color.fromRGBO(215, 215, 215, 1.0);

  static Color blackNavy = Color.fromRGBO(23, 25, 46, 1.0);
  static Color white = Color.fromRGBO(255, 255, 255, 1.0);

  static Color yellowDark = Color.fromRGBO(255, 197, 48, 1.0);
  static Color yellowLight = Color.fromRGBO(219, 158, 0, 1.0);

  static Color greyBorder = Color.fromRGBO(184, 184, 184, 1.0);
  static Color blueBorder = Color.fromRGBO(129, 149, 222, 1.0);

  static Color darkPurple = Color.fromRGBO(184,115,255,1);
  static Color lightPurple = Color.fromRGBO(121,48,197,1);

  static Color secondTextGrayDark = Color.fromRGBO(164,164,164,1);
  static Color secondTextGrayLight = Color.fromRGBO(100,100,100,1);
  static Color lightActiveBorder = Color.fromRGBO(23,25,46,1);

  static Color walletLightPurple = Color.fromRGBO(121,48,197,1);
  static Color walletDark = Color.fromRGBO(60, 59, 108, 1);

  static Color walletDarkPurple1 = Color.fromRGBO(148, 0, 211, 1);
  static Color walletDarkPurple2 = Color.fromRGBO(75, 0, 130, 1);

  static Color greenDark = Color.fromRGBO(108, 216, 107, 1);
  static Color greenLight = Color.fromRGBO(32, 143, 21, 1);
}

