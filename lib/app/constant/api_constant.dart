class MyConstant{
  //./sandbox goal clerk send -a 10000000 -f PK6GXEAAVCFJ2KD5TKLVCOUHOXGS2WAAVXYSCMZS6LDRDNHZOUAX7O7HWI -t 4MMTNJMUZZSO4EYU4F2LLPQUCPUEK3YOLBTWK3HEEPO53XC5TJUUOAP2DU
  //./sandbox goal clerk send -a 0 -f YHMAABQZGUUN64KFEDR4FAUS3E5TJNBUM2S43DD353MQNSH3YXNNGH2MC4 -t YHMAABQZGUUN64KFEDR4FAUS3E5TJNBUM2S43DD353MQNSH3YXNNGH2MC4 --rekey-to CF4MKS3TURHQPX225LIRO3S3WNZQGS3IW2ZNQKBD6CS5BDLHGYYRAXOIHA --out rekey.txn

  static String TOKEN_URL = "http://ms.producc.xyz/api";
  static String TESTNET_URL = "https://wallet.wagmiswap.io/testnet/api/";
  static String MAIN_URL = "https://wallet.wagmiswap.io/api/";
  static String LOCALHOST_URL = "http://127.0.0.1:8081/api/";

  static String USED_URL = "https://wallet.wagmiswap.io/api/";

  static String ACCOUNTS = "/v2/accounts";
  static String PENDING_TRANSACTION = "/v2/transactions/pending";
  static String FORWARD = "/forward";
  static String ASSETS = "/v2/assets";
  static String ASSETS_ONLY = "/assets";

  static String TOKEN_BEARER = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0a"
      "SI6IjliNDAwYzFmNGI1MjIyM2E0Yjc0OWI0OTA0ODc2ZmU3Njg3NWJhMTYwNDU2Mzk0YTdlNjRh"
      "YjBlYmZmZThkZjRlYTU1YWI1ODJlYjgzNjIyIn0.eyJhdWQiOiIzIiwianRpIjoiOWI0MDBjMWY"
      "0YjUyMjIzYTRiNzQ5YjQ5MDQ4NzZmZTc2ODc1YmExNjA0NTYzOTRhN2U2NGFiMGViZmZlOGRmNGV"
      "hNTVhYjU4MmViODM2MjIiLCJpYXQiOjE2Mjc3MTQwODcsIm5iZiI6MTYyNzcxNDA4NywiZXhwIjox"
      "NjU5MjUwMDg3LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.B3LlNIJ_hWZz6zhvpBopUYQL9rc5fpF5-"
      "mpocXpZ2AEM1n2uQcOBB12qrLxcZkarTlZA6GB_UH_Z4xms7xE_Dn5NC-zEve0emVzhk1i9M22svL"
      "4Gtuy5hJkAY9PfL9zbT-4n9wpsLD4JVL4QyBQRUJMLffYlCyYx8Ru5jngSY8crmDxafIOiIHmqYWKD"
      "GCNVV7IqW3Vb5JJDZ9TRM63NwuoQO8BHceVkuMoeqbC0vQ0ye7fDJWkueAX7wrRATV1DyrxgIcwSVA"
      "taUrngDYHgDARwjdv6UlLCpFWdCwZqu5WX_Lv4OaA_naGWU0jwvK-nVmskPyv6WjSFU4AnWXQ5eS6ME"
      "_5VB_sDV2Cwaz2ZoIZFX_FhoTSlEBTTeBKQa15B2zquL4QotT4zdv2Kl-XcYS71lFEKMfZwhdu8zdEer"
      "ly1FHpTe77IUWhvCmbUkPW3MKRw-jDlFHRHYFi9YUYgHFTDMQd9RFCS9EIcKNVd4CBmyBkXPkc9Jm_JN"
      "rb29VgPDx76V4D9tCaaV9EEjbKoygw7eRgKiuG6GoEoPlz_m4QBHjPkaPU7aFlszW35waUoqq31WszFvt"
      "veLemxuogNul_y_-iO-CULAEYEuJB-P6_uoZtJO8lQdyCzd6DRzAT2n6K06IO_CqJy8ESMESDplN5lq6EHxcnySUXBaIy9E0M";

  static String exampleManaged = "{\"master_account\":{\"address\":\"PVSI33PEYDVKUXX3XNNLCJVC5KO66GZC6YERACX4YX5MVZ75IYGGP7Y33Q\",\"mnemonic\":\"manualenlistcurtainhoneyactualexitcouplespooncoachbulkiconnutabsentinnocentrulecrosscompanyboilartefactagreespheremidnighthollowabstractnight\",\"name\":\"WAGMIswap\"},\"name\":\"WAGMIswap\",\"sub_accounts\":[{\"name\":\"WAGMIswap-1\",\"address\":\"SPHRKYUNA6G7GZYWFNCBLUQN5L7V7YECNGXYDVG23L75TJSKYHW6WKFXTE\",\"mnemonic\":\"volumeeldertruckfloorsupplysisterburdenconsiderneckkeeppoolfunnyinhalepointmazefatiguepillbonepurchasefetchdiaryallliquidabsorbadd\",\"active\":false},{\"name\":\"WAGMIswap-2\",\"address\":\"CQKPJYRYZQHKLZO235WQ4ZTGREVO6EWT7G7Z4ECG6U46B3Y3CVOMESYKHA\",\"mnemonic\":\"ratherschemenoblepanellaughfragileclinicfarmmadbleaklessonsafeprogramshaftdogforestinsectmarriageevidencevendorradiofrostdrinkabandonstamp\",\"active\":false}],\"url\":\"https://testnet.wagmiswap.io\"}";
  static String BROADCAST = "broadcast";
  static String BROADCAST_SETTING = "broadcast_setting";
}

