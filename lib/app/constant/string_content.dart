class StringContent {
  static String about_algo = "Algorand is a self-sustaining, decentralized, blockchain-based network that supports a wide range of applications. These systems are secure, scalable and efficient, all critical properties for effective applications in the real world. Algorand will support computations that require reliable performance guarantees to create new forms of trust.";
  static String about_planet = "PlanetWatch decentralizes, incentivizes, and gamifies environmental monitoring. Currently, we build dense air quality sensor networks streaming real-time data to our platform. Validated data earn Planet token rewards and build a global immutable air quality ledger on the Algorand blockchain. Sensing is mining!";
  static String about_yldy = "Yieldly is a suite of DeFi apps on the Algorand blockchain, including no-loss prize games, multi-asset staking, and cross-chain swapping. Learn more about Yieldly on their website, and join their community on telegram.";
  static String about_arcc = "QMG Labs is an economic development reserve and advisory established to promote distributed inclusive economic development for the urban working poor in emerging markets. The QMG Labs economic development model is focused on financially inclusive wealth distribution through a micro finance platform which promotes adoption and participation. The basis of the model is on early and direct distribution to the individual of a 'micro asset' via a process called 'Social Proof of Work'. Through the issuance and support of the ARCC (Asia Reserve Currency Coin). QMG Labs intends to break the cycle of poverty, corruption and wealth disparity in urban emerging markets through the decentralized network effect of blockchain technology.";
  static String about_usdt = "Tether is a token backed by actual assets, including USD and Euros. One Tether equals one underlying unit of the currency backing it, e.g., the U.S. Dollar, and is backed 100% by actual assets in the Tether platform's reserve account. Being anchored or 'tethered' to real world currency, Tether provides protection from the volatility of cryptocurrencies. Tether enables businesses – including exchanges, wallets, payment processors, financial services and ATMs – to easily use fiat-backed tokens on blockchains. By leveraging Blockchain technology, Tether allows you to store, send and receive digital tokens person-to-person, globally, instantly, and securely for a fraction of the cost of alternatives. Tether's platform is built to be fully transparent at all times.";
  static String about_opul = "Opulous brings NFTs and DeFi to the music industry, changing how musicians access the funding they need and providing a launchpad for the first music copyright-backed NFTs and Crypto Loans.";
  static String about_usdc = "USDC provides a fully collateralized US dollar stablecoin, and is based on the open source asset-backed stablecoin framework developed by Centre. Centre stablecoins are issued by regulated and licensed financial institutions that maintain full reserves of the equivalent fiat currency. Issuers are required to regularly report their USD reserve holdings, and Grant Thornton LLP issues reports on those holdings every month.";

  static double bottomMargin = 24;
  static double globalTopMarginButton = 24;

  static String welcome_messge_wallet = "WELCOME TO WAGMISWAP WALLET!";
  static String welcome_description = "WAGMIswap.io was developed to support Defi "
      "on Algorand! Sit back and relax, while we walk you through the setup process. "
      "This wallet is designed to scale with every future defi app on Algorand as well "
      "as act as a standard on chain wallet for ALGO and all Algorand Standard Assets.";
  static String lets_begin = "LET'S BEGIN";
  static String select_an_option = "SELECT AN OPTION";
  static String create_new_wallet = "CREATE NEW ACCOUNT";
  static String import_account = "IMPORT ACCOUNT";
  static String import_accounts = "Import Account";
  static String create_account = "CREATE ACCOUNT";
  static String import_exist_wallet = "IMPORT EXISTING WALLET";
  static String import_with_seed = "IMPORT AN ACCOUNT WITH SEED PHRASE";
  static String seed_phrase_paste = "Seed phrase (Paste from clipboard)";
  static String seed_phrase_copy = "Seed phrase (Tap to copy)";
  static String new_passcode = "New passcode";
  static String show = "Show";
  static String done = "DONE";
  static String setup_passcode = "SET UP PASSCODE";
  static String enter_passcode = "ENTER PASSCODE";
  static String enter_new_passcode = "ENTER NEW PASSCODE";
  static String re_enter_passcode = "RE-ENTER PASSCODE";
  static String reenter_pass = "Re-enter passcode";
  static String sign_with_face = "Sign in with Face ID?";
  static String tnc_agree = "I have read and agree to the";
  static String tnc = "Terms of Use";
  static String create_passcode = "CREATE YOUR PASSCODE";
  static String use_pin_instead = "USE PIN INSTEAD";
  static String login_with_face = "LOGIN WITH FACE";
  static String next = "NEXT";
  static String secret_phrase = "Secret Phrase";
  static String secret_phrase_desc = "Lorem ipsum totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.\n\n"
      "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium "
      "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis";
  static String secret_example = "blank maid tower woods duty gallery singer wine glass crunch crew gaze lounge section space extra kingdom clean begin fries eyes guitar fearless girl";
  static String protect_wallet = "PROTECT YOUR WALLET";
  static String ASSETS_SAFE = "Keep your assets safe with a passcode to prevent unauthorized access to your wallet.";
  static String delete = "Delete";
  static String setup_face = "SET UP FACE ID";
  static String setup_touch_id = "SET UP TOUCH ID";
  static String layer_extra = "Add an extra layer of security with Face ID to keep your assets safe.";
  static String later = "LATER";
  static String acknowledge = "Please read and acknowledge:";
  static String acknowledge1 = "Backup your account with your mnemonic phrase. Without your mnemonic phrase, if you lose your device or delete the app, you will permanently lose access to your Algorand account.";
  static String acknowledge2 = "Prepare to write down your mnemonic phrase. The only way to recover an Algorand account is with this mnemonic phrase.";
  static String acknowledge3 = "Store the mnemonic phrase in a secure environment. Do not share this mnemonic phrase with anyone as it grants full access to your account.";
  static String mese_content = "Next, WAGMIswap will generate a new account with a 25 word mnemonic phrase.";
  static String mnemonic = "Mnemonic";
  static String your_mnemonic = "YOUR MNEMONIC";
  static String your_mnemonic_desc = "Name your account so that you may easily identify it while using the WAGMIswap wallet.";
  static String your_verify = "VERIFY YOUR MNEMONIC";
  static String enter_account_name = "Enter Account Name";
  static String mnemonic_hint = "E.g: banana apple (use space delimiter)";
  static String account_name = "Account Name";
  static String store_mnemonic = "Please store your mnemonic in a safe place. Losing it will lead to permanent loss to your assets. Make sure no one is watching your screen.";
  static String tap_reveal_mnemonic = "TAP TO REVEAL MNEMONIC";
  static String tap_to_copy = "TAP TO COPY";
  static String tap_to_upload = "Tap to upload";
  static String backup_now = "BACK UP NOW";
  static String backup = "BACK UP";
  static String copied = "COPIED";
  static String confirm = "CONFIRM";
  static String enter_word = "Enter Word";
  static String error = "ERROR";
  static String start_using_wallet = "START USING WALLET";
  static String account_created = "ACCOUNT CREATED";
  static String account_imported = "ACCOUNT IMPORTED";
  static String account_created_phrase = "Keep this phrase in a secure location that only you have access to. Do not lose it.";
  static String account_created_phrase2 = "Your account has been successfully imported!";
  static String wrong_password = "Passcode doesn't match";
  static String password_incorrect = "Incorrect Passcode";
  static String managed = "MANAGED";
  static String regular = "REGULAR";
  static String upload = "UPLOAD";
  static String import = "IMPORT";
  static String qr_scan = "QR SCAN QR CODE";
  static String paste_mnemonic = "Paste Your Mnemonic Here";
  static String your_managed = "Your managed account and it's sub-accounts can be backed up using the manage wallet option.";
  static String balance = "BALANCE";
  static String transaction = "TRANSACTIONS";
  static String add_asset = "ADD ASSET";
  static String my_asset = "+/- ASSETS";
  static String choose_asset = "CHOOSE ASSET";
  static String select_asset = "SELECT ASSET";
  static String search_asset_hint = "Search Asset ID";
  static String search_asset_hint2 = "Search Asset";
  static String send = "SEND";
  static String receive = "RECEIVE";
  static String send2 = "Send";
  static String receive2 = "Receive";
  static String asset = "Asset";
  static String appId = "Application ID";
  static String fee = "Fee";
  static String max = "MAX";
  static String paste = "PASTE";
  static String cancel = "CANCEL";
  static String connect = "CONNECT";
  static String note = "Note";
  static String notes = "Notes";
  static String close = "CLOSE";
  static String recipient = "Recipient";
  static String from = "from";
  static String to = "to";
  static String recipient_address = "Recipient Address";
  static String amount = "Amount";
  static String total_amount = "Total Amount";
  static String account_address = "Account Address";
  static String share_address = "SHARE ADDRESS";
  static String transaction_type = "Transaction Type";
  static String type = "Type";
  static String asset_id = "Asset ID";
  static String sign = "SIGN";
  static String reject = "REJECT";
  static String transaction_info = "TRANSACTION INFO";
  static String status = "Status";
  static String tx_id = "Transaction ID";
  static String algo_explorer = "Algoexplorer";
  static String date = "Date";
  static String clear_filter = "CLEAR FILTERS";
  static String your_notification = "Your Notifications";
  static String read_more = "READ MORE";
  static String read_less = "SHOW LESS";
  static String lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  static String algos = "Algos";
  static String rewards = "Rewards";
  static String market_cap = "Market Cap";
  static String in_circulation = "In Circulation";
  static String trading_volume = "Trading Volume (24H)";
  static String inception_date = "Inception Date";
  static String settings = "SETTINGS";
  static String account = "Account";
  static String display = "Display";
  static String support = "Support";
  static String accounts = "Accounts";
  static String wallet_connect = "Wallet Connect";
  static String security = "Security";
  static String notification = "Notification";
  static String push_notification = "Push Notification";
  static String notifications = "Notifications";
  static String currency = "Currency";
  static String language = "Language";
  static String theme = "Theme";
  static String about_us = "About Us";
  static String faq = "FAQ";
  static String managed_accounts = "Managed Accounts";
  static String standard_accounts = "Standard Accounts";
  static String mese_dex = "Mese Dex";
  static String download_backup = "Download Backup";
  static String name = "Name";
  static String unlink = "UNLINK";
  static String unlink_confirmation = "UNLINK CONFIRMATION";
  static String save = "SAVE";
  static String unlink_detail = "Are you sure want to unlink your account? \n\nThis will "
      "remove your account <ACCOUNT_ADDRESS> from MESE wallet \n\nIMPORTANT! In case you need "
      "to restore this account in the future, make sure you backedup the mnemonic.";
  static String account_details = "ACCOUNT DETAILS";
  static String select_network = "SELECT NETWORK";
  static String passcode = "Passcode";
  static String edit_passcode = "Edit Passcode";
  static String create_new_account = "Create New Account";
  static String face_id = "Face ID";
  static String face_finger = "Finger Print";
  static String about_us_detail = "Version: 1.6.4\n\nOperating under the International "
      "Blockchain Monetary Reserve, IBMR.io, WAGMIswap’s mission is to create financial inclusion "
      "through the open distributed access of digital tokens for wealth creation and asset management "
      "for emerging and developing markets. More than just banking the unbanked, WAGMIswap is about providing "
      "accessible wealth creation directly to the individual and creating a new paradigm in microfinance "
      "with microassets. WAGMIswap is built on the Algorand Blockchain protocol."
      "\n\nwagmiswap.io\n\nwww.ibmr.io\n\nwww.arcc.one\n\nwagmiswap.io/privacy-policy";
  static String frequently_ask = "FREQUENTLY ASKED QUESTIONS";
  static String account_name_error = "Account Name needs to be at least 5 characters long!";
  static String add_coin = "ADD COIN";
  static String invalid_address = "Invalid address!";
  static String sort_by = "SORT BY";
  static String set_date = "SET DATE";
  static String verify = "VERIFY";
  static String make_sure = "Make sure to record these words in the correct order, using the corresponding numbers.";
  static String removing_asset = "REMOVING ASSET";
  static String adding_asset = "ADDING ASSET";
  static String proceed = "PROCEED";
  static String transfer_balance = "TRANSFER BALANCE";
  static String approve = "APPROVE";
  static String accept = "ACCEPT";
  static String adding_detail = "Adding an asset requires sending a transaction with 0 amount and minimum transaction fee.";
  static String screenshot_detected = "SCREENSHOT DETECTED";
  static String sreenshot_detail = "Do not backup your recovery passphrase by taking a screenshot. Having an image containing your passphrase is a secuirty risk.";
  static String user_exist = "Username already exist!";
  static String only_json = "JSON file only can be imported!";
  static String please_import_file = "Please import json file!";
  static String successfully_add_asset = "Successfully add asset!";
  static String successfully_remove_asset = "Successfully remove asset!";
  static String zero_algorand = "Your algorand amount is 0!";
  static String insufficient_amount = "Insufficient funds.";
  static String name_used = "Name already used!";
  static String insufficient_minimum_amount = "You dont have enough minimum balance";
  static String allow_wagmi = "Allow WAGMIswap to manage wallet accounts.";
  static String youve_entered = "You’ve entered the wrong words. Please try again.";
  static String transaction_request = "External application also called as dApps can make transaction requests that the "
      "WAGIswap Wallet does not control. Please ensure you review these transactions carefully before approving "
      "and signing them.\n\nAll transactions are irreversible. Never approve transactions from dApp that you don't know.";
  static String confirmation = "Once confirmed, the connected application can send these transactions out at their discretion. Please make sure you review the transactions carefully as this process is irreversible.";

  static int CREATE_ACCOUNT_VALUE = 47;
  static int SWAP_ACCOUNT_VALUE = 50;
}

